// "Copyright @ 2020, Prog Leasing, LLC. All rights reserved"

import hudson.EnvVars

def GROOVY_LOCATION
def GROOVY_VERSION
def JAVA_LOCATION
def JAVA_VERSION
def MAVEN_LOCATION
def MAVEN_VERSION
def AUTOMATION_PATH
def REPORT_LOCATION
def SAUCELABS

pipeline {
    options {
        timestamps()
        buildDiscarder(logRotator(numToKeepStr: '30', artifactNumToKeepStr: '30', daysToKeepStr: '14', artifactDaysToKeepStr: '14'))
        // disableConcurrentBuilds()
    }
    parameters {
        separator(name: "header")
        separator(
                name: "environmentSettings",
                sectionHeader: "Environment Settings",
                separatorStyle: "border-width: 0",
                sectionHeaderStyle: """
				background-color: #7ea6d3;
				text-align: center;
				padding: 4px;
				color: #343434;
				font-size: 22px;
				font-weight: normal;
				text-transform: uppercase;
				font-family: 'Orienta', sans-serif;
				letter-spacing: 1px;
				font-style: italic;
			"""
        )
        choice(
                name: 'environment',
                choices: 'QA08\nRC-Draper\nRC-Phoenix\nProduction-Draper\nProduction-Phoenix\nDemo\nQA01\nQA02\nQA03\nQA04\nQA05\nQA06\nQA07\nQA09\nQA10\nQA11\nQA12\nQA13\nQA14\nQA15\nQA16\nQA17\nQA18\nQA19\nQA20\nQA21\nQA22\nQA23\nQA24\nQA25\nQA26\nQA27\nQA28\nQA29\nQA30\nQA31\nQA32\nQA33\nQA34\nQA35\nQA36\nQA37\nQA38\nQA39\nQA40',
                description: 'Select the target environment. Production should only be used for health checks <br><b>The QA Environment that Approve.Me uses consists of the following: One Box Nodes:</b><br><br><ul><li>QA08</li><li>QA17</li><li>QA18</li><li>QA19 (Design Team)</li></ul><br>'
        )
        separator(
                name: "testSettings",
                sectionHeader: "Test Settings",
                separatorStyle: "border-width: 0",
                sectionHeaderStyle: """
				background-color: #dbdb8e;
				text-align: center;
				padding: 4px;
				color: #343434;
				font-size: 22px;
				font-weight: normal;
				text-transform: uppercase;
				font-family: 'Orienta', sans-serif;
				letter-spacing: 1px;
				font-style: italic;
			"""
        )
        choice(
                name: 'packageName',
                choices: "${params.packageName}" ?: 'All\nBadges\nInputs\nTypographyTokens\nDocQuickTest\nMicroFrontEnd\nRadios\nViewPort',
                description: 'What package should this job run?'
        )
        string(
                name: 'testName',
                defaultValue: '',
                description: 'Test to run for single test execution (e.g. G_3421_Test)'
        )
        string(
                name: 'numberOfThread',
                defaultValue: "${params.numberOfThread}" ?: '',
                description: 'Number of threads run simultaneously? (leave it empty if you want to use the default)'
        )
        string(
                name: 'defaultWaitTime',
                defaultValue: "${params.defaultWaitTime}" ?: '60',
                description: ''
        )
        separator(
                name: "sauceSettings",
                sectionHeader: "Sauce Labs Settings",
                separatorStyle: "border-width: 0",
                sectionHeaderStyle: """
				background-color: #e1b382;
				text-align: center;
				padding: 4px;
				color: #343434;
				font-size: 22px;
				font-weight: normal;
				text-transform: uppercase;
				font-family: 'Orienta', sans-serif;
				letter-spacing: 1px;
				font-style: italic;
			"""
        )
        string(
                name: 'sauceLabsUserID',
                defaultValue: "${params.sauceLabsUserID}" ?: 'daxnorton',
                description: 'Enter the Sauce Labs user ID to use for the tests'
        )
        string(
                name: 'sauceLabsAuthKey',
                defaultValue: "${params.sauceLabsAuthKey}" ?: '87787169-d87e-462b-8cae-8062437f767b',
                description: 'Enter the Sauce Labs Authentication Key for the sauceLabsUserID'
        )
        string(
                name: 'sauceLabsTunnelName',
                defaultValue: "${params.sauceLabsTunnelName}" ?: 'Dtunnel',
                description: 'Enter the name of the Sauce Labs Tunnel'
        )
        separator(
                name: "gitBranches",
                sectionHeader: "Git Branches",
                separatorStyle: "border-width: 0",
                sectionHeaderStyle: """
				background-color: #fceed1;
				text-align: center;
				padding: 4px;
				color: #343434;
				font-size: 22px;
				font-weight: normal;
				text-transform: uppercase;
				font-family: 'Orienta', sans-serif;
				letter-spacing: 1px;
				font-style: italic;
			"""
        )
        string(
                name: 'automationBranch',
                defaultValue: "master",
                description: 'What branch will be used for this run?'
        )
        separator(
                name: "jenkinsNode",
                sectionHeader: "Jenkins Node",
                separatorStyle: "border-width: 0",
                sectionHeaderStyle: """
				background-color: #f6f5f3;
				text-align: center;
				padding: 4px;
				color: #343434;
				font-size: 22px;
				font-weight: normal;
				text-transform: uppercase;
				font-family: 'Orienta', sans-serif;
				letter-spacing: 1px;
				font-style: italic;
			"""
        )
        string(
                name: 'node',
                defaultValue: 'Katalon || Java',
                description: 'What Jenkins node(s) should be used to execute the test(s). (e.g. KAT-SLC-QASAPPKAT01 or KATALON)'
        )
        separator(name: "end")
    }

    agent { label "${params.node}" }

    stages {
        stage('Configure Environment') {
            options { timeout(5) }
            steps {
                echo '*********************************'
                echo '*                               *'
                echo '*     Configure Environment     *'
                echo '*                               *'
                echo '*********************************'

                script{
                    // Set global var values
                    GROOVY_LOCATION = "C:\\Jenkins\\tools\\groovy"
                    GROOVY_VERSION = GROOVY_LOCATION + "\\groovy-3.0.3.txt"
                    JAVA_LOCATION = "C:\\Jenkins\\tools\\java"
                    JAVA_VERSION = JAVA_LOCATION + "\\jdk8u252-b09.txt"
                    MAVEN_LOCATION = "C:\\Jenkins\\tools\\apache-maven"
                    MAVEN_VERSION = MAVEN_LOCATION + "\\apache-maven-3.6.3.txt"
                    AUTOMATION_PATH = "c:\\jenkins\\workspace\\progfin-design_systems_test_automation"
                    REPORT_LOCATION = "C:\\Jenkins\\Workspace\\progfin-design_systems_test_automation\\target\\surefire-reports"
                }
            }
        }
        stage('Checkout progfin-design_system_test_automation') {
            options { timeout(5) }
            steps {
                echo '******************************************************'
                echo '*                                                    *'
                echo '*   Checkout progfin-design_system_test_automation   *'
                echo '*                                                    *'
                echo '******************************************************'

                script {
                    checkout(
                            [
                                    $class                           : 'GitSCM',
                                    branches                         : [[name: "${params.automationBranch}"]],
                                    doGenerateSubmoduleConfigurations: false,
                                    extensions                       : [
                                            [$class: 'CleanBeforeCheckout'],
                                            [$class           : 'RelativeTargetDirectory',
                                             relativeTargetDir: AUTOMATION_PATH]
                                    ],
                                    gitTool                          : 'LocalGit',
                                    submoduleCfg                     : [],
                                    userRemoteConfigs                : [
                                            [credentialsId: 'a139e519-7bbe-4bb5-9127-da4d8dad21c5',
                                             url          : 'git@bitbucket.org:DaxNorton/progfin-design_systems_test_automation.git']
                                    ]
                            ]
                    )
                }
            }
        }
        stage('Dependency Check') {
            options { timeout(5) }
            steps{
                echo '*********************************'
                echo '*                               *'
                echo '*       Dependency Check        *'
                echo '*                               *'
                echo '*********************************'

                script {
                    echo 'Checking for JDK'
                    try {
                        if (fileExists(JAVA_LOCATION + "\\bin\\java.exe")) {
                            echo 'Java Found. Checking Version'
                            if (fileExists(JAVA_VERSION)) {
                                echo 'Expected version of Java found'
                            } else {
                                echo 'Incorrect version of Java. Attempting to update...'
                                // TODO: Add code to update java
                            }
                        } else {
                            echo 'ERROR: JAVA NOT FOUND. ATTEMPTING INSTALL...'
                            powershell encoding: 'UTF-8', label: 'Install JDK8', returnStdout: true, script: 'Expand-Archive -Force C:\\Jenkins\\workspace\\qecommon\\tools\\java.zip C:\\Jenkins\\tools'
                        }
                    } catch(err) {
                        echo err.getMessage()
                    }

                    echo 'Checking for Groovy'
                    try {
                        if (fileExists(GROOVY_LOCATION + "\\bin\\groovy.bat")) {
                            echo 'Groovy Found. Checking Version'
                            if (fileExists(GROOVY_VERSION)) {
                                echo 'Expected version of Groovy found'
                            } else {
                                echo 'Incorrect version of Groovy. Attempting to update...'
                                // TODO: Add code to update Groovy
                            }
                        } else {
                            echo 'ERROR: GROOVY NOT FOUND. ATTEMPTING INSTALL...'
                            powershell encoding: 'UTF-8', label: 'Install Groovy', returnStdout: true, script: 'Expand-Archive -Force C:\\Jenkins\\workspace\\qecommon\\tools\\groovy.zip C:\\Jenkins\\tools'
                        }
                    } catch(err) {
                        echo err.getMessage()
                    }

                    echo 'Checking for Maven'
                    try {
                        if (fileExists(MAVEN_LOCATION + "\\bin\\mvn.cmd")) {
                            echo 'Maven Found. Checking Version'
                            if (fileExists(MAVEN_VERSION)) {
                                echo 'Expected version of Maven found'
                            } else {
                                echo 'Incorrect version of Maven. Attempting to update...'
                                // TODO: Add code to update maven
                            }
                        } else {
                            echo 'ERROR: MAVEN NOT FOUND. ATTEMPTING INSTALL...'
                            powershell encoding: 'UTF-8', label: 'Install Maven', returnStdout: true, script: 'Expand-Archive -Force C:\\Jenkins\\workspace\\qecommon\\tools\\apache-maven.zip C:\\Jenkins\\tools'
                        }
                    } catch(err) {
                        echo err.getMessage()
                    }

                    try {
                        powershell encoding: 'UTF-8', label: 'Install JDBC Driver', returnStdout: true, script: "C:\\Jenkins\\workspace\\qecommon\\RestAssured\\JDBC_driver_install.bat"
                    } catch(err) {
                        echo err.getMessage()
                    }
                }
            }
        }

        stage('Testing') {
            options { timeout(60) }
            steps {
                echo '*********************************'
                echo '*                               *'
                echo '*            TESTING            *'
                echo '*                               *'
                echo '*********************************'

                script {
                    switch("${params.environment}".toLowerCase()) {
                        case "rc-draper":
                            env = "releasecandidate"
                            break
                        case "rc-phoenix":
                            env = "rc_phx"
                            break
                        case "production-draper":
                            env = "prod"
                            break
                        case "production-phoenix":
                            env = "prod_phx"
                            break
                        case "demo":
                            env = "${params.environment}"
                            break
                        default:
                            env = "QAENV" + "${params.environment}"[-2..-1]
                    }

                    testSuite = "${params.packageName}".replaceAll('/','.')
                    mvnCommonParams = "cd " + AUTOMATION_PATH + "\r\n" +
                            "set JAVA_HOME=" + JAVA_LOCATION + "\r\n" +
                            "set GROOVY_HOME=" + GROOVY_LOCATION + "\r\n" +
                            "set MAVEN_HOME=" + MAVEN_LOCATION + "\r\n" +
                            "set PATH=%PATH%;%JAVA_HOME%\\bin;%GROOVY_HOME%\\bin;%MAVEN_HOME%\\bin" + "\r\n" +
                            "mvn clean test" +
                            " -DDEFAULT_WAIT_TIME=${params.defaultWaitTime}" +
                            " -DENV_NAME=" + env +
                            " -DSAUCE_AUTH_KEY=${params.sauceLabsAuthKey}" +
                            " -DSAUCE_TUNNEL_NAME=${params.sauceLabsTunnelName}" +
                            " -DSAUCE_USER_NAME=${params.sauceLabsUserID}" +
                            " -DWHERE_TO_RUN_TESTS=" + testLocation

                    if("${params.testName}" != '') {
                        mvnCommand = mvnCommonParams +
                                " -Dtest=designsystems." + testSuite + ".${params.testName} -e && exit %%ERRORLEVEL%%"

                    } else if("${params.numberOfThread}" != '') {
                        mvnCommand = mvnCommonParams +
                                " -DnumberOfThread=${params.numberOfThread}" +
                                " -Dtest=designsystems." + testSuite + ".** -e && exit %%ERRORLEVEL%%"

                    } else {
                        mvnCommand = mvnCommonParams +
                                " -P=" + testSuite + ".** -e && exit %%ERRORLEVEL%%"
                    }

                    if(SAUCELABS) {
                        switch("${params.sauceLabsUserID}".toLowerCase()) {
                            case "agautam2019":
                                sauceLabsAuthentication = 'b2132f0d-680b-44c9-a26d-36170bad23fd'
                                break
                            case "ccall":
                                sauceLabsAuthentication = '0f99ff4f-f888-43c0-9d8c-3408fcdd24d5'
                                break
                            case "cjrains":
                                sauceLabsAuthentication = 'ad70dc40-1ec6-444a-8237-833474954d01'
                                break
                            case "courtross":
                                sauceLabsAuthentication = 'c7ec3f9f-011c-423f-923b-eaf57593e4ee'
                                break
                            case "daxnorton":
                                sauceLabsAuthentication = '87787169-d87e-462b-8cae-8062437f767b'
                                break
                            case "dferran":
                                sauceLabsAuthentication = 'a638e233-fa91-447c-903f-7bec6e49c542'
                                break
                            case "dhanalaxmi.kandibanda":
                                sauceLabsAuthentication = '3a7abacc-a2bd-4c7d-b970-5ee19337abb7'
                                break
                            case "gbergeson":
                                sauceLabsAuthentication = '9e038bbf-04ae-4313-ab9e-0692757bccb9'
                                break
                            case "gregory.vonarx":
                                sauceLabsAuthentication = 'c964e631-ca8c-48e8-8d07-c9938c93f311'
                                break
                            case "jaeford":
                                sauceLabsAuthentication = '11274cb7-2fff-40c7-af62-7121c9c0effa'
                                break
                            case "jayallenloew":
                                sauceLabsAuthentication = 'f192c98c-50a7-4c17-b7e9-6d2c17e9f5a3'
                                break
                            case "jeremywatts":
                                sauceLabsAuthentication = 'f2d93134-9971-405e-8cda-d0ed13afd8e2'
                                break
                            case "kferrell":
                                sauceLabsAuthentication = '9ea2f98a-b917-4be0-af94-c5235a3fc353'
                                break
                            case "leoabner":
                                sauceLabsAuthentication = '63b5f184-ee63-44fc-8367-59673d19e401'
                                break
                            case "maheshkatri":
                                sauceLabsAuthentication = 'ee1ab396-476d-4882-9478-3b00387c7879'
                                break
                            case "mcurtis":
                                sauceLabsAuthentication = '3991a08d-479c-497f-b432-052c485250f5'
                                break
                            case "mistyjensen":
                                sauceLabsAuthentication = '12d87075-b5e7-4200-be3c-de1d265b225c'
                                break
                            case "msnow":
                                sauceLabsAuthentication = '008260ac-3edf-425a-9630-eb35291ed7d5'
                                break
                            default:
                                sauceLabsAuthentication = 'f192c98c-50a7-4c17-b7e9-6d2c17e9f5a3'
                        }
                        sauce(sauceLabsAuthentication) {
                            sauceconnect(options: "--tunnel-identifier ${params.sauceLabsTunnelName}", sauceConnectPath: '', useLatestSauceConnect: true, useGeneratedTunnelIdentifier: false, verboseLogging: false) {
                                bat label: 'Execute Selenium Tests', script: mvnCommand
                            }
                        }
                    } else {
                        bat label: 'Execute Selenium Tests', script: mvnCommand
                    }
                }
            }
        }
    }

    post {
        always {
            echo '*********************************'
            echo '*                               *'
            echo '*      REPORTING RESULTS        *'
            echo '*                               *'
            echo '*********************************'

            script {
                if (SAUCELABS) {
                    saucePublisher()
                }
            }

            publishHTML(
                    [
                            allowMissing: true,
                            alwaysLinkToLastBuild: true,
                            keepAll: true, reportDir: REPORT_LOCATION,
                            reportFiles: 'index.html',
                            reportName: 'TestNG Report',
                            reportTitles: ''
                    ]
            )

            /// Set the build Name/Result
            script {
                currentBuild.displayName = "${currentBuild.number}" + ' | ENV ' + "${params.environment}" + ' | ' + "${currentBuild.currentResult}"  + ' | ' + "${params.testName}"

                if ("${params.testName}" == '') {
                    def xmlReport = REPORT_LOCATION + "\\testng-results.xml"

                    def cmdPercent = new StringBuilder()
                    cmdPercent.append("@ECHO OFF\n")
                    cmdPercent.append("set JAVA_HOME=" + JAVA_LOCATION + "\n")
                    cmdPercent.append("set GROOVY_HOME=" + GROOVY_LOCATION + "\n")
                    cmdPercent.append("set PATH=%PATH%;%JAVA_HOME%\\bin;%GROOVY_HOME%\\bin" + "\n")
                    cmdPercent.append("groovy C:\\Jenkins\\workspace\\qecommon\\restAssured\\testNG-Percent-Calc.groovy " + xmlReport)

                    def cmdStdOut = bat(
                            returnStdout: true,
                            script: "${cmdPercent.toString()}"
                    )

                    String[] cmdArray = cmdStdOut.split()
                    echo "${currentBuild.number}" + ' | ENV ' + "${params.environment}" + ' | ' + cmdArray[-1] + "%"
                    currentBuild.displayName = "${currentBuild.number}" + ' | ENV ' + "${params.environment}" + ' | ' + cmdArray[-1] + "%"
                }
            }
        }
    }
}
