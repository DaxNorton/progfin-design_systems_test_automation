package designsystems.set_baselines.gritcorecomponents.badges;

import designsystems.gritcore.badges.Badges_Base;
import designsystems.pages.imagepaths.SauceWindowsEdge_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Badges_Baseline_Sauce_Windows_Edge extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = sauceWindowsEdge(method.getName());
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Badge_SectionOverview_Sauce_Windows_Edge(){
        page.setBaseline_Badge_SectionOverview(SauceWindowsEdge_Paths.BASELINE_BADGE_OVERVIEW);
    }

    @Test
    public void setBaseline_Badge_DefaultWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_DefaultWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_DEFAULT_ICON);
    }

    @Test
    public void setBaseline_Badge_InfoWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_InfoWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_INFO_ICON);
    }

    @Test
    public void setBaseline_Badge_PrimaryWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_PrimaryWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_PRIMARY_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_AttentionWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_ATTENTION_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionStrongWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_AttentionStrongWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON);
    }

    @Test
    public void setBaseline_Badge_SuccessWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_SuccessWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_SUCCESS_ICON);
    }

    @Test
    public void setBaseline_Badge_WarningWithIcon_Sauce_Windows_Edge(){
        page.setBaseline_Badge_WarningWithIcon(SauceWindowsEdge_Paths.BASELINE_BADGE_WARNING_ICON);
    }

}