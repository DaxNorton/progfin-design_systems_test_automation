package designsystems.set_baselines.gritcorecomponents.badges;

import designsystems.gritcore.badges.Badges_Base;
import designsystems.pages.imagepaths.LocalFirefox_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Badges_Baseline_Local_Firefox extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localFirefoxHeadless();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Badge_SectionOverview_Local_Firefox(){
        page.setBaseline_Badge_SectionOverview(LocalFirefox_Paths.BASELINE_BADGE_OVERVIEW);
    }

    @Test
    public void setBaseline_Badge_DefaultWithIcon_Local_Firefox(){
        page.setBaseline_Badge_DefaultWithIcon(LocalFirefox_Paths.BASELINE_BADGE_DEFAULT_ICON);
    }

    @Test
    public void setBaseline_Badge_InfoWithIcon_Local_Firefox(){
        page.setBaseline_Badge_InfoWithIcon(LocalFirefox_Paths.BASELINE_BADGE_INFO_ICON);
    }

    @Test
    public void setBaseline_Badge_PrimaryWithIcon_Local_Firefox(){
        page.setBaseline_Badge_PrimaryWithIcon(LocalFirefox_Paths.BASELINE_BADGE_PRIMARY_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionWithIcon_Local_Firefox(){
        page.setBaseline_Badge_AttentionWithIcon(LocalFirefox_Paths.BASELINE_BADGE_ATTENTION_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionStrongWithIcon_Local_Firefox(){
        page.setBaseline_Badge_AttentionStrongWithIcon(LocalFirefox_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON);
    }

    @Test
    public void setBaseline_Badge_SuccessWithIcon_Local_Firefox(){
        page.setBaseline_Badge_SuccessWithIcon(LocalFirefox_Paths.BASELINE_BADGE_SUCCESS_ICON);
    }

    @Test
    public void setBaseline_Badge_WarningWithIcon_Local_Firefox(){
        page.setBaseline_Badge_WarningWithIcon(LocalFirefox_Paths.BASELINE_BADGE_WARNING_ICON);
    }
}