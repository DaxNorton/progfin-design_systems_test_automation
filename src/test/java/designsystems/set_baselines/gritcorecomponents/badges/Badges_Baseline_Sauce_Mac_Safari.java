package designsystems.set_baselines.gritcorecomponents.badges;

import designsystems.gritcore.badges.Badges_Base;
import designsystems.pages.imagepaths.SauceMacSafari_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Badges_Baseline_Sauce_Mac_Safari extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = sauceMacSafari(method.getName());
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Badge_DefaultWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_DefaultWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_DEFAULT_ICON);
    }

    @Test
    public void setBaseline_Badge_InfoWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_InfoWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_INFO_ICON);
    }

    @Test
    public void setBaseline_Badge_PrimaryWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_PrimaryWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_PRIMARY_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_AttentionWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_ATTENTION_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionStrongWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_AttentionStrongWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON);
    }

    @Test
    public void setBaseline_Badge_SuccessWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_SuccessWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_SUCCESS_ICON);
    }

    @Test
    public void setBaseline_Badge_WarningWithIcon_Sauce_Mac_Safari(){
        page.setBaseline_Badge_WarningWithIcon(SauceMacSafari_Paths.BASELINE_BADGE_WARNING_ICON);
    }

}