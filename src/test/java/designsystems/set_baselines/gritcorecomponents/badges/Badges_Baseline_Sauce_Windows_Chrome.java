package designsystems.set_baselines.gritcorecomponents.badges;

import designsystems.gritcore.badges.Badges_Base;
import designsystems.pages.imagepaths.SauceWindowsChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Badges_Baseline_Sauce_Windows_Chrome extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = sauceWindowsChrome(method.getName());
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Badge_SectionOverview_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_SectionOverview(SauceWindowsChrome_Paths.BASELINE_BADGE_OVERVIEW);
    }

    @Test
    public void setBaseline_Badge_DefaultWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_DefaultWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_DEFAULT_ICON);
    }

    @Test
    public void setBaseline_Badge_InfoWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_InfoWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_INFO_ICON);
    }

    @Test
    public void setBaseline_Badge_PrimaryWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_PrimaryWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_PRIMARY_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_AttentionWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_ATTENTION_ICON);
    }

    @Test
    public void setBaseline_Badge_AttentionStrongWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_AttentionStrongWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON);
    }

    @Test
    public void setBaseline_Badge_SuccessWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_SuccessWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_SUCCESS_ICON);
    }

    @Test
    public void setBaseline_Badge_WarningWithIcon_Sauce_Windows_Chrome(){
        page.setBaseline_Badge_WarningWithIcon(SauceWindowsChrome_Paths.BASELINE_BADGE_WARNING_ICON);
    }

}