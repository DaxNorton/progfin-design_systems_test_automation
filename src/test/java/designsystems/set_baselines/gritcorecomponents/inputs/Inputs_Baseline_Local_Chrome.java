package designsystems.set_baselines.gritcorecomponents.inputs;

import designsystems.gritcore.inputs.Inputs_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Inputs_Baseline_Local_Chrome extends Inputs_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Input_NormalField_Local_Chrome() throws InterruptedException {
        org.openqa.selenium.Dimension dimension = new Dimension(767,850);
        driver.manage().window().setSize(dimension);
        Thread.sleep(500);
        driver.findElement(By.xpath(page.NORMAL_FIELD_INPUT_XPATH)).sendKeys("gjq lowercase low hanging letter test");
        page.setbaseline_Inputs_Normal_Field(LocalChrome_Paths.BASELINE_INPUT_NORMAL_FIELD);
    }

    @Test
    public void setBaseline_Input_Overview_Local_Chrome(){
        page.setbaseline_Inputs_Overview(LocalChrome_Paths.BASELINE_INPUT_OVERVIEW);
    }

    @Test
    public void setBaseline_Input_Overview_Mobile_Local_Chrome(){
        org.openqa.selenium.Dimension dimension = new Dimension(767,850);
        driver.manage().window().setSize(dimension);
        page.setbaseline_Inputs_Overview(LocalChrome_Paths.BASELINE_INPUT_OVERVIEW_MOBILE);
    }

}