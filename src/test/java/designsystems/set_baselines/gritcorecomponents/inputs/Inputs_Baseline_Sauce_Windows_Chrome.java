package designsystems.set_baselines.gritcorecomponents.inputs;

import designsystems.gritcore.inputs.Inputs_Base;
import designsystems.pages.imagepaths.SauceWindowsChrome_Paths;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Inputs_Baseline_Sauce_Windows_Chrome extends Inputs_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsChrome(method.getName());
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Input_NormalField_Sauce_Windows_Chrome() throws InterruptedException {
        Dimension dimension = new Dimension(767,850);
        driver.manage().window().setSize(dimension);
        Thread.sleep(500);
        driver.findElement(By.xpath(page.NORMAL_FIELD_INPUT_XPATH)).sendKeys("gjq lowercase low hanging letter test");
        page.setbaseline_Inputs_Normal_Field(SauceWindowsChrome_Paths.BASELINE_INPUT_NORMAL_FIELD);
    }

    @Test(enabled = false)
    public void setBaseline_Input_Overview_Sauce_Windows_Chrome(){
        page.setbaseline_Inputs_Overview(SauceWindowsChrome_Paths.BASELINE_INPUT_OVERVIEW);
    }

    @Test(enabled = false)
    public void setBaseline_Input_Overview_Mobile_Sauce_Windows_Chrome(){
        Dimension dimension = new Dimension(767,850);
        driver.manage().window().setSize(dimension);
        page.setbaseline_Inputs_Overview(SauceWindowsChrome_Paths.BASELINE_INPUT_OVERVIEW_MOBILE);
    }

}