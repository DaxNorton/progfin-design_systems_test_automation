package designsystems.set_baselines.gritcorecomponents.buttons;

import designsystems.gritcore.buttons.Buttons_Base;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

public class Buttons_Baseline_Local_Chrome extends Buttons_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

}