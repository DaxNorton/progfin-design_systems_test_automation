package designsystems.set_baselines.gritcorecomponents.spinners;

import designsystems.gritcore.badges.Badges_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Spinners_Baseline_Local_Chrome extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_SectionOverview_Local_Chrome(){
        page.setBaseline_Badge_SectionOverviewRetina(LocalChrome_Paths.BASELINE_BADGE_OVERVIEW);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_DefaultWithIcon_Local_Chrome(){
        page.setBaseline_Badge_DefaultWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_DEFAULT_ICON);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_InfoWithIcon_Local_Chrome(){
        page.setBaseline_Badge_InfoWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_INFO_ICON);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_PrimaryWithIcon_Local_Chrome(){
        page.setBaseline_Badge_PrimaryWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_PRIMARY_ICON);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_AttentionWithIcon_Local_Chrome(){
        page.setBaseline_Badge_AttentionWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_ATTENTION_ICON);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_AttentionStrongWithIcon_Local_Chrome(){
        page.setBaseline_Badge_AttentionStrongWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_SuccessWithIcon_Local_Chrome(){
        page.setBaseline_Badge_SuccessWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_SUCCESS_ICON);
    }

    @Test(enabled = false)
    public void setBaseline_Badge_WarningWithIcon_Local_Chrome(){
        page.setBaseline_Badge_WarningWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_WARNING_ICON);
    }
}