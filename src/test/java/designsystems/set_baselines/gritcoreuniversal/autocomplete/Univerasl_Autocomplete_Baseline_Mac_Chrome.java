package designsystems.set_baselines.gritcoreuniversal.autocomplete;

import designsystems.grituniversal.autocomplete.AutoComplete_Base;
import designsystems.pages.imagepaths.ImagePaths;
import designsystems.pages.imagepaths.SauceMacChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Univerasl_Autocomplete_Baseline_Mac_Chrome extends AutoComplete_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
    }

    @Test
    public void setbaseline_Universal_Autocomplete_Default(){
        setAutocompleteDefaultVisual(SauceMacChrome_Paths.BASELINE_UNIVERSAL_AUTOCOMPLETE_DEFAULT);
    }
}
