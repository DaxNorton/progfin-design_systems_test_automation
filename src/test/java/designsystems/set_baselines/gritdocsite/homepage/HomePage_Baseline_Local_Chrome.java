package designsystems.set_baselines.gritdocsite.homepage;

import designsystems.gritdocsite.homepage.HomePage_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class HomePage_Baseline_Local_Chrome extends HomePage_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Roadmap_Quick_Local_Chrome(){
        page.setbaseline_HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW);
    }
}