package designsystems.set_baselines.gritdocsite.homepage;

import designsystems.gritdocsite.homepage.HomePage_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.Test;
import java.lang.reflect.Method;
@Test(enabled = false)
public class ViewportService_Local_Chrome_Baseline extends HomePage_Base{

    @Test
    public void setBaseline_HomePage_ViewportService_DesktopView_Local_Chrome(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        page.setbaseline_HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW);
    }

    @Test
    public void setBaseline_HomePage_ViewportService_TabletView_Local_Chrome(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        page.setbaseline_HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW_TABLETVIEW);
    }

    @Test
    public void setBaseline_HomePage_ViewportService_MobileView_Local_Chrome(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        page.setbaseline_HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW_MOBILEVIEW);
    }

}