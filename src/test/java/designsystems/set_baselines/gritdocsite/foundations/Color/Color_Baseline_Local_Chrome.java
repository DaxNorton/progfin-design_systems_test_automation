package designsystems.set_baselines.gritdocsite.foundations.Color;

import designsystems.gritdocsite.foundations.color.Color_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Color_Baseline_Local_Chrome extends Color_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Foundations_Color_Quick_Local_Chrome(){
        page.setbaseline_Foundations_Color_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_COLOR);
    }
}