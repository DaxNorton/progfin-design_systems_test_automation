package designsystems.set_baselines.gritdocsite.foundations.Typography;

import designsystems.gritdocsite.foundations.typography.Typography_Base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import java.lang.reflect.Method;
@Test(enabled = false)
public class Typography_Baseline_Local_Chrome extends Typography_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Foundations_Typography_Quick_Local_Chrome(){
        page.setbaseline_Foundations_Typography_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_TYPOGRAPHY);
    }
}