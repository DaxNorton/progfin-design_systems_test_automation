package designsystems.set_baselines.gritdocsite.foundations.Spacing;

import designsystems.gritdocsite.foundations.spacing.Spacing_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Spacing_Baseline_Local_Chrome extends Spacing_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Foundations_Spacing_Quick_Local_Chrome(){
        page.setbaseline_Foundations_Spacing_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_SPACING);
    }
}