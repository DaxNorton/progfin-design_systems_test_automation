package designsystems.set_baselines.gritdocsite.foundations.Home;

import designsystems.gritdocsite.foundations.main.Foundations_Base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import java.lang.reflect.Method;
@Test(enabled = false)
public class Foundations_Baseline_Local_Chrome extends Foundations_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Foundations_Quick_Local_Chrome(){
        page.setbaseline_Foundations_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS);
    }
}