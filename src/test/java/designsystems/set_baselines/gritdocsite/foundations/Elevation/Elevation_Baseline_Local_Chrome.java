package designsystems.set_baselines.gritdocsite.foundations.Elevation;

import designsystems.gritdocsite.foundations.elevation.Elevation_Base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import java.lang.reflect.Method;
@Test(enabled = false)
public class Elevation_Baseline_Local_Chrome extends Elevation_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Foundations_Elevation_Quick_Local_Chrome(){
        page.setbaseline_Foundations_Elevation_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_ELEVATION);
    }
}