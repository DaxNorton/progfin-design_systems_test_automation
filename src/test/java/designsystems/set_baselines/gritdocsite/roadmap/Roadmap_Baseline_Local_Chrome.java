package designsystems.set_baselines.gritdocsite.roadmap;

import designsystems.gritdocsite.roadmap.Roadmap_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;
@Test(enabled = false)
public class Roadmap_Baseline_Local_Chrome extends Roadmap_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Roadmap_Quick_Local_Chrome(){
        page.setbaseline_Roadmap_Quick(LocalChrome_Paths.BASELINE_ROADMAP);
    }
}