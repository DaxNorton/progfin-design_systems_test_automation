package designsystems.set_baselines.gritdocsite.getstarted.Developer;

import designsystems.gritdocsite.getstarted.developer.GetStarted_Developer_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Baseline_Local_Chrome extends GetStarted_Developer_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Hero_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Hero_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_HERO);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Section1_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Section1_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_1);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Section2_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Section2_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_2);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Section3_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Section3_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_3);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Section4_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Section4_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_4);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Section5_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Section5_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_5);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Section6_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Developer_Section6_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_6);
    }

    @Test
    public void setBaseline_GetStarted_Developer_Local_Chrome_Quick(){
        page.setBaseline_GetStarted_Developer_Quick(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER);
    }
}