package designsystems.set_baselines.gritdocsite.getstarted.Main;

import designsystems.gritdocsite.getstarted.main.GetStarted_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;
@Test(enabled = false)
public class Baseline_Local_Chrome extends GetStarted_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_GetStarted_Quick_Local_Chrome(){
        page.setBaseline_GetStarted_Quick(LocalChrome_Paths.BASELINE_GETSTARTED);
    }

    @Test
    public void setBaseline_GetStarted_Hero_Wrapper_Local_Chrome(){
        page.setBaseline_GetStarted_Hero_Content(LocalChrome_Paths.BASELINE_GETSTARTED_HERO_WRAPPER);
    }
    @Test
    public void setBaseline_GetStarted_Home_Section_Local_Chrome(){
        page.setBaseline_GetStarted_Home_Content(LocalChrome_Paths.BASELINE_GETSTARTED_HOME_CONTENT);
    }
}