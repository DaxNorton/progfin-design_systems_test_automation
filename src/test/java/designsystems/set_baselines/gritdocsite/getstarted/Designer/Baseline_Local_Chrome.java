package designsystems.set_baselines.gritdocsite.getstarted.Designer;

import designsystems.gritdocsite.getstarted.designer.GetStarted_Designer_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Baseline_Local_Chrome extends GetStarted_Designer_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Hero_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Hero_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_HERO);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section1_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section1_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_1);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section2_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section2_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_2);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section3_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section3_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_3);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section4_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section4_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_4);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section5_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section5_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_5);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section6_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section6_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_6);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Section7_Content_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Section7_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_7);
    }

    @Test
    public void setBaseline_GetStarted_Designer_Quick_Local_Chrome(){
        page.setBaseline_GetStarted_Designer_Quick(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER);
    }
}