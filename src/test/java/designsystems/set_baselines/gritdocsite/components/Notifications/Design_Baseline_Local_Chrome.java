package designsystems.set_baselines.gritdocsite.components.Notifications;

import designsystems.gritdocsite.components.notifications.design.Notifications_Design_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Design_Baseline_Local_Chrome extends Notifications_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Notifications_Design_Quick_Local_Chrome(){
        page.setbaseline_Components_Notifications_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_NOTIFICATIONS_DESIGN);
    }
}