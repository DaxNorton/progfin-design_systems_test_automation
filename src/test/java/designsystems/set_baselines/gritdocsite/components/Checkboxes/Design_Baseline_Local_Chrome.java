package designsystems.set_baselines.gritdocsite.components.Checkboxes;

import designsystems.gritdocsite.components.checkboxes.design.Checkboxes_Design_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Design_Baseline_Local_Chrome extends Checkboxes_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Checkboxes_Design_Quick_Local_Chrome(){
        page.setbaseline_Components_Checkboxes_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_CHECKBOXES_DESIGN);
    }
}