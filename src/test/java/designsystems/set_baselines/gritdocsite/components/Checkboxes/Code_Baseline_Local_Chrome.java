package designsystems.set_baselines.gritdocsite.components.Checkboxes;

import designsystems.gritdocsite.components.checkboxes.code.Checkboxes_Code_Base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import java.lang.reflect.Method;

public class Code_Baseline_Local_Chrome extends Checkboxes_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Checkboxes_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_Checkboxes_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_CHECKBOXES_CODE);
    }
}