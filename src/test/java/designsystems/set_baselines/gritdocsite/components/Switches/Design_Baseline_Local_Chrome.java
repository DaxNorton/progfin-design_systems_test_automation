package designsystems.set_baselines.gritdocsite.components.Switches;

import designsystems.gritdocsite.components.switches.design.Switches_Design_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Design_Baseline_Local_Chrome extends Switches_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Switches_Design_Local_Chrome_Quick(){
        page.setbaseline_Components_Switches_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_SWITCHES_DESIGN);
    }
}