package designsystems.set_baselines.gritdocsite.components.Overlays;

import designsystems.gritdocsite.components.overlays.code.Overlays_Code_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Code_Baseline_Local_Chrome extends Overlays_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Checkboxes_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_Overlays_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_OVERLAYS_CODE);
    }
}