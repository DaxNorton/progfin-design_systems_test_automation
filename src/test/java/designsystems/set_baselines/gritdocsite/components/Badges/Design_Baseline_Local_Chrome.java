package designsystems.set_baselines.gritdocsite.components.Badges;

import designsystems.gritdocsite.components.badges.design.Badges_Design_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Design_Baseline_Local_Chrome extends Badges_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Badges_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_Badges_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_BADGES_DESIGN);
    }
}