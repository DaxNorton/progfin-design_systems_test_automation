package designsystems.set_baselines.gritdocsite.components.TextInputs;

import designsystems.gritdocsite.components.textinputs.code.TextInputs_Code_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Code_Baseline_Local_Chrome extends TextInputs_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Components_Checkboxes_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_TextInputs_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_TEXT_INPUTS_CODE);
    }

}