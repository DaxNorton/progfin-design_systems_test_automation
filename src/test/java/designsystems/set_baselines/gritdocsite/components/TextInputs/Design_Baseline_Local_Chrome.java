package designsystems.set_baselines.gritdocsite.components.TextInputs;

import designsystems.gritdocsite.components.textinputs.design.TextInputs_Design_Base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import java.lang.reflect.Method;
@Test(enabled = false)
public class Design_Baseline_Local_Chrome extends TextInputs_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Components_Badges_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_TextInputs_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_TEXT_INPUTS_DESIGN);
    }
}