package designsystems.set_baselines.gritdocsite.components.Loaders;

import designsystems.gritdocsite.components.loaders.design.Loaders_Design_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Design_Baseline_Local_Chrome extends Loaders_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Checkboxes_Design_Quick_Local_Chrome(){
        page.setbaseline_Components_Loaders_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_LOADERS_DESIGN);
    }
}