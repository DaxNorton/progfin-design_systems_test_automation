package designsystems.set_baselines.gritdocsite.components.Loaders;

import designsystems.gritdocsite.components.loaders.code.Loaders_Code_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Code_Baseline_Local_Chrome extends Loaders_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Checkboxes_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_Loaders_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_LOADERS_CODE);
    }
}