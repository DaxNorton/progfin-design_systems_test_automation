package designsystems.set_baselines.gritdocsite.components.Radios;

import designsystems.gritdocsite.components.radios.code.Radios_Code_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
@Test(enabled = false)
public class Code_Baseline_Local_Chrome extends Radios_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void setBaseline_Components_Radios_Code_Quick_Local_Chrome(){
        page.setbaseline_Components_Radios_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE);
    }

    @Test
    public void test_Radios_Code_Hero_Content_Desktop_Local_Chrome(){
        page.setbaseline_Radios_Code_Hero_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_HERO);
    }

    @Test
    public void test_Radios_Code_Section1_Content_Local_Chrome(){
        page.setbaseline_Radios_Code_Section1_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_SECTION_1);
    }

    @Test
    public void test_Radios_Code_Section2_Content_Local_Chrome(){
        page.setbaseline_Radios_Code_Section2_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_SECTION_2);
    }

    @Test
    public void test_Radios_Code_Section3_Content_Local_Chrome(){
        page.setbaseline_Radios_Code_Section3_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_SECTION_3);
    }

}