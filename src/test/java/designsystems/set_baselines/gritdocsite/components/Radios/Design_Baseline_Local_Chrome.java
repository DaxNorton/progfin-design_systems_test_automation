package designsystems.set_baselines.gritdocsite.components.Radios;

import designsystems.gritdocsite.components.radios.design.Radios_Design_Base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import java.lang.reflect.Method;

public class Design_Baseline_Local_Chrome extends Radios_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Radios_Design_Quick_Local_Chrome(){
        page.setbaseline_Components_Radios_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_DESIGN);
    }
}