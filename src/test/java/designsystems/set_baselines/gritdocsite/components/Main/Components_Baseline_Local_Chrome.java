package designsystems.set_baselines.gritdocsite.components.Main;

import designsystems.gritdocsite.components.main.Doc_Base;
import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Components_Baseline_Local_Chrome extends Doc_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Quick_Local_Chrome(){
        page.setBaseline_Components_Quick(LocalChrome_Paths.BASELINE_COMPONENTS);
    }

    @Test(enabled = false)
    public void setBaseline_Components_Hero_Wrapper_Local_Chrome(){
        page.setBaseline_Components_Hero_Content(LocalChrome_Paths.BASELINE_COMPONENTS_HERO_WRAPPER);
    }
    @Test(enabled = false)
    public void setBaseline_Components_Home_Section_Local_Chrome(){
        page.setBaseline_Components_Home_Content(LocalChrome_Paths.BASELINE_COMPONENTS_HOME_CONTENT);
    }
}