package designsystems.grituniversal.inputs;

import com.deque.axe.AXE;
import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.InputsPage;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.*;

import java.lang.reflect.Method;

public class Inputs_Base extends GritCore_Base {

    public InputsPage page;
    public String urlUnderTest = page.URL_INPUTS;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new InputsPage(driver, driverWait);
    }

    public boolean normalFieldHeight(){
        Integer height = page.getNormalFieldHeight().height;
        return height.equals(48);
    }

    public boolean smallFieldHeight(){
        Integer height = page.getSmallFieldHeight().height;
        return height.equals(48);
    }

    public boolean mediumFieldHeight(){
        Integer height = page.getMediumFieldHeight().height;
        return height.equals(62);
    }

    public boolean normalFieldLabelFont(){
        return page.getNormalFieldLabelFont().equals("Montserrat, sans-serif");
    }

    public boolean smallFieldLabelFont(){
        return page.getSmallFieldLabelFont().equals("Montserrat, sans-serif");
    }

    public boolean mediumFieldLabelFont(){
        return page.getMediumFieldLabelFont().equals("Montserrat, sans-serif");
    }

    public boolean normalFieldInputFont(){
        page.clickNormalField();
        page.inputNormalField("Test");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getNormalFieldInputFont().equals("Lato, sans-serif");
    }

    public boolean smallFieldInputFont(){
        page.clickSmallField();
        page.inputSmallField("Test");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getSmallFieldInputFont().equals("Lato, sans-serif");
    }

    public boolean mediumFieldInputFont(){
        page.clickMediumField();
        page.inputMediumField("Test");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getMediumFieldInputFont().equals("Lato, sans-serif");
    }

    public boolean iconFieldPromptColor(){
        page.clickIconField();
        page.clickNormalField();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getFieldWithIconLabelColor().contains("216, 8, 0");
    }

    public boolean validationMessageDoesNotShiftPage(){
        Point locationBefore = page.getElementCoordinates(page.ICON_EMAIL_FIELD_ID);
        driver.findElement(By.id(page.ICON_FIELD_ID)).click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Point locationAfter = page.getElementCoordinates(page.ICON_EMAIL_FIELD_ID);
        return locationBefore.equals(locationAfter);
    }

    public boolean ariaLabelFalse(){
        return driver.findElement(By.id(page.ICON_EMAIL_FIELD_INPUT_ID)).getAttribute("aria-invalid").equals("false");
    }

    public boolean ariaLabelTrue(){
        driver.findElement(By.id(page.ICON_EMAIL_FIELD_ID)).click();
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id(page.ICON_FIELD_ID)).click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return driver.findElement(By.id(page.ICON_EMAIL_FIELD_INPUT_ID)).getAttribute("aria-invalid").equals("true");
    }

    public boolean axeAPIAccessibilityCheckInputs(Method testName){
        JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).exclude("grit-wc-icon > i").analyze();
        JSONArray violations = responseJSON.getJSONArray("violations");
        if (violations.length() == 0) {
            return true;
        } else {
            AXE.writeResults(testName.getName(), responseJSON);
            AXE.report(violations);
            return false;
        }
    }
}