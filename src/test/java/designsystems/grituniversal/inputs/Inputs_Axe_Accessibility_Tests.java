package designsystems.grituniversal.inputs;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Inputs_Axe_Accessibility_Tests extends Inputs_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testAxeAPIAccessibilityInputsGritCoreUniversal(Method method){
        Assert.assertTrue(axeAPIAccessibilityCheckInputs(method));
    }
}