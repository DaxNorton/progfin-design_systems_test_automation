package designsystems.grituniversal.inputs;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

//Only dimension tests can be run in Firefox at the current time, due to Firefox shadowDom bug with seleenium
public class Inputs_Design_Firefox_Tests extends Inputs_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localFirefoxHeadless();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(5000);
    }

    @Test
    public void testInputDesign(){
        SoftAssert softAssert = new SoftAssert();
        //sleep to give components time to finish rendering, if measurements are taken too
        //quickly then they come back inconsistent.
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //softAssert.assertTrue(normalFieldLabelFont(), "Normal Field Label Font"); //Firefox scroll into view bug
        //softAssert.assertTrue(smallFieldLabelFont(), "Small Field Label Font"); //Firefox scroll into view bug
        //softAssert.assertTrue(mediumFieldLabelFont(), "Medium Field Label Font"); //Firefox scroll into view bug
        //softAssert.assertTrue(normalFieldInputFont(), "Normal Field Input Font"); //Firefox scroll into view bug
        //softAssert.assertTrue(smallFieldInputFont(), "Small Field Input Font"); //Firefox scroll into view bug
        //softAssert.assertTrue(mediumFieldInputFont(), "Medium Field Input Font"); //Firefox scroll into view bug
        //softAssert.assertTrue(iconFieldPromptColor(), "Icon Field Prompt Color"); //Firefox scroll into view bug
        //softAssert.assertTrue(validationMessageDoesNotShiftPage(), "Validation Message does not shift page"); //Firefox scroll into view bug
        //softAssert.assertTrue(ariaLabelFalse(), "Aria Label set to False"); //Firefox scroll into view bug
        //softAssert.assertTrue(ariaLabelTrue(),"Aria Label set to True"); //Firefox scroll into view bug
        softAssert.assertTrue(normalFieldHeight(), "Normal Field Height");
        softAssert.assertTrue(smallFieldHeight(), "Small Field Height");
        softAssert.assertTrue(mediumFieldHeight(), "Medium Field Height");
        softAssert.assertAll();
    }

}