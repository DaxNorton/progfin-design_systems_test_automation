package designsystems.grituniversal.inputs;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Inputs_Design_Windows_Edge_Tests extends Inputs_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsEdge(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testInputDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(normalFieldLabelFont(), "Normal Field Label Font");
        softAssert.assertTrue(smallFieldLabelFont(), "Small Field Label Font");
        softAssert.assertTrue(mediumFieldLabelFont(), "Medium Field Label Font");
        softAssert.assertTrue(normalFieldInputFont(), "Normal Field Input Font");
        softAssert.assertTrue(smallFieldInputFont(), "Small Field Input Font");
        softAssert.assertTrue(mediumFieldInputFont(), "Medium Field Input Font");
        softAssert.assertTrue(iconFieldPromptColor(), "Icon Field Prompt Color");
        softAssert.assertTrue(validationMessageDoesNotShiftPage(), "Validation Message does not shift page");
        softAssert.assertTrue(ariaLabelFalse(), "Aria Label set to False");
        softAssert.assertTrue(ariaLabelTrue(),"Aria Label set to True");
        softAssert.assertTrue(normalFieldHeight(), "Normal Field Height");
        softAssert.assertTrue(smallFieldHeight(), "Small Field Height");
        softAssert.assertTrue(mediumFieldHeight(), "Medium Field Height");
        softAssert.assertAll();
    }

}