package designsystems.grituniversal.calendars;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Calendars_Design_Firefox_Tests extends Calendars_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localFirefoxHeadless();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testCalendarDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(calendarsLabelIdleColor(), "Label Idle Color");
        //softAssert.assertTrue(calendarsLabelActiveColor(), "Label Active Color"); //Firefox bug cannot scroll into view
        //softAssert.assertTrue(calendarsLabelFont(), "Label Font"); //Firefox ShadowDOM bug
        //softAssert.assertTrue(calendarsDisplayFont(), "Display Font"); //Firefox ShadowDOM bug
        softAssert.assertAll();
    }
}