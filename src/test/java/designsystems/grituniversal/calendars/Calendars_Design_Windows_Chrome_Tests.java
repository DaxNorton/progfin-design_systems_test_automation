package designsystems.grituniversal.calendars;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Calendars_Design_Windows_Chrome_Tests extends Calendars_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
    }

    @Test
    public void testCalendarDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(calendarsLabelIdleColor(), "Label Idle Color");
        //softAssert.assertTrue(calendarsLabelActiveColor(), "Label Active Color"); //Focus state going away with calendar input update
        softAssert.assertTrue(calendarsLabelFont(), "Label Font");
        softAssert.assertTrue(calendarsDisplayFont(), "Display Font");
        softAssert.assertAll();
    }
}