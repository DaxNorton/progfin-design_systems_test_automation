package designsystems.grituniversal.calendars;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Calendars_Functional_Chrome_Tests extends Calendars_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testCalendarDisplaySelectedDate(){
        SoftAssert softAssert = new SoftAssert();
        //softAssert.assertTrue(calendarDisplaysSelectedDate());
        softAssert.assertAll();
    }
}