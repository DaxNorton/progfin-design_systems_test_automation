package designsystems.grituniversal.calendars;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.CalendarsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Method;

public class Calendars_Base extends GritCore_Base {

    public CalendarsPage page;
    public String urlUnderTest = page.URL_CALENDAR;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new CalendarsPage(driver, driverWait);
    }

    public boolean calendarsLabelActiveColor(){
        driver.findElement(By.cssSelector(page.CALENDAR_DEFAULT_CSS)).click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String test = page.getCalendarLabelColor();
        return page.getCalendarLabelColor().contains(page.COLOR_ACTIVE_LABEL);
    }

    public boolean calendarsLabelIdleColor(){
        return page.getCalendarLabelColor().contains(page.COLOR_IDLE_LABEL);
    }

    public boolean calendarsLabelFont(){
        return page.getCalendarLabelFont().equals("Montserrat, sans-serif");
    }

    public boolean calendarsDisplayFont(){
        return page.getCalendarDisplayFont().equals("Lato, sans-serif");
    }

    public boolean calendarDisplaysSelectedDate(){
        driver.findElement(By.cssSelector(page.CALENDAR_DATE_CSS)).click();
        WebElement shadowRoot1 = page.getElementShadowRootByCSS(page.CONTROL_CALENDAR_SHADOW_ROOT_1_CSS);
        WebElement root2 = shadowRoot1.findElement(By.cssSelector(page.CONTROL_CALENDAR_SHADOW_ROOT_2_CSS));
        WebElement shadowRoot2 = page.getElementShadowRoot(root2);
        shadowRoot2.findElement(By.cssSelector(page.CONTROL_CALENDAR_DATE_CSS)).click();
        String test = driver.findElement(By.cssSelector(page.CONTROL_CALENDAR_CONTENT_CSS)).getAttribute("value");
        return driver.findElement(By.cssSelector(page.CONTROL_CALENDAR_CONTENT_CSS)).getAttribute("value").equals("Mon Oct 05 2020 00:00:00 GMT-0600 (Mountain Daylight Time)");
    }
}
