package designsystems.grituniversal.linearloaders;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.LinearLoadersPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class LinearLoaders_Base extends GritCore_Base {

    public LinearLoadersPage page;
    public String urlUnderTest = page.URL_LINLOADER;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new LinearLoadersPage(driver, driverWait);
    }

    public boolean loaderHeight(){
        return page.getWrapperHeight().equals("4");
    }

    public boolean loaderWrapperFlexGrow(){
        return page.getWrapperFlexGrow().equals("1");
    }

    public boolean slowAnimationDelay(){
        return page.getSlowAnimationDelay().equals("0.25s");
    }

    public boolean slowAnimationDirection(){
        return page.getSlowAnimationDirection().equals("normal");
    }

    public boolean slowAnimationDuration(){
        return page.getSlowAnimationDuration().equals("3s");
    }

    public boolean slowAnimationIteration(){
        return page.getSlowAnimationIteration().equals("infinite");
    }

    public boolean fastAnimationDelay(){
        return page.getFastAnimationDelay().equals("1.4s");
    }

    public boolean fastAnimationDirection(){
        return page.getFastAnimationDirection().equals("normal");
    }

    public boolean fastAnimationDuration(){
        return page.getFastAnimationDuration().equals("3s");
    }

    public boolean fastAnimationIteration(){
        return page.getFastAnimationIteration().equals("infinite");
    }
}