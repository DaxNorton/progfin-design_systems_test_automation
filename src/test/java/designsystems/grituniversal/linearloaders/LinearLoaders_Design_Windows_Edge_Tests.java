package designsystems.grituniversal.linearloaders;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class LinearLoaders_Design_Windows_Edge_Tests extends LinearLoaders_Base{

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsEdge(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
    }

    @Test
    public void testLinearLoaderDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(loaderHeight(), "Height of the loader bar");
        softAssert.assertTrue(loaderWrapperFlexGrow(), "Wrapper flexGrow");
        softAssert.assertTrue(slowAnimationDelay(), "Slow animation delay");
        softAssert.assertTrue(slowAnimationDirection(), "Slow animation direction");
        softAssert.assertTrue(slowAnimationDuration(), "Slow animation duration");
        softAssert.assertTrue(slowAnimationIteration(), "Slow animation iteration count");
        softAssert.assertTrue(fastAnimationDelay(), "Fast animation delay");
        softAssert.assertTrue(fastAnimationDirection(), "Fast animation direction");
        softAssert.assertTrue(fastAnimationDuration(), "Fast animation duration");
        softAssert.assertTrue(fastAnimationIteration(), "Fast animation iteration count");
        softAssert.assertAll();
    }

}