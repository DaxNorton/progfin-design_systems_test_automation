package designsystems.grituniversal.selects;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.GritColorsPage;
import designsystems.pages.universal.SelectsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Selects_Base extends GritCore_Base {

    public SelectsPage page;
    public String urlUnderTest = page.URL_SELECTS;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        page = new SelectsPage(driver, driverWait);
    }

    public boolean isDisabled(){
        page.clickHelperDisable();
        try {
            Thread.sleep(250); //intentional sleep for element state switch
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getSelectBackgroundColor().contains("0, 0, 0");
    }

    public boolean submittedOptionDisplays(){
        page.selectFromMenu(2);
        page.clickSubmit();
        return page.getSubmittedFormValue().equals("cars-select: saab");
    }

    public boolean listDefaultValue(){
        page.clickSubmit();
        return page.getSubmittedFormValue().equals("cars-select: -- Select an Option --");
    }

    public boolean selectHeight(){
        try {
            Thread.sleep(500); //don't rush the height capture.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String height = String.valueOf(page.selectDemo().findElement(By.id(page.SELECT_ID)).getSize().getHeight());
        return height.equals("48");
    }

    public boolean selectLabelActiveColor(){
        page.clickHelperFocus();
        try {
            Thread.sleep(500); //time for animation
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.selectDemo().findElement(By.id(page.SELECT_LABEL_ID)).getCssValue(
                "color").contains(GritColorsPage.RGB_BLUE_500);
    }
}