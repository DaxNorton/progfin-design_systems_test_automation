package designsystems.grituniversal.selects;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Selects_Functional_Windows_Chrome_Tests extends Selects_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testSelectsFunctionality(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(listDefaultValue(), "Default value displays");
        softAssert.assertTrue(submittedOptionDisplays(), "Selected value updates");
        softAssert.assertTrue(isDisabled(), "Disable toggle sets to Disabled");
        softAssert.assertAll();
    }
}