package designsystems.grituniversal.selects;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Selects_Design_Firefox_Tests extends Selects_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localFirefoxHeadless();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testSelectsDesign(){
        SoftAssert softAssert = new SoftAssert();
        //softAssert.assertTrue(selectHeight(), "Select height"); //Firefox ShadowDom bug
        //softAssert.assertTrue(selectLabelActiveColor(), "Select Active Colors"); //Firefox ShadowDom bug
        softAssert.assertAll();
    }
}