package designsystems.grituniversal.selects;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Selects_Functional_Mac_Safari_Tests extends Selects_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacSafari(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testSelectsFunctionality(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(listDefaultValue(), "Default value displays");
        //softAssert.assertTrue(submittedOptionDisplays(), "Selected value updates"); Javascript click SafariDriver error
        //softAssert.assertTrue(isDisabled(), "Disable toggle sets to Disabled"); ShadowDom issues
        softAssert.assertAll();
    }
}