package designsystems.grituniversal.buttons;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Buttons_Design_Windows_Edge_Tests extends Buttons_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsEdge(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testButtonDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(smallButtonHeight(), "Small button height");
        softAssert.assertTrue(mediumButtonHeight(), "Medium button height");
        softAssert.assertTrue(largeButtonHeight(), "Large button height");
        softAssert.assertTrue(buttonFont(), "Button Font");
        softAssert.assertTrue(primaryButtonColor(), "Button Color: Primary");
        softAssert.assertTrue(secondaryButtonColor(), "Button Color: Secondary");
        softAssert.assertTrue(subtleButtonColor(), "Button Color: Subtle");
        softAssert.assertTrue(successButtonColor(), "Button Color: Success");
        softAssert.assertTrue(destroyButtonColor(), "Button Color: Destroy");
        softAssert.assertTrue(lightButtonColor(), "Button Color: Light");
        softAssert.assertTrue(buttonFontColor(), "Button Font Color");
        softAssert.assertTrue(primaryButtonHoverColor(), "Button Hover Color: Primary");
        softAssert.assertTrue(secondaryButtonHoverColor(), "Button Hover Color: Secondary");
        softAssert.assertTrue(subtleButtonHoverColor(), "Button Hover Color: Subtle");
        softAssert.assertTrue(successButtonHoverColor(), "Button Hover Color: Success");
        softAssert.assertTrue(destroyButtonHoverColor(), "Button Hover Color: Destroy");
        softAssert.assertTrue(lightButtonHoverColor(), "Button Hover Color: Light");
        softAssert.assertTrue(primaryButtonActiveColor(), "Button Active Color: Primary");
        softAssert.assertTrue(secondaryButtonActiveColor(), "Button Active Color: Secondary");
        softAssert.assertTrue(subtleButtonActiveColor(), "Button Active Color: Subtle");
        softAssert.assertTrue(successButtonActiveColor(), "Button Active Color: Success");
        softAssert.assertTrue(destroyButtonActiveColor(), "Button Active Color: Destroy");
        softAssert.assertTrue(lightButtonActiveColor(), "Button Active Color: Light");
        softAssert.assertAll();
    }
}