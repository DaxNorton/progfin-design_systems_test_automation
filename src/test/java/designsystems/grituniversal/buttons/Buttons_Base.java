package designsystems.grituniversal.buttons;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.ButtonsPage;
import designsystems.pages.imagepaths.ImagePaths;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

public class Buttons_Base extends GritCore_Base{

    public ButtonsPage page;
    public String urlUnderTest = page.URL_BUTTONS;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ButtonsPage(driver, driverWait);
    }

    public Boolean smallButtonHeight(){
        return page.getHeightSmallButton().equals("32");
    }

    public Boolean mediumButtonHeight(){
        return page.getHeightMediumButton().equals("48");
    }

    public Boolean largeButtonHeight(){
        return page.getHeightLargeButton().equals("64");
    }

    public Boolean primaryButtonColor(){
        return page.getColorPrimary().contains(page.COLOR_PRIMARY_COLOR);
    }

    public Boolean secondaryButtonColor(){
        return page.getColorSecondary().contains(page.COLOR_SECONDARY_COLOR);
    }

    public Boolean subtleButtonColor(){
        return page.getColorSubtle().contains(page.COLOR_SUBTLE_COLOR);
    }

    public Boolean successButtonColor(){
        return page.getColorSuccess().contains(page.COLOR_SUCCESS_COLOR);
    }

    public Boolean destroyButtonColor(){
        return page.getColorDestroy().contains(page.COLOR_DESTROY_COLOR);
    }

    public Boolean lightButtonColor(){
        return page.getColorPrimaryLight().contains(page.COLOR_LIGHT_COLOR);
    }

    public Boolean buttonFontColor(){
        return page.getFontColorPrimary().contains(page.COLOR_PRIMARY_TEXT);
    }

    public Boolean buttonFont(){
        return page.getFont().equals(page.FONT);
    }

    public Boolean primaryButtonHoverColor(){
        Actions hover = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_PRIMARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        hover.moveToElement(element).perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_PRIMARY_HOVER);
    }

    public Boolean primaryButtonActiveColor(){
        Actions action = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_PRIMARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        action.moveToElement(element);
        action.clickAndHold().perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_PRIMARY_ACTIVE);
    }

    public Boolean secondaryButtonHoverColor(){
        Actions hover = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_SECONDARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        hover.moveToElement(element).perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_SECONDARY_HOVER);
    }

    public Boolean secondaryButtonActiveColor(){
        Actions action = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_SECONDARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        action.moveToElement(element);
        action.clickAndHold().perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_SECONDARY_ACTIVE);
    }

    public Boolean subtleButtonHoverColor(){
        Actions hover = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_SUBTLE_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        hover.moveToElement(element).perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_SUBTLE_HOVER);
    }

    public Boolean subtleButtonActiveColor(){
        Actions action = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_SUBTLE_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        action.moveToElement(element);
        action.clickAndHold().perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_SUBTLE_ACTIVE);
    }

    public Boolean successButtonHoverColor(){
        Actions hover = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_SUCCESS_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        hover.moveToElement(element).perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_SUCCESS_HOVER);
    }

    public Boolean successButtonActiveColor(){
        Actions action = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_SUCCESS_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        action.moveToElement(element);
        action.clickAndHold().perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_SUCCESS_ACTIVE);
    }

    public Boolean destroyButtonHoverColor(){
        Actions hover = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_DESTROY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        hover.moveToElement(element).perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_DESTROY_HOVER);
    }

    public Boolean destroyButtonActiveColor(){
        Actions action = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_DESTROY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        action.moveToElement(element);
        action.clickAndHold().perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_DESTROY_ACTIVE);
    }

    public Boolean lightButtonHoverColor(){
        Actions hover = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_PRIMARY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        hover.moveToElement(element).perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_LIGHT_HOVER);
    }

    public Boolean lightButtonActiveColor(){
        Actions action = new Actions(driver);
        WebElement element = page.getElementShadowRootByCSS(page.BUTTON_COLOR_PRIMARY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
        action.moveToElement(element);
        action.clickAndHold().perform();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element.getCssValue("background-color").contains(page.COLOR_LIGHT_ACTIVE);
    }

    public Boolean buttonTarget(){
        driver.findElement(By.cssSelector(page.BUTTON_TARGET_BLANK_CSS)).click();
        Boolean testFLag = null;
        if(driver instanceof FirefoxDriver){
            String parentWindow = driver.getWindowHandle();
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Set<String> windowGroup = driver.getWindowHandles();
            for(String handle: windowGroup) {
                if (!handle.equals(parentWindow)) {
                    driver.switchTo().window(handle);
                }
                if(driver.getCurrentUrl().equals("https://www.w3schools.com/html/html_links.asp")){
                    testFLag = true;
                } else {
                    driver.close();
                    driver.switchTo().window(parentWindow);
                    testFLag = false;
                }
            }
            return testFLag;
        } else {
            ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
            driver.switchTo().window(tabs.get(1));
            if(driver.getCurrentUrl().equals("https://www.w3schools.com/html/html_links.asp")){
                testFLag = true;
            } else {
                driver.close();
                driver.switchTo().window(tabs.get(0));
                testFLag = false;
            }
            return testFLag;
        }
    }

    public Boolean buttonTargetSelf(){
        Boolean testFlag;
        WebElement element = driver.findElement(By.cssSelector(page.BUTTON_TARGET_SELF_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driverWait.until(WebDriver -> ((JavascriptExecutor) WebDriver).executeScript(
                "return document.readyState").equals("complete"));
        if(driver.getCurrentUrl().equals("https://www.w3schools.com/html/html_links.asp")){
            testFlag = true;
        } else {
            testFlag = false;
        }
        driver.navigate().to(urlUnderTest);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return testFlag;
    }

    public Boolean buttonTargetDefault(){
        Boolean testFlag;
        WebElement element = driver.findElement(By.cssSelector(page.BUTTON_TARGET_DEFAULT_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driverWait.until(WebDriver -> ((JavascriptExecutor) WebDriver).executeScript(
                "return document.readyState").equals("complete"));
        if(driver.getCurrentUrl().equals("https://www.w3schools.com/html/html_links.asp")){
            testFlag = true;
        } else {
            testFlag = false;
        }
        driver.navigate().to(urlUnderTest);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        page.clickButtons();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return testFlag;
    }

    public Boolean buttonTargetDownload(){
        String fileName = "MyImage.html";
        driver.findElement(By.cssSelector(page.BUTTON_DOWNLOAD_CSS)).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean flag = false;
        File dir = new File(ImagePaths.currentDir);
        File[] dir_contents = dir.listFiles();
        for (int i = 0; i < dir_contents.length; i++) {
            if (dir_contents[i].getName().equals(fileName)) {
                flag = true;
                File deleteFile = new File(ImagePaths.currentDir + File.separator + fileName);
                deleteFile.delete();
            }
        }
        return flag;
    }

    public Boolean buttonRelProp(){
        return driver.findElement(By.cssSelector(page.BUTTON_REL_PROP_CSS)).getAttribute("rel").equals("nofollow");
    }
}