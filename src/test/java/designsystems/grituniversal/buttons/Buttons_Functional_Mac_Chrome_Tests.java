package designsystems.grituniversal.buttons;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Buttons_Functional_Mac_Chrome_Tests extends Buttons_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testButtonFunctionality() {
        SoftAssert softAssert = new SoftAssert();
        //softAssert.assertTrue(buttonTargetDownload(), "Button Target download initiates download"); Create Firefox profile for autoDownload
        softAssert.assertTrue(buttonRelProp(), "Button Relation Prop");
        softAssert.assertTrue(buttonTargetSelf(), "Button Target opens in existing tab");
        softAssert.assertTrue(buttonTargetDefault(), "Button Target opens in existing tab");
        softAssert.assertTrue(buttonTarget(), "Button Target opens in new tab");
        softAssert.assertAll();
    }
}