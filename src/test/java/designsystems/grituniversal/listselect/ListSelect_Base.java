package designsystems.grituniversal.listselect;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.GritColorsPage;
import designsystems.pages.universal.ListSelectPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class ListSelect_Base extends GritCore_Base {

    public ListSelectPage page;
    public String urlUnderTest = page.URL_LISTSELECT;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new ListSelectPage(driver, driverWait);
    }

    public boolean defaultOptionHeight(){
        page.clickListSelect3();
        page.clickSubmit();
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getListItemHeight(page.LIST_SELECT_1_CSS).equals("56");
    }

    public boolean activeColor(){
        return page.getBackgroundColor(page.LIST_SELECT_1_CSS).contains(GritColorsPage.RGB_BLUE_500);
    }

    public boolean inactiveColor(){
        page.clickListSelect3();
        page.clickSubmit();
        return page.getBackgroundColor(page.LIST_SELECT_1_CSS).contains(GritColorsPage.RGB_NEUTRAL_100);
    }

    public boolean activeTextColor(){
        page.clickListSelect1();
        page.clickSubmit();
        try { //sleep for animation
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getFontColor(page.LIST_SELECT_1_CSS).contains(GritColorsPage.RGB_NEUTRAL_100);
    }

    public boolean inactiveTextColor(){
        page.clickListSelect3();
        page.clickSubmit();
        return page.getFontColor(page.LIST_SELECT_1_CSS).contains(GritColorsPage.RGB_NEUTRAL_900);
    }

    public boolean labelFont(){
        return page.getLabelFont(page.LIST_SELECT_1_CSS).equals("Montserrat, sans-serif");
    }

    public boolean contentFont(){
        page.clickListSelect1();
        page.clickSubmit();
        return page.getDescriptionFont(page.LIST_SELECT_1_CSS).equals("Lato, sans-serif");
    }

    public boolean selectedUpdates(){
        page.clickListSelect3();
        page.clickSubmit();
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getSelectedOutput().equals("You've selected option: 3");
    }

    public boolean selectedIsActive(){
        page.clickListSelect1();
        page.clickSubmit();
        try { //sleep for animation
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getBackgroundColor(page.LIST_SELECT_1_CSS).contains(GritColorsPage.RGB_BLUE_500);
    }
}