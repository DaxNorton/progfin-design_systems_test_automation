package designsystems.grituniversal.listselect;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class ListSelect_Functional_Windows_Edge_Tests extends ListSelect_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsEdge(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testListSelectFunction(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(selectedUpdates(), "Option selected updates form");
        softAssert.assertTrue(selectedIsActive(), "Option selected becomes active");
        softAssert.assertAll();
    }
}