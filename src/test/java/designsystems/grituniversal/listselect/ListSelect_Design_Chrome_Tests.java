package designsystems.grituniversal.listselect;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class ListSelect_Design_Chrome_Tests extends ListSelect_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testListSelectDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(activeColor(), "Active color is Blue500");
        softAssert.assertTrue(defaultOptionHeight(), "Default option height");
        softAssert.assertTrue(inactiveColor(), "Inactive color is neutral100");
        softAssert.assertTrue(activeTextColor(), "Active Text Color is neutral100");
        softAssert.assertTrue(inactiveTextColor(), "Inactive Text Color is neutral100");
        softAssert.assertTrue(labelFont(), "Label font is montserrat");
        softAssert.assertTrue(contentFont(), "Content font is Lato");
        softAssert.assertAll();
    }
}