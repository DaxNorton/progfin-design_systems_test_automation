package designsystems.grituniversal.angular.input;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.AngularInputPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Angular_Inputs_Base extends GritCore_Base {

    public AngularInputPage page;
    public String urlUnderTest = page.URL_ANGULAR_INPUTS;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        page = new AngularInputPage(driver, driverWait);
    }

    public boolean isSmallHeightDimensionCorrect(){
        Integer height = page.getInputFieldHeight().height;
        return height.equals(48);
    }

    public boolean smallFieldLabelFont(){
        return page.getInputLabelFont().equals("Montserrat, sans-serif");
    }
}
