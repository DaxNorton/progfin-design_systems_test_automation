package designsystems.grituniversal.topbar;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.TopNavBarPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class TopNavBar_Base extends GritCore_Base {

    public TopNavBarPage page;
    public String urlUnderTest = page.urlUniversalHome;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new TopNavBarPage(driver, driverWait);
    }

    public boolean topBarLooseHeight(){
        Integer height = driver.findElement(By.cssSelector(page.TOP_BAR_DEMO_CSS)).getSize().height;
        return height.equals(page.TOP_BAR_HEIGHT);
    }

    public boolean topBarFixedHeight(){
        driver.findElement(By.id(page.PARAM_FIX_TOP_BAR_CSS)).click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Integer height = driver.findElement(By.cssSelector(page.TOP_BAR_DEMO_CSS)).getSize().height;
        return height.equals(page.TOP_BAR_HEIGHT);
    }
}