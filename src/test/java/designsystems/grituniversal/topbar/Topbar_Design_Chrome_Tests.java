package designsystems.grituniversal.topbar;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;

public class Topbar_Design_Chrome_Tests extends TopNavBar_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        page.clickTopNavBar();
        Thread.sleep(2500);
    }

    @Test
    public void topNavBarDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(topBarLooseHeight(), "Top Bar Height when secured in place holder.");
        softAssert.assertTrue(topBarFixedHeight(), "Top Bar Height when fixed at top of page.");
        softAssert.assertAll();
    }

}
