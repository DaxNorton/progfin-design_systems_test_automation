package designsystems.grituniversal.checkboxes;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Checkboxes_Design_Mac_Chrome_Tests extends Checkboxes_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testCheckboxDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(checkboxIdleBorder(), "Idle border color");
        softAssert.assertTrue(checkboxActiveBorder(), "Active border color");
        softAssert.assertTrue(disabledCheckboxBorder(), "Disabled border color");
        softAssert.assertTrue(checkBoxSize(), "Checkbox size");
        softAssert.assertAll();
    }
}
