package designsystems.grituniversal.checkboxes;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.CheckboxesPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Checkboxes_Base extends GritCore_Base {

    public CheckboxesPage page;
    public String urlUnderTest = page.URL_CHECKBOXES;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new CheckboxesPage(driver, driverWait);
    }

    public boolean checkboxIdleBorder(){
        String test = page.getCheckboxBorder();
        return page.getCheckboxBorder().contains(page.COLOR_CHECKBOX_IDLE);
    }

    public boolean checkboxActiveBorder(){
        driver.findElement(By.cssSelector(page.CHECKBOX_ACTIVE_CSS)).click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return page.getCheckboxBorder().contains(page.COLOR_CHECKBOX_ACTIVE);
    }

    public boolean disabledCheckboxBorder(){
        return page.getDisabledCheckboxBorder().contains(page.COLOR_CHECKBOX_DISABLED);
    }

    public boolean checkBoxSize(){
        return page.getCheckboxSize().toString().equals("(16, 16)");
    }
}
