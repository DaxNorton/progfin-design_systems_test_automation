package designsystems.grituniversal.notifications;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.NotificationsPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Notifications_Base extends GritCore_Base {

    public NotificationsPage page;
    public String urlUnderTest = page.urlUniversalHome;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new NotificationsPage(driver, driverWait);
    }

}
