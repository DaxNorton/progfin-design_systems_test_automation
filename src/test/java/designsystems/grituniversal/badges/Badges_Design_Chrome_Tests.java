package designsystems.grituniversal.badges;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Badges_Design_Chrome_Tests extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testBadgesDesign() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(badgeDefaultColor(), "Badge Default Color");
        softAssert.assertTrue(badgeInfoColor(), "Badge Info Color");
        softAssert.assertTrue(badgePrimaryColor(), "Badge Primary Color");
        softAssert.assertTrue(badgeSuccessColor(), "Badge Success Color");
        softAssert.assertTrue(badgeAttentionColor(), "Badge Attention Color");
        softAssert.assertTrue(badgeAttentionStrongColor(), "Badge Attention Strong Color");
        softAssert.assertTrue(badgeWarningColor(), "Badge Warning Color");
        softAssert.assertTrue(badgeHeight(), "Badge height");
        softAssert.assertAll();
    }
}
