package designsystems.grituniversal.badges;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.BadgesPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Badges_Base extends GritCore_Base {

    public BadgesPage page;
    public String urlUnderTest = page.URL_UNDER_TEST;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new BadgesPage(driver, driverWait);
    }

    public boolean badgeDefaultColor(){
        return page.getColor(page.BADGE_ICON_DEFAULT).contains(page.COLOR_DEFAULT);
    }

    public boolean badgeInfoColor(){
        return page.getColor(page.BADGE_ICON_INFO_CSS).contains(page.COLOR_INFO);
    }

    public boolean badgePrimaryColor(){
        return page.getColor(page.BADGE_ICON_PRIMARY_CSS).contains(page.COLOR_PRIMARY);
    }

    public boolean badgeSuccessColor(){
        return page.getColor(page.BADGE_ICON_SUCCESS_CSS).contains(page.COLOR_SUCCESS);
    }

    public boolean badgeAttentionColor(){
        return page.getColor(page.BADGE_ICON_ATTENTION_CSS).contains(page.COLOR_ATTENTION);
    }

    public boolean badgeAttentionStrongColor(){
        return page.getColor(page.BADGE_ICON_ATTENTION_STRONG_CSS).contains(page.COLOR_ATTENTION_STRONG);
    }

    public boolean badgeWarningColor(){
        return page.getColor(page.BADGE_ICON_WARNING_CSS).contains(page.COLOR_WARNING);
    }

    public boolean badgeHeight(){
         String height = String.valueOf(page.getSize(page.BADGE_DEFAULT).getHeight());
         return height.equals("20");
    }
}
