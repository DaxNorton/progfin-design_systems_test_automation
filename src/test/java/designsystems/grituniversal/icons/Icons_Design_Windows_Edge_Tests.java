package designsystems.grituniversal.icons;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Icons_Design_Windows_Edge_Tests extends Icons_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsEdge(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testIconDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(defaultIconSize(), "Default Icon Size");
        softAssert.assertTrue(defaultIconBorderedSize(), "Default Icon Bordered Size");
        softAssert.assertTrue(size10IconSize(), "Size 10");
        softAssert.assertTrue(size10BorderedIconSize(), "Size 10 Bordered");
        softAssert.assertTrue(size20IconSize(), "Size 20");
        softAssert.assertTrue(size20BorderedIconSize(), "Size 20 Bordered");
        softAssert.assertTrue(size44IconSize(), "Size 44");
        softAssert.assertTrue(size44BorderedIconSize(), "Size 44 Bordered");
        softAssert.assertTrue(size80IconSize(), "Size 80");
        softAssert.assertTrue(size80BorderedIconSize(), "Size 80 Bordered");
        softAssert.assertTrue(coloredIconsNeutral(), "Neutral Icon Color");
        softAssert.assertTrue(coloredIconsPurple(), "Purple Icon Color");
        softAssert.assertTrue(coloredIconsBlue(), "Blue Icon Color");
        softAssert.assertTrue(coloredIconsGreen(), "Green Icon Color");
        softAssert.assertTrue(coloredIconsYellow(), "Yellow Icon Color");
        softAssert.assertTrue(coloredIconsOrange(), "Orange Icon Color");
        softAssert.assertTrue(coloredIconsRed(), "Red Icon Color");
        softAssert.assertTrue(coloredIconBorderedNeutral(), "Neutral Icon Border Color");
        softAssert.assertTrue(coloredIconBorderedPurple(), "Purple Icon Border Color");
        softAssert.assertTrue(coloredIconBorderedBlue(), "Blue Icon Border Color");
        softAssert.assertTrue(coloredIconBorderedGreen(), "Green Icon Border Color");
        softAssert.assertTrue(coloredIconBorderedYellow(), "Yellow Icon Border Color");
        softAssert.assertTrue(coloredIconBorderedOrange(), "Orange Icon Border Color");
        softAssert.assertTrue(coloredIconBorderedRed(), "Red Icon Border Color");
        softAssert.assertAll();
    }
}