package designsystems.grituniversal.icons;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.GritColorsPage;
import designsystems.pages.universal.IconsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Icons_Base extends GritCore_Base {

    public IconsPage page;
    public String urlUnderTest = page.URL_ICONS;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new IconsPage(driver, driverWait);
    }

    public boolean defaultIconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_ALARM_CSS)).getSize()).equals("(24, 24)");
    }

    public boolean defaultIconBorderedSize(){
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_ALARM_BLACK_BORDERED_CSS)).getSize())
                .equals("(32, 32)");
    }

    public boolean size10IconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_10_CSS)).getSize()).equals("(10, 10)");
    }

    public boolean size10BorderedIconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_10_BORDER_CSS)).getSize()).equals("(32, 32)");
    }

    public boolean size20IconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_20_CSS)).getSize()).equals("(24, 24)");
    }

    public boolean size20BorderedIconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_20_BORDER_CSS)).getSize()).equals("(32, 32)");
    }

    public boolean size44IconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_44_CSS)).getSize()).equals("(44, 44)");
    }

    public boolean size44BorderedIconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_44_BORDER_CSS)).getSize()).equals("(64, 64)");
    }

    public boolean size80IconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_80_CSS)).getSize()).equals("(80, 80)");
    }

    public boolean size80BorderedIconSize() {
        return String.valueOf(driver.findElement(By.cssSelector(page.ICON_SIZE_80_BORDER_CSS)).getSize()).equals("(121, 122)");
    }

    public boolean coloredIconsNeutral(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_BLACK_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_NEUTRAL_900);
    }

    public boolean coloredIconsPurple(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_PURPLE_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_PURPLE_600);
    }

    public boolean coloredIconsBlue(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_BLUE_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_BLUE_600);
    }

    public boolean coloredIconsGreen(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_GREEN_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_GREEN_600);
    }

    public boolean coloredIconsYellow(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_YELLOW_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_YELLOW_500);
    }

    public boolean coloredIconsOrange(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_ORANGE_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_ORANGE_500);
    }

    public boolean coloredIconsRed(){
        return driver.findElement(By.cssSelector(page.ICON_ALARM_RED_CSS)).getCssValue("color")
                .contains(GritColorsPage.RGB_RED_500);
    }

    public boolean coloredIconBorderedNeutral(){
        return page.getBorderColor(page.ICON_ALARM_BLACK_BORDERED_CSS).contains(GritColorsPage.RGB_NEUTRAL_400);
    }

    public boolean coloredIconBorderedPurple(){
        return page.getBorderColor(page.ICON_ALARM_PURPLE_BORDERED_CSS).contains(GritColorsPage.RGB_PURPLE_200);
    }

    public boolean coloredIconBorderedBlue(){
        return page.getBorderColor(page.ICON_ALARM_BLUE_BORDERED_CSS).contains(GritColorsPage.RGB_BLUE_200);
    }

    public boolean coloredIconBorderedGreen(){
        return page.getBorderColor(page.ICON_ALARM_GREEN_BORDERED_CSS).contains(GritColorsPage.RGB_GREEN_200);
    }

    public boolean coloredIconBorderedYellow(){
        return page.getBorderColor(page.ICON_ALARM_YELLOW_BORDERED_CSS).contains(GritColorsPage.RGB_YELLOW_200);
    }

    public boolean coloredIconBorderedOrange(){
        return page.getBorderColor(page.ICON_ALARM_ORANGE_BORDERED_CSS).contains(GritColorsPage.RGB_ORANGE_200);
    }

    public boolean coloredIconBorderedRed(){
        return page.getBorderColor(page.ICON_ALARM_RED_BORDERED_CSS).contains(GritColorsPage.RGB_RED_200);
    }

}