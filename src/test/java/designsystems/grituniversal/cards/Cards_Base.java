package designsystems.grituniversal.cards;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.CardsPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Cards_Base extends GritCore_Base {

    public CardsPage page;
    public String urlUnderTest = page.URL_CALENDAR;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new CardsPage(driver, driverWait);
    }

    public boolean cardShadowColor(){
        return page.getBoxShadow(page.CARD_ELEVATION_5_CSS).contains("33, 43, 54");
    }

    public boolean cardElevation0(){
        return page.getBoxShadow(page.CARD_ELEVATION_0_CSS).contains("0px 0px");
    }

    public boolean cardElevation1(){
        return page.getBoxShadow(page.CARD_ELEVATION_1_CSS).contains("1px 1px");
    }

    public boolean cardElevation2(){
        return page.getBoxShadow(page.CARD_ELEVATION_2_CSS).contains("3px 4px");
    }

    public boolean cardElevation3(){
        return page.getBoxShadow(page.CARD_ELEVATION_3_CSS).contains("8px 10px");
    }

    public boolean cardElevation4(){
        return page.getBoxShadow(page.CARD_ELEVATION_4_CSS).contains("16px 16px");
    }

    public boolean cardElevation5(){
        return page.getBoxShadow(page.CARD_ELEVATION_5_CSS).contains("24px 24px");
    }

    public boolean cardHeight(){
        return page.getCardHeight().equals("56");
    }
}
