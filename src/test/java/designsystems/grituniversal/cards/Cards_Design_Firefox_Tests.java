package designsystems.grituniversal.cards;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Cards_Design_Firefox_Tests extends Cards_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localFirefoxHeadless();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void testCardsDesign(){
        SoftAssert softAssert = new SoftAssert();
        //softAssert.assertTrue(cardShadowColor()); //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardElevation0());  //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardElevation1());  //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardElevation2());  //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardElevation3());  //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardElevation4());  //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardElevation5());  //Firefox ShadowDOM bug
        //softAssert.assertTrue(cardHeight());  //Firefox ShadowDOM bug
        softAssert.assertAll();
    }

}