package designsystems.grituniversal.cards;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Cards_Design_Windows_Chrome_Tests extends Cards_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceWindowsChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
    }

    @Test
    public void testCardsDesign(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(cardShadowColor());
        softAssert.assertTrue(cardElevation0());
        softAssert.assertTrue(cardElevation1());
        softAssert.assertTrue(cardElevation2());
        softAssert.assertTrue(cardElevation3());
        softAssert.assertTrue(cardElevation4());
        softAssert.assertTrue(cardElevation5());
        softAssert.assertTrue(cardHeight());
        softAssert.assertAll();
    }

}