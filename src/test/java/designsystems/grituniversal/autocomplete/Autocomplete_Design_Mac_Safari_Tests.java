package designsystems.grituniversal.autocomplete;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Autocomplete_Design_Mac_Safari_Tests extends AutoComplete_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacSafari(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
    }

    @Test
    public void testAutocompleteDesign(){
        SoftAssert softAssert = new SoftAssert();
        //softAssert.assertTrue(autocompleteHeight(), "Height");
        softAssert.assertTrue(autocompleteIdleBorderColor(), "Idle Border Color");
        softAssert.assertTrue(autocompleteActiveBorderColor(), "Active Border Color");
        softAssert.assertTrue(autocompleteFont(), "Font");
        softAssert.assertTrue(autocompleteDisabledBorderColor(), "Disabled Border Color");
        softAssert.assertAll();
    }
}