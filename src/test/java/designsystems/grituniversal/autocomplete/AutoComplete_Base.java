package designsystems.grituniversal.autocomplete;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.AutoCompletePage;
import org.openqa.selenium.*;

import java.lang.reflect.Method;

public class AutoComplete_Base extends GritCore_Base {

    public AutoCompletePage page;
    public String urlUnderTest = page.URL_UNDER_TEST;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new AutoCompletePage(driver, driverWait);
    }

    public boolean keyboardAccessibilityDownArrow() {
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS));
        element.click();
        element.sendKeys(Keys.DOWN, Keys.DOWN, Keys.DOWN, Keys.ENTER);
        return element.getAttribute("value").equals("Item 3");
    }

    public boolean keyboardAccessibilityUpArrow(){
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS));
        element.click();
        element.sendKeys(Keys.DOWN, Keys.DOWN, Keys.UP, Keys.ENTER);
        return element.getAttribute("value").equals("Item 1");
    }

    public boolean keyboardAccessibilityUserInput(){
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS));
        element.click();
        element.sendKeys("1");
        element.sendKeys(Keys.DOWN, Keys.ENTER);
        return element.getAttribute("value").equals("Item 1");
    }

    public boolean userInputEmptyResultList(){
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS));
        element.click();
        element.sendKeys("asd");
        element.sendKeys(Keys.DOWN, Keys.ENTER);
        String test = element.getAttribute("value");
        if(test==null){
            return true;
        } else {
            return false;
        }
    }

    public boolean autocompleteListHiddenByDefault(){
        if (page.isDisplayedAutocompleteNumberList() == false){
            return true;
        } else {
            return false;
        }
    }

    public boolean autocompleteFullMatch(){
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CASE_MATCH_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
        element.sendKeys("Item");
        element.sendKeys(Keys.DOWN, Keys.ENTER);
        return element.getAttribute("value").equals("Item 1");
    }

    public boolean autocompleteFullMatchNegative(){
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CASE_MATCH_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
        element.sendKeys("item");
        element.sendKeys(Keys.DOWN, Keys.ENTER);
        String test = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CASE_MATCH_CSS)).getAttribute("value");
        if(test==null){
            return true;
        } else {
            return false;
        }
    }

    public boolean autocompleteActiveBorderColor(){
        WebElement element = driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS));
        element.click();
        return element.getCssValue("border-top-color").contains("0, 106, 255");
    }

    public boolean autocompleteIdleBorderColor(){
        return driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS)).getCssValue("border-top-color").contains("69, 79, 91");
    }

    public boolean autocompleteFont(){
        WebElement shadowRoot1 = page.getElementShadowRootByCSS(page.AUTOCOMPLETE_CSS);
        return shadowRoot1.findElement(By.cssSelector(page.AUTOCOMPLETE_INPUT_CSS)).getCssValue("font-family").equals("Lato, sans-serif");
    }

    public boolean autocompleteHeight(){
        String height = String.valueOf(driver.findElement(By.cssSelector(page.AUTOCOMPLETE_CSS)).getSize().height);
        return height.equals("50");
    }

    public boolean autocompleteDisabledBorderColor(){
        WebElement shadowRoot1 = page.getElementShadowRootByCSS(page.AUTOCOMPLETE_DISABLED_CSS);
        return shadowRoot1.findElement(By.cssSelector(page.AUTOCOMPLETE_LABEL_CSS)).getCssValue("border-top-color").contains("145, 158, 171");
    }

    public boolean autocompleteDefaultVisual(String baselineFile, String actualFile){
        return page.imageUtility.GetElementImage_Compare(page.getDefaultView(), baselineFile,
                actualFile);
    }

    public void setAutocompleteDefaultVisual(String baselineFIle){
        page.imageUtility.GetElementImage(page.getDefaultView(), baselineFIle);
    }

}