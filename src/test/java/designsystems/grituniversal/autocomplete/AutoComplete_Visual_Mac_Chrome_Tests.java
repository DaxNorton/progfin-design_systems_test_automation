package designsystems.grituniversal.autocomplete;

import designsystems.pages.imagepaths.SauceMacChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class AutoComplete_Visual_Mac_Chrome_Tests extends AutoComplete_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
    }

    @Test
    public void autocompleteDefaultVisual(){
        Assert.assertTrue(autocompleteDefaultVisual(SauceMacChrome_Paths.BASELINE_UNIVERSAL_AUTOCOMPLETE_DEFAULT,
                SauceMacChrome_Paths.ACTUAL_UNIVERSAL_AUTOCOMPLETE_DEFAULT));
    }
}
