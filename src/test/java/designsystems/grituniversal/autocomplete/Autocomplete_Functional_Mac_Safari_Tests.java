package designsystems.grituniversal.autocomplete;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Autocomplete_Functional_Mac_Safari_Tests extends AutoComplete_Base{

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacSafari(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);
    }

    @Test
    public void testAutocompleteFunctionA(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(autocompleteListHiddenByDefault(), "List hidden by default");
        softAssert.assertTrue(keyboardAccessibilityDownArrow(), "Keyboard accessible down-arrow");
        softAssert.assertTrue(autocompleteFullMatch(), "Full Match Case Test");
        softAssert.assertAll();
    }

    @Test
    public void testAutocompleteFunctionB(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(keyboardAccessibilityUpArrow(), "Keyboard accessible up-arrow");
        softAssert.assertTrue(autocompleteFullMatchNegative(), "Full Match Case Negative Test");
        softAssert.assertAll();
    }

    @Test
    public void testAutocompleteFunctionC(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(keyboardAccessibilityUserInput(), "User input and then keyboard accessible selection");
        softAssert.assertAll();
    }

    @Test
    public void testAutocompleteFunctionD(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(userInputEmptyResultList(), "User Input does not match, empty list");
        softAssert.assertAll();
    }
}