package designsystems.gritdocsite;

import designsystems.utilities.drivers.local.LocalChromeHeadless;
import designsystems.utilities.drivers.local.LocalFirefoxHeadless;
import designsystems.utilities.saucelabs.drivers.desktop.BaseDriverDesktop;
import designsystems.utilities.saucelabs.drivers.mobileemulator.BaseDriverMobile;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import java.lang.reflect.Method;

public class GritDoc_Base {

    public WebDriver driver;
    public WebDriverWait driverWait;
    public final String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();


    private LocalChromeHeadless driver1;
    public WebDriver localChromeDriver(){
        driver1 = new LocalChromeHeadless();
        return ((LocalChromeHeadless) driver1).getPreparedDriver();
    }

    private BaseDriverDesktop driver2;
    public WebDriver SauceMacChrome(String nameOfCallingTest){
        driver2 = new designsystems.utilities.saucelabs.drivers.desktop.MacChrome();
        driver2.setTestNameIn(nameOfCallingTest);
        driver2.setRecordVideo(true);
        driver2.setVideoUploadOnPass(true);
        driver2.setRecordScreenshots(true);
        driver2.setUseBuild(false);
        return ((designsystems.utilities.saucelabs.drivers.desktop.MacChrome) driver2).getPreparedDriver();
    }

    private BaseDriverMobile driver3;
    public WebDriver SauceIPhoneXR(String nameOfCallingTest){
        driver3 = new designsystems.utilities.saucelabs.drivers.mobileemulator.iPhoneXRSim();
        driver3.setTestNameIn(nameOfCallingTest);
        driver3.setRecordVideo(true);
        driver3.setVideoUploadOnPass(true);
        driver3.setRecordScreenshots(true);
        driver3.setUseBuild(false);
        return ((designsystems.utilities.saucelabs.drivers.mobileemulator.iPhoneXRSim) driver3).getPreparedDriver();
    }

    @AfterMethod
    public void afterMethod(ITestResult result) throws InterruptedException {
        StringBuilder messageTemp = new StringBuilder("\ndriver quit....");
        try {
            ((JavascriptExecutor) driver).executeScript(
                    "sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
            messageTemp.insert(1, "Sauce ");
        }catch(WebDriverException wDE){
            /*
             * Local drivers will cause a WebDriverException here, and it's OK.
             */
            messageTemp.insert(1, "Local ");
        }
        if(!(null==driver)) {
            driver.quit();
            System.out.println(messageTemp);
        }
        messageTemp.delete(0, messageTemp.length());
    }

    public void setupDriverWaitPage(Method method, WebDriver driver, String urlUnderTest){
        SessionId session = ((RemoteWebDriver)driver).getSessionId();
        if(null==session) {
            throw new IllegalStateException(method.getName() + " - FAIL:\t" + "driver instantiation failure");
        }else {
            System.out.println(method.getName() + " - PASS:\t" + "driver instantiated... (OK)");
        }
        driverWait = new WebDriverWait(driver, 3);
        driver.navigate().to(urlUnderTest);
        System.out.println("Testing: " + urlUnderTest);
        driverWait.until(WebDriver -> ((JavascriptExecutor) WebDriver).executeScript(
                "return document.readyState").equals("complete"));
        try{
            Thread.sleep(500);
        }catch (InterruptedException e){
            //deliberate swallow
        }
        ((JavascriptExecutor) driver)
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
        try{
            Thread.sleep(500);
        }catch (InterruptedException e){
            //deliberate swallow
        }
        ((JavascriptExecutor) driver)
                .executeScript("window.scrollTo(0, 0)");
    }
}
