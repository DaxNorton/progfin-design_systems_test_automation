package designsystems.gritdocsite.roadmap;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Chrome_QuickTests extends Roadmap_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Roadmap_Local_Chrome_Quick(){
        Assert.assertTrue(page.Roadmap_Quick(LocalChrome_Paths.BASELINE_ROADMAP, LocalChrome_Paths.ACTUAL_ROADMAP));
    }

}