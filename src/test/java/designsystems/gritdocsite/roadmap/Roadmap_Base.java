package designsystems.gritdocsite.roadmap;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.RoadmapPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Roadmap_Base extends GritDoc_Base {

    public RoadmapPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver) {
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new RoadmapPage(driver, driverWait);
    }
}