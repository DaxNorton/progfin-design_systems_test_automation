package designsystems.gritdocsite.foundations.elevation;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Elevation_Chrome_Tests extends Elevation_Base {

    @BeforeMethod
    public void BeforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Foundations_Elevation_Local_Chrome_Quick(){
        Assert.assertTrue(page.Foundations_Elevation_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_ELEVATION,
                LocalChrome_Paths.ACTUAL_FOUNDATIONS_ELEVATION));
    }
}