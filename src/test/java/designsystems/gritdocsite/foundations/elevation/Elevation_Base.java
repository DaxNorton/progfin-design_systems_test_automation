package designsystems.gritdocsite.foundations.elevation;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.FoundationsElevationPage;
import org.openqa.selenium.WebDriver;
import java.lang.reflect.Method;

public class Elevation_Base extends GritDoc_Base {

    public FoundationsElevationPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new FoundationsElevationPage(driver, driverWait);
    }
}
