package designsystems.gritdocsite.foundations.main;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Chrome_Tests extends Foundations_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Foundations_Local_Chrome_Quick(){
        Assert.assertTrue(page.Foundations_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS,
                LocalChrome_Paths.ACTUAL_FOUNDATIONS));
    }
}