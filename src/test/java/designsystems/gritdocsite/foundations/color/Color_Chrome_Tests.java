package designsystems.gritdocsite.foundations.color;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Color_Chrome_Tests extends Color_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Foundations_Color_Local_Chrome_Quick(){
        Assert.assertTrue(page.Foundations_Color_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_COLOR,
                LocalChrome_Paths.ACTUAL_FOUNDATIONS_COLOR));
    }

}