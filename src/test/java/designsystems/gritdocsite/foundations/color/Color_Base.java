package designsystems.gritdocsite.foundations.color;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.FoundationsColorPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Color_Base extends GritDoc_Base {

    public FoundationsColorPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new FoundationsColorPage(driver, driverWait);
    }

}