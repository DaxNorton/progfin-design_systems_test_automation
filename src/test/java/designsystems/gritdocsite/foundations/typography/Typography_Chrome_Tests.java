package designsystems.gritdocsite.foundations.typography;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Typography_Chrome_Tests extends Typography_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Foundations_Typography_Local_Chrome_Quick(){
        Assert.assertTrue(page.Foundations_Typography_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_TYPOGRAPHY,
                LocalChrome_Paths.ACTUAL_FOUNDATIONS_TYPOGRAPHY));
    }

}