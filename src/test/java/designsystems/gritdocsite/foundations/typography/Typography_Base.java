package designsystems.gritdocsite.foundations.typography;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.FoundationsTypographyPage;
import org.openqa.selenium.WebDriver;
import java.lang.reflect.Method;

public class Typography_Base extends GritDoc_Base {

    public FoundationsTypographyPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new FoundationsTypographyPage(driver, driverWait);
    }

}