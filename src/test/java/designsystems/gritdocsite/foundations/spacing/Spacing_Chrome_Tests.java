package designsystems.gritdocsite.foundations.spacing;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Spacing_Chrome_Tests extends Spacing_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Foundations_Spacing_Local_Chrome_Quick(){
        Assert.assertTrue(page.Foundations_Spacing_Quick(LocalChrome_Paths.BASELINE_FOUNDATIONS_SPACING,
                LocalChrome_Paths.ACTUAL_FOUNDATIONS_SPACING));
    }
}