package designsystems.gritdocsite.homepage;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Chrome_QuickTests extends HomePage_Base {

    @BeforeMethod
    public void beforeMethod(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Home_View_Local_Chrome_Quick(){
        Assert.assertTrue(page.HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW, LocalChrome_Paths.ACTUAL_HOMEVIEW));
    }
}