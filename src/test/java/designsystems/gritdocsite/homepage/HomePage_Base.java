package designsystems.gritdocsite.homepage;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.HomePage;
import org.openqa.selenium.WebDriver;
import designsystems.utilities.drivers.local.LocalChromeHeadless;

import java.lang.reflect.Method;

public class HomePage_Base extends GritDoc_Base {

    public HomePage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new HomePage(driver, driverWait);
    }

    private LocalChromeHeadless mobileViewDriver;
    public WebDriver localChromeMobileView(){
        mobileViewDriver = new LocalChromeHeadless("mobile");
        return ((LocalChromeHeadless) mobileViewDriver).getPreparedDriver();
    }

    private LocalChromeHeadless tabletViewDriver;
    public WebDriver localChromeTabletView(){
        mobileViewDriver = new LocalChromeHeadless("tablet");
        return ((LocalChromeHeadless) mobileViewDriver).getPreparedDriver();
    }

}