package designsystems.gritdocsite.homepage;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class ViewportService_Chrome_Tests extends HomePage_Base{

    @Test
    public void test_HomePage_ViewportService_DesktopView_Local_Chrome(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW, LocalChrome_Paths.ACTUAL_HOMEVIEW));
    }

    @Test
    public void test_HomePage_ViewportService_TabletView_Local_Chrome(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW_TABLETVIEW,
                LocalChrome_Paths.ACTUAL_HOMEVIEW_TABLETVIEW));
    }

    @Test
    public void test_HomePage_ViewportService_MobileView_Local_Chrome(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.HomeView_Quick(LocalChrome_Paths.BASELINE_HOMEVIEW_MOBILEVIEW,
                LocalChrome_Paths.ACTUAL_HOMEVIEW_MOBILEVIEW));
    }

}
