package designsystems.gritdocsite.getstarted.main;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.GetStartedPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class GetStarted_Base extends GritDoc_Base {

    public GetStartedPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new GetStartedPage(driver, driverWait);
    }

}