package designsystems.gritdocsite.getstarted.main;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;


public class GetStarted_Chrome_Tests extends GetStarted_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_GetStarted_Hero_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Hero_Content(LocalChrome_Paths.BASELINE_GETSTARTED_HERO_WRAPPER,
                LocalChrome_Paths.ACTUAL_GETSTARTED_HERO_CONTENT));
    }

    @Test
    public void test_GetStarted_Home_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Home_Content(LocalChrome_Paths.BASELINE_GETSTARTED_HOME_CONTENT,
                LocalChrome_Paths.ACTUAL_GETSTARTED_HOME_CONTENT));
    }

}