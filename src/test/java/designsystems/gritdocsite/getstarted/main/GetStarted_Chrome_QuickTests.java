package designsystems.gritdocsite.getstarted.main;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;


public class GetStarted_Chrome_QuickTests extends GetStarted_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_GetStarted_Desktop_Local_Chrome_Quick(){
        Assert.assertTrue(page.GetStarted_Quick(LocalChrome_Paths.BASELINE_GETSTARTED, LocalChrome_Paths.ACTUAL_GETSTARTED));
    }

}