package designsystems.gritdocsite.getstarted.designer;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.GetStartedDesignerPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class GetStarted_Designer_Base extends GritDoc_Base {

    public GetStartedDesignerPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new GetStartedDesignerPage(driver, driverWait);
    }

}