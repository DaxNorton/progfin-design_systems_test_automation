package designsystems.gritdocsite.getstarted.designer;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Designer_Chrome_QuickTests extends GetStarted_Designer_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void GetStarted_Designer_Quick_Test_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Quick(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER));
    }

}