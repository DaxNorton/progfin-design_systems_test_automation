package designsystems.gritdocsite.getstarted.designer;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class GetStarted_Designer_Chrome_Test extends GetStarted_Designer_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_GetStarted_Designer_Hero_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Hero_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_HERO,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_HERO));
    }

    @Test
    public void test_GetStarted_Designer_Section1_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section1_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_1,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_1));
    }

    @Test
    public void test_GetStarted_Designer_Section2_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section2_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_2,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_2));
    }

    @Test
    public void test_GetStarted_Designer_Section3_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section3_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_3,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_3));
    }

    @Test
    public void test_GetStarted_Designer_Section4_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section4_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_4,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_4));
    }

    @Test
    public void test_GetStarted_Designer_Section5_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section5_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_5,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_5));
    }

    @Test
    public void test_GetStarted_Designer_Section6_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section6_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_6,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_6));
    }

    @Test
    public void test_GetStarted_Designer_Section7_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Designer_Section7_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DESIGNER_SECTION_7,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DESIGNER_SECTION_7));
    }
}
