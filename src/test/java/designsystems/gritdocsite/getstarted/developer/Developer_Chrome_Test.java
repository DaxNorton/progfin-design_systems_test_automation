package designsystems.gritdocsite.getstarted.developer;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Developer_Chrome_Test extends GetStarted_Developer_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_GetStarted_Developer_Hero_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Hero_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_HERO,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_HERO));
    }

    @Test
    public void test_GetStarted_Developer_Section1_Content_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Section1_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_1,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_SECTION_1));
    }

    @Test
    public void test_GetStarted_Developer_Section2_Content_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Section2_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_2,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_SECTION_2));
    }

    @Test
    public void test_GetStarted_Developer_Section3_Content_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Section3_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_3,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_SECTION_3));
    }

    @Test
    public void test_GetStarted_Developer_Section4_Content_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Section4_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_4,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_SECTION_4));
    }

    @Test
    public void test_GetStarted_Developer_Section5_Content_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Section5_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_5,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_SECTION_5));
    }

    @Test
    public void test_GetStarted_Developer_Section6_Content_Local_Chrome(){
        Assert.assertTrue(page.GetStarted_Developer_Section6_Content(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER_SECTION_6,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER_SECTION_6));
    }
}