package designsystems.gritdocsite.getstarted.developer;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Developer_Chrome_QuickTests extends GetStarted_Developer_Base {


    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_GetStarted_Developer_Local_Chrome_Quick(){
        Assert.assertTrue(page.GetStarted_Developer_Quick(LocalChrome_Paths.BASELINE_GETSTARTED_DEVELOPER,
                LocalChrome_Paths.ACTUAL_GETSTARTED_DEVELOPER));
    }

}