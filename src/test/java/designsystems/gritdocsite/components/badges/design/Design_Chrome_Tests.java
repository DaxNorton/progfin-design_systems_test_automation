package designsystems.gritdocsite.components.badges.design;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.testng.AutomationGroups;
import java.lang.reflect.Method;

public class Design_Chrome_Tests extends Badges_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(groups = {AutomationGroups.DESIGNSYSTEMS, AutomationGroups.DS_DOCS, AutomationGroups.TIER_1, AutomationGroups.TIER_2}, description = "Page Quick Test")
    public void test_Components_Badges_Design_Local_Chrome_Quick() {
        Assert.assertTrue(page.Components_Badges_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_BADGES_DESIGN,
                LocalChrome_Paths.ACTUAL_COMPONENTS_BADGES_DESIGN));
    }
}