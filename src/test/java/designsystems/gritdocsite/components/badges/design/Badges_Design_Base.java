package designsystems.gritdocsite.components.badges.design;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsBadgesDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Badges_Design_Base extends GritDoc_Base {

    public ComponentsBadgesDesignPage page;
    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/badge/design";

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ComponentsBadgesDesignPage(driver, driverWait);
    }
}