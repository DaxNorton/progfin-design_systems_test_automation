package designsystems.gritdocsite.components.badges.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.testng.AutomationGroups;
import java.io.IOException;
import java.lang.reflect.Method;

public class Code_Chrome_Tests extends Badges_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(enabled=true, groups = {AutomationGroups.TANC}, description="Page Quick Test")
    public void test_Components_Badges_Code_Local_Chrome_Quick() throws IOException, InterruptedException {
        Assert.assertTrue(page.Components_Badges_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_BADGES_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_BADGES_CODE));
    }

}