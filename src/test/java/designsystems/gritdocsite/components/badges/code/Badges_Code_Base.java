package designsystems.gritdocsite.components.badges.code;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsBadgesCodePage;
import org.openqa.selenium.WebDriver;
import java.lang.reflect.Method;

public class Badges_Code_Base extends GritDoc_Base {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/badge/code";

    public ComponentsBadgesCodePage page;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPageWithDriver(driver);
    }

    public void setPageWithDriver(WebDriver driver){
        page = new ComponentsBadgesCodePage(driver, driverWait);
    }

}