package designsystems.gritdocsite.components.checkboxes.design;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;

import java.lang.reflect.Method;

public class Checkboxes_Design_Chrome_Tests extends Checkboxes_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void Components_Checkboxes_Design_Quick_Test_Local_Chrome(){
        Assert.assertTrue(page.Components_Checkboxes_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_CHECKBOXES_DESIGN,
                LocalChrome_Paths.ACTUAL_COMPONENTS_CHECKBOXES_DESIGN));
    }

}