package designsystems.gritdocsite.components.checkboxes.design;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsCheckboxesDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Checkboxes_Design_Base extends GritDoc_Base {

    public ComponentsCheckboxesDesignPage page;
    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/checkbox/design";

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ComponentsCheckboxesDesignPage(driver, driverWait);
    }
}