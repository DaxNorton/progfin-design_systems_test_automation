package designsystems.gritdocsite.components.checkboxes.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Checkboxes_Code_Chrome_Tests extends Checkboxes_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void Components_Checkboxes_Code_Quick_Test_Local_Chrome(){
        Assert.assertTrue(page.Components_Checkboxes_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_CHECKBOXES_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_CHECKBOXES_CODE));
    }

}