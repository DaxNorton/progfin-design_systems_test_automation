package designsystems.gritdocsite.components.checkboxes.code;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsCheckboxesCodePage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Checkboxes_Code_Base extends GritDoc_Base {

    public ComponentsCheckboxesCodePage page;
    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/checkbox/code";

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ComponentsCheckboxesCodePage(driver, driverWait);
    }
}