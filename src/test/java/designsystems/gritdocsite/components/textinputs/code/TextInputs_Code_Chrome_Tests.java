package designsystems.gritdocsite.components.textinputs.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class TextInputs_Code_Chrome_Tests extends TextInputs_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_TextInputs_Code_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_TextInputs_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_TEXT_INPUTS_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_TEXT_INPUTS_CODE));
    }

}