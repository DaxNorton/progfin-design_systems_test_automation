package designsystems.gritdocsite.components.textinputs.code;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsTextInputsCodePage;
import org.openqa.selenium.WebDriver;
import java.lang.reflect.Method;

public class TextInputs_Code_Base extends GritDoc_Base {

    public ComponentsTextInputsCodePage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsTextInputsCodePage(driver, driverWait);
    }
}
