package designsystems.gritdocsite.components.textinputs.design;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsTextInputsDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class TextInputs_Design_Base extends GritDoc_Base {

    public ComponentsTextInputsDesignPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsTextInputsDesignPage(driver, driverWait);
    }
}