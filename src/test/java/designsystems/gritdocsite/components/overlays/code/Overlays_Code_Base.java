package designsystems.gritdocsite.components.overlays.code;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsOverlayCodePage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Overlays_Code_Base extends GritDoc_Base {

    public ComponentsOverlayCodePage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsOverlayCodePage(driver, driverWait);
    }

}
