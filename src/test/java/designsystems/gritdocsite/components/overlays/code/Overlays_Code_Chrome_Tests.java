package designsystems.gritdocsite.components.overlays.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Overlays_Code_Chrome_Tests extends Overlays_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Overlays_Code_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Overlays_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_OVERLAYS_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_OVERLAYS_CODE));
    }
}