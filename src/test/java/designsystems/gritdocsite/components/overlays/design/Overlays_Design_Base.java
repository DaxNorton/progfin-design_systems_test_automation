package designsystems.gritdocsite.components.overlays.design;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsOverlaysDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Overlays_Design_Base extends GritDoc_Base {

    public ComponentsOverlaysDesignPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsOverlaysDesignPage(driver, driverWait);
    }

}