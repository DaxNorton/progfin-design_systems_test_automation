package designsystems.gritdocsite.components.buttons.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.testng.AutomationGroups;

import java.lang.reflect.Method;

public class Buttons_Code_Chrome_Tests extends Buttons_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test(groups = {AutomationGroups.DESIGNSYSTEMS, AutomationGroups.DS_DOCS, AutomationGroups.TIER_1, AutomationGroups.TIER_2}, description="Page Quick Test")
    public void test_Components_Buttons_Code_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Buttons_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_BUTTONS_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_BUTTONS_CODE));
    }

}