package designsystems.gritdocsite.components.buttons.code;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsButtonsCodePage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Buttons_Code_Base extends GritDoc_Base {

    public ComponentsButtonsCodePage page;
    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/button/code";

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ComponentsButtonsCodePage(driver, driverWait);
    }
}
