package designsystems.gritdocsite.components.buttons.design;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsButtonsDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Buttons_Design_Base extends GritDoc_Base {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/button/design";
    public ComponentsButtonsDesignPage page;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ComponentsButtonsDesignPage(driver, driverWait);
    }
}