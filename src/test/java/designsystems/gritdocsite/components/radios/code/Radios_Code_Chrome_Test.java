package designsystems.gritdocsite.components.radios.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Radios_Code_Chrome_Test extends Radios_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Radios_Code_Hero_Content_Desktop_Local_Chrome(){
        Assert.assertTrue(page.Radios_Code_Hero_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_HERO,
                LocalChrome_Paths.ACTUAL_COMPONENTS_RADIOS_CODE_HERO));
    }

    @Test
    public void test_Radios_Code_Section1_Content_Local_Chrome(){
        Assert.assertTrue(page.Radios_Code_Section1_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_SECTION_1,
                LocalChrome_Paths.ACTUAL_COMPONENTS_RADIOS_CODE_SECTION_1));
    }

    @Test
    public void test_Radios_Code_Section2_Content_Local_Chrome(){
        Assert.assertTrue(page.Radios_Code_Section2_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_SECTION_2,
                LocalChrome_Paths.ACTUAL_COMPONENTS_RADIOS_CODE_SECTION_2));
    }

    @Test
    public void test_Radios_Code_Section3_Content_Local_Chrome(){
        Assert.assertTrue(page.Radios_Code_Section3_Content(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE_SECTION_3,
                LocalChrome_Paths.ACTUAL_COMPONENTS_RADIOS_CODE_SECTION_3));
    }

}