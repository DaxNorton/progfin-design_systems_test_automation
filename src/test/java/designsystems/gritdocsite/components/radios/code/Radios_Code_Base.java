package designsystems.gritdocsite.components.radios.code;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsRadiosCodePage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Radios_Code_Base extends GritDoc_Base {

    public ComponentsRadiosCodePage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsRadiosCodePage(driver, driverWait);
    }

}