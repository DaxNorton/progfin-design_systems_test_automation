package designsystems.gritdocsite.components.radios.code;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Radios_Cod_Chrome_QuickTests extends Radios_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Radios_Code_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Radios_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_RADIOS_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_RADIOS_CODE));
    }
}