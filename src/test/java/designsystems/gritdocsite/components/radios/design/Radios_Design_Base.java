package designsystems.gritdocsite.components.radios.design;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsRadiosDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Radios_Design_Base extends GritDoc_Base {

    public ComponentsRadiosDesignPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsRadiosDesignPage(driver, driverWait);
    }

}