package designsystems.gritdocsite.components.notifications.code;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsNotificationsCodePage;
import org.openqa.selenium.WebDriver;
import java.lang.reflect.Method;

public class Notifications_Code_Base extends GritDoc_Base {

    public ComponentsNotificationsCodePage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsNotificationsCodePage(driver, driverWait);
    }

}
