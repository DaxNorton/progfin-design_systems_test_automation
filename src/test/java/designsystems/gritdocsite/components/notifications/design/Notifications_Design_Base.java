package designsystems.gritdocsite.components.notifications.design;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsNotificationsDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Notifications_Design_Base extends GritDoc_Base {

    public ComponentsNotificationsDesignPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsNotificationsDesignPage(driver, driverWait);
    }

}
