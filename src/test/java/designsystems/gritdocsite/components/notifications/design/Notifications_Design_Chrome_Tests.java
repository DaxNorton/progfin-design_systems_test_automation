package designsystems.gritdocsite.components.notifications.design;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Notifications_Design_Chrome_Tests extends Notifications_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Notifications_Design_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Notifications_Design_Quick(
                LocalChrome_Paths.BASELINE_COMPONENTS_NOTIFICATIONS_DESIGN,
                LocalChrome_Paths.ACTUAL_COMPONENTS_NOTIFICATIONS_DESIGN));
    }

}