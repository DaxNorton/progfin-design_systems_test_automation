package designsystems.gritdocsite.components.loaders.code;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsLoadersCodePage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Loaders_Code_Base extends GritDoc_Base {

    public ComponentsLoadersCodePage page;
    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/loader/code";

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsLoadersCodePage(driver, driverWait);
    }
}