package designsystems.gritdocsite.components.loaders.design;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsLoadersDesignPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Loaders_Design_Base extends GritDoc_Base {

    public ComponentsLoadersDesignPage page;
    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/loader/design";

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver) {
        page = new ComponentsLoadersDesignPage(driver, driverWait);
    }
}