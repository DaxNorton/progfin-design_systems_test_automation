package designsystems.gritdocsite.components.main;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Doc_Base extends GritDoc_Base {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "components";

    public ComponentsPage page;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsPage(driver, driverWait);
    }

}
