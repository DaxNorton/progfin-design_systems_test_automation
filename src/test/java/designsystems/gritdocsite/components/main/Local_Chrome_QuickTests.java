package designsystems.gritdocsite.components.main;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Local_Chrome_QuickTests extends Doc_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Quick(LocalChrome_Paths.BASELINE_COMPONENTS,
                LocalChrome_Paths.ACTUAL_COMPONENTS));
    }

}