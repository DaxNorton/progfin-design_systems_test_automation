package designsystems.gritdocsite.components.main;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Local_Chrome_Tests extends Doc_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Home_Content_Local_Chrome(){
        Assert.assertTrue(page.Components_Home_Content(LocalChrome_Paths.BASELINE_COMPONENTS_HOME_CONTENT,
                LocalChrome_Paths.ACTUAL_COMPONENTS_HOME_CONTENT));
    }

    @Test
    public void test_Components_Hero_Content_Local_Chrome(){
        Assert.assertTrue(page.Components_Hero_Content(LocalChrome_Paths.BASELINE_COMPONENTS_HERO_WRAPPER,
                LocalChrome_Paths.ACTUAL_COMPONENTS_HERO_WRAPPER));
    }

}