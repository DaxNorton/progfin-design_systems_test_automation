package designsystems.gritdocsite.components.switches.design;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class Switches_Design_Chrome_Tests extends Switches_Design_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Switches_Design_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Switches_Design_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_SWITCHES_DESIGN,
                LocalChrome_Paths.ACTUAL_COMPONENTS_SWITCHES_DESIGN));
    }

}