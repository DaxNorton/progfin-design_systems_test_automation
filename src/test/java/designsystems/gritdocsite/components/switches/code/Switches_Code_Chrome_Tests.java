package designsystems.gritdocsite.components.switches.code;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import designsystems.pages.imagepaths.LocalChrome_Paths;

import java.lang.reflect.Method;

public class Switches_Code_Chrome_Tests extends Switches_Code_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Components_Switches_Code_Local_Chrome_Quick(){
        Assert.assertTrue(page.Components_Switches_Code_Quick(LocalChrome_Paths.BASELINE_COMPONENTS_SWITCHES_CODE,
                LocalChrome_Paths.ACTUAL_COMPONENTS_SWITCHES_CODE));
    }

}