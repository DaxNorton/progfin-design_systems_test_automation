package designsystems.gritdocsite.components.switches.code;

import designsystems.gritdocsite.GritDoc_Base;
import designsystems.pages.grit.ComponentsSwitchesCodePage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Switches_Code_Base extends GritDoc_Base {

    public ComponentsSwitchesCodePage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    private void setPage(WebDriver driver){
        page = new ComponentsSwitchesCodePage(driver, driverWait);
    }

}