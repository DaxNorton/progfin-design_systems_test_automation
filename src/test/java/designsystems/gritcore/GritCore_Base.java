package designsystems.gritcore;

import designsystems.utilities.drivers.local.LocalChrome;
import designsystems.utilities.drivers.local.LocalFirefoxHeadless;
import designsystems.utilities.saucelabs.drivers.desktop.BaseDriverDesktop;
import designsystems.utilities.saucelabs.drivers.desktop.MacChrome;
import designsystems.utilities.saucelabs.drivers.desktop.MacSafari;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import java.lang.reflect.Method;
import java.net.URL;

public class GritCore_Base {

    public WebDriver driver;
    public WebDriverWait driverWait;

    public static final URL scriptUrl = GritCore_Base.class.getResource("/axe.min.js");

    private LocalChrome driver1;
    public WebDriver localChromeDriver(){
        driver1 = new LocalChrome();
        return ((LocalChrome) driver1).getPreparedDriver();
    }

    private LocalFirefoxHeadless driver2;
    public WebDriver localFirefoxHeadless(){
        driver2 = new LocalFirefoxHeadless();
        return ((LocalFirefoxHeadless) driver2).getPreparedDriver();
    }

    private BaseDriverDesktop driver3;
    public WebDriver sauceWindowsChrome(String nameOfCallingTest){
        driver3 = new designsystems.utilities.saucelabs.drivers.desktop.WindowsChrome();
        driver3.setTestNameIn(nameOfCallingTest);
        driver3.setRecordVideo(true);
        driver3.setVideoUploadOnPass(true);
        driver3.setRecordScreenshots(true);
        driver3.setUseBuild(false);
        ((designsystems.utilities.saucelabs.drivers.desktop.WindowsChrome)driver3).instantiateMutableCapabilities();
        return ((designsystems.utilities.saucelabs.drivers.desktop.WindowsChrome) driver3).getPreparedDriver();
    }

    private BaseDriverDesktop driver5;
    public WebDriver sauceMacChrome(String nameOfCallingTest) {
        driver5 = new MacChrome();
        driver5.setTestNameIn(nameOfCallingTest);
        driver5.setRecordVideo(true);
        driver5.setVideoUploadOnPass(true);
        driver5.setRecordScreenshots(true);
        driver5.setUseBuild(false);
        ((MacChrome) driver5).instantiateMutableCapabilities();
        return ((MacChrome) driver5).getPreparedDriver();
    }

    private BaseDriverDesktop driver6;
    public WebDriver sauceMacSafari(String nameOfCallingTest) {
        driver6 = new MacSafari();
        driver6.setTestNameIn(nameOfCallingTest);
        driver6.setRecordVideo(true);
        driver6.setVideoUploadOnPass(true);
        driver6.setRecordScreenshots(true);
        driver6.setUseBuild(false);
        ((MacSafari) driver6).instantiateMutableCapabilities();
        return ((MacSafari) driver6).getPreparedDriver();
    }

    private BaseDriverDesktop driver7;
    public WebDriver sauceWindowsEdge(String nameOfCallingTest){
        driver7 = new designsystems.utilities.saucelabs.drivers.desktop.WindowsEdge();
        driver7.setTestNameIn(nameOfCallingTest);
        ((designsystems.utilities.saucelabs.drivers.desktop.WindowsEdge)driver7).instantiateMutableCapabilities();
        return ((designsystems.utilities.saucelabs.drivers.desktop.WindowsEdge) driver7).getPreparedDriver();
    }

    @AfterMethod
    public void afterMethod(ITestResult result) throws InterruptedException {
        StringBuilder messageTemp = new StringBuilder("\ndriver quit....");
        try {
            ((JavascriptExecutor) driver).executeScript(
                    "sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
            messageTemp.insert(1, "Sauce ");
        }catch(WebDriverException wDE){
            /*
             * Local drivers will cause a WebDriverException here, and it's OK.
             */
            messageTemp.insert(1, "Local ");
        }
        if(!(null==driver)) {
            driver.quit();
            System.out.println(messageTemp);
        }
        messageTemp.delete(0, messageTemp.length());
    }

    public void setupDriverWaitPage(Method method, WebDriver driver, String urlUnderTest){
        SessionId session = ((RemoteWebDriver)driver).getSessionId();
        if(null==session) {
            throw new IllegalStateException(method.getName() + " - FAIL:\t" + "driver instantiation failure");
        }else {
            System.out.println(method.getName() + " - PASS:\t" + "driver instantiated... (OK)");
        }
        driverWait = new WebDriverWait(driver, 90);
        driver.navigate().to(urlUnderTest);
        System.out.println("Testing: " + urlUnderTest);
        driverWait.until(WebDriver -> ((JavascriptExecutor) WebDriver).executeScript(
                "return document.readyState").equals("complete"));
    }

}