package designsystems.gritcore.buttons;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.ButtonsTestPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Buttons_Base extends GritCore_Base {

    public ButtonsTestPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new ButtonsTestPage(driver, driverWait);
    }
}
