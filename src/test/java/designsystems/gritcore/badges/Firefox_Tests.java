package designsystems.gritcore.badges;

import designsystems.pages.imagepaths.LocalFirefox_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;

public class Firefox_Tests extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localFirefoxHeadless();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Badge_Visual_Regression(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(page.Badge_DefaultWithIcon(LocalFirefox_Paths.BASELINE_BADGE_DEFAULT_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_DEFAULT_ICON), "Visual Regression Default Badge w/ Icon");
        softAssert.assertTrue(page.Badge_InfoWithIcon(LocalFirefox_Paths.BASELINE_BADGE_INFO_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_INFO_ICON), "Visual Regression Info Badge w/ Icon");
        softAssert.assertTrue(page.Badge_PrimaryWithIcon(LocalFirefox_Paths.BASELINE_BADGE_PRIMARY_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_PRIMARY_ICON),"Visual Regression Primary Badge w/ Icon");
        softAssert.assertTrue(page.Badge_AttentionWithIcon(LocalFirefox_Paths.BASELINE_BADGE_ATTENTION_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_ATTENTION_ICON),"Visual Regression Attention Badge w/ Icon ");
        softAssert.assertTrue(page.Badge_AttentionStrongWithIcon(LocalFirefox_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_ATTENTIONSTRONG_ICON), "Visual Regression Attention Strong Badge" +
                "w/ Icon");
        softAssert.assertTrue(page.Badge_SuccessWithIcon(LocalFirefox_Paths.BASELINE_BADGE_SUCCESS_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_SUCCESS_ICON), "Visual Regression Success Badge w/ Icon");
        softAssert.assertTrue(page.Badge_WarningWithIcon(LocalFirefox_Paths.BASELINE_BADGE_WARNING_ICON,
                LocalFirefox_Paths.ACTUAL_BADGE_WARNING_ICON), "Visual Regression Warning Badge w/ Icon");
        softAssert.assertAll();
    }
}