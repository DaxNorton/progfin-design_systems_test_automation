package designsystems.gritcore.badges;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.BadgesTestPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Badges_Base extends GritCore_Base {

    public BadgesTestPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new BadgesTestPage(driver, driverWait);
    }

}