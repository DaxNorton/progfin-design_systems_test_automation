package designsystems.gritcore.badges;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;

public class Chrome_Tests extends Badges_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void test_Badge_Visual_Regression(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(page.Badge_DefaultWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_DEFAULT_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_DEFAULT_ICON), "Visual Regression Default Badge w/ Icon");
        softAssert.assertTrue(page.Badge_InfoWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_INFO_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_INFO_ICON), "Visual Regression Info Badge w/ Icon");
        softAssert.assertTrue(page.Badge_PrimaryWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_PRIMARY_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_PRIMARY_ICON),"Visual Regression Primary Badge w/ Icon");
        softAssert.assertTrue(page.Badge_AttentionWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_ATTENTION_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_ATTENTION_ICON),"Visual Regression Attention Badge w/ Icon ");
        softAssert.assertTrue(page.Badge_AttentionStrongWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_ATTENTIONSTRONG_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_ATTENTIONSTRONG_ICON), "Visual Regression Attention Strong Badge" +
                "w/ Icon");
        softAssert.assertTrue(page.Badge_SuccessWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_SUCCESS_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_SUCCESS_ICON), "Visual Regression Success Badge w/ Icon");
        softAssert.assertTrue(page.Badge_WarningWithIconRetina(LocalChrome_Paths.BASELINE_BADGE_WARNING_ICON,
                LocalChrome_Paths.ACTUAL_BADGE_WARNING_ICON), "Visual Regression Warning Badge w/ Icon");
        softAssert.assertAll();
    }
}