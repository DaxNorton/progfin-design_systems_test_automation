package designsystems.gritcore.typographytokens;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.TypographyTokensPage;
import designsystems.utilities.drivers.local.LocalChromeHeadless;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class TypographyTokens_Base extends GritCore_Base {

    public TypographyTokensPage page;
    public String urlUnderTest = page.urlUnderTest;

    private LocalChromeHeadless driverLocalChromeMobileView;
    public WebDriver localChromeMobileView(){
        driverLocalChromeMobileView = new LocalChromeHeadless("mobile");
        return ((LocalChromeHeadless) driverLocalChromeMobileView).getPreparedDriver();
    }

    private LocalChromeHeadless driverLocalChromeSmallMobileView;
    public WebDriver localChromeSmallMobileView(){
        driverLocalChromeSmallMobileView = new LocalChromeHeadless("smallmobile");
        return ((LocalChromeHeadless) driverLocalChromeSmallMobileView).getPreparedDriver();
    }

    private LocalChromeHeadless driverLocalChromeTabletView;
    public WebDriver localChromeTabletView(){
        driverLocalChromeTabletView = new LocalChromeHeadless("tablet");
        return ((LocalChromeHeadless) driverLocalChromeTabletView).getPreparedDriver();
    }

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new TypographyTokensPage(driver, driverWait);
    }

}