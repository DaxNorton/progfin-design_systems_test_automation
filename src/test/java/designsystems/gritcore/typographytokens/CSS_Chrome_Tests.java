package designsystems.gritcore.typographytokens;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.lang.reflect.Method;

public class CSS_Chrome_Tests extends TypographyTokens_Base {

    // Font Display Large | Mixin Font Display Large
    @Test
    public void tokenRegressionFontDisplayLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeDesktop(page.fontDisplayLarge()));
    }
    @Test
    public void tokenRegressionMixinsFontDisplayLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeDesktop(page.mixinFontDisplayLarge()));
    }
    @Test
    public void tokenRegressionFontDisplayLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeModal(page.fontDisplayLarge()));
    }
    @Test
    public void tokenRegressionMixinFontDisplayLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeModal(page.mixinFontDisplayLarge()));
    }
    @Test
    public void tokenRegressionFontDisplayLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeMobileL(page.fontDisplayLarge()));
    }
    @Test
    public void tokenRegressionMixinFontDisplayLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeMobileL(page.mixinFontDisplayLarge()));
    }
    @Test
    public void tokenRegressionFontDisplayLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeSmall(page.fontDisplayLarge()));
    }
    @Test
    public void tokenRegressionMixinFontDisplayLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayLargeSmall(page.mixinFontDisplayLarge()));
    }


    //Font Display Medium | Mixin Font Display Medium
    @Test
    public void tokenRegressionFontDisplayMediumViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayMediumDesktop(page.fontDisplayMedium()));
    }
    @Test
    public void tokenRegressionMixinsFontDisplayMediumViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayMediumDesktop(page.mixinFontDisplayMedium()));
    }
    @Test
    public void tokenRegressionFontDisplayMediumViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValueToToken_DisplayMediumModal(page.fontDisplayMedium()));
    }
    @Test
    public void tokenRegressionMixinFontDisplayMediumViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValueToToken_DisplayMediumModal(page.mixinFontDisplayMedium()));
    }
    @Test
    public void tokenRegressionFontDisplayMediumViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayMediumMobileL(page.fontDisplayMedium()));
    }
    @Test
    public void tokenRegressionMixinFontDisplayMediumViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayMediumMobileL(page.mixinFontDisplayMedium()));
    }
    @Test
    public void tokenRegressionFontDisplayMediumViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayMediumSmall(page.fontDisplayMedium()));
    }
    @Test
    public void tokenRegressionMixinFontDisplayMediumViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplayMediumSmall(page.mixinFontDisplayMedium()));
    }
    // Font Display Small
    @Test
    public void tokenRegressionFontDisplaySmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallDesktop(page.fontDisplaySmall()));
    }
    @Test
    public void tokenRegressionMixinFontDisplaySmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallDesktop(page.mixinFontDisplaySmall()));
    }
    @Test
    public void tokenRegressionFontDisplaySmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallModal(page.fontDisplaySmall()));
    }
    @Test
    public void tokenRegressionMixinFontDisplaySmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallModal(page.mixinFontDisplaySmall()));
    }
    @Test
    public void tokenRegressionFontDisplaySmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallMobileL(page.fontDisplaySmall()));
    }
    @Test
    public void tokenRegressionMixinFontDisplaySmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallMobileL(page.mixinFontDisplaySmall()));
    }
    @Test
    public void tokenRegressionFontDisplaySmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallSmall(page.fontDisplaySmall()));
    }
    @Test
    public void tokenRegressionMixinFontDisplaySmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_DisplaySmallSmall(page.mixinFontDisplaySmall()));
    }

    //Headline

    @Test
    public void tokenRegressionHeadlineViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineDesktop(page.fontHeadline()));
    }
    @Test
    public void tokenRegressionMixinHeadlineViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineDesktop(page.mixinFontHeadline()));
    }
    @Test
    public void tokenRegressionHeadlineViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineModal(page.fontHeadline()));
    }
    @Test
    public void tokenRegressionMixinHeadlineViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineModal(page.mixinFontHeadline()));
    }
    @Test
    public void tokenRegressionHeadlineViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineMobileL(page.fontHeadline()));
    }
    @Test
    public void tokenRegressionMixinHeadlineViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineMobileL(page.mixinFontHeadline()));
    }
    @Test
    public void tokenRegressionHeadlineViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineSmall(page.fontHeadline()));
    }
    @Test
    public void tokenRegressionMixinHeadlineViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_HeadlineSmall(page.mixinFontHeadline()));
    }

    //Title-Large

    @Test
    public void tokenRegressionTitleLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeDesktop(page.fontTitleLarge()));
    }
    @Test
    public void tokenRegressionMixinTitleLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeDesktop(page.mixinsFontTitleLarge()));
    }
    @Test
    public void tokenRegressionTitleLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeModal(page.fontTitleLarge()));
    }
    @Test
    public void tokenRegressionMixinTitleLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeModal(page.mixinsFontTitleLarge()));
    }
    @Test
    public void tokenRegressionTitleLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeMobileL(page.fontTitleLarge()));
    }
    @Test
    public void tokenRegressionMixinTitleLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeMobileL(page.mixinsFontTitleLarge()));
    }
    @Test
    public void tokenRegressionTitleLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeSmall(page.fontTitleLarge()));
    }
    @Test
    public void tokenRegressionMixinTitleLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleLargeSmall(page.mixinsFontTitleLarge()));
    }

    //Title-Medium

    @Test
    public void tokenRegressionTitleMediumViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumDesktop(page.fontTitleMedium()));
    }
    @Test
    public void tokenRegressionMixinTitleMediumViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumDesktop(page.mixinsFontTitleMedium()));
    }
    @Test
    public void tokenRegressionTitleMediumViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumModal(page.fontTitleMedium()));
    }
    @Test
    public void tokenRegressionMixinTitleMediumViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumModal(page.mixinsFontTitleMedium()));
    }
    @Test
    public void tokenRegressionTitleMediumViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumMobileL(page.fontTitleMedium()));
    }
    @Test
    public void tokenRegressionMixinTitleMediumViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumMobileL(page.mixinsFontTitleMedium()));
    }
    @Test
    public void tokenRegressionTitleMediumViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumSmall(page.fontTitleMedium()));
    }
    @Test
    public void tokenRegressionMixinTitleMediumViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleMediumSmall(page.mixinsFontTitleMedium()));
    }

    //Font Title Small

    @Test
    public void tokenRegressionTitleSmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallDesktop(page.fontTitleSmall()));
    }
    @Test
    public void tokenRegressionMixinTitleSmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallDesktop(page.mixinsFontTitleSmall()));
    }
    @Test
    public void tokenRegressionTitleSmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallModal(page.fontTitleSmall()));
    }
    @Test
    public void tokenRegressionMixinTitleSmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallModal(page.mixinsFontTitleSmall()));
    }
    @Test
    public void tokenRegressionTitleSmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallMobileL(page.fontTitleSmall()));
    }
    @Test
    public void tokenRegressionMixinTitleSmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallMobileL(page.mixinsFontTitleSmall()));
    }
    @Test
    public void tokenRegressionTitleSmalliewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallSmall(page.fontTitleSmall()));
    }
    @Test
    public void tokenRegressionMixinTitleSmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontTitleSmallSmall(page.mixinsFontTitleSmall()));
    }

    //Subheading

    @Test
    public void tokenRegressionTitleSubheadingViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingDesktop(page.fontSubheading()));
    }
    @Test
    public void tokenRegressionMixinSubheadingViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingDesktop(page.mixinsFontSubheading()));
    }
    @Test
    public void tokenRegressionSubheadingViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingModal(page.fontSubheading()));
    }
    @Test
    public void tokenRegressionMixinSubheadingViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingModal(page.mixinsFontSubheading()));
    }
    @Test
    public void tokenRegressionSubheadingViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingMobileL(page.fontSubheading()));
    }
    @Test
    public void tokenRegressionMixinSubheadingViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingMobileL(page.mixinsFontSubheading()));
    }
    @Test
    public void tokenRegressionSubheadingViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingSmall(page.fontSubheading()));
    }
    @Test
    public void tokenRegressionMixinSubheadingViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_SubheadingSmall(page.mixinsFontSubheading()));
    }

    //Font Body Large

    @Test
    public void tokenRegressionTitleFontBodyLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeDesktop(page.fontBodyLarge()));
    }
    @Test
    public void tokenRegressionMixinFontBodyLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeDesktop(page.mixinsFontBodyLarge()));
    }
    @Test
    public void tokenRegressionFontBodyLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeModal(page.fontBodyLarge()));
    }
    @Test
    public void tokenRegressionMixinFontBodyLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeModal(page.mixinsFontBodyLarge()));
    }
    @Test
    public void tokenRegressionFontBodyLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeMobileL(page.fontBodyLarge()));
    }
    @Test
    public void tokenRegressionMixinFontBodyLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeMobileL(page.mixinsFontBodyLarge()));
    }
    @Test
    public void tokenRegressionFontBodyLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeSmall(page.fontBodyLarge()));
    }
    @Test
    public void tokenRegressionMixinFontBodyLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodyLargeSmall(page.mixinsFontBodyLarge()));
    }

    //Font Body Small

    @Test
    public void tokenRegressionTitleFontBodySmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallDesktop(page.fontBodySmall()));
    }
    @Test
    public void tokenRegressionMixinFontBodySmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallDesktop(page.mixinsFontBodySmall()));
    }
    @Test
    public void tokenRegressionFontBodySmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallModal(page.fontBodySmall()));
    }
    @Test
    public void tokenRegressionMixinFontBodySmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallModal(page.mixinsFontBodySmall()));
    }
    @Test
    public void tokenRegressionFontBodySmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallMobileL(page.fontBodySmall()));
    }
    @Test
    public void tokenRegressionMixinFontBodySmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallMobileL(page.mixinsFontBodySmall()));
    }
    @Test
    public void tokenRegressionFontBodySmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallSmall(page.fontBodySmall()));
    }
    @Test
    public void tokenRegressionMixinFontBodySmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FontBodySmallSmall(page.mixinsFontBodySmall()));
    }

    //Button

    @Test
    public void tokenRegressionButtonViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonDesktop(page.fontButton()));
    }
    @Test
    public void tokenRegressionMixinButtonViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonDesktop(page.mixinsFontButton()));
    }
    @Test
    public void tokenRegressionButtonViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonModal(page.fontButton()));
    }
    @Test
    public void tokenRegressionMixinButtonViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonModal(page.mixinsFontButton()));
    }
    @Test
    public void tokenRegressionButtonViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonMobileL(page.fontButton()));
    }
    @Test
    public void tokenRegressionMixinButtonViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonMobileL(page.mixinsFontButton()));
    }
    @Test
    public void tokenRegressionButtonViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonSmall(page.fontButton()));
    }
    @Test
    public void tokenRegressionMixinButtonViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_ButtonSmall(page.mixinsFontButton()));
    }

    //Caption

    @Test
    public void tokenRegressionCaptionViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionDesktop(page.fontCaption()));
    }
    @Test
    public void tokenRegressionMixinCaptionViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionDesktop(page.mixinsFontCaption()));
    }
    @Test
    public void tokenRegressionCaptionViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionModal(page.fontCaption()));
    }
    @Test
    public void tokenRegressionMixinCaptionViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionModal(page.mixinsFontCaption()));
    }
    @Test
    public void tokenRegressionCaptionViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionMobileL(page.fontCaption()));
    }
    @Test
    public void tokenRegressionMixinCaptionViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionMobileL(page.mixinsFontCaption()));
    }
    @Test
    public void tokenRegressionCaptionViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionSmall(page.fontCaption()));
    }
    @Test
    public void tokenRegressionMixinsCaptionViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_CaptionSmall(page.mixinsFontCaption()));
    }

    //Field Label Large

    @Test
    public void tokenRegressionFieldLabelLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeDesktop(page.fontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionMixinFieldLabelLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeDesktop(page.mixinsFontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionFieldLabelLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeModal(page.fontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionMixinFieldLabelLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeModal(page.mixinsFontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionFieldLabelLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeMobileL(page.fontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionMixinFieldLabelLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeMobileL(page.mixinsFontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionFieldLabelLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeSmall(page.fontFieldLabelLarge()));
    }
    @Test
    public void tokenRegressionMixinsFieldLabelLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelLargeSmall(page.mixinsFontFieldLabelLarge()));
    }

    //Field Label Small

    @Test
    public void tokenRegressionFieldLabelSmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallDesktop(page.fontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionMixinFieldLabelSmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallDesktop(page.mixinsFontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionFieldLabelSmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallModal(page.fontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionMixinFieldLabelSmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallModal(page.mixinsFontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionFieldLabelSmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallMobileL(page.fontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionMixinFieldLabelSmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallMobileL(page.mixinsFontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionFieldLabelSmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallSmall(page.fontFieldLabelSmall()));
    }
    @Test
    public void tokenRegressionMixinsFieldLabelSmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_FieldLabelSmallSmall(page.mixinsFontFieldLabelSmall()));
    }

    //Link Large

    @Test
    public void tokenRegressionLinkLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeDesktop(page.fontLinkLarge()));
    }
    @Test
    public void tokenRegressionMixinsLinkLargeViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeDesktop(page.mixinsFontLinkLarge()));
    }
    @Test
    public void tokenRegressionLinkLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeModal(page.fontLinkLarge()));
    }
    @Test
    public void tokenRegressionMixinsLinkLargeViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeModal(page.mixinsFontLinkLarge()));
    }
    @Test
    public void tokenRegressionLinkLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeMobileL(page.fontLinkLarge()));
    }
    @Test
    public void tokenRegressionMixinsLinkLargeViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeMobileL(page.mixinsFontLinkLarge()));
    }
    @Test
    public void tokenRegressionLinkLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeSmall(page.fontLinkLarge()));
    }
    @Test
    public void tokenRegressionMixinsLinkLargeViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkLargeSmall(page.mixinsFontLinkLarge()));
    }

    //Link Small

    @Test
    public void tokenRegressionLinkSmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallDesktop(page.fontLinkSmall()));
    }
    @Test
    public void tokenRegressionMixinsLinkSmallViewportDesktop(Method method){
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallDesktop(page.mixinsFontLinkSmall()));
    }
    @Test
    public void tokenRegressionLinkSmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallModal(page.fontLinkSmall()));
    }
    @Test
    public void tokenRegressionMixinsLinkSmallViewportModal(Method method){
        driver = localChromeTabletView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallModal(page.mixinsFontLinkSmall()));
    }
    @Test
    public void tokenRegressionLinkSmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallMobileL(page.fontLinkSmall()));
    }
    @Test
    public void tokenRegressionMixinsLinkSmallViewportMobileL(Method method){
        driver = localChromeMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallMobileL(page.mixinsFontLinkSmall()));
    }
    @Test
    public void tokenRegressionLinkSmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallSmall(page.fontLinkSmall()));
    }
    @Test
    public void tokenRegressionMixinsLinkSmallViewportSmall(Method method){
        driver = localChromeSmallMobileView();
        getDriverSetPage(method, driver);
        Assert.assertTrue(page.compareCSSValuesToTokens_LinkSmallSmall(page.mixinsFontLinkSmall()));
    }

}