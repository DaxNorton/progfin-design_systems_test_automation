package designsystems.gritcore.radios;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Firefox_Tests extends Radio_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localFirefoxHeadless();
        setupDriverWaitPage(method, driver);
    }

    @Test
    public void radio_Test_Visual(){
        //visual regression test on the presentation of the radios
    }

    @Test
    public void radio_Event_Emitter_Initial(){
        Assert.assertEquals(page.get_Event_Emitter_Message(), ""); //check that event is empty
        page.click_Radio_Option_1();
        Assert.assertEquals(page.get_Event_Emitter_Message(), page.EVENT_EMITTED_RADIO_OPTION_1);
    }

    @Test
    public void radio_Event_Emitter_Updates(){
        page.click_Radio_Option_1();
        Assert.assertEquals(page.get_Event_Emitter_Message(),page.EVENT_EMITTED_RADIO_OPTION_1);
        Assert.assertNotEquals(page.get_Event_Emitter_Message(), page.EVENT_EMITTED_RADIO_OPTION_2);
        page.click_Radio_Option_2();
        Assert.assertEquals(page.get_Event_Emitter_Message(), page.EVENT_EMITTED_RADIO_OPTION_2);
        Assert.assertNotEquals(page.get_Event_Emitter_Message(), page.EVENT_EMITTED_RADIO_OPTION_1);
        page.click_Radio_Option_3();
        Assert.assertEquals(page.get_Event_Emitter_Message(), page.EVENT_EMITTED_RADIO_OPTION_3);
    }

    @Test
    public void radio_Event_Emitter_Negative(){
        page.click_Radio_Option_3();
        Assert.assertNotEquals(page.get_Event_Emitter_Message(), page.EVENT_EMITTED_RADIO_OPTION_1);
    }
}
