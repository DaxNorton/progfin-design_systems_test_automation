package designsystems.gritcore.radios;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.RadiosTestPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Method;

public class Radio_Base extends GritCore_Base {

    public RadiosTestPage page;

    public void setupDriverWaitPage(Method method, WebDriver driver){
        SessionId session = ((RemoteWebDriver)driver).getSessionId();
        if(null==session) {
            throw new IllegalStateException(method.getName() + " - FAIL:\t" + "driver instantiation failure");
        }else {
            System.out.println(method.getName() + " - PASS:\t" + "driver instantiated... (OK)");
        }
        driverWait = new WebDriverWait(driver, 3);
        driver.navigate().to(page.urlUnderTest);
        System.out.println("Testing: " + ProgressiveLeasingConstants.COMPONENTENVRIONMENTUNDERTEST);
        driverWait.until(WebDriver -> ((JavascriptExecutor) WebDriver).executeScript(
                "return document.readyState").equals("complete"));
        setPageWithDriver();
    }

    public void setPageWithDriver(){
        page = new RadiosTestPage(driver, driverWait);
    }
}
