package designsystems.gritcore.inputs;

import designsystems.pages.imagepaths.LocalChrome_Paths;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class Chrome_Tests extends Inputs_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
    }

    @Test
    public void subscriptEventEmitterTest(){
        Assert.assertTrue(page.isSubscriptEventEmitterInitial());
    }

    @Test
    public void subscriptEventUpdated(){
        Assert.assertTrue(page.isSubscriptEventEmitterUpdate());
    }

    @Test
    public void inputNormalFieldVisual() throws InterruptedException {
        org.openqa.selenium.Dimension dimension = new Dimension(767,850);
        driver.manage().window().setSize(dimension);
        Thread.sleep(500);
        driver.findElement(By.xpath(page.NORMAL_FIELD_INPUT_XPATH)).sendKeys("gjq lowercase low hanging letter test");
        Assert.assertTrue(page.InputNormal_Visual(LocalChrome_Paths.BASELINE_INPUT_NORMAL_FIELD, LocalChrome_Paths.ACTUAL_INPUT_NORMAL_FIELD, LocalChrome_Paths.FEEDBACK_INPUT_NORMAL_FIELD));
    }

    @Test
    public void inputNormalOverviewVisual(){
        Assert.assertTrue(page.InputOverview_Visual(LocalChrome_Paths.BASELINE_INPUT_OVERVIEW, LocalChrome_Paths.ACTUAL_INPUT_OVERVIEW, LocalChrome_Paths.FEEDBACK_INPUT_OVERVIEW));
    }

    @Test
    public void inputNormalOverViewMobileVisual(){
        org.openqa.selenium.Dimension dimension = new Dimension(767,850);
        driver.manage().window().setSize(dimension);
        Assert.assertTrue(page.InputOverview_Visual(LocalChrome_Paths.BASELINE_INPUT_OVERVIEW_MOBILE, LocalChrome_Paths.ACTUAL_INPUT_OVERVIEW_MOBILE, LocalChrome_Paths.FEEDBACK_INPUT_OVERVIEW_MOBILE));
    }
}