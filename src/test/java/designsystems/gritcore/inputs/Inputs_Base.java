package designsystems.gritcore.inputs;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.gritcore.InputsTestPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class Inputs_Base extends GritCore_Base {

    public InputsTestPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new InputsTestPage(driver, driverWait);
    }
}
