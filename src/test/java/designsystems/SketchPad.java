package designsystems;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.annotations.Test;
import designsystems.utilities.CommonDriversLocal;
import designsystems.utilities.DriverTypeLocal;

import java.lang.reflect.Method;

public class SketchPad {

    private WebDriver driver;

    @Test(enabled = false)
    public void figuringItOut(Method method){

        driver = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();

        SessionId session = ((RemoteWebDriver)driver).getSessionId();
        if(null==session) {
            throw new IllegalStateException(method.getName() + " - FAIL:\t" + "driver instantiation failure");
        }else {
            System.out.println(method.getName() + " - PASS:\t" + "driver instantiated... (OK)");
        }

        driver.navigate().to("https://google.com");
        driver.manage().window().maximize();

    }
}
