package designsystems.microfrontend;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Mac_Chrome_Tests extends MicroFrontEnd_Base{

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = sauceMacChrome(method.getName());
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);

    }

    @Test
    public void testEventEmitter(){
        page.clickEventButton();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(page.jsonEventOutput().isDisplayed());
        softAssert.assertTrue(page.jsonEventOutput().getText().contains("this has been hit"));
        softAssert.assertAll();
    }
}