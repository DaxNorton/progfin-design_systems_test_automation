package designsystems.microfrontend;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.TestHarnessPage;
import helpers.ServicesURLEnum;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class MicroFrontEnd_Base extends GritCore_Base {

    public TestHarnessPage page;
    public String urlUnderTest = ServicesURLEnum.GritMicroFrontLoader.getURL();

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }

    public void setPage(WebDriver driver){
        page = new TestHarnessPage(driver, driverWait);
    }

}