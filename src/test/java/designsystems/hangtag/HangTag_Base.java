package designsystems.hangtag;

import designsystems.gritcore.GritCore_Base;
import designsystems.pages.universal.HangTagPage;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Method;

public class HangTag_Base extends GritCore_Base {

    public HangTagPage page;
    public String urlUnderTest = page.urlUnderTest;

    public void getDriverSetPage(Method method, WebDriver driver){
        setupDriverWaitPage(method, driver, urlUnderTest);
        setPage(driver);
    }
    public void setPage(WebDriver driver){
        page = new HangTagPage(driver, driverWait);
    }

}
