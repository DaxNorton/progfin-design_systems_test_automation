package designsystems.hangtag;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

public class Chrome_Tests extends HangTag_Base {

    @BeforeMethod
    public void beforeMethod(Method method) throws InterruptedException {
        driver = localChromeDriver();
        getDriverSetPage(method, driver);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @Test
    public void testHangTagTotalOfPayments(){
        Assert.assertTrue(page.getHangTagTotalPaymentAmount().contains(page.configInitialBalance().getText()));
    }

    @Test
    public void testHangTagCostOfRental(){
        Assert.assertTrue(page.getHangTagCostOfRental().contains(page.configCostOfRental().getText()));
    }

    @Test
    public void testHangTagCashPrice(){
        Assert.assertTrue(page.getHangTagCashPrice().contains(page.configInitialCashPrice().getText()));
    }

    @Test
    public void testHangTagAmountOfEachPaymentInitial(){
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains(page.configInitialLow().getText()));
    }

    @Test
    public void testHangTagAmountOfEachPaymentInitialHigh(){
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains(page.configInitialHigh().getText()));
    }

    @Test
    public void testHangTagAmountOfEachPaymentPeriodicPayment(){
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains(page.configPeriodicPayment().getText()));
    }

    @Test
    public void testHangTagAmountOfEachPaymentFrequency1(){
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains("every week"));
    }

    @Test
    public void testHangTagAmountOfEachPaymentFrequency2(){
        page.configPaymentFrequency().clear();
        page.configPaymentFrequency().sendKeys("2");
        page.clickRender();
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains("every other week"));
    }

    @Test
    public void testHangTagAmountOfEachPaymentFrequency3(){
        page.configPaymentFrequency().clear();
        page.configPaymentFrequency().sendKeys("3");
        page.clickRender();
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains("twice per month"));
    }

    @Test
    public void testHangTagAmountOfEachPaymentFrequency4(){
        page.configPaymentFrequency().clear();
        page.configPaymentFrequency().sendKeys("4");
        page.clickRender();
        Assert.assertTrue(page.getHangTagAmountOfEachPaymentDescription().contains("every month"));

    }

    @Test
    public void testNegativeValuesInput(){
        page.configInitialBalance().clear();
        page.clickRender();
    }

    @Test
    public void testHangTagNumberOfPayments(){
        Assert.assertTrue(page.getHangTagNumberOfPayments().contains(page.configNumberOfPayments().getText()));
    }

    @Test
    public void testHangTagRentalPeriod(){
        Assert.assertTrue(page.getHangTagRentalPeriod().contains(page.configTermTotal().getText()));
    }

    @Test
    public void testHangTagTotalOfPaymentsDescription(){
        Assert.assertTrue(page.getHangTagTotalOfPaymentsDescription().equals(page.HANG_TAG_TOTAL_OF_PAYMENTS_DESCRIPTION));
    }
}
