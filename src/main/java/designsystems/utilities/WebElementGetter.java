/*
 * Copyright Progressive Leasing LLC.
 */
package designsystems.utilities;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * This WebElement "engine" safely iterates through a Set of location attempts,
 * then either returns the desired WebElement, or throws a proper exception.
 * <p>
 * The class has a single public static getter, which is typically called
 * from page objects, which in turn are typically called from downstream
 * tests.
 * <p>
 * Version 1.0 2019-01-04
 * <p>
 * Version 1.1 2019-03-26 add tenth second sleep in iteration for greater reliability with Magento
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1 2019-03-26
 */
public class WebElementGetter {

    public static WebElement fetchThisElement(String methodNameIn, WebDriver driverIn, Set<By> byIn) throws NoSuchElementException {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            //deliberate swallow
        }
        WebElement localElement = null;
        Iterator<By> iterate = byIn.iterator();
        while(iterate.hasNext()) {
            try {
                localElement = driverIn.findElement((By)iterate.next());
            }catch(Exception any) {
                // deliberate swallow
            }
            try {
                Thread.sleep(100);
            }catch(InterruptedException iE) {
                // deliberate swallow
            }
            if(null!=localElement) {
                break;
            }
        }
        if(null==localElement) {
            throw new NoSuchElementException(methodNameIn);
        }
        return localElement;
    }
}
