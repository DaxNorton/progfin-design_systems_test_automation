/*
 * Copyright Progressive Leasing LLC.
 */
package designsystems.utilities;

/**
 * An optional class with boolean methods for automatically detecting
 * the currently running operating system.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class OperatingSystemDetector {

    private static String operatingSystemDetected;

    static {
        operatingSystemDetected = System.getProperty("os.name").toLowerCase();
    }

    public static boolean isWindows() {
        return (operatingSystemDetected.indexOf("win") >= 0);
    }

    public static boolean isMac() {
        return (operatingSystemDetected.indexOf("mac") >= 0);
    }

    public static boolean isSolaris() {
        return (operatingSystemDetected.indexOf("sunos") >= 0);
    }

    public static boolean isUnix() {

        boolean isUnixLocal = false;

        if(operatingSystemDetected.indexOf("nix") >= 0) {
            isUnixLocal = true;
        } else if(operatingSystemDetected.indexOf("aix") >= 0) {
            isUnixLocal = true;
        } else if(operatingSystemDetected.indexOf("nux") >= 0) {
            isUnixLocal = true;
        }

        return isUnixLocal;
    }

}
