package designsystems.utilities.image;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ImageUtility {

    WebDriver driver;
    WebDriverWait driverWait;
    GetImages getImages;

    public ImageUtility(WebDriver driverIn, WebDriverWait driverWaitIn){
        this.driver = driverIn;
        this.driverWait = driverWaitIn;
        getImages = new GetImages(driver);
    }

    private void getPageImage(String filePath){
        getImages.GetViewImage(filePath);
    }

    //Retina display
    private void getElementImageRetina(WebElement webElement, String filePath){
        getImages.SaveElementScreenshotRetina(webElement, filePath);
    }
    //non-Retina display
    private void getElementImage(WebElement webElement, String filePath){
        getImages.SaveElementScreenshot(webElement, filePath);
    }

    private boolean getIsEqual(String baselineFile, String actualFile){
       return ImageComparison.imageIsEqual(baselineFile, actualFile);
    }

    private boolean getIsEqualAndFeedback(String baselineFile, String actualFile, String feedbackFile){
       return ImageComparison.imageIsEqualAndFeedback(baselineFile, actualFile, feedbackFile);
    }

    public void GetPageImage(String baselineFile){
        getPageImage(baselineFile);
    }

    public boolean GetPageImage_Compare(String baselineFile, String actualFile){
        getPageImage(actualFile);
        return getIsEqual(baselineFile, actualFile);
    }

    public boolean GetPageImage_Compare_Feedback(String baselineFile, String actualFile, String feedbackFile){
        getPageImage(actualFile);
        return getIsEqualAndFeedback(baselineFile, actualFile, feedbackFile);
    }

    public void GetElementImageRetina(WebElement elementToCapture, String baselineFile){
        WebElement element = null;
        try{
            element = driverWait.until(ExpectedConditions.visibilityOf(elementToCapture));
            System.out.println("Element to capture: " + elementToCapture);
        }catch(NoSuchElementException nsee){
            System.out.println("Unable to find: " + elementToCapture + " for capture");
        }
        getElementImageRetina(element, baselineFile);
    }

    public void GetElementImage(WebElement elementToCapture, String baselineFile){
        WebElement element = null;
        try{
            element = driverWait.until(ExpectedConditions.visibilityOf(elementToCapture));
            System.out.println("Element to capture: " + elementToCapture);
        }catch(NoSuchElementException nsee){
            System.out.println("Unable to find: " + elementToCapture + " for capture");
        }
        getElementImage(element, baselineFile);
    }

    public boolean GetElementImageRetina_Compare(WebElement elementToCapture, String baselineFile, String actualFile){
        WebElement element = null;
        try{
            element = driverWait.until(ExpectedConditions.visibilityOf(elementToCapture));
            System.out.println("Element to capture: " + elementToCapture);
        }catch(NoSuchElementException nsee){
            System.out.println("Unable to find: " + elementToCapture + " for capture");
        }
        getElementImageRetina(element, actualFile);
        return getIsEqual(baselineFile, actualFile);
    }

    public boolean GetElementImage_Compare(WebElement elementToCapture, String baselineFile, String actualFile){
        WebElement element = null;
        try{
            element = driverWait.until(ExpectedConditions.visibilityOf(elementToCapture));
            System.out.println("Element to capture: " + elementToCapture);
        }catch(NoSuchElementException nsee){
            System.out.println("Unable to find: " + elementToCapture + " for capture");
        }
        getElementImage(element, actualFile);
        return getIsEqual(baselineFile, actualFile);
    }

    public boolean GetElementImageRetina_Compare_Feedback(WebElement elementToCapture, String baselineFile, String actualFile, String feedbackFile){
        WebElement element = null;
        try{
            element = driverWait.until(ExpectedConditions.visibilityOf(elementToCapture));
            System.out.println("Element to capture: " + elementToCapture);
        }catch(NoSuchElementException nsee){
            System.out.println("Unable to find: " + elementToCapture + " for capture");
        }
        getElementImageRetina(element, actualFile);
        return getIsEqualAndFeedback(baselineFile, actualFile, feedbackFile);
    }

    public boolean GetElementImage_Compare_Feedback(WebElement elementToCapture, String baselineFile, String actualFile, String feedbackFile){
        WebElement element = null;
        try{
            element = driverWait.until(ExpectedConditions.visibilityOf(elementToCapture));
            System.out.println("Element to capture: " + elementToCapture);
        }catch(NoSuchElementException nsee){
            System.out.println("Unable to find: " + elementToCapture + " for capture");
        }
        getElementImage(element, actualFile);
        return getIsEqualAndFeedback(baselineFile, actualFile, feedbackFile);
    }
}