package designsystems.utilities.image;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.Coords;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class GetImages {

    WebDriver driver;

    public GetImages(WebDriver driverIn){
        this.driver = driverIn;
    }

    private void saveViewImage(String filePath){
        try{
            Thread.sleep(2500);
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        TakesScreenshot scrShot = ((TakesScreenshot) driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile=new File(filePath);
        try{
            FileUtils.copyFile(SrcFile, DestFile);
            System.out.println("Image Saved to: " + DestFile);
        }catch(IOException ioe){
            System.out.println("Image Save FAIL: unable to save " + DestFile);
        }
    }
    /**
     Will take shot of entire element and save to filePath
     If when ran on a non-ios device the viewportRetina does not work then use ShootingStrategies.scaling(2)).
     Mac = ShootingStrategies.viewportRetina(100, 0, 0, 2))
     */
    private void saveElementScreenshotRetina(WebElement webElement, String filePath) {
        try{
            Thread.sleep(2500);
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        Screenshot screenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).shootingStrategy(ShootingStrategies.viewportRetina(100, 0, 0, 2)).takeScreenshot(driver, webElement);
        try{
            ImageIO.write(screenshot.getImage(), "PNG", new File(filePath));
            System.out.println("Image Saved to: " + filePath);
        }catch(IOException IOe){
            System.out.println("Image Save FAIL: unable to save " + filePath);
            IOe.printStackTrace();
        }
    }

    private void saveElementScreenshot(WebElement webElement, String filePath){
        try{
            Thread.sleep(2500);
        }catch(InterruptedException ie){
            ie.printStackTrace();
        }
        Screenshot screenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).shootingStrategy(ShootingStrategies.viewportNonRetina(1500, 0,0)).takeScreenshot(driver, webElement);
        try{
            ImageIO.write(screenshot.getImage(), "PNG", new File(filePath));
            System.out.println("Image Saved to: " + filePath);
        }catch(IOException IOe){
            System.out.println("Image Save FAIL: unable to save " + filePath);
            IOe.printStackTrace();
        }
    }
    /**
     This method stitches together multiple frame screenshots seamlessly.
     Used with convertToBuffered to convert AShot into BufferedImage. Work needed if element censor is ever required.
     */
    private BufferedImage takeEntirePageImage(Integer timeoutms){
        int timeout = timeoutms;
        List<By> byList = null;
        AShot aShot = new AShot().
                coordsProvider(new WebDriverCoordsProvider()).
                shootingStrategy(ShootingStrategies.viewportRetina(timeout, 0, 0, 2));
        //for (By by : byList) {
        //    aShot = aShot.addIgnoredElement(by);
        //}
        Screenshot screenshot = aShot.takeScreenshot(driver);
        BufferedImage bi =  convertToBuffered(screenshot);
        BufferedImage result;
        result = bi;
        return result;
    }
    /**
     original use to censor defined elements
     nulling the element list in takeEntirePageImage results with the best found solution for converting Ashot to buffered.
     */
    private BufferedImage convertToBuffered(Screenshot screenshot){
        BufferedImage bi = screenshot.getImage();
        Graphics2D g2D = bi.createGraphics();
        g2D.setColor(Color.LIGHT_GRAY);
        Set<Coords> paintedAreas = screenshot.getIgnoredAreas();
        for (Coords rect : paintedAreas) {
            int x = (int)rect.getX();
            int y = (int)rect.getY();
            int width = (int)rect.getWidth();
            int height = (int)rect.getHeight();
            g2D.fillRect(x, y, width, height);
        }
        return bi;
    }


    public void GetViewImage(String filePath){
        saveViewImage(filePath);
    }

    public void SaveElementScreenshotRetina(WebElement webElement, String filePath){
        saveElementScreenshotRetina(webElement, filePath);
    }

    public void SaveElementScreenshot(WebElement webElement, String filePath) {
        saveElementScreenshot(webElement, filePath);
    }

    public void EntirePageImage(String file, Integer timeout) {
        BufferedImage image = takeEntirePageImage(timeout);
        try{
            ImageIO.write(image, "PNG", new File(file));
        }catch (IOException e) {
            e.printStackTrace();
        }
    }











}
