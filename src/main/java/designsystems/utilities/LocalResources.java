/*
w * Copyright Progressive Leasing LLC.
 */
package designsystems.utilities;

import java.io.File;

/**
 * Commonly referenced resources for local test execution.
 * <p>
 * Change these values as you configure your machine.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class LocalResources {


    static File currentDirFile = new File(".");
    static String helper = currentDirFile.getAbsolutePath();
    static String currentDir = helper.substring(0, helper.length()-1);
    /**
     * Root path to your local repo.
     * <p>
     * You must update this one time according to where you
     * have cloned down this repo on your local machine.
     * <p>
     * I have left two recent examples from my own usage in here,
     * one WinTel and one Mac.
     * <p>
     * @return A String representing the base directory.
     */
    public static final String getPathToLocalRepo() {

        // WinTel example
//		return "C:" + File.separator +
//				"git-repositories" + File.separator +
//				"ecommerce.frontendtest" + File.separator;

        // MacOS example
		/*return File.separator +
				"Users" +
				File.separator +
				"jay.loew" + // use your name of course
				File.separator +
				"git-repositories" +
				File.separator +
				"ecom_selenium" + // and whatever your repo is if it's not this one
				File.separator;*/

        // Windows example
        String localRepoPath = currentDir + File.separator;
        return localRepoPath;
    }


    public static final String getPathToSrcTestResources() {
        return LocalResources.getPathToLocalRepo() +
                "src" +
                File.separator +
                "test" +
                File.separator +
                "resources" +
                File.separator;
    }


    /**
     * On your Mac, it is customary to let the installer put the
     * Firefox binary in /Applications. This method assumes that.
     * <p>
     * @return String representing full path to a Mac binary.
     */
    public static final String getPathChromeDriverMac() {
        return LocalResources.getPathToSrcTestResources() +
                "chromedriver";
    }

    /**
     * Reminder, you need Geckodriver for Firefox after Selenium 3.
     * <p>
     * @return String representing full path to a Mac binary.
     */
    public static final String getPathGeckoDriverMac() {
        return LocalResources.getPathToSrcTestResources() +
                "geckodriver";
    }


    /**
     * On WinTel, It is customary to override the installer's default and
     * relocate the binary to src/test/resources. This method assumes that.
     * <p>
     * @return String representing full path to a WinTel binary.
     */
    public static final String getPathGeckoDriverWindows() {
        return getPathToSrcTestResources() + "geckodriver.exe";
    }


    /**
     * On WinTel, It is customary to override the installer's default and
     * relocate the binary to src/test/resources. This method assumes that.
     * <p>
     * @return String representing full path to a WinTel binary.
     */
    public static final String getPathChromeDriverWindows() {
        return  getPathToSrcTestResources() + "chromedriver.exe";
    }
}
