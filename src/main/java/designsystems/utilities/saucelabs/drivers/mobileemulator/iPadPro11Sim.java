package designsystems.utilities.saucelabs.drivers.mobileemulator;

import designsystems.common.SauceLabsConstants;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * New driver class for a mobile emulator.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen</a>
 * @author <a href="mailto:Leo.Abner@progleasing.com">Leo</a>
 * @version 2.0
 */
public class iPadPro11Sim extends BaseDriverMobile {

	private MutableCapabilities sauceOptions;
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private ThreadLocal<String> sessionId = new ThreadLocal<>();

    /**
	 * In future it'll be an instance of MutableCapabilities but for now 
	 * every mobile emulator instance will require some dCaps.
	 */
	private DesiredCapabilities dCaps;


	/**
	 * Public no-args constructor sets several things in motion.
	 */
	public iPadPro11Sim() {

		/*
		 * This call needs to be here in the constructor; don't move it.
		 */
		this.dCaps = DesiredCapabilities.iphone(); // device-specific
	}

	

	
	public void instantiateMutableCapabilities() { 
		
		sauceOptions = new MutableCapabilities();

		/*
		 * Authenticate: merge authentication capabilities from superclass.
		 */
		sauceOptions.merge(getAuthenticationCapabilities());
		
		/*
		 * Tunnel: merge authentication capabilities from superclass.
		 */
		sauceOptions.merge(getTunnelCapabilities());
		
		/*
		 * Global timeouts: merge authentication capabilities from superclass.
		 */
		sauceOptions.merge(getGlobalTimeoutCapabilities());
		
		
		/*
		 * Name will appear in Sauce Labs Dashboard.
		 */
		sauceOptions.setCapability("name", getTestNameIn());
		
		/*
		 * Default video capture is true and can be overridden.
		 */
		sauceOptions.setCapability("recordVideo", getRecordVideo());
		
		/*
		 * Default screenshot capture is true and can be overridden.
		 */
		sauceOptions.setCapability("recordScreenshots", getRecordScreenshots());
		
		/*
		 * Default: video is captured when tests pass or fail.
		 * <p>
		 * Option: set this to false, in which case video is captured when tests fail.
		 */
		sauceOptions.setCapability("videoUploadOnPass", getVideoUploadOnPass());
		

		//		 ******* device-specific ********
		/*
		 * These settings are device-specific and as per 
		 * the Sauce Labs configurator. This is the heart 
		 * of this specific driver type. Don't change 
		 * these casually.
		 */
		sauceOptions.setCapability("appiumVersion", "1.13.0");
		sauceOptions.setCapability("deviceName","iPad Pro (11 inch) Simulator");
		sauceOptions.setCapability("deviceOrientation", "portrait");
		sauceOptions.setCapability("platformVersion","12.2");
		sauceOptions.setCapability("platformName", "iOS");
		sauceOptions.setCapability("browserName", "Safari");
        
        /*
         * To put a test into a build, set these values in the calling test:
         * 
         * driverClass.setUseBuild(true);
         * driverClass.setBuildName("SomeBuildName");
         */
        if(getUseBuild()) {
        	sauceOptions.setCapability("build", getBuildName());
        }
        
		/*
		 * Merge MutableCapabilities into DesiredCapabilities (required here).
		 */
		dCaps.merge(sauceOptions);
        
				
        try {
            webDriver.set(new RemoteWebDriver(new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA), dCaps));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        sessionId.set(((RemoteWebDriver)webDriver.get()).getSessionId().toString());

        // set current sessionId
        String id = ((RemoteWebDriver) getPreparedDriver()).getSessionId().toString();
        sessionId.set(id);
		
	}	

    /**
     * @return the {@link WebDriver} for the current thread
     */
    public WebDriver getPreparedDriver() {
        return webDriver.get();
    }
	
    
    
//    /**
//	 * Fetch a ready-to-use instance of WebDriver 
//	 * (more specifically: RemoteWebDriver) for 
//	 * use in the calling test.
//	 */
//	public WebDriver getPreparedDriver() { 
//
//		/*
//		 * In future, so we're told, most arguments will 
//		 * be MutableCapabilities, and DesiredCapabilities 
//		 * will be deprecated. Getting ready for that day....
//		 */
//		MutableCapabilities mCaps = new MutableCapabilities();
//		
//		
//		/*
//		 * Authenticate: merge authentication capabilities from superclass.
//		 */
//		mCaps.merge(getAuthenticationCapabilities());
//		
//		/*
//		 * Tunnel: merge authentication capabilities from superclass.
//		 */
//		mCaps.merge(getTunnelCapabilities());
//		
//		/*
//		 * Global timeouts: merge authentication capabilities from superclass.
//		 */
//		mCaps.merge(getGlobalTimeoutCapabilities());
//		
//		
//		/*
//		 * Name will appear in Sauce Labs Dashboard.
//		 */
//		mCaps.setCapability("name", getTestNameIn());
//		
//		/*
//		 * Default video capture is true and can be overridden.
//		 */
//		mCaps.setCapability("recordVideo", getSaveVideo());
//		
//		
//
//		//		 ******* device-specific ********
//		/*
//		 * These settings are device-specific and as per 
//		 * the Sauce Labs configurator. This is the heart 
//		 * of this specific driver type. Don't change 
//		 * these casually.
//		 */
//		mCaps.setCapability("appiumVersion", "1.9.1");
//		mCaps.setCapability("deviceName","Google Pixel C GoogleAPI Emulator");
//		mCaps.setCapability("deviceOrientation", "portrait");
//		mCaps.setCapability("browserName", "Chrome");
//		mCaps.setCapability("platformVersion", "8.1");
//		mCaps.setCapability("platformName","Android");		
//		
//		/*
//		 * Merge MutableCapabilities into DesiredCapabilities (required here).
//		 */
//		dCaps.merge(mCaps);
//		
//		/*
//		 * This URL determines which Sauce Labs data center your 
//		 * test runs in. Default is and should be USA. If need 
//		 * be, you can override it in your downstream test with 
//		 * a call to a setter method. See the superclass for details.
//		 */
//		URL toDeviceCloud = null;
//		try { 
//			toDeviceCloud = new URL(getDataCenter());
//		}catch (MalformedURLException mUE) {
//			System.out.println(getTestNameIn() + ":\tError creating new URL from input");
//			throw new IllegalStateException(getTestNameIn(), mUE);
//		}
//
//		
//		return new RemoteWebDriver(toDeviceCloud,dCaps);
//	}

}
