package designsystems.utilities.saucelabs.drivers.mobileemulator;

import designsystems.common.SauceLabsConstants;
import org.openqa.selenium.MutableCapabilities;
import designsystems.utilities.saucelabs.DataCenter;

/**
 * Base class for experimental drivers for new mobile emulators.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class BaseDriverMobile {

    public BaseDriverMobile() {
		// default no args
	}
	
	private String buildName = "";    // leave it empty by default
	private boolean useBuild = false; // leave it false by default
	/**
	 * Pass in a build name that will appear in the builds tab. Builds are optional. 
	 * You don't have to organize all tests into builds.
	 * <p>
	 * If you decide to use a build, you should name it. An unnamed build in your 
	 * builds tab isn't helpful.
	 * <p>
	 * WARNING: Before you enable builds, you should know that builds 
	 * cannot be deleted from your dashboard. This is a known limitation 
	 * of Sauce Labs. More info here:<BR>
	 * https://saucelabs.ideas.aha.io/ideas/DEVT-I-47
	 * <p>
	 * @param buildName A String: name of build you want your downstream tests to be in.
	 */
	public void setBuildName(String buildName) { 
		this.buildName = buildName;
	}
	public String getBuildName() { 
		return this.buildName;
	}
	public void setUseBuild(boolean useBuild) { 
		this.useBuild = useBuild;
	}
	public boolean getUseBuild() { 
		return this.useBuild;
	}
	
	/**
	 * Data Center is and should be USA by default, can be overridden below.
	 */
	private DataCenter dataCenter = DataCenter.USA;
	

	/**
	 * The "master switch" for video capture.
	 * <p>
	 * Default true: video will be captured.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean recordVideo = true;
	public void setRecordVideo(boolean saveVideo) { 
		this.recordVideo = saveVideo;
	}
	protected boolean getRecordVideo() { 
		return this.recordVideo;
	}	

	/**
	 * Works in conjunction with recordVideo.
	 * <p>
	 * True: video captured for all tests pass or fail.
	 * <p>
	 * False: video for passing tests not captured.
	 * <p>
	 * You may wish to same time and storage by suppressing video when your tests are passing.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean videoUploadOnPass = false;
	public void setVideoUploadOnPass(boolean videoUploadOnPass) { 
    	this.videoUploadOnPass = videoUploadOnPass;
    }
	public boolean getVideoUploadOnPass() { 
    	return this.videoUploadOnPass;
    }
	
	/**
	 * Sauce Labs always captures selenium-server.log for all tests. This is invariable.
	 * <p>
	 * Sauce Labs always captures automator.log for tests run in the Firefox drivers (Mac and WinTel). This too is invariable.
	 * <p>
	 * A third log, the JSON log, is captured by default, but can be suppressed.
	 * <p>
	 * Default true: log.json is captured.
	 * <p>
	 * False: log.json is not captured.
	 * <p>
	 * Again, this has no effect on selenium-server.log or automator.log.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean recordLogs = true;
	public void setRecordLogs(boolean recordLogs) { 
    	this.recordLogs = recordLogs;
    }
	public boolean getRecordLogs() { 
    	return this.recordLogs;
    }
	
	/**
	 * Sauce Labs can capture screenshots as your test runs, and regardless of pass-fail.
	 * <p>
	 * This capture happens by default, but can be suppressed.
	 * <p>
	 * Default true: screenshots are captured.
	 * <p>
	 * False: screenshots are not captured.
	 * <p>
	 * These are the screenshots that will (or not) appear in the METADATA tab.
	 * This has nothing to do with native Selenium screen capture.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean recordScreenshots = true;
	public void setRecordScreenshots(boolean recordScreenshots) { 
    	this.recordScreenshots = recordScreenshots;
    }
	public boolean getRecordScreenshots() { 
    	return this.recordScreenshots;
    }		
	
	/**
	 * Normally passed in from your downstream test but 
	 * not strictly required; see below.
	 */
	private String testNameIn;
	public void setTestNameIn(String testNameIn) { 
    	this.testNameIn = testNameIn;
    }
	public String getTestNameIn() { 
    	return this.testNameIn;
    }

	/**
	 * If you omit the data center (and you can), expect to 
	 * be on the USA data center. Or override it here if 
	 * you want the EU data center. Default is and should 
	 * be USA.
	 * <p>
	 * @param nameOfCallingTest - The test name will appear in your Dashboard.
	 */
	public void setDataCenter(DataCenter dataCenter) { 
		this.dataCenter = dataCenter;
	}
	public String getDataCenter() { 
		String toReturn = null;
		if(dataCenter.equals(DataCenter.USA)) { 
			toReturn =  SauceLabsConstants.URL_DEVICE_CLOUD_USA;
		}
		if(dataCenter.equals(DataCenter.EU)) { 
			toReturn = SauceLabsConstants.URL_DEVICE_CLOUD_EU;
		}
		return toReturn;
	}	
	
	/**
	 * Your Sauce Labs username and access key as already defined in your system variables.
	 */
	protected MutableCapabilities getAuthenticationCapabilities() {
		MutableCapabilities mCaps = new MutableCapabilities();
		mCaps.setCapability("username", System.getenv("SAUCE_USERNAME"));
		mCaps.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));	
		return mCaps;
	}
	
	/**
	 * This Sauce Labs test will try to run in this tunnel, so make 
	 * sure it's open first.
	 * <p>
	 * If this tunnel isn't open when your test begins, your test 
	 * will fail from the setup method in a recognizable way. Start 
	 * up your tunnel, and try again.
	 */
	protected MutableCapabilities getTunnelCapabilities() {
		MutableCapabilities mCaps = new MutableCapabilities();
		mCaps.setCapability("tunnelIdentifier", System.getenv("SAUCE_TUNNEL"));
		return mCaps;
	}
	
	/**
	 * The next three values are defensive, and appropriate for most of our testing. 
	 * <p>
	 * These values can be overridden, but please be careful. And do it on your 
	 * own branch. 
	 * <p>
	 * Whatever these values are set to, they will apply to all tests running 
	 * in Sauce Labs.
	 * <p>
	 * See the JavaDoc for these values in SauceLabsConstants.java.
	 * <p>
	 * @see src/main/java/designsystems.common/SauceLabsConstants.java
	 */
	protected MutableCapabilities getGlobalTimeoutCapabilities() {
		MutableCapabilities mCaps = new MutableCapabilities();
		mCaps.setCapability("commandTimeout", SauceLabsConstants.COMMAND_TIMEOUT);
		mCaps.setCapability("idleTimeout", SauceLabsConstants.IDLE_TIMEOUT);
		mCaps.setCapability("maxDuration", SauceLabsConstants.MAX_DURATION);
		return mCaps;
	}

}
