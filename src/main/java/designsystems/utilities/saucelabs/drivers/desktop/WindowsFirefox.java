package designsystems.utilities.saucelabs.drivers.desktop;

import designsystems.common.SauceLabsConstants;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WindowsFirefox extends BaseDriverDesktop {

	private MutableCapabilities sauceOptions;
	private FirefoxOptions browserOptions;
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private ThreadLocal<String> sessionId = new ThreadLocal<>();
	
    public WindowsFirefox() {
		// default no args constructor
	}
    
	public void instantiateMutableCapabilities() { 
		
		sauceOptions = new MutableCapabilities();

		/*
		 * Authenticate: merge authentication capabilities from superclass.
		 */
		sauceOptions.merge(getAuthenticationCapabilities());

		/*
		 * Tunnel: merge tunnel capabilities from superclass.
		 */
		sauceOptions.merge(getTunnelCapabilities());
		
		sauceOptions.setCapability("recordVideo", getRecordVideo());
		sauceOptions.setCapability("videoUploadOnPass", getVideoUploadOnPass());
		sauceOptions.setCapability("recordLogs", getRecordLogs());
		sauceOptions.setCapability("recordScreenshots", getRecordScreenshots());
        sauceOptions.setCapability("seleniumVersion", "3.141.59");
        sauceOptions.setCapability("name", getTestNameIn());
        
        /*
         * To put a test into a build, set these values in the calling test:
         * 
         * driverClass.setUseBuild(true);
         * driverClass.setBuildName("SomeBuildName");
         * 
         * BUT BEFORE YOU DO, wait for Sauce Labs to fix this problem:
         * https://saucelabs.ideas.aha.io/ideas/DEVT-I-47
         */
        if(getUseBuild()) {
        	sauceOptions.setCapability("build", getBuildName());
        }
		
		browserOptions = new FirefoxOptions();
		
		browserOptions.setCapability("platformName", "Windows 10");
		browserOptions.setCapability("browserVersion", "latest-1");
		browserOptions.setCapability("sauce:options", sauceOptions);
		
		try {
			webDriver.set(new RemoteWebDriver(new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA), browserOptions));
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		sessionId.set(((RemoteWebDriver)webDriver.get()).getSessionId().toString());

		// set current sessionId
		String id = ((RemoteWebDriver) getPreparedDriver()).getSessionId().toString();
		sessionId.set(id);
	}

	/**
	 * @return the {@link WebDriver} for the current thread
	 */
	public WebDriver getPreparedDriver() {
		return webDriver.get();
	}
}
