package designsystems.utilities.saucelabs.drivers.desktop;

import designsystems.common.SauceLabsConstants;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WindowsEdge extends BaseDriverDesktop {private MutableCapabilities sauceOptions;
    private EdgeOptions browserOptions;
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private ThreadLocal<String> sessionId = new ThreadLocal<>();

    public WindowsEdge() {
        // default no args constructor
    }

    public void instantiateMutableCapabilities() {

        sauceOptions = new MutableCapabilities();

        /*
         * Authenticate: merge authentication capabilities from superclass.
         */
        sauceOptions.merge(getAuthenticationCapabilities());

        /*
         * Tunnel: merge tunnel capabilities from superclass.
         */
        sauceOptions.merge(getTunnelCapabilities());

        sauceOptions.setCapability("recordVideo", getRecordVideo());
        sauceOptions.setCapability("videoUploadOnPass", getVideoUploadOnPass());
        sauceOptions.setCapability("recordLogs", getRecordLogs());
        sauceOptions.setCapability("recordScreenshots", getRecordScreenshots());
        sauceOptions.setCapability("seleniumVersion", "3.141.59");
        sauceOptions.setCapability("name", getTestNameIn());

        /*
         * To put a test into a build, set these values in the calling test:
         *
         * driverClass.setUseBuild(true);
         * driverClass.setBuildName("SomeBuildName");
         *
         * BUT BEFORE YOU DO, wait for Sauce Labs to fix this problem:
         * https://saucelabs.ideas.aha.io/ideas/DEVT-I-47
         */
        if(getUseBuild()) {
            sauceOptions.setCapability("build", getBuildName());
        }

        browserOptions = new EdgeOptions();

        browserOptions.setCapability("platformName", "Windows 10");
        browserOptions.setCapability("browserVersion", "latest-2");
        browserOptions.setCapability("sauce:options", sauceOptions);

        try {
            webDriver.set(new RemoteWebDriver(new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA), browserOptions));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        sessionId.set(((RemoteWebDriver)webDriver.get()).getSessionId().toString());

        // set current sessionId
        String id = ((RemoteWebDriver) getPreparedDriver()).getSessionId().toString();
        sessionId.set(id);
    }

    /*
     * Leave this commented out method in here please thank you.
     */
    //		public WebDriver getPreparedDriver() {
    //			try {
    //				driver = new RemoteWebDriver(new URL(URL), browserOptions);
    //			} catch (MalformedURLException mUE) {
    //				System.out.println(getClass().getSimpleName() + ": ERROR: " + mUE.getClass().getSimpleName());
    //			}
    //			return driver;
    //		}

    /**
     * @return the {@link WebDriver} for the current thread
     */
    public WebDriver getPreparedDriver() {
        return webDriver.get();
    }
}