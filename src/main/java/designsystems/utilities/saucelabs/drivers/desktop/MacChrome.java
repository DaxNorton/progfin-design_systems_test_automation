package designsystems.utilities.saucelabs.drivers.desktop;

import designsystems.common.SauceLabsConstants;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class MacChrome extends BaseDriverDesktop {

	private MutableCapabilities sauceOptions;
	private ChromeOptions browserOptions;
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();
    private ThreadLocal<String> sessionId = new ThreadLocal<>();
	
    public MacChrome() {
		// default no args constructor
	}

	public void instantiateMutableCapabilities() { 
		
		sauceOptions = new MutableCapabilities();
		
		/*
		 * Authenticate: merge authentication capabilities from superclass.
		 */
		sauceOptions.merge(getAuthenticationCapabilities());

		/*
		 * Tunnel: merge authentication capabilities from superclass.
		 */
		sauceOptions.merge(getTunnelCapabilities());
		
		sauceOptions.setCapability("recordVideo", getRecordVideo());
		sauceOptions.setCapability("videoUploadOnPass", getVideoUploadOnPass());
		sauceOptions.setCapability("recordLogs", getRecordLogs());
		sauceOptions.setCapability("recordScreenshots", getRecordScreenshots());

		/* Leave the next few commented lines in here please thank you. */
//		sauceOptions.setCapability("tunnelIdentifier", System.getenv("SAUCE_TUNNEL"));
//      sauceOptions.setCapability("username", System.getenv("SAUCE_USERNAME"));
//      sauceOptions.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));

		sauceOptions.setCapability("seleniumVersion", "3.141.59");
        sauceOptions.setCapability("name", getTestNameIn());
        
        /*
         * To put a test into a build, set these values in the calling test:
         * 
         * driverClass.setUseBuild(true);
         * driverClass.setBuildName("SomeBuildName");
         * 
         * BUT BEFORE YOU DO, wait for Sauce Labs to fix this problem:
         * https://saucelabs.ideas.aha.io/ideas/DEVT-I-47
         */
        if(getUseBuild()) {
        	sauceOptions.setCapability("build", getBuildName());
        }
		
		browserOptions = new ChromeOptions();
		
		browserOptions.setExperimentalOption("w3c", true);

		browserOptions.setCapability("platformName", "macOS 10.13");
		browserOptions.setCapability("browserVersion", "77.0");
		browserOptions.setCapability("sauce:options", sauceOptions);
		
        try {
            webDriver.set(new RemoteWebDriver(new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA), browserOptions));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        sessionId.set(((RemoteWebDriver)webDriver.get()).getSessionId().toString());

        // set current sessionId
        String id = ((RemoteWebDriver) getPreparedDriver()).getSessionId().toString();
        sessionId.set(id);
		
	}

    /**
     * @return the {@link WebDriver} for the current thread
     */
    public WebDriver getPreparedDriver() {
        return webDriver.get();
    }
}
