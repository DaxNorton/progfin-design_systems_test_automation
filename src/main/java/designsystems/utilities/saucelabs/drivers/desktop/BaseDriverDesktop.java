package designsystems.utilities.saucelabs.drivers.desktop;

import org.openqa.selenium.MutableCapabilities;

public class BaseDriverDesktop {

	public static final String URL = "http://" + System.getenv("SAUCE_USERNAME") + ":" + System.getenv("SAUCE_ACCESS_KEY") + "@ondemand.saucelabs.com:80/wd/hub";	
	
	private String buildName = "";    // leave it empty by default
	private boolean useBuild = false; // leave it false by default
	/**
	 * Pass in a build name that will appear in the builds tab. Builds are optional. You don't have to organize all tests into builds.
	 * <p>
	 * If you decide to use a build, you should name it. An unnamed build in your builds tab isn't helpful.
	 * <p>
	 * WARNING: Before you enable builds, you should know that builds 
	 * cannot be deleted from your dashboard. This is a known limitation 
	 * of Sauce Labs and is in the queue to be fixed.
	 * <p>
	 * @param buildName A String: name of build you want your downstream tests to be in.
	 */
	public void setBuildName(String buildName) { 
		this.buildName = buildName;
	}
	public String getBuildName() { 
		return this.buildName;
	}
	public void setUseBuild(boolean useBuild) { 
		this.useBuild = useBuild;
	}
	public boolean getUseBuild() { 
		return this.useBuild;
	}
	
	
	public BaseDriverDesktop() {
		System.out.println(getClass().getSimpleName() + " (Sauce) constructor....");
	}

	/**
	 * The "master switch" for video capture.
	 * <p>
	 * Default true: video will be captured.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean recordVideo = true;
	public void setRecordVideo(boolean recordVideo) { 
    	this.recordVideo = recordVideo;
    }
	public boolean getRecordVideo() { 
    	return this.recordVideo;
    }
	
	/**
	 * Works in conjunction with recordVideo.
	 * <p>
	 * Default true: video captured for all tests pass or fail.
	 * <p>
	 * False: video for passing tests not captured.
	 * <p>
	 * You may wish to same time and storage by suppressing video when your tests are passing.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean videoUploadOnPass = true;
	public void setVideoUploadOnPass(boolean videoUploadOnPass) { 
    	this.videoUploadOnPass = videoUploadOnPass;
    }
	public boolean getVideoUploadOnPass() { 
    	return this.videoUploadOnPass;
    }
	
	/**
	 * Sauce Labs always captures selenium-server.log for all tests. This is invariable.
	 * <p>
	 * Sauce Labs always captures automator.log for tests run in the Firefox drivers (Mac and WinTel). This too is invariable.
	 * <p>
	 * A third log, the JSON log, is captured by default, but can be suppressed.
	 * <p>
	 * Default true: log.json is captured.
	 * <p>
	 * False: log.json is not captured.
	 * <p>
	 * Again, this has no effect on selenium-server.log or automator.log.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean recordLogs = true;
	public void setRecordLogs(boolean recordLogs) { 
    	this.recordLogs = recordLogs;
    }
	public boolean getRecordLogs() { 
    	return this.recordLogs;
    }
	
	/**
	 * Sauce Labs can capture screenshots as your test runs, and regardless of pass-fail.
	 * <p>
	 * This capture happens by default, but can be suppressed.
	 * <p>
	 * Default true: screenshots are captured.
	 * <p>
	 * False: screenshots are not captured.
	 * <p>
	 * These are the screenshots that will (or not) appear in the METADATA tab.
	 * This has nothing to do with native Selenium screen capture.
	 * <p>
	 * Variable name equals the name of the switch in Sauce Labs configuration.
	 */
	private boolean recordScreenshots = true;
	public void setRecordScreenshots(boolean recordScreenshots) { 
    	this.recordScreenshots = recordScreenshots;
    }
	public boolean getRecordScreenshots() { 
    	return this.recordScreenshots;
    }	
	
	private String testNameIn;
	public void setTestNameIn(String testNameIn) { 
    	this.testNameIn = testNameIn;
    }
	public String getTestNameIn() { 
    	return this.testNameIn;
    }

	
	/**
	 * Your Sauce Labs username and access key as already defined in your system variables.
	 */
	protected MutableCapabilities getAuthenticationCapabilities() { 
		MutableCapabilities mCaps = new MutableCapabilities();
		mCaps.setCapability("username", System.getenv("SAUCE_USERNAME"));
		mCaps.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));	
		return mCaps;
	}
	
	/**
	 * This Sauce Labs test will try to run in this tunnel, so make 
	 * sure it's open first.
	 * <p>
	 * If this tunnel isn't open when your test begins, your test 
	 * will fail from the setup method in a recognizable way. Start 
	 * up your tunnel, and try again.
	 */
	protected MutableCapabilities getTunnelCapabilities() { 
		MutableCapabilities mCaps = new MutableCapabilities();
		mCaps.setCapability("tunnelIdentifier", System.getenv("SAUCE_TUNNEL"));
		return mCaps;
	}
}
