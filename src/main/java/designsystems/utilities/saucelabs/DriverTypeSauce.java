package designsystems.utilities.saucelabs;

/**
 * Currently supported OS-browser combinations for front end 
 * testing in Sauce Labs. This collection is expected to change 
 * over time.
 * <p>
 * "RD" appended == real device in the Sauce Labs device cloud, not 
 * a mobile emulator.
 * <p>
 * Version 1.0 2019-03-14
 * <p>
 * Version 1.1 2019-03-27
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.1
 */
public enum DriverTypeSauce {
	
	// desktop
	WINDOWS_CHROME,
	WINDOWS_FIREFOX,
	MAC_CHROME,
	MAC_FIREFOX,
	MAC_SAFARI,
	
	// mobile simulator
	IPHONE_6SP_SIM,
	IPHONE_8_SIM,
	IPHONE_X_SIM,
	SAMSUNG_GALAXY_S7_SIM,
	GOOGLE_PIXEL_C_SIM,
	
	// real device
	IPHONE_6SP_RD,
	IPHONE_8_PLUS_RD,
	IPAD_AIR_3_RD,
	GLXY_NOTE_8_RD, 
	GLXY_A5_RD, 
	GGL_PXL_3_XL_RD, 
}
