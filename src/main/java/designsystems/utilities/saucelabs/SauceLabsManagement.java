package designsystems.utilities.saucelabs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

/*
 * A class for accessing and deleting jobs using the Sauce Labs Job API
 * 
 */

/**
 * Delete a job or jobs from your Sauce Labs dashboard.
 * <p>
 * From Maven command line:
 * <p>
 * <pre>mvn exec:java@sauce-delete</pre> to delete N jobs where N is the argument currently defined in the sauce-delete profile in pom.xml</p>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 */
public class SauceLabsManagement {
	private static String searchQuery = "https://saucelabs.com/rest/v1/%s/jobs?%s";
	private static String deleteQuery = "https://saucelabs.com/rest/v1/%s/jobs/%s";
	private static final String deleteVerb = "DELETE";
	private static final String getVerb = "GET";

	public static void deleteJobsByLimit(int limit) {
		String sauceUsername = System.getenv("SAUCE_USERNAME");

		// If the environment value doesn't exist we can't continue
		if (sauceUsername == null) {
			throw new IllegalArgumentException("\ndeleteJobsByLimit() call failed, sauceUsername not found");
		}

		// TODO: String.format is not the optimal solution, need to refactor. (Slower
		// performance)
		String fullSearchQuery = String.format(searchQuery, sauceUsername, "limit=" + limit);

		String jsonResponse = executeApiCall(fullSearchQuery, getVerb);

		// Convert the array object that comes back from the ApiCall into a generic
		// array
		// I didn't want to create models to deserialize
		JSONArray jsonArray = new JSONArray(jsonResponse);

		for (int i = 0; i < jsonArray.length(); i++) {
			// This is a generic object that can be used to grab properties out of
			JSONObject jsonJobRecord = jsonArray.getJSONObject(i);
			String jobId = jsonJobRecord.optString("id");

			deleteJobById(jobId);
			System.out.println("\ndeleteJobsByLimit() Deleted Sauce JobId: " + jobId);
		}
	}

	/*
	 * Delete a sauce labs job by the given id
	 * 
	 * @param jobId a string that represents the job Id (Formatted as a string GUID)
	 * 
	 */
	public static void deleteJobById(String jobId) {
		String sauceUsername = System.getenv("SAUCE_USERNAME");

		if (sauceUsername == null) {
			throw new IllegalArgumentException("\ndeleteJobsById() call failed, sauceUsername not found");
		}

		String fullDeleteQuery = String.format(deleteQuery, sauceUsername, jobId);

		executeApiCall(fullDeleteQuery, deleteVerb);
		System.out.println("\ndeleteJobsById Deleting Sauce JobId: " + jobId);
	}

	/*
	 * A Generic method for consuming sauce labs API
	 * 
	 * @param requestUrl the SauceLabs url for the operation you are performing
	 * 
	 * @param verb the http verb for the operation you are performing, e.g PUT,
	 * POST, DELETE, GET
	 * 
	 * @response a json formatted string
	 */
	private static String executeApiCall(String requestUrl, String verb) {
		String sauceAccessKey = System.getenv("SAUCE_ACCESS_KEY");
		String sauceUsername = System.getenv("SAUCE_USERNAME");

		if (sauceUsername == null || sauceAccessKey == null) {
			throw new IllegalArgumentException(
					"\nexecuteApiCall() call failed, sauceUsername or sauceAccessKey not found");
		}

		HttpURLConnection connection = null;
		String responseBody = "";

		try {
			URL url = new URL(requestUrl);
			connection = (HttpURLConnection) url.openConnection();

			// Headers and Request values
			connection.setRequestMethod(verb);
			connection.setRequestProperty("Content-Type", "application/json");

			String encodedCreds = Base64.getEncoder().encodeToString(
					(String.format("%s:%s", sauceUsername, sauceAccessKey)).getBytes(StandardCharsets.UTF_8));

			connection.setRequestProperty("Authorization", "Basic " + encodedCreds);

			int responseCode = connection.getResponseCode();
			System.out.println("\nexecuteApiCall() Response Code: " + responseCode);

			BufferedReader buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = buffer.readLine()) != null) {
				sb.append(line + "\n");
			}

			buffer.close();
			responseBody = sb.toString();
		} catch (ProtocolException e) {
			System.out.println(
					"\nexecuteApiCall() failed. An internal connection error has occurred while accessing sauce labs api");
			e.printStackTrace();
		} catch (MalformedURLException e) {
			System.out.println(
					"\nexecuteApiCall() failed. The sauce labs URL specified is formmatted incorrectly. RequestURL: "
							+ requestUrl);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("\nexecuteApiCall() failed. The saucelabs api appears to be having issues: RequestURL: "
					+ requestUrl);
			e.printStackTrace();
		}

		finally {
			// Ensure the connection isn't already null before closing
			if (connection != null) {
				connection.disconnect();
			}
		}

		return responseBody;
	}

	/*
	 * Main method for executing deletions inside of Sauce Labs or at command line. 
	 * 
	 * Maven command line: mvn exec:java@sauce-delete
	 * 
	 * Will delete whatever number is set in pom.xml; search designsystems.utilities.SauceLabsManagement.
	 * 
	 * Note: Please ensure to check the plugin for parameters passed here
	 * 
	 */
	public static void main(String[] args) {

		 // Check if we are deleting by jobId
		 int deleteByIdIndexValue = Arrays.binarySearch(args, DeleteByIdArg);
		 
		 if(deleteByIdIndexValue > -1) {
			if(args[1] != null) {
				deleteJobById(args[1]);
			} else {
				System.out.println("\nFailed deleting job by Id, you must specify the ID");
			}
		 }
		 
		 // Check if we are deleting by limit
		 int deleteByLimitIndexValue =  Arrays.binarySearch(args, DeleteByLimitArg);
		 
		 if(deleteByLimitIndexValue > -1) {
				if(args[1] != null) {
					try {
						int limitValue = Integer.parseInt(args[1]);
						deleteJobsByLimit(limitValue);
					} catch(NumberFormatException ex) {
						System.out.println("\nFailed deleting jobs by limit, the limit was not a valid number");
					}
				
				} else {
					System.out.println("\nFailed deleting jobs by limit, you must specify the limit you want");
				}
			 }
		 
		System.out.println("\nRemoval of Sauce labs Jobs finished");
	}

	private static String DeleteByIdArg = "-id";
	private static String DeleteByLimitArg = "-limit";
}
