package designsystems.utilities.saucelabs;

/**
 * SauceLabs has data centers in the USA and in the EU.
 * <p>
 * You'll need to pick one when you instantiate a driver.
 * <p>
 * Then the code in CommonDriversSauce.java will do what's needed 
 * based on your selection (your VMs will be allocated from the 
 * data center of your choice).
 * <p>
 * Version 1.0 2019-03-14
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.0
 * @see CommonDriverSauceDesktop.java
 */
public enum DataCenter { 
	USA,USA_RD,EU;
}

