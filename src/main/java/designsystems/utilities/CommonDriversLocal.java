/*
 * Copyright Progressive Leasing LLC.
 */
package designsystems.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Simplify local driver instantiation by driver type on Mac or Windows.
 * <p>
 * Reminder: Local drivers are, by definition, local. You need to have
 * the corresponding browser installed on your machine, i.e. if you
 * want a ChromeDriver instance, you need Chrome on your machine.
 * <p>
 * Version 1.0 - 2019-01-04
 * <p>
 * Version 1.1 - 2019-01-07 - Additional driver types (headless).
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * //@see CommonDriversLocalTests.java
 */
public class CommonDriversLocal {

    private WebDriver driverToReturn;
    private DriverTypeLocal localDriverType;

    public CommonDriversLocal(DriverTypeLocal type) {

        this.localDriverType = type;

        if(OperatingSystemDetector.isUnix()) {
            throw new IllegalArgumentException("Linux support pending. Stay tuned...");
        } else if(OperatingSystemDetector.isSolaris()) {
            throw new IllegalArgumentException("Solaris is not supported.");
        }
    }

    public WebDriver getPreparedDriver() {

        switch(this.localDriverType) {

            case CHROME : {

                if(OperatingSystemDetector.isMac()) {
                    System.setProperty("webdriver.chrome.driver", LocalResources.getPathChromeDriverMac());
                    driverToReturn = new ChromeDriver();
                } else if(OperatingSystemDetector.isWindows()) {
                    System.setProperty("webdriver.chrome.driver", (LocalResources.getPathToSrcTestResources() + "chromedriver.exe") );
                    driverToReturn = new ChromeDriver();
                }

                break;
            }

            case CHROME_HEADLESS : {

                if(OperatingSystemDetector.isMac()) {
                    System.setProperty("webdriver.chrome.driver", LocalResources.getPathChromeDriverMac());
                    ChromeOptions cOpts = new ChromeOptions();
                    cOpts.addArguments("headless");
                    driverToReturn = new ChromeDriver(cOpts);
                } else if(OperatingSystemDetector.isWindows()) {
                    System.setProperty("webdriver.chrome.driver", (LocalResources.getPathToSrcTestResources() + "chromedriver.exe") );
                    ChromeOptions cOpts = new ChromeOptions();
                    cOpts.addArguments("headless");
                    driverToReturn = new ChromeDriver(cOpts);
                }

                break;
            }

            case FIREFOX : {

                if(OperatingSystemDetector.isMac()) {
                    System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverMac());
                    driverToReturn = new FirefoxDriver();
                } else if(OperatingSystemDetector.isWindows()) {
                    FirefoxProfile geoDisabled = new FirefoxProfile();
                    geoDisabled.setPreference("geo.enabled", false);
                    geoDisabled.setPreference("geo.provider.use_corelocation", false);
                    geoDisabled.setPreference("geo.prompt.testing", false);
                    geoDisabled.setPreference("geo.prompt.testing.allow", false);
                    DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                    capabilities.setCapability(FirefoxDriver.PROFILE, geoDisabled);

                    System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverWindows());
                    driverToReturn = new FirefoxDriver(capabilities);
                }

                break;
            }


            case FIREFOX_HEADLESS : {

                if(OperatingSystemDetector.isMac()) {
                    FirefoxBinary fBin = new FirefoxBinary();
                    fBin.addCommandLineOptions("--headless");
                    System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverMac());
                    FirefoxOptions fOpts = new FirefoxOptions();
                    fOpts.setBinary(fBin);
                    driverToReturn = new FirefoxDriver(fOpts);
                } else if(OperatingSystemDetector.isWindows()) {
                    FirefoxBinary fBin = new FirefoxBinary();
                    fBin.addCommandLineOptions("--headless");
                    System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverWindows());
                    FirefoxOptions fOpts = new FirefoxOptions();
                    fOpts.setBinary(fBin);
                    System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverWindows());
                    driverToReturn = new FirefoxDriver();
                }

                break;
            }


        }

        return driverToReturn;



    }

}
