package designsystems.utilities.drivers.local;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import designsystems.utilities.LocalResources;
import designsystems.utilities.OperatingSystemDetector;

/**
 * Local driver.
 * <p>
 * For Sauce Labs drivers, see designsystems.utilities.saucelabs.drivers.desktop
 * <p>
 * Version 1.0 2019-10-14
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class LocalFirefoxHeadless {

    private FirefoxDriver webDriver;
    private static final String SMALLMOBILE = "smallmobile";
    private static final String MOBILEVIEWPORT = "mobile";
    private static final String TABLETVIEWPORT = "tablet";
    public static final String GECKO_PROPERTY = "webdriver.gecko.driver";

    public LocalFirefoxHeadless() {
        checkOperatingSystem();
        instantiate();
    }

    public LocalFirefoxHeadless(String viewport) {
        checkOperatingSystem();
        instantiate(viewport);
    }

    private void checkOperatingSystem() {
        if(OperatingSystemDetector.isUnix()) {
            throw new IllegalArgumentException("Linux support pending. Stay tuned...");
        } else if(OperatingSystemDetector.isSolaris()) {
            throw new IllegalArgumentException("Solaris is not supported.");
        } else if(OperatingSystemDetector.isWindows()) {
            throw new IllegalArgumentException("This is for the Mac driver.");
        }
    }

    private void instantiate() {
        FirefoxBinary fBin = new FirefoxBinary();
        fBin.addCommandLineOptions("--headless");
        System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverMac());
        FirefoxOptions fOpts = new FirefoxOptions();
        fOpts.setBinary(fBin);
        webDriver = new FirefoxDriver(fOpts);
        webDriver.manage().window().setSize(new Dimension(1630,939));
    }

    private void instantiate(String viewport) {
        FirefoxBinary fBin = new FirefoxBinary();
        fBin.addCommandLineOptions("--headless");
        System.setProperty("webdriver.gecko.driver", LocalResources.getPathGeckoDriverMac());
        FirefoxOptions fOpts = new FirefoxOptions();
        fOpts.setBinary(fBin);
        if (viewport == MOBILEVIEWPORT){
        webDriver = new FirefoxDriver(fOpts);
        webDriver.manage().window().setSize(new Dimension(767,850));
        }else if(viewport == SMALLMOBILE) {
            webDriver = new FirefoxDriver(fOpts);
            webDriver.manage().window().setSize(new Dimension(299, 850));
        }else if(viewport == TABLETVIEWPORT){
            webDriver = new FirefoxDriver(fOpts);
            webDriver.manage().window().setSize(new Dimension(1024,850));
        }
    }

    /**
     * @return the {@link WebDriver} for the current thread
     */
    public WebDriver getPreparedDriver() {
        final String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        String messageLocal = "\n" + methodNameLocal + ": returning local driver....\n";
        System.out.println(messageLocal);
        return webDriver;
    }

}
