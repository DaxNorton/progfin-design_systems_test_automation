package designsystems.utilities.drivers.local;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.chrome.ChromeOptions;
import designsystems.utilities.LocalResources;
import designsystems.utilities.OperatingSystemDetector;

/**
 * Local driver.
 * <p>
 * For Sauce Labs drivers, see designsystems.utilities.saucelabs.drivers.desktop
 * <p>
 * Version 1.0 2019-10-14
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class LocalChrome {


    private ChromeDriver webDriver;
    public final String CHROME_PROPERTY = "webdriver.chrome.driver";

    public LocalChrome() {
        checkOperatingSystem();
        setProperty();
        instantiate();
    }

    private void checkOperatingSystem() {
        if(OperatingSystemDetector.isUnix()) {
            throw new IllegalArgumentException("Linux support pending. Stay tuned...");
        } else if(OperatingSystemDetector.isSolaris()) {
            throw new IllegalArgumentException("Solaris is not supported.");
        }
    }

    private boolean isWindows(){
        return OperatingSystemDetector.isWindows();
    }

    private void setProperty() {
        if (isWindows()){
            System.setProperty(CHROME_PROPERTY, LocalResources.getPathChromeDriverWindows());
        }else{
            System.setProperty(CHROME_PROPERTY, LocalResources.getPathChromeDriverMac());
        }
    }

    private void instantiate() {
        ChromeOptions cOpts = new ChromeOptions();
        cOpts.addArguments("window-size=1920,1080");
        cOpts.addArguments("headless");
        webDriver = new ChromeDriver(cOpts);
    }

    public WebDriver getPreparedDriver() {
        final String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        String messageLocal = "\n" + methodNameLocal + ": returning local driver....\n";
        System.out.println(messageLocal);
        return webDriver;
    }
}