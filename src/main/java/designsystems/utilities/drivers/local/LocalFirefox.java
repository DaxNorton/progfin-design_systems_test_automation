package designsystems.utilities.drivers.local;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import designsystems.utilities.LocalResources;
import designsystems.utilities.OperatingSystemDetector;

/**
 * Local driver.
 * <p>
 * For Sauce Labs drivers, see designsystems.utilities.saucelabs.drivers.desktop
 * <p>
 * Version 1.0 2019-10-14
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class LocalFirefox {

    private FirefoxDriver webDriver;
    public static final String GECKO_PROPERTY = "webdriver.gecko.driver";

    public LocalFirefox() {
        checkOperatingSystem();
        setProperty();
        instantiate();
    }

    private void checkOperatingSystem() {
        if(OperatingSystemDetector.isUnix()) {
            throw new IllegalArgumentException("Linux support pending. Stay tuned...");
        } else if(OperatingSystemDetector.isSolaris()) {
            throw new IllegalArgumentException("Solaris is not supported.");
        } else if(OperatingSystemDetector.isWindows()) {
            throw new IllegalArgumentException("This is for the Mac driver.");
        }
    }

    private void setProperty() {
        System.setProperty(GECKO_PROPERTY, LocalResources.getPathGeckoDriverMac());
    }

    private void instantiate() {
        webDriver = new FirefoxDriver();
    }

    /**
     * @return the {@link WebDriver} for the current thread
     */
    public WebDriver getPreparedDriver() {
        final String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        String messageLocal = "\n" + methodNameLocal + ": returning local driver....\n";
        System.out.println(messageLocal);
        return webDriver;
    }

}
