package designsystems.utilities.drivers.local;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import designsystems.utilities.LocalResources;
import designsystems.utilities.OperatingSystemDetector;

/**
 * Local driver.
 * <p>
 * For Sauce Labs drivers, see designsystems.utilities.saucelabs.drivers.desktop
 * <p>
 * Version 1.0 2019-10-14
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class LocalChromeHeadless {

    private ChromeDriver webDriver;
    private static final String SMALLMOBILE = "smallmobile";
    private static final String MOBILEVIEWPORT = "mobile";
    private static final String TABLETVIEWPORT = "tablet";
    public static final String CHROME_PROPERTY = "webdriver.chrome.driver";


    public LocalChromeHeadless() {
        checkOperatingSystem();
        setProperty();
        instantiate();
    }

    public LocalChromeHeadless(String viewport) {
        checkOperatingSystem();
        setProperty();
        instantiate(viewport);
    }

    private void checkOperatingSystem() {
        if(OperatingSystemDetector.isUnix()) {
            throw new IllegalArgumentException("Linux support pending. Stay tuned...");
        } else if(OperatingSystemDetector.isSolaris()) {
            throw new IllegalArgumentException("Solaris is not supported.");
        } else if(OperatingSystemDetector.isWindows()) {
            throw new IllegalArgumentException("This is for the Mac driver.");
        }
    }

    private void setProperty() {
        System.setProperty(CHROME_PROPERTY, LocalResources.getPathChromeDriverMac());
    }

    private void instantiate() {
        ChromeOptions cOpts = new ChromeOptions();
        cOpts.addArguments("window-size=1630,939");
        cOpts.addArguments("headless");
        webDriver = new ChromeDriver(cOpts);
    }

    private void instantiate(String viewport) {
        ChromeOptions cOpts = new ChromeOptions();
        cOpts.addArguments("headless");
        if (viewport == MOBILEVIEWPORT){
            cOpts.addArguments("window-size=767,850");
            webDriver = new ChromeDriver(cOpts);
        }
        else if (viewport == SMALLMOBILE){
            cOpts.addArguments("window-size=299x850");
            webDriver = new ChromeDriver(cOpts);
        }
        else if (viewport == TABLETVIEWPORT){
            cOpts.addArguments("window-size=1024,850");
            webDriver = new ChromeDriver(cOpts);
        }
    }

    /**
     * @return the {@link WebDriver} for the current thread
     */
    public WebDriver getPreparedDriver() {
        final String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        String messageLocal = "\n" + methodNameLocal + ": returning local driver....\n";
        System.out.println(messageLocal);
        return webDriver;
    }

}
