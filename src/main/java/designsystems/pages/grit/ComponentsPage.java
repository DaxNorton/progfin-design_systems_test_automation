package designsystems.pages.grit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class ComponentsPage extends CorePage {

    public ComponentsPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public WebElement getElementHomeSectionWrapper() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".docs-component-list-wrapper:nth-of-type(1)"));
        locators.add(By.xpath("/html//app-root//docs-components-list-layout//div[@class='docs-component-list-wrapper']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public boolean Components_Quick(String baselinePath, String actualPath){
        return imageUtility.GetPageImage_Compare(baselinePath, actualPath);
    }

    public boolean Components_Hero_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementHeroWrapper(), baselineFile, actualFile);
    }

    public boolean Components_Home_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementHomeSectionWrapper(), baselineFile, actualFile);
    }

    public void setBaseline_Components_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }

    public void setBaseline_Components_Hero_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementHeroWrapper(), baselineFile);
    }

    public void setBaseline_Components_Home_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementHomeSectionWrapper(), baselineFile);
    }
}