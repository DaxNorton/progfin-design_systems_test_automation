package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GetStartedDeveloperPage extends CorePage{

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "get-started/developer";

    public GetStartedDeveloperPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public boolean GetStarted_Developer_Hero_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementHeroWrapper(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Section1_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection1Content(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Section2_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection2Content(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Section3_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection3Content(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Section4_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection4Content(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Section5_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection5Content(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Section6_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection6Content(), baselineFile,
                actualFile);
    }

    public boolean GetStarted_Developer_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }



    public void setBaseline_GetStarted_Developer_Hero_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementHeroWrapper(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Section1_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection1Content(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Section2_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection2Content(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Section3_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection3Content(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Section4_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection4Content(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Section5_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection5Content(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Section6_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection6Content(), baselineFile);
    }

    public void setBaseline_GetStarted_Developer_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}