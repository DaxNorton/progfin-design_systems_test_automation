package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsSwitchesDesignPage extends CorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/switch/design";

    public ComponentsSwitchesDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Switches_Design_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Switches_Design_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }

}