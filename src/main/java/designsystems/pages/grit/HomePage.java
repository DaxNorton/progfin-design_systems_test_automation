package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.HashSet;
import java.util.Set;

public class HomePage extends CorePage{

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST;

    public HomePage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public WebElement getElementReadRelease() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-grid-card-layout > div > a:nth-of-type(1)"));
        locators.add(By.cssSelector(".grid-card-layout .grid-card:nth-of-type(1)"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementReadQuickStart() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-grid-card-layout > div > a:nth-of-type(2)"));
        locators.add(By.cssSelector(".grid-card-layout .grid-card:nth-of-type(2)"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']/main/docs-home-page-layout//app-grid-card-layout//a[@href='./get-started']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public boolean HomeView_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_HomeView_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}