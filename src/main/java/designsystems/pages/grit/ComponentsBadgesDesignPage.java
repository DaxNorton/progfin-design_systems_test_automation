package designsystems.pages.grit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsBadgesDesignPage extends CorePage {

    public ComponentsBadgesDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Badges_Design_Quick(String baselineFile, String actualFile){
      return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Badges_Design_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }

}