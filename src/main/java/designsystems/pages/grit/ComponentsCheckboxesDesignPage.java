package designsystems.pages.grit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsCheckboxesDesignPage extends CorePage{

    public ComponentsCheckboxesDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Checkboxes_Design_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Checkboxes_Design_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}