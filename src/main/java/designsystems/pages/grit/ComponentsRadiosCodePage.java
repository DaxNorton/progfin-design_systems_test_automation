package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsRadiosCodePage extends CorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/radio/code";

    public ComponentsRadiosCodePage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Radios_Code_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public boolean Radios_Code_Hero_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementHeroWrapper(), baselineFile, actualFile);
    }

    public boolean Radios_Code_Section1_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection1Content(), baselineFile, actualFile);
    }

    public boolean Radios_Code_Section2_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection2Content(), baselineFile, actualFile);
    }

    public boolean Radios_Code_Section3_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementSection3Content(), baselineFile, actualFile);
    }

    public void setbaseline_Components_Radios_Code_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }

    public void setbaseline_Radios_Code_Hero_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementHeroWrapper(), baselineFile);
    }

    public void setbaseline_Radios_Code_Section1_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection1Content(), baselineFile);
    }

    public void setbaseline_Radios_Code_Section2_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection2Content(), baselineFile);
    }

    public void setbaseline_Radios_Code_Section3_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSection3Content(), baselineFile);
    }

}