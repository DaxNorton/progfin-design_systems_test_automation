package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class GetStartedPage extends CorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "get-started";

    public GetStartedPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public WebElement getElementDeveloperTile() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".grid-card-layout .grid-card:nth-of-type(1)"));
        locators.add(By.cssSelector("app-grid-card-layout > div > a:nth-of-type(1)"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']/main/app-get-started-home-layout//app-grid-card-layout//a[@href='./get-started/developer']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementDesignerTile() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".grid-card-layout .grid-card:nth-of-type(2)"));
        locators.add(By.cssSelector("app-grid-card-layout > div > a:nth-of-type(2)"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']/main/app-get-started-home-layout//app-grid-card-layout//a[@href='./get-started/designer']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementHomeSection() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".get-started-home-section-wrapper:nth-of-type(1)"));
        locators.add(By.xpath("//*[@class='get-started-home-section-wrapper']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public boolean GetStarted_Hero_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementHeroDescription(), baselineFile, actualFile);
    }

    public boolean GetStarted_Home_Content(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementHomeSection(), baselineFile, actualFile);
    }

    public boolean GetStarted_Quick(String baselinePath, String actualPath){
        return imageUtility.GetPageImage_Compare(baselinePath, actualPath);
    }

    public void setBaseline_GetStarted_Hero_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementHeroDescription(), baselineFile);
    }

    public void setBaseline_GetStarted_Home_Content(String baselineFile){
        imageUtility.GetElementImageRetina(getElementHomeSection(), baselineFile);
    }

    public void setBaseline_GetStarted_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}