package designsystems.pages.grit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsCheckboxesCodePage extends CorePage{

    public ComponentsCheckboxesCodePage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Checkboxes_Code_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Checkboxes_Code_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }

}