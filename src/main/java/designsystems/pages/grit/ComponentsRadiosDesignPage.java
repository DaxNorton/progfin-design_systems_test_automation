package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsRadiosDesignPage extends CorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/radio/design";

    public ComponentsRadiosDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    /**
    public boolean Components_Radios_Design_Quick(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getPageContent(), baselineFile, actualFile);
    }
*/
    public boolean Components_Radios_Design_Quick(String baselineFile, String actualFile, String feedback){
        return imageUtility.GetElementImageRetina_Compare_Feedback(getPageContent(), baselineFile, actualFile, feedback);
    }


    public void setbaseline_Components_Radios_Design_Quick(String baselineFile){
        imageUtility.GetElementImageRetina(getPageContent(), baselineFile);
    }
}