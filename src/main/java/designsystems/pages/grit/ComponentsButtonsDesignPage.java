package designsystems.pages.grit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsButtonsDesignPage extends CorePage{

    public ComponentsButtonsDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Buttons_Design_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Buttons_Design_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }

}