package designsystems.pages.grit;

import designsystems.utilities.image.ImageUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import designsystems.utilities.WebElementGetter;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class CorePage {

    WebDriver driver;
    WebDriverWait driverWait;
    ImageUtility imageUtility;

    public CorePage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        this.driver = driverIn;
        this.driverWait = driverWaitIn;
        imageUtility = new ImageUtility(driver, driverWait);
    }

    /**
     * LeftNav designsystems.common elements on devdocs site
     */

    public WebElement getPageContent() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".docs-page-body-content"));
        locators.add(By.id("id"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }
    public WebElement getElementHomeIcon() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".logo > .side-nav-logo"));
        locators.add(By.className("side-nav-logo"));
        locators.add(By.xpath("//app-root//app-side-nav//a[@href='/grit/']/img[@class='side-nav-logo']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementGetStartedMenu() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(1)  .side-nav-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(1) .side-nav-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[1]//div[@class='side-nav-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementGetStartedActiveDesigner() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(1) app-dropdown-item:nth-of-type(1) .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector("app-nav-section:nth-of-type(1)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(2) > .side-nav-dropdown-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[1]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[1]/div[@class='side-nav-dropdown-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementGetStartedActiveDeveloper() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(1) app-dropdown-item:nth-of-type(2) .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector("app-nav-section:nth-of-type(1)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(2) > .side-nav-dropdown-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[1]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[2]/div[@class='side-nav-dropdown-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementFoundationsMenu() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(2)  .side-nav-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(2) .side-nav-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[2]//div[@class='side-nav-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementFoundationsActiveColor() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(2)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(1) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(2) app-dropdown-item:nth-of-type(1) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[2]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[1]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementFoundationsActiveElevation() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(2)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(2) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(2) app-dropdown-item:nth-of-type(2) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[2]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[2]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementFoundationsActiveSpacing() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(2)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(3) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(2) app-dropdown-item:nth-of-type(3) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[2]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[3]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementFoundationsActiveTypography() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(2)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(4) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(2) app-dropdown-item:nth-of-type(4) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[2]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[4]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsMenu() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) .side-nav-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveBadges() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(1) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(1) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[1]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveButtons() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(2) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(2) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[2]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveCheckboxes() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(3) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(3) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[3]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveLoaders() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(4) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(4) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[4]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveNotifications() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(5) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(5) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[5]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveOverlays() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(6) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(6) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[6]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveRadios() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(7) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(7) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[7]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveSwitches() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(8) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(8) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[8]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementComponentsActiveTextInputs() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(3)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(9) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(3) app-dropdown-item:nth-of-type(9) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[3]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[9]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementReleasesMenu() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(4)  .side-nav-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(4) .side-nav-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[4]//div[@class='side-nav-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementReleasesActiveFeb2020() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(4)  .side-nav-dropdown-menu > app-dropdown-item:nth-of-type(1) > .side-nav-dropdown-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(4) app-dropdown-item:nth-of-type(1) div"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[4]//div[@class='side-nav-dropdown-menu']/app-dropdown-item[1]/div"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementRoadmapMenu() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-nav-section:nth-of-type(5)  .side-nav-menu-item"));
        locators.add(By.cssSelector(".side-nav-menu app-nav-section:nth-of-type(5) .side-nav-menu-item"));
        locators.add(By.xpath("/html//app-root//app-side-nav//div[@class='side-nav-menu']/app-nav-section[5]//div[@class='side-nav-menu-item']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBitbucket() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("a:nth-of-type(1) > .side-nav-link-item-name"));
        locators.add(By.cssSelector(".side-nav-outside-links [target='_blank']:nth-of-type(1) .side-nav-link-item-name"));
        locators.add(By.xpath("/html//app-root//app-side-nav//a[@href='https://bitbucket.org/progfin-ondemand/grit-core/src/master/']/span[@class='side-nav-link-item-name']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementAbstract() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("a:nth-of-type(2) > .side-nav-link-item-name"));
        locators.add(By.cssSelector(".side-nav-outside-links [target='_blank']:nth-of-type(2) .side-nav-link-item-name"));
        locators.add(By.xpath("/html//app-root//app-side-nav//a[@href='https://app.goabstract.com/projects/979df3f0-612b-11e9-a02c-5b3b0e7fe0db']/span[@class='side-nav-link-item-name']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementVersion() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".side-nav-version-number:nth-of-type(2)"));
        locators.add(By.className("side-nav-version-number"));
        locators.add(By.xpath("/html//app-root//app-side-nav//a[@class='side-nav-version-number']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }


    /**
     * Page designsystems.common elements on devdocs site, Hero nav button elements are found on their individual designsystems.pages.
     */
    public static final String SECTION_1 = "app-section-generator > div:nth-of-type(1)";
    public static final String SECTION_1_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(1) .title-component-style";
    public static final String SECTION_2 = "app-section-generator > div:nth-of-type(2)";
    public static final String SECTION_2_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(2) .title-component-style";
    public static final String SECTION_3 = "app-section-generator > div:nth-of-type(3)";
    public static final String SECTION_3_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(3) .title-component-style";
    public static final String SECTION_4 = "app-section-generator > div:nth-of-type(4)";
    public static final String SECTION_4_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(4) .title-component-style";
    public static final String SECTION_5 = "app-section-generator > div:nth-of-type(5)";
    public static final String SECTION_5_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(5) .title-component-style";
    public static final String SECTION_6 = "app-section-generator > div:nth-of-type(6)";
    public static final String SECTION_6_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(6) .title-component-style";
    public static final String SECTION_7 = "app-section-generator > div:nth-of-type(7)";
    public static final String SECTION_7_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(7) .title-component-style";
    public static final String SECTION_8 = "app-section-generator > div:nth-of-type(8)";
    public static final String SECTION_8_TITLE = "app-section-generator .docs-page-section-wrapper:nth-of-type(8) .title-component-style";

    public WebElement getElementHeroWrapper() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("docs-hero-section > .docs-hero-wrapper"));
        locators.add(By.className("docs-hero-wrapper"));
        locators.add(By.xpath("/html//app-root//app-get-started-home-layout//docs-hero-section/div[@class='docs-hero-wrapper']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }
    public WebElement getElementHeroTitle() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".docs-hero-title"));
        locators.add(By.className("docs-hero-title"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']//docs-component-page-layout//docs-hero-section//h1[@class='docs-hero-title']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementHeroDescription() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".docs-hero-description:nth-of-type(1)"));
        locators.add(By.className("docs-hero-description"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']//docs-component-page-layout//docs-hero-section//div[@class='docs-hero-description']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementCodeSwitch() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".switch-content-wrapper > a:nth-of-type(1)"));
        locators.add(By.cssSelector(".switch-content-wrapper .switch-label-wrapper:nth-of-type(1)"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']//docs-component-page-layout//docs-hero-section//app-page-switch//a[@href='/grit/component/notification/code']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementDesignSwitch() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".switch-content-wrapper > a:nth-of-type(2)"));
        locators.add(By.cssSelector(".switch-content-wrapper .switch-label-wrapper:nth-of-type(2)"));
        locators.add(By.xpath("/html//app-root/div[@class='grit-docs-site-wrapper']//docs-component-page-layout//docs-hero-section//app-page-switch//a[@href='/grit/component/notification/design']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection1Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(1) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(1)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[1]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection1Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(1)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(1)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[1]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection2Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(2) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(2)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[2]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection2Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(2)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(2)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[2]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection3Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(3) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(3)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[3]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection3Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(3)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(3)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[3]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection4Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(4) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(4)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[4]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection4Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(4)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(4)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[4]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection5Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(5) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(5)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[5]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection5Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(5)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(5)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[5]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection6Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(6) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(6)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[6]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection6Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(6)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(6)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[6]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection7Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(7) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(7)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[7]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection7Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(7)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(7)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[7]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection8Title() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(8) .title-component-style"));
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(8)  app-title > .desktop.title-component > .title-component-style"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[8]//app-title//h1[@class='title-component-style']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementSection8Content() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("app-section-generator > div:nth-of-type(8)"));
        locators.add(By.cssSelector("app-section-generator .docs-page-section-wrapper:nth-of-type(8)"));
        locators.add(By.xpath("/html//app-root//docs-component-page-layout//app-section-generator/div[8]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public void waitPageLoaded(int customSeconds) {
        WebDriverWait waitLocal = new WebDriverWait(driver, customSeconds);
        try {
            waitLocal.until(ExpectedConditions.elementToBeClickable(getElementSection1Content()));//define what should be available if the page is ready.
        }catch(TimeoutException tE) {
            System.out.println("page was not found ready for test");//Capture the error and report that page was not ready for test.
        }
    }

}