package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsOverlaysDesignPage extends CorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "component/overlay/design";

    public ComponentsOverlaysDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Overlays_Design_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Overlays_Design_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}