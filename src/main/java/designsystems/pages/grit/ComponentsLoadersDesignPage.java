package designsystems.pages.grit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComponentsLoadersDesignPage extends CorePage{

    public ComponentsLoadersDesignPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public boolean Components_Loaders_Design_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Components_Loaders_Design_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}