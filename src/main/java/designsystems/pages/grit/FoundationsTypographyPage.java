package designsystems.pages.grit;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FoundationsTypographyPage extends CorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.DOCENVUNDERTEST + "foundation/typography";

    public FoundationsTypographyPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public boolean Foundations_Typography_Quick(String baselineFile, String actualFile){
        return imageUtility.GetPageImage_Compare(baselineFile, actualFile);
    }

    public void setbaseline_Foundations_Typography_Quick(String baselineFile){
        imageUtility.GetPageImage(baselineFile);
    }
}