package designsystems.pages.gritcore;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class TypographyTokensPage extends TestCorePage {

    public static String urlUnderTest = ProgressiveLeasingConstants.COMPONENTENVRIONMENTUNDERTEST + "tokens";

    public TypographyTokensPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    //Page Elements
    public WebElement htmlClassNamesOverview() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("gr-tokens-test-grit > div:nth-of-type(1)"));
        locators.add(By.xpath("//gr-tokens-test-grit/div[1]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinOverview() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("gr-tokens-test-grit > div:nth-of-type(2)"));
        locators.add(By.xpath("//gr-tokens-test-grit/div[2]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontDisplayLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-display-l"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontDisplayMedium() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-display-m"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontDisplaySmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-display-s"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontHeadline() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-headline"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontTitleLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-title-l"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontTitleMedium() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-title-m"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontTitleSmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-title-s"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontSubheading() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-subheading"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontBodyLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='font-body-l']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontBodySmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-body-s"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontButton() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-button"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontCaption() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-caption"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontFieldLabelLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-field-label-l"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontFieldLabelSmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-field-label-s"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontLinkLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-link-l"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement fontLinkSmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".font-link-s"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }


    public WebElement mixinFontDisplayLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".mixins-font-display-l:nth-of-type(1)"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinFontDisplayMedium() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".mixins-font-display-m:nth-of-type(2)"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinFontDisplaySmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-display-s']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinFontHeadline() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-headline']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontTitleLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".mixins-font-title-l:nth-of-type(5)"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontTitleMedium() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-title-m']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontTitleSmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-title-s']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontSubheading() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-subheading']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontBodyLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-body-l']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontBodySmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-body-s']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontButton() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-button']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontCaption() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-caption']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontFieldLabelLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-field-label-l']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontFieldLabelSmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-field-label-s']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontLinkLarge() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-link-l']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement mixinsFontLinkSmall() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='mixins-font-link-s']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    // Token compare method, token values must be translated from em/rem to the px equivalent represented in the
    // test harness, that px will be hard coded to each test until a method of translating em/rem values to px is
    // completed and implemented.
    public boolean cssValuesCompare(WebElement element, String fontFamilyIn, String fontSizeIn, String fontWeightIn, String lineHeightIn, String letterSpacingIn){
        boolean fontfamily = false;
        boolean fontsize = false;
        boolean fontweight = false;
        boolean lineheight = false;
        boolean letterspacing = false;

        String fontFamilyValue = element.getCssValue("font-family").toLowerCase();
        String fontsizeValue = element.getCssValue("font-size").toLowerCase();
        String fontweightValue = element.getCssValue("font-weight").toLowerCase();
        String lineheightValue = element.getCssValue("line-height").toLowerCase();
        String letterspacingValue = element.getCssValue("letter-spacing").toLowerCase();

        String fontFamilyToken = fontFamilyIn;
        String fontSizeToken = fontSizeIn;
        String fontWeightToken = fontWeightIn;
        String lineHeightToken = lineHeightIn;
        String letterSpacingToken = letterSpacingIn;

        if(fontFamilyValue.equals(fontFamilyToken)){
            fontfamily = true;
        }
        if(fontsizeValue.equals(fontSizeToken)){
            fontsize = true;
        }
        if(fontweightValue.equals(fontWeightToken)){
            fontweight = true;
        }
        if(lineheightValue.equals(lineHeightToken)){
            lineheight = true;
        }
        if(letterspacingValue.equals(letterSpacingToken)){
            letterspacing = true;
        }
        boolean[] myArray = new boolean[5];
        myArray[0] = fontfamily;
        myArray[1] = fontsize;
        myArray[2] = fontweight;
        myArray[3] = lineheight;
        myArray[4] = letterspacing;

        return isAllTrue(myArray);
    }

    public boolean cssValuesUnderlinedCompare(WebElement element, String fontFamilyIn, String fontSizeIn,
                                              String fontWeightIn, String lineHeightIn, String letterSpacingIn,
                                              String textDecorationIn){
        boolean fontfamily = false;
        boolean fontsize = false;
        boolean fontweight = false;
        boolean lineheight = false;
        boolean letterspacing = false;
        boolean textdecoration = false;

        String fontFamilyValue = element.getCssValue("font-family").toLowerCase();
        String fontsizeValue = element.getCssValue("font-size").toLowerCase();
        String fontweightValue = element.getCssValue("font-weight").toLowerCase();
        String lineheightValue = element.getCssValue("line-height").toLowerCase();
        String letterspacingValue = element.getCssValue("letter-spacing").toLowerCase();
        String textdecorationValue = element.getCssValue("text-decoration").toLowerCase();

        String fontFamilyToken = fontFamilyIn;
        String fontSizeToken = fontSizeIn;
        String fontWeightToken = fontWeightIn;
        String lineHeightToken = lineHeightIn;
        String letterSpacingToken = letterSpacingIn;
        String textDecorationToken = textDecorationIn;

        if(fontFamilyValue.equals(fontFamilyToken)){
            fontfamily = true;
        }
        if(fontsizeValue.equals(fontSizeToken)){
            fontsize = true;
        }
        if(fontweightValue.equals(fontWeightToken)){
            fontweight = true;
        }
        if(lineheightValue.equals(lineHeightToken)){
            lineheight = true;
        }
        if(letterspacingValue.equals(letterSpacingToken)){
            letterspacing = true;
        }
        if(textdecorationValue.equals(textDecorationToken)){
            textdecoration = true;
        }
        boolean[] myArray = new boolean[6];
        myArray[0] = fontfamily;
        myArray[1] = fontsize;
        myArray[2] = fontweight;
        myArray[3] = lineheight;
        myArray[4] = letterspacing;
        myArray[5] = textdecoration;

        return isAllTrue(myArray);
    }

    public static boolean isAllTrue(boolean[] array)
    {
        for(boolean b : array) if(!b) return false;
        return true;
    }

    // Tests
    //Visual Regression Retina viewport

    public boolean compareTokenHtmlClassNamesOverviewRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(htmlClassNamesOverview(), baselineFile,
                actualFile);
    }

    public boolean compareTokenMixinOverviewRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(mixinOverview(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontDisplayLargeRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(fontDisplayLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontDisplayMediumRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(fontDisplayMedium(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontDisplaySmallRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(fontDisplaySmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontHeadlineRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(fontHeadline(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontTitleLargeRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontTitleLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontTitleMediumRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontTitleMedium(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontTitleSmallRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontTitleSmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontSubheadingRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontDisplayLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontBodyLargeRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontBodyLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontBodySmallRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontBodySmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontButtonRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontButton(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontCaptionRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontCaption(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontFieldLabelLargeRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontFieldLabelLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontFieldLabelSmallRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontFieldLabelSmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontLinkLargeRetina(String baselineFile, String actualFile) {
        return imageUtility.GetElementImageRetina_Compare(fontLinkLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontLinkSmallRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(fontLinkSmall(), baselineFile,
                actualFile);
    }

    //Visual Regression Non-Retina viewport
    public boolean compareTokenHtmlClassNamesOverview(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(htmlClassNamesOverview(), baselineFile,
                actualFile);
    }

    public boolean compareTokenMixinOverview(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(mixinOverview(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontDisplayLarge(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(fontDisplayLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontDisplayMedium(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(fontDisplayMedium(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontDisplaySmall(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(fontDisplaySmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontHeadline(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(fontHeadline(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontTitleLarge(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontTitleLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontTitleMedium(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontTitleMedium(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontTitleSmall(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontTitleSmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontSubheading(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontDisplayLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontBodyLarge(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontBodyLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontBodySmall(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontBodySmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontButton(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontButton(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontCaption(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontCaption(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontFieldLabelLarge(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontFieldLabelLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontFieldLabelSmall(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontFieldLabelSmall(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontLinkLarge(String baselineFile, String actualFile) {
        return imageUtility.GetElementImage_Compare(fontLinkLarge(), baselineFile,
                actualFile);
    }

    public boolean compareTokenFontLinkSmall(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(fontLinkSmall(), baselineFile,
                actualFile);
    }

    // css value tests
    //FontDisplayLarge

    /**
     * DisplayLargeDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 5.61rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayLargeDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "89.76px";
        String fontweight = "700";
        String lineheight = "107.712px";
        String letterSpacing = "3.5904px";
        if(driver instanceof FirefoxDriver){
            fontsize = "89.7667px";
            letterSpacing = "3.59067px";
            lineheight = "107.717px";
        }
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplayLargeModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 4.209rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayLargeModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "67.344px";
        String fontweight = "700";
        String lineheight = "80.8128px";
        String letterSpacing = "2.69376px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplayLargeMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 3.157rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayLargeMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "50.512px";
        String fontweight = "700";
        String lineheight = "60.6144px";
        String letterSpacing = "2.02048px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplayLargeSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 2.369rem;
     * font-weight: 700
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayLargeSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "37.904px";
        String fontweight = "700";
        String lineheight = "45.4848px";
        String letterSpacing = "1.51616px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //FontDisplayMedium

    /**
     * DisplayMediumDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 4.209rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayMediumDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "67.344px";
        String fontweight = "700";
        String lineheight = "80.8128px";
        String letterSpacing = "2.69376px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplayMediumModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 3.157rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValueToToken_DisplayMediumModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "50.512px";
        String fontweight = "700";
        String lineheight = "60.6144px";
        String letterSpacing = "2.02048px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplayMediumMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 2.369rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayMediumMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "37.904px";
        String fontweight = "700";
        String lineheight = "45.4848px";
        String letterSpacing = "1.51616px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplayMediumSmall
     * Actual Token Values
     * Font: Montserrat, sans-serif
     * Size: 1.777rem
     * Weight: 700
     * Height: 1.2em
     * Letter-Spacing: 0.04em
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplayMediumSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "28.432px";
        String fontweight = "700";
        String lineheight = "34.1184px";
        String letterSpacing = "1.13728px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Font Display Small

    /**
     * DisplaySmallDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 3.157rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplaySmallDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "50.512px";
        String fontweight = "700";
        String lineheight = "60.6144px";
        String letterSpacing = "2.02048px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplaySmallModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 2.369rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplaySmallModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "37.904px";
        String fontweight = "700";
        String lineheight = "45.4848px";
        String letterSpacing = "1.51616px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplaySmallMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.777rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplaySmallMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "28.432px";
        String fontweight = "700";
        String lineheight = "34.1184px";
        String letterSpacing = "1.13728px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * DisplaySmallSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_DisplaySmallSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "700";
        String lineheight = "25.5936px";
        String letterSpacing = "0.85312px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Headline Style

    /**
     * HeadlineDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 2.369rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_HeadlineDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "37.904px";
        String fontweight = "700";
        String lineheight = "45.4848px";
        String letterSpacing = "1.51616px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * HeadlineModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.777rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_HeadlineModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "28.432px";
        String fontweight = "700";
        String lineheight = "34.1184px";
        String letterSpacing = "1.13728px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * HeadlineMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.777rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_HeadlineMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "28.432px";
        String fontweight = "700";
        String lineheight = "34.1184px";
        String letterSpacing = "1.13728px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * HeadlineSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_HeadlineSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "700";
        String lineheight = "25.5936px";
        String letterSpacing = "0.85312px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Font Title Large

    /**
     * FontTitleLargeDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.777rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleLargeDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "28.432px";
        String fontweight = "700";
        String lineheight = "34.1184px";
        String letterSpacing = "1.13728px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleLargeModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleLargeModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "700";
        String lineheight = "25.5936px";
        String letterSpacing = "0.85312px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleLargeMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleLargeMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "700";
        String lineheight = "25.5936px";
        String letterSpacing = "0.85312px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleLargeSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleLargeSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Font Title Medium

    /**
     * FontTitleMediumDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleMediumDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleMediumModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleMediumModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleMediumMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleMediumMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleMediumSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleMediumSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Font Title Small

    /**
     * FontTitleSmallDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: .8125rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleSmallDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "700";
        String lineheight = "15.6px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleSmallModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleSmallModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "700";
        String lineheight = "15.6px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleSmallMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleSmallMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "700";
        String lineheight = "15.6px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontTitleSmallSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontTitleSmallSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "700";
        String lineheight = "15.6px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Subheading

    /**
     * SubheadingDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 700;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_SubheadingDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "700";
        String lineheight = "31.992px";
        String letterSpacing = "0.85312px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * SubheadingModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_SubheadingModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * SubheadingMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_SubheadingMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * SubheadingSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1.2em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_SubheadingSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "19.2px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Font Body Large

    /**
     * FontBodyLargeDesktop
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodyLargeDesktop(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "400";
        String lineheight = "31.992px";
        String letterSpacing = "0.85312px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontBodyLargeModal
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodyLargeModal(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontBodyLargeMobileL
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodyLargeMobileL(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontBodyLargeSmall
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodyLargeSmall(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Font Body Small

    /**
     * FontBodySmallDesktop
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodySmallDesktop(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontBodySmallModal
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodySmallModal(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontBodySmallMobileL
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodySmallMobileL(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FontBodySmallSmall
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FontBodySmallSmall(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Button

    /**
     * ButtonDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_ButtonDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "16px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * ButtonModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_ButtonModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "16px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * ButtonMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_ButtonMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "16px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * ButtonSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 700;
     * line-height: 1em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_ButtonSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "700";
        String lineheight = "16px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Caption

    /**
     * CaptionDesktop
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_CaptionDesktop(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * CaptionModal
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_CaptionModal(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * CaptionMobileL
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_CaptionMobileL(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * CaptionSmall
     * Actual Token Values
     * font-family: 'Lato', sans-serif;
     * font-size: .8125rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_CaptionSmall(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13px";
        String fontweight = "400";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Field Label Large

    /**
     * FieldLabelLargeDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 600;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelLargeDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "600";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FieldLabelLargeModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 600;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelLargeModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "600";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FieldLabelLargeMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 600;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelLargeMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "600";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FieldLabelLargeSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 500;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelLargeSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "500";
        String lineheight = "24px";
        String letterSpacing = "0.64px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Field Label Small

    /**
     * FieldLabelSmallDesktop
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 0.85rem; → .8125rem
     * font-weight: 600;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelSmallDesktop(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "600";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FieldLabelSmallModal
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 0.85rem; → .8125rem
     * font-weight: 600;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelSmallModal(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "600";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FieldLabelSmallMobileL
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 0.85rem; → .8125rem
     * font-weight: 600;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelSmallMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "600";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    /**
     * FieldLabelSmallSmall
     * Actual Token Values
     * font-family: 'Montserrat', sans-serif;
     * font-size: 0.85rem; → .8125rem
     * font-weight: 500;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_FieldLabelSmallSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "13px";
        String fontweight = "500";
        String lineheight = "19.5px";
        String letterSpacing = "0.52px";
        return cssValuesCompare(type, font, fontsize, fontweight, lineheight, letterSpacing);
    }

    //Link Large

    /**
     * LinkLargeDesktop
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Lato', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkLargeDesktop(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "400";
        String lineheight = "31.992px";
        String letterspacing = "0.85312px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);
    }

    /**
     * LinkLargeModal
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Lato', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkLargeModal(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "21.328px";
        String fontweight = "400";
        String lineheight = "31.992px";
        String letterspacing = "0.85312px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);

    }

    /**
     * LinkLargeMobileL
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkLargeMobileL(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterspacing = "0.64px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);
    }

    /**
     * LinkLargeSmall
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkLargeSmall(WebElement type){
        String font = "montserrat, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterspacing = "0.64px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);
    }

    //Link Small

    /**
     * LinkSmallDesktop
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Lato', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkSmallDesktop(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterspacing = "0.64px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);
    }

    /**
     * LinkSmallModal
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Lato', sans-serif;
     * font-size: 1.333rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkSmallModal(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "16px";
        String fontweight = "400";
        String lineheight = "24px";
        String letterspacing = "0.64px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);

    }

    /**
     * LinkSmallMobileL
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkSmallMobileL(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13.6px";
        String fontweight = "400";
        String lineheight = "20.4px";
        String letterspacing = "0.544px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);
    }

    /**
     * LinkSmallSmall
     * Actual Token Values
     * text-decoration: underline;
     * font-family: 'Montserrat', sans-serif;
     * font-size: 1rem;
     * font-weight: 400;
     * line-height: 1.5em;
     * letter-spacing: 0.04em;
     * Values translated to px for testing harness below
     */
    public boolean compareCSSValuesToTokens_LinkSmallSmall(WebElement type){
        String font = "lato, sans-serif";
        String fontsize = "13.6px";
        String fontweight = "400";
        String lineheight = "20.4px";
        String letterspacing = "0.544px";
        String textdecoration = "underline solid rgb(0, 0, 0)";
        return cssValuesUnderlinedCompare(type, font, fontsize, fontweight, lineheight, letterspacing, textdecoration);
    }
}
