package designsystems.pages.gritcore;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class InputsTestPage extends TestCorePage {

    public InputsTestPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static String urlUnderTest = ProgressiveLeasingConstants.COMPONENTENVRIONMENTUNDERTEST + "input";

    public WebElement normalField() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".inputDiv > .grit:nth-of-type(1) .pg-field-control-container"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement normalFieldInput(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@id='pg-input-10']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public static final String NORMAL_FIELD_INPUT_XPATH = "//*[@id='pg-input-0']";

    public WebElement getSmallField() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".inputDiv > .grit:nth-of-type(2) .pg-field-control-container"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getFieldWithIcon() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".inputDiv > .grit:nth-of-type(3) .pg-field-control-container"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getSubscriptMessageJson() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@class='json-display']/pre"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getDisabledField() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".inputDiv > .grit:nth-of-type(3) .pg-field-control-container"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getOverview() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//gr-root/gr-input-test-grit/div[@class='inputDiv']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }


    public Boolean isSubscriptEventEmitterInitial(){
        getFieldWithIcon().click();
        getSmallField().click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getSubscriptMessageJson().getText().contains("This field is required");
    }

    public Boolean isSubscriptEventEmitterUpdate(){
        getFieldWithIcon().click();
        return getSubscriptMessageJson().getText().contains("This is a hint");
    }

    public boolean InputOverview_Visual(String baselineFile, String actualFile, String feedbackFile){
        return imageUtility.GetElementImageRetina_Compare_Feedback(getOverview(), baselineFile, actualFile, feedbackFile);
    }

    public boolean InputNormal_Visual(String baselineFile, String actualFile, String feedbackFile){
        return imageUtility.GetElementImageRetina_Compare_Feedback(normalField(), baselineFile, actualFile, feedbackFile);
    }

    public void setbaseline_Inputs_Normal_Field(String baselineFile){
        imageUtility.GetElementImageRetina(normalField(), baselineFile);
    }

    public void setbaseline_Inputs_Overview(String baselineFile){
        imageUtility.GetElementImageRetina(getOverview(), baselineFile);
    }
}
