package designsystems.pages.gritcore;

public class GritColorsPage {

    //Primary
    public static final String RGB_BLUE_100 = "229, 238, 252"; //#E5EEFC
    public static final String RGB_BLUE_200 = "189, 215, 252"; //#BDD7FC
    public static final String RGB_BLUE_300 = "109, 168, 253"; //#6DA8FD
    public static final String RGB_BLUE_400 = "49, 135, 254"; //#3187FE
    public static final String RGB_BLUE_500 = "0, 106, 255"; //#006AFF
    public static final String RGB_BLUE_600 = "7, 94, 214"; //#075ED6
    public static final String RGB_BLUE_700 = "16, 78, 165"; //#104EA5
    public static final String RGB_BLUE_800 = "34, 49, 67"; //#223143

    //Secondary
    public static final String RGB_PURPLE_100 = "239, 240, 252"; //#EFF0FC
    public static final String RGB_PURPLE_200 = "219, 221, 252"; //#DBDDFC
    public static final String RGB_PURPLE_300 = "179, 182, 253"; //#B3B6FD
    public static final String RGB_PURPLE_400 = "149, 155, 254"; //#959BFE
    public static final String RGB_PURPLE_500 = "125, 131, 255"; //#7D83FF
    public static final String RGB_PURPLE_600 = "107, 114, 214"; //#6B72D6
    public static final String RGB_PURPLE_700 = "86, 92, 165"; //#565CA5
    public static final String RGB_PURPLE_800 = "44, 51, 67"; //#2C3343

    //Tertiary
    public static final String RGB_TURQUOISE_100 = "236, 246, 248"; //#ECF6F8
    public static final String RGB_TURQUOISE_200 = "209, 239, 238"; //#D1EFEE
    public static final String RGB_TURQUOISE_300 = "156, 224, 219"; //#9CE0DB
    public static final String RGB_TURQUOISE_400 = "116, 215, 206"; //#74D7CE
    public static final String RGB_TURQUOISE_500 = "84, 206, 195"; //#54CEC3
    public static final String RGB_TURQUOISE_600 = "74, 174, 166"; //#4AAEA6
    public static final String RGB_TURQUOISE_700 = "63, 134, 131"; //#3F8683
    public static final String RGB_TURQUOISE_800 = "41, 57, 63"; //#29393F

    //Attention-Low
    public static final String RGB_YELLOW_100 = "248, 246, 232"; //#F8F6E8
    public static final String RGB_YELLOW_200 = "246, 237, 191"; //#F6EDBF
    public static final String RGB_YELLOW_300 = "242, 218, 110"; //#F2DA6E
    public static final String RGB_YELLOW_400 = "239, 205, 50"; //#EFCD32
    public static final String RGB_YELLOW_500 = "237, 194, 0"; //#EDC200
    public static final String RGB_YELLOW_600 = "197, 164, 10"; //#C5A40A
    public static final String RGB_YELLOW_700 = "117, 105, 31"; //#75691F
    public static final String RGB_YELLOW_800 = "53, 57, 47"; //#35392F

    //Attention-High
    public static final String RGB_ORANGE_100 = "249, 242, 237"; //#F9F2ED
    public static final String RGB_ORANGE_200 = "248, 225, 207"; //#F8E1CF
    public static final String RGB_ORANGE_300 = "246, 191, 147"; //#F6BF93
    public static final String RGB_ORANGE_400 = "244, 168, 103"; //#F4A867
    public static final String RGB_ORANGE_500 = "244, 147, 66"; //#F49342
    public static final String RGB_ORANGE_600 = "202, 127, 63"; //#CA7F3F
    public static final String RGB_ORANGE_700 = "120, 86, 57"; //#785639
    public static final String RGB_ORANGE_800 = "54, 53, 52"; //#363534

    //Error
    public static final String RGB_RED_100 = "246, 231, 232"; //#F6E7E8
    public static final String RGB_RED_200 = "241, 192, 191"; //#F1C0BF
    public static final String RGB_RED_300 = "230, 113, 110"; //#E6716E
    public static final String RGB_RED_400 = "222, 56, 50"; //#DE3832
    public static final String RGB_RED_500 = "216, 8, 0"; //#D80800
    public static final String RGB_RED_600 = "180, 15, 10"; //#B40F0A
    public static final String RGB_RED_700 = "137, 23, 22"; //#891716
    public static final String RGB_RED_800 = "51, 42, 47"; //#332A2F

    //Success
    public static final String RGB_GREEN_100 = "233, 245, 244"; //#E9F5F4
    public static final String RGB_GREEN_200 = "201, 236, 226"; //#C9ECE2
    public static final String RGB_GREEN_300 = "137, 215, 192"; //#89D7C0
    public static final String RGB_GREEN_400 = "89, 202, 167"; //#59CAA7
    public static final String RGB_GREEN_500 = "50, 190, 146"; //#32BE92
    public static final String RGB_GREEN_600 = "47, 161, 127"; //#2FA17F
    public static final String RGB_GREEN_700 = "42, 103, 89"; //#2A6759
    public static final String RGB_GREEN_800 = "38, 56, 59"; //#26383B

    //Neutral
    public static final String RGB_NEUTRAL_100 = "255, 255, 255"; //#FFFFFF
    public static final String RGB_NEUTRAL_200 = "249, 250, 252"; //#F9FAFC
    public static final String RGB_NEUTRAL_300 = "244, 246, 248"; //#F4F6F8
    public static final String RGB_NEUTRAL_400 = "223, 227, 232"; //#DFE3E8
    public static final String RGB_NEUTRAL_500 = "196, 205, 213"; //#C4CDD5
    public static final String RGB_NEUTRAL_600 = "145, 158, 171"; //#919EAB
    public static final String RGB_NEUTRAL_700 = "122, 138, 153"; //#7A8A99
    public static final String RGB_NEUTRAL_800 = "99, 115, 129"; //#637381
    public static final String RGB_NEUTRAL_900 = "69, 79, 91"; //#454F5B
    public static final String RGB_NEUTRAL_1000 = "33, 43, 54"; //#212B36

    //Brand
    public static final String RGB_BRAND_100 = "229, 242, 248"; //#E5F2F8
    public static final String RGB_BRAND_200 = "189, 225, 239"; //#BDE1EF
    public static final String RGB_BRAND_300 = "109, 191, 223"; //#6DBFDF
    public static final String RGB_BRAND_400 = "49, 168, 211"; //#31A8D3
    public static final String RGB_BRAND_500 = "0, 147, 201"; //#0093C9
    public static final String RGB_BRAND_600 = "7, 127, 171"; //#077FAB
    public static final String RGB_BRAND_700 = "22, 86, 111"; //#16566F
    public static final String RGB_BRAND_800 = "34, 53, 63"; //#22353F
}