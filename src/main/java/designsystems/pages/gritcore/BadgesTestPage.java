package designsystems.pages.gritcore;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class BadgesTestPage extends TestCorePage{

    public BadgesTestPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        super(driverIn, driverWaitIn);
    }

    public static String urlUnderTest = ProgressiveLeasingConstants.COMPONENTENVRIONMENTUNDERTEST + "badge";

    public WebElement getElementSectionOverview() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".badgeTest:nth-of-type(1)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div[@class='badgeTest']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement elementBadgesWithIconDefault(){
        return driver.findElement(By.cssSelector("div > grit-badge:nth-of-type(1)"));
    }

    public WebElement getElementBadgesWithIconDefault() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(1)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(2)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[1]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBadgesWithIconInfo() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(2)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(3)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[2]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBadgesWithIconPrimary() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(3)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(4)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[3]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBadgesWithIconAttention() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(4)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(6)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[4]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBadgesWithIconAttentionStrong() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(5)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(7)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[5]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBadgesWithIconSuccess() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(6)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(9)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[6]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement getElementBadgesWithIconWarning() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("div > grit-badge:nth-of-type(7)"));
        locators.add(By.cssSelector(".badgeTest .pg-badge:nth-child(10)"));
        locators.add(By.xpath("//gr-root/gr-badge-test-grit[@class='ng-star-inserted']/div/grit-badge[7]"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }


    //Test Methods Retina
    public boolean Badge_SectionOverviewRetina(String baselineFile, String actualFile, String feedbackFile){
        return imageUtility.GetElementImageRetina_Compare_Feedback(getElementSectionOverview(), baselineFile,
                actualFile, feedbackFile);
    }

    public boolean Badge_DefaultWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconDefault(), baselineFile,
                actualFile);
    }

    public boolean Badge_InfoWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconInfo(), baselineFile,
                actualFile);
    }

    public boolean Badge_PrimaryWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconPrimary(), baselineFile,
                actualFile);
    }

    public boolean Badge_AttentionWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconAttention(), baselineFile,
                actualFile);
    }

    public boolean Badge_AttentionStrongWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconAttentionStrong(), baselineFile,
                actualFile);
    }

    public boolean Badge_SuccessWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconSuccess(), baselineFile,
                actualFile);
    }

    public boolean Badge_WarningWithIconRetina(String baselineFile, String actualFile){
        return imageUtility.GetElementImageRetina_Compare(getElementBadgesWithIconWarning(), baselineFile,
                actualFile);
    }



    //Test Methods non-Retina
    public boolean Badge_SectionOverview(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementSectionOverview(), baselineFile,
                actualFile);
    }

    public boolean Badge_DefaultWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconDefault(), baselineFile,
                actualFile);
    }

    public boolean Badge_InfoWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconInfo(), baselineFile,
                actualFile);
    }

    public boolean Badge_PrimaryWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconPrimary(), baselineFile,
                actualFile);
    }

    public boolean Badge_AttentionWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconAttention(), baselineFile,
                actualFile);
    }

    public boolean Badge_AttentionStrongWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconAttentionStrong(), baselineFile,
                actualFile);
    }

    public boolean Badge_SuccessWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconSuccess(), baselineFile,
                actualFile);
    }

    public boolean Badge_WarningWithIcon(String baselineFile, String actualFile){
        return imageUtility.GetElementImage_Compare(getElementBadgesWithIconWarning(), baselineFile,
                actualFile);
    }



    //Baseline Methods - Retina
    public void setBaseline_Badge_SectionOverviewRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementSectionOverview(), baselineFile);
    }

    public void setBaseline_Badge_DefaultWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconDefault(), baselineFile);
    }

    public void setBaseline_Badge_InfoWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconInfo(), baselineFile);
    }

    public void setBaseline_Badge_PrimaryWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconPrimary(), baselineFile);
    }

    public void setBaseline_Badge_AttentionWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconAttention(), baselineFile);
    }

    public void setBaseline_Badge_AttentionStrongWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconAttentionStrong(), baselineFile);
    }

    public void setBaseline_Badge_SuccessWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconSuccess(), baselineFile);
    }

    public void setBaseline_Badge_WarningWithIconRetina(String baselineFile){
        imageUtility.GetElementImageRetina(getElementBadgesWithIconWarning(), baselineFile);
    }



    //Non-Retina
    public void setBaseline_Badge_SectionOverview(String baselineFile){
        imageUtility.GetElementImage(getElementSectionOverview(), baselineFile);
    }

    public void setBaseline_Badge_DefaultWithIcon(String baselineFile){
        imageUtility.GetElementImage(elementBadgesWithIconDefault(), baselineFile);
    }

    public void setBaseline_Badge_InfoWithIcon(String baselineFile){
        imageUtility.GetElementImage(getElementBadgesWithIconInfo(), baselineFile);
    }

    public void setBaseline_Badge_PrimaryWithIcon(String baselineFile){
        imageUtility.GetElementImage(getElementBadgesWithIconPrimary(), baselineFile);
    }

    public void setBaseline_Badge_AttentionWithIcon(String baselineFile){
        imageUtility.GetElementImage(getElementBadgesWithIconAttention(), baselineFile);
    }

    public void setBaseline_Badge_AttentionStrongWithIcon(String baselineFile){
        imageUtility.GetElementImage(getElementBadgesWithIconAttentionStrong(), baselineFile);
    }

    public void setBaseline_Badge_SuccessWithIcon(String baselineFile){
        imageUtility.GetElementImage(getElementBadgesWithIconSuccess(), baselineFile);
    }

    public void setBaseline_Badge_WarningWithIcon(String baselineFile){
        imageUtility.GetElementImage(getElementBadgesWithIconWarning(), baselineFile);
    }

}
