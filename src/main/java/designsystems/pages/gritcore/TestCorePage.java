package designsystems.pages.gritcore;

import designsystems.utilities.image.ImageUtility;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestCorePage {

    public WebDriver driver;
    public WebDriverWait driverWait;
    public ImageUtility imageUtility;

    public TestCorePage(WebDriver driverIn, WebDriverWait driverWaitIn){
        this.driver = driverIn;
        this.driverWait = driverWaitIn;
        imageUtility = new ImageUtility(driver, driverWait);
    }
}
