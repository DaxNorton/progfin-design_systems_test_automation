package designsystems.pages.gritcore;

import designsystems.common.ProgressiveLeasingConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ButtonsTestPage extends TestCorePage {

    public ButtonsTestPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static String urlUnderTest = ProgressiveLeasingConstants.COMPONENTENVRIONMENTUNDERTEST + "badge";
}
