package designsystems.pages.gritcore;

import designsystems.common.ProgressiveLeasingConstants;
import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class RadiosTestPage extends TestCorePage {

    public RadiosTestPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static String urlUnderTest = ProgressiveLeasingConstants.COMPONENTENVRIONMENTUNDERTEST + "radio";

    public static final String EVENT_EMITTED_RADIO_OPTION_1 = "Option 1";
    public static final String EVENT_EMITTED_RADIO_OPTION_2 = "Option 2";
    public static final String EVENT_EMITTED_RADIO_OPTION_3 = "Option 3";

    public WebElement radioOption1() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".ng-star-inserted.ng-tns-c3-0.pg-radio-button > .grit-radio-control.radio-control"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement radioOption2() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".ng-star-inserted.ng-tns-c3-1.pg-radio-button > .grit-radio-control.radio-control"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement radioOption3() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".ng-star-inserted.ng-tns-c3-2.pg-radio-button > .grit-radio-control.radio-control"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement eventEmittedText() {
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector("span:nth-of-type(1)"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public void click_Radio_Option_1(){
        radioOption1().click();
    }

    public void click_Radio_Option_2(){
        radioOption2().click();
    }

    public void click_Radio_Option_3(){
        radioOption3().click();
    }

    public String get_Event_Emitter_Message(){
       return eventEmittedText().getText();
    }

}
