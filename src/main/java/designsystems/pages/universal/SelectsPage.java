package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SelectsPage extends GritCoreUniversalPage {

    public SelectsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_SELECTS = ServicesURLEnum.GritCoreUniversal.getURL() + "/selects";

    public static final String SELECT_DEMO_CSS = "selects-demo";
    public static final String SELECT_ID = "select-1";
    public static final String SELECT_INPUT_ID = "select-1-input";
    public static final String SELECT_LABEL_ID = "select-1-input-control-label";
    public static final String SELECT_BACKGROUND_ID = "select-1-input-content";
    public static final String SELECT_SELECTOR_ID = "select-1-select-input-el";
    public static final String SELECT_OPTION_1_CSS = "#select-1-select-input-el option:nth-child(2)";
    public static final String SELECT_OPTION_2_CSS = "#select-1-select-input-el option:nth-child(3)";
    public static final String SELECT_OPTION_3_CSS = "#select-1-select-input-el option:nth-child(4)";
    public static final String SELECT_OPTION_4_CSS = "#select-1-select-input-el option:nth-child(5)";
    public static final String SELECT_CARS_INPUT_ID = "cars-select";
    public static final String SELECT_CARS_LABEL_ID = "cars-select-input-control-label";
    public static final String SELECT_CARS_BACKGROUND_ID = "cars-select-input-content";
    public static final String SELECT_CARS_SELECTOR_ID = "cars-select-select-input-el";
    public static final String HELPER_DISABLE_CSS = "div > button:nth-child(1)";
    public static final String HELPER_NEW_VALUE_CSS = "div > button:nth-child(2)";
    public static final String HELPER_ADD_OPTION_CSS = "div > button:nth-child(3)";
    public static final String HELPER_REMOVE_OPTION_CSS = "div > button:nth-child(4)";
    public static final String HELPER_FOCUS_CSS = "div > button:nth-child(5)";
    public static final String SUBMIT_BUTTON_CSS = ".form-example .actions .hydrated";
    public static final String FORM_VALUE_CSS = "h4 > li";

    public WebElement selectDemo(){
        return getElementShadowRootByCSS(SELECT_DEMO_CSS);
    }

    public String getSelectValue(){
        return selectDemo().findElement(By.id(FORM_VALUE_CSS)).getAttribute("value");
    }

    public void selectFromMenu(Integer option){
        Select selectDropDown = new Select(selectDemo().findElement(By.id(SELECT_CARS_SELECTOR_ID)));
        selectDropDown.selectByIndex(option);
        selectDemo().findElement(By.id(SELECT_CARS_SELECTOR_ID)).click();
        clickSubmit();
    }

    public void clickSubmit(){
        WebElement submit = selectDemo().findElement(By.cssSelector(SUBMIT_BUTTON_CSS));
        WebElement nestedSubmit = getElementShadowRoot(submit).findElement(By.cssSelector("span > button"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", nestedSubmit);
    }

    public String getSubmittedFormValue(){
        String test = selectDemo().findElement(By.cssSelector(FORM_VALUE_CSS)).getText();
        return selectDemo().findElement(By.cssSelector(FORM_VALUE_CSS)).getText();
    }

    public void clickHelperDisable(){
        selectDemo().findElement(By.cssSelector(HELPER_DISABLE_CSS)).click();
    }

    public void clickHelperNew(){
        selectDemo().findElement(By.cssSelector(HELPER_NEW_VALUE_CSS)).click();
    }

    public void clickHelperAdd(){
        selectDemo().findElement(By.cssSelector(HELPER_ADD_OPTION_CSS)).click();
    }

    public void clickHelperRemove(){
        selectDemo().findElement(By.cssSelector(HELPER_REMOVE_OPTION_CSS)).click();
    }

    public void clickHelperFocus(){
        selectDemo().findElement(By.cssSelector(HELPER_FOCUS_CSS)).click();
    }

    public String getSelectBackgroundColor(){
        return selectDemo().findElement(By.id(SELECT_BACKGROUND_ID)).getCssValue("background-color");
    }

}
