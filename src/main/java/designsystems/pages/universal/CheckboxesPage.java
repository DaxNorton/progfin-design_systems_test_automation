package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckboxesPage extends GritCoreUniversalPage {

    public CheckboxesPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_CHECKBOXES = ServicesURLEnum.GritCoreUniversal.getURL() + "/checkbox";

    public static final String COLOR_CHECKBOX_IDLE = "145, 158, 171";
    public static final String COLOR_CHECKBOX_ACTIVE = "0, 106, 255";
    public static final String COLOR_CHECKBOX_DISABLED = "196, 205, 213";

    public static final String CHECKBOX_ACTIVE_CSS = "checkbox-demo .hydrated:nth-of-type(1) .grit-checkbox-custom";
    public static final String CHECKBOX_STATUS_CSS = ".show-status";
    public static final String CHECKBOX_INACTIVE_CSS = ".disabled.grit-checkbox-custom";

    public String getCheckboxBorder(){
        return driver.findElement(By.cssSelector(CHECKBOX_ACTIVE_CSS)).getCssValue("border");
    }

    public Dimension getCheckboxSize(){
        return driver.findElement(By.cssSelector(CHECKBOX_ACTIVE_CSS)).getSize();
    }

    public String getDisabledCheckboxBorder(){
        return driver.findElement(By.cssSelector(CHECKBOX_INACTIVE_CSS)).getCssValue("border");
    }
}
