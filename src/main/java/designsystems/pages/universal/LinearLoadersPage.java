package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LinearLoadersPage extends GritCoreUniversalPage {

    public LinearLoadersPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_LINLOADER = ServicesURLEnum.GritCoreUniversal.getURL() + "/linear-loader";

    public static final String LINEAR_LOADER_BAR_WRAPPER_CSS = "#indeterminate-indeterminate-load-bar-wrapper";
    public static final String LINEAR_LOADER_ANIMATION_SLOW_CSS = "div#indeterminate-slow-bar-animation";
    public static final String LINEAR_LOADER_ANIMATION_FAST_CSS = "div#indeterminate-faster-bar-animation";

    public String getWrapperHeight(){
        Integer height = driver.findElement(By.cssSelector(LINEAR_LOADER_BAR_WRAPPER_CSS)).getSize().height;
        return String.valueOf(height);
    }

    public String getWrapperFlexGrow(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_BAR_WRAPPER_CSS)).getCssValue("flex-grow");
    }

    public String getSlowAnimationDelay(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_SLOW_CSS)).getCssValue("animation-delay");
    }

    public String getSlowAnimationDirection(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_SLOW_CSS)).getCssValue("animation-direction");
    }

    public String getSlowAnimationDuration(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_SLOW_CSS)).getCssValue("animation-duration");
    }

    public String getSlowAnimationIteration(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_SLOW_CSS)).getCssValue("animation-iteration-count");
    }

    public String getFastAnimationDelay(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_FAST_CSS)).getCssValue("animation-delay");
    }

    public String getFastAnimationDirection(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_FAST_CSS)).getCssValue("animation-direction");
    }

    public String getFastAnimationDuration(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_FAST_CSS)).getCssValue("animation-duration");
    }

    public String getFastAnimationIteration(){
        return driver.findElement(By.cssSelector(LINEAR_LOADER_ANIMATION_FAST_CSS)).getCssValue("animation-iteration-count");
    }
}