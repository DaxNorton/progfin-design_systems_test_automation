package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BadgesPage extends GritCoreUniversalPage{

    public BadgesPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_UNDER_TEST = ServicesURLEnum.GritCoreUniversal.getURL() + "/badges";

    public static final String BADGE_DEFAULT = "grit-wc-badge:nth-of-type(1)";
    public static final String BADGE_INFO_CSS = "grit-wc-badge:nth-of-type(2)";
    public static final String BADGE_PRIMARY_CSS = "grit-wc-badge:nth-of-type(3)";
    public static final String BADGE_SUCCESS_CSS = "grit-wc-badge:nth-of-type(4)";
    public static final String BADGE_ATTENTION_CSS = "grit-wc-badge:nth-of-type(5)";
    public static final String BADGE_ATTENTION_STRONG_CSS = "grit-wc-badge:nth-of-type(6)";
    public static final String BADGE_WARNING_CSS = "grit-wc-badge:nth-of-type(7)";

    public static final String BADGE_ICON_DEFAULT = "grit-wc-badge:nth-of-type(1)  .grit-icon";
    public static final String BADGE_ICON_INFO_CSS = "grit-wc-badge:nth-of-type(2)  .grit-icon";
    public static final String BADGE_ICON_PRIMARY_CSS = "grit-wc-badge:nth-of-type(3)  .grit-icon";
    public static final String BADGE_ICON_SUCCESS_CSS = "grit-wc-badge:nth-of-type(4)  .grit-icon";
    public static final String BADGE_ICON_ATTENTION_CSS = "grit-wc-badge:nth-of-type(5)  .grit-icon";
    public static final String BADGE_ICON_ATTENTION_STRONG_CSS = "grit-wc-badge:nth-of-type(6)  .grit-icon";
    public static final String BADGE_ICON_WARNING_CSS = "grit-wc-badge:nth-of-type(7)  .grit-icon";

    public static final String COLOR_DEFAULT = "122, 138, 153";
    public static final String COLOR_INFO = "125, 131, 255";
    public static final String COLOR_PRIMARY = "0, 106, 255";
    public static final String COLOR_SUCCESS = "50, 190, 146";
    public static final String COLOR_ATTENTION = "237, 194, 0";
    public static final String COLOR_ATTENTION_STRONG = "244, 147, 66";
    public static final String COLOR_WARNING = "216, 8, 0";

    public String getColor(String identifier){
        return driver.findElement(By.cssSelector(identifier)).getCssValue("color");
    }

    public Dimension getSize(String identifier){
        return driver.findElement(By.cssSelector(identifier)).getSize();
    }
}
