package designsystems.pages.universal;

import designsystems.utilities.WebElementGetter;
import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class HangTagPage extends HostAppPage {

    public HangTagPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static String urlUnderTest = ServicesURLEnum.GritHangTag.getURL();

    public static final String SHADOW_ROOT_XPATH = "//*/grit-hang-tag";
    public static final String HANG_TAG_RENTAL_PERIOD_ID = "rental-period";
    public static final String HANG_TAG_TOTAL_PAYMENT_AMOUNT_ID = "total-payments-amount";
    public static final String HANG_TAG_NUMBER_OF_PAYMENTS_ID = "number-of-payments";
    public static final String HANG_TAG_AMOUNT_OF_EACH_PAYMENT_DESCRIPTION_ID = "amount-of-each-payment-description";
    public static final String HANG_TAG_CASH_PRICE_AMOUNT_ID = "cash-price-amount";
    public static final String HANG_TAG_COST_OF_RENTAL_AMOUNT_ID = "cost-of-rental-amount";
    public static final String HANG_TAG_TOTAL_PAYMENTS_AMOUNT_ID = "total-payments-amount";
    public static final String HANG_TAG_TOTAL_OF_PAYMENTS_DESCRIPTION_ID = "total-payments-description";
    public static final String HANG_TAG_COST_OF_RENTAL_DESCRIPTION_XPATH = "//*[@class='grit-ht-description'][2]";
    public static final String HANG_TAG_CASH_PRICE_DESCRIPTION_XPATH = "//*[@class='grit-ht-description'][3]";

    public static final String HANG_TAG_TOTAL_OF_PAYMENTS_DESCRIPTION = "You must pay this amount to own the property" +
            " if you make all the regular payments. You can buy the property for less under the early purchase option.";
    public static final String HANG_TAG_COST_OF_RENTAL_DESCRIPTION = "Amount over cash price you will pay if you make" +
            " all regular payments.";
    public static final String HANG_TAG_CASH_PRICE_DESCRIPTION = "Property available at this price from the retailer." +
            " See about your early purchase option rights.";



    public WebElement shadowRootHangTag(){ //add .children after the shadowRoot
        WebElement ShadowHost = driver.findElement(By.xpath("//*/grit-hang-tag"));
        WebElement ele = (WebElement) ((JavascriptExecutor)driver).executeScript("return arguments[0].shadowRoot", ShadowHost);
        return ele;
    }

    public String getHangTagTotalPaymentAmount(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_TOTAL_PAYMENT_AMOUNT_ID)).getText();
    }

    public String getHangTagRentalPeriod(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_RENTAL_PERIOD_ID)).getText();
    }

    public String getHangTagNumberOfPayments(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_NUMBER_OF_PAYMENTS_ID)).getText();
    }

    public String getHangTagAmountOfEachPaymentDescription(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_AMOUNT_OF_EACH_PAYMENT_DESCRIPTION_ID)).getText();
    }

    public String getHangTagCashPrice(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_CASH_PRICE_AMOUNT_ID)).getText();
    }

    public String getHangTagCostOfRental(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_COST_OF_RENTAL_AMOUNT_ID)).getText();
    }

    public String getHangTagTotalPaymentsAmount(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_TOTAL_PAYMENTS_AMOUNT_ID)).getText();
    }

    public String getHangTagTotalOfPaymentsDescription(){
        return shadowRootHangTag().findElement(By.id(HANG_TAG_TOTAL_OF_PAYMENTS_DESCRIPTION_ID)).getText();
    }

    public String getHangTagCostOfRentalDescription(){
        return shadowRootHangTag().findElement(By.xpath(HANG_TAG_COST_OF_RENTAL_DESCRIPTION_XPATH)).getText();
    }

    public String getHangTagCashPriceDescription(){
        return shadowRootHangTag().findElement(By.xpath(HANG_TAG_CASH_PRICE_DESCRIPTION_XPATH)).getText();
    }




    public WebElement configInitialBalance(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Initial Balance']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configCostOfRental(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Cost of Rental']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configInitialCashPrice(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Initial Cash Price']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configInitialLow(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Initial Payment']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configInitialHigh(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Initial Payment High']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configPeriodicPayment(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Periodic Payment']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configNumberOfPayments(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Number Of Payments']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configTermTotal(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Term Total']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configPaymentFrequency(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Payment Frequency']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configLanguage(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Language']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement configItemStatus(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@placeholder='Item Status']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public void clickRender(){
        driver.findElement(By.id("render")).click();
    }
}
