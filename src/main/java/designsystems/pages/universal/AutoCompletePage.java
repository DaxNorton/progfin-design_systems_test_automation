package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutoCompletePage extends GritCoreUniversalPage {

    public AutoCompletePage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_UNDER_TEST = ServicesURLEnum.GritCoreUniversal.getURL() + "/autocomplete";

    public static final String AUTOCOMPLETE_CSS = "autocomplete-demo .grit-component.hydrated:nth-child(2)";
    public static final String AUTOCOMPLETE_GRIT_WC_INPUT_CSS = "grit-wc-input";
    public static final String AUTOCOMPLETE_INPUT_CSS = ".hydrated .grit-component .grit-input-element";
    public static final String AUTOCOMPLETE_LABEL_CSS = ".grit-component .grit-input-content .grit-input-content__label > label";
    public static final String AUTOCOMPLETE_NUMBER_CSS = "grit-wc-autocomplete#number-autocomplete";
    public static final String AUTOCOMPLETE_NUMBER_LIST_CSS = ".listbox";
    public static final String AUTOCOMPLETE_DISABLED_CSS = "autocomplete-demo > grit-wc-autocomplete:nth-of-type(3)";
    public static final String AUTOCOMPLETE_CASE_MATCH_CSS = "autocomplete-demo > grit-wc-autocomplete:nth-of-type(4)";


    public boolean isDisplayedAutocompleteNumberList(){
        return getElementShadowRootByCSS(AUTOCOMPLETE_NUMBER_CSS).findElement(By.cssSelector(AUTOCOMPLETE_NUMBER_LIST_CSS)).isDisplayed();
    }

    public WebElement getDefaultView() {
        return driver.findElement(By.cssSelector(AUTOCOMPLETE_CSS));
    }
}