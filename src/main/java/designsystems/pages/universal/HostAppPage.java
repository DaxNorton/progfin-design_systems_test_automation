package designsystems.pages.universal;

import designsystems.utilities.WebElementGetter;
import designsystems.utilities.image.ImageUtility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;

public class HostAppPage {

    public WebDriver driver;
    public WebDriverWait driverWait;
    public ImageUtility imageUtility;

    public HostAppPage(WebDriver driverIn, WebDriverWait driverWaitIn){
        this.driver = driverIn;
        this.driverWait = driverWaitIn;
        imageUtility = new ImageUtility(driver, driverWait);
    }

    public static String EVENT_BUTTON_CSS = ".pg-button";
    public static String MICRO_FRONT_END_MENU_CSS = ".nav-wrapper .hydrated:nth-of-type(1)";

    public void clickEventButton(){
        driver.findElement(By.cssSelector(EVENT_BUTTON_CSS)).click();
    }

    public void clickMenuMicroFrontEnd(){
        driver.findElement(By.cssSelector(MICRO_FRONT_END_MENU_CSS)).click();
    }

    public WebElement menuGritCoreUniversal(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".nav-wrapper > grit-wc-button:nth-of-type(2)"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }

    public WebElement menuGritHangTag(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.xpath("//*[@routerlink='grit-hang-tag']"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }


}
