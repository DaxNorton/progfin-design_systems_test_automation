package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InputsPage extends GritCoreUniversalPage {

    public InputsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_INPUTS = ServicesURLEnum.GritCoreUniversal.getURL() + "/inputs";

    public static final String NORMAL_FIELD_ID = "normal-field";
    public static final String NORMAL_FIELD_INPUT_ID = "normal-field-control";
    public static final String NORMAL_FIELD_LABEL_ID = "normal-field-control-label";
    public static final String MEDIUM_FIELD_ID = "medium-field";
    public static final String MEDIUM_FIELD_INPUT_ID = "medium-field-control";
    public static final String MEDIUM_FIELD_LABEL_ID = "medium-field-control-label";
    public static final String SMALL_FIELD_ID = "small-field";
    public static final String SMALL_FIELD_INPUT_ID = "small-field-control";
    public static final String SMALL_FIELD_LABEL_ID = "small-field-control-label";
    public static final String ICON_FIELD_ID = "create-field-1";
    public static final String ICON_FIELD_INPUT_ID = "create-field-1-control";
    public static final String ICON_FIELD_LABEL_ID = "create-field-1-control-label";
    public static final String ICON_MULTI_ERROR_FIELD_ID = "multi-error-field";
    public static final String ICON_MULTI_ERROR_FIELD_INPUT_ID = "multi-error-field-control";
    public static final String ICON_MULTI_ERROR_FIELD_LABEL_ID = "multi-error-field-control-label";
    public static final String ICON_EMAIL_FIELD_ID = "create-field-2";
    public static final String ICON_EMAIL_FIELD_INPUT_ID = "create-field-2-control";
    public static final String ICON_EMAIL_FIELD_LABEL_ID = "create-field-2-control-label";
    public static final String DISABLED_ICON_NO_DATA_FIELD_CSS = ".inputs-container #create-field:nth-child(13)";
    public static final String DISABLED_ICON_DATA_FIELD_CSS = ".inputs-container #create-field:nth-child(15)";

    public void clickNormalField(){
        driver.findElement(By.id(NORMAL_FIELD_ID)).click();
    }

    public void clickSmallField(){
        driver.findElement(By.id(SMALL_FIELD_ID)).click();
    }

    public void clickMediumField(){
        driver.findElement(By.id(MEDIUM_FIELD_ID)).click();
    }

    public void clickIconField(){
        driver.findElement(By.id(ICON_FIELD_ID)).click();
    }

    public void inputNormalField(String value){
        driver.findElement(By.id(NORMAL_FIELD_INPUT_ID)).sendKeys(value);
    }

    public void inputSmallField(String value){
        driver.findElement(By.id(SMALL_FIELD_INPUT_ID)).sendKeys(value);
    }

    public void inputMediumField(String value){
        driver.findElement(By.id(MEDIUM_FIELD_INPUT_ID)).sendKeys(value);
    }

    public Dimension getNormalFieldHeight(){
        return driver.findElement(By.id(NORMAL_FIELD_ID)).getSize();
    }

    public Dimension getMediumFieldHeight(){
        return driver.findElement(By.id(MEDIUM_FIELD_ID)).getSize();
    }

    public Dimension getSmallFieldHeight(){
        return driver.findElement(By.id(SMALL_FIELD_ID)).getSize();
    }

    public String getNormalFieldLabelFont(){
        return driver.findElement(By.id(NORMAL_FIELD_LABEL_ID)).getCssValue("font-family");
    }

    public String getSmallFieldLabelFont(){
        return driver.findElement(By.id(SMALL_FIELD_LABEL_ID)).getCssValue("font-family");
    }

    public String getMediumFieldLabelFont(){
        return driver.findElement(By.id(MEDIUM_FIELD_LABEL_ID)).getCssValue("font-family");
    }

    public String getNormalFieldInputFont(){
        return driver.findElement(By.id(NORMAL_FIELD_INPUT_ID)).getCssValue("font-family");
    }

    public String getSmallFieldInputFont(){
        return driver.findElement(By.id(SMALL_FIELD_INPUT_ID)).getCssValue("font-family");
    }

    public String getMediumFieldInputFont(){
        return driver.findElement(By.id(MEDIUM_FIELD_INPUT_ID)).getCssValue("font-family");
    }

    public String getFieldWithIconLabelColor(){
        return driver.findElement(By.id(ICON_FIELD_LABEL_ID)).getCssValue("color");
    }

    public Point getElementCoordinates(String identifier){
        return driver.findElement(By.id(identifier)).getLocation();
    }
}