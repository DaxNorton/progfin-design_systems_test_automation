package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CalendarsPage extends GritCoreUniversalPage {

    public CalendarsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_CALENDAR = ServicesURLEnum.GritCoreUniversal.getURL() + "/calendar";

    public static final String CALENDAR_DEFAULT_CSS = "div > grit-wc-input:nth-of-type(1)";
    public static final String CALENDAR_DATE_CSS = "div > grit-wc-input:nth-of-type(2)";
    public static final String CALENDAR_LABEL_CSS = "grit-wc-input:nth-of-type(1) .grit-input-content__label-element";
    public static final String CALENDAR_INPUT_CSS = ".grit-component .grit-input-element";
    public static final String CONTROL_CALENDAR_CONTENT_CSS = "grit-wc-input:nth-of-type(2) > .grit-component.grit-input.grit-input-sm > .grit-input-element";
    public static final String CONTROL_CALENDAR_SHADOW_ROOT_1_CSS = "div > grit-wc-calendar:nth-of-type(2)";
    public static final String CONTROL_CALENDAR_SHADOW_ROOT_2_CSS = "grit-wc-calendar-board";
    public static final String CONTROL_CALENDAR_DATE_CSS = "div:nth-of-type(2) > button:nth-of-type(2)";

    public static final String COLOR_ACTIVE_LABEL = "0, 106, 255";
    public static final String COLOR_IDLE_LABEL = "99, 115, 129";

    public String getCalendarDisplayFont(){
        return driver.findElement(By.cssSelector(CALENDAR_INPUT_CSS)).getCssValue("font-family");
    }

    public String getCalendarDisplayText(String identifier){
        return getElementShadowRootByCSS(identifier).findElement(By.cssSelector(CALENDAR_INPUT_CSS)).getText();
    }

    public String getCalendarLabelColor(){
        return driver.findElement(By.cssSelector(CALENDAR_LABEL_CSS)).getCssValue("color");
    }

    public String getCalendarLabelFont(){
        return driver.findElement(By.cssSelector(CALENDAR_LABEL_CSS)).getCssValue("font-family");
    }
}
