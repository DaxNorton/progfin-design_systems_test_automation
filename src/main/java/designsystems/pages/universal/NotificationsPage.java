package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NotificationsPage extends GritCoreUniversalPage {

    public NotificationsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_NOTIFICATIONS = ServicesURLEnum.GritCoreUniversal.getURL() + "/notifications";

    public static final String NOTIFICATION_PROXIMITY_WARNING_CSS = "notifications-demo div:nth-of-type(1) [type='proximity']:nth-of-type(1)";
    public static final String NOTIFICATION_PROXIMITY_CAUTION_CSS = "notifications-demo div:nth-of-type(1) [type='proximity']:nth-of-type(2)";
    public static final String NOTIFICATION_PROXIMITY_INFORMATION_CSS = "notifications-demo div:nth-of-type(1) [type='proximity']:nth-of-type(3)";
    public static final String NOTIFICATION_PROXIMITY_SUCCESS_CSS = "notifications-demo div:nth-of-type(1) [type='proximity']:nth-of-type(4)";
    public static final String NOTIFICATION_OPEN_BOLD_FLAG_CSS = "notifications-demo .button-container:nth-child(9) #open-flag:nth-of-type(1)";
    public static final String NOTIFICATION_OPEN_FLAG_CSS = "notifications-demo .button-container:nth-child(9) #open-flag:nth-of-type(2)";
    public static final String FLAG_NOTIFICATION_BOLD_CSS = "div > grit-wc-notification:nth-of-type(5)";
    public static final String FLAG_NOTIFICATION_CSS = "div > grit-wc-notification:nth-of-type(6)";
    public static final String FLAG_NOTIFICATION_BOLD_DISMISS_CSS = "grit-wc-notification:nth-of-type(5)  .hydrated";
    public static final String NOTIFICATION_PAGE_WITH_ICON_ID = "page-notif-6";
    public static final String NOTIFICATION_PAGE_WITH_ICON_ICON_ID = "action-icon";
    public static final String NOTIFICATION_PAGE_WITH_ICON_DISMISS_CSS = "grit-wc-notification:nth-of-type(12) > div:nth-of-type(3) > grit-wc-notification-action:nth-of-type(1)";
    public static final String NOTIFICATION_PAGE_WITH_ICON_ALERT_CSS = "grit-wc-notification:nth-of-type(12) > div:nth-of-type(3) > grit-wc-notification-action:nth-of-type(2)";

    public void clickOpenBoldFlag(){
        driver.findElement(By.cssSelector(NOTIFICATION_OPEN_BOLD_FLAG_CSS)).click();
    }

    public void clickOpenFlag(){
        driver.findElement(By.cssSelector(NOTIFICATION_OPEN_FLAG_CSS)).click();
    }

    public void clickNotificationIcon(){
        getElementShadowRootById(NOTIFICATION_PAGE_WITH_ICON_ID).findElement(By.id(NOTIFICATION_PAGE_WITH_ICON_ICON_ID)).click();
    }

    public void clickNotificationDismiss(){
        driver.findElement(By.cssSelector(NOTIFICATION_PAGE_WITH_ICON_DISMISS_CSS)).click();
    }

    public void clickNotificationAlert(){
        driver.findElement(By.cssSelector(NOTIFICATION_PAGE_WITH_ICON_ALERT_CSS)).click();
    }
}
