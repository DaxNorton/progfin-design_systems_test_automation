package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ListSelectPage extends GritCoreUniversalPage {

    public ListSelectPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_LISTSELECT = ServicesURLEnum.GritCoreUniversal.getURL() + "/list-select";

    public static final String LIST_SELECT_1_CSS = "grit-wc-list-select-item#list-select-item-1";
    public static final String LIST_SELECT_2_CSS = "div#test-group-wrapper > grit-wc-list-select-item[value='2']";
    public static final String LIST_SELECT_3_CSS = "div#test-group-wrapper > grit-wc-list-select-item[value='3']";
    public static final String LIST_SELECT_CARD_WRAPPER_CSS = "div > div";
    public static final String LIST_SELECT_LABEL_CSS = "div > div > div .grit-list-select-item-top-wrapper .grit-list-select-item__label";
    public static final String LIST_SELECT_CONTENT_CSS = "div > div > div .selected-subtitle";
    public static final String LIST_SELECTED_CSS = "p";
    public static final String SUBMIT_BUTTON_CSS = ".actions .hydrated";

    public void clickListSelect1() {
        WebElement element = driver.findElement(By.cssSelector(LIST_SELECT_1_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
    }

    public void clickListSelect2(){
        WebElement element = driver.findElement(By.cssSelector(LIST_SELECT_2_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
    }

    public void clickListSelect3(){
        WebElement element = driver.findElement(By.cssSelector(LIST_SELECT_3_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element.click();
    }

    public void clickSubmit(){
        WebElement submit = driver.findElement(By.cssSelector(SUBMIT_BUTTON_CSS));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", submit);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        submit.click();
    }

    public String getBackgroundColor(String selector){
        return getElementShadowRootByCSS(selector).findElement(By.cssSelector(LIST_SELECT_CARD_WRAPPER_CSS))
                .getCssValue("background-color");
    }

    public String getLabelFont(String selector){
        return getElementShadowRootByCSS(selector).findElement(By.cssSelector(LIST_SELECT_LABEL_CSS))
                .getCssValue("font-family");
    }

    public String getDescriptionFont(String selector){
        return getElementShadowRootByCSS(selector).findElement(By.cssSelector(LIST_SELECT_CONTENT_CSS))
                .getCssValue("font-family");
    }

    public String getFontColor(String selector){
        return getElementShadowRootByCSS(selector).findElement(By.cssSelector(LIST_SELECT_LABEL_CSS))
                .getCssValue("color");
    }

    public String getDescriptionText(String selector){
        return getElementShadowRootByCSS(selector).findElement(By.cssSelector(LIST_SELECT_CONTENT_CSS))
                .getText();
    }

    public String getListItemHeight(String selector){
        int height = driver.findElement(By.cssSelector(selector)).getSize().height;
        return String.valueOf(height);
    }
    public String getSelectedOutput(){
        return driver.findElement(By.cssSelector(LIST_SELECTED_CSS)).getText();
    }

}