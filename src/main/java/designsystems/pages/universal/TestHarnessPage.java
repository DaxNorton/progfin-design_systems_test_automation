package designsystems.pages.universal;

import designsystems.utilities.WebElementGetter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashSet;
import java.util.Set;


public class TestHarnessPage extends HostAppPage{

    public TestHarnessPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public WebElement jsonEventOutput(){
        String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
        Set<By> locators = new HashSet<By>();
        locators.add(By.cssSelector(".json-display pre"));
        return WebElementGetter.fetchThisElement(methodNameLocal, driver, locators);
    }
}
