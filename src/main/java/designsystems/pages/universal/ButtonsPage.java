package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ButtonsPage extends GritCoreUniversalPage{

    public ButtonsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_BUTTONS = ServicesURLEnum.GritCoreUniversal.getURL() + "/buttons";

    public static final String COLOR_PRIMARY_COLOR = "0, 106, 255";
    public static final String COLOR_PRIMARY_HOVER = "7, 94, 214";
    public static final String COLOR_PRIMARY_ACTIVE = "16, 78, 165";
    public static final String COLOR_PRIMARY_TEXT = "255, 255, 255";
    public static final String COLOR_SECONDARY_COLOR = "145, 158, 171";
    public static final String COLOR_SECONDARY_HOVER = "122, 138, 153";
    public static final String COLOR_SECONDARY_ACTIVE = "99, 115, 129";
    public static final String COLOR_SUBTLE_COLOR = "244, 246, 248";
    public static final String COLOR_SUBTLE_HOVER = "223, 227, 232";
    public static final String COLOR_SUBTLE_ACTIVE = "196, 205, 213";
    public static final String COLOR_SUCCESS_COLOR = "50, 190, 146";
    public static final String COLOR_SUCCESS_HOVER = "47, 161, 127";
    public static final String COLOR_SUCCESS_ACTIVE = "42, 103, 89";
    public static final String COLOR_DESTROY_COLOR = "216, 8, 0";
    public static final String COLOR_DESTROY_HOVER = "180, 15, 10";
    public static final String COLOR_DESTROY_ACTIVE = "137, 23, 22";
    public static final String COLOR_LIGHT_COLOR = "0, 0, 0";
    public static final String COLOR_LIGHT_HOVER = "244, 246, 248";
    public static final String COLOR_LIGHT_ACTIVE = "223, 227, 232";

    public static final String FONT = "Montserrat, sans-serif";

    public static final String BUTTON_PRIMARY_LEFT_ICON_CSS= "buttons-demo .button-padding:nth-of-type(1) .button-margin:nth-of-type(7) grit-wc-button";
    public static final String BUTTON_PRIMARY_RIGHT_ICON_CSS= "buttons-demo .button-padding:nth-of-type(1) .button-margin:nth-of-type(8) grit-wc-button";
    public static final String BUTTON_SECONDARY_ICON_CSS= "buttons-demo .button-padding:nth-of-type(1) .button-margin:nth-of-type(9) grit-wc-button";
    public static final String BUTTON_FULL_WIDTH_ICON_CSS= "buttons-demo .fullwidth-button-margin:nth-of-type(1) grit-wc-button";
    public static final String BUTTON_FULL_WIDTH_LEFT_ICON_CSS= "buttons-demo .fullwidth-button-margin:nth-of-type(2) grit-wc-button";
    public static final String BUTTON_FULL_WIDTH_RIGHT_ICON_CSS= "buttons-demo .fullwidth-button-margin:nth-of-type(3) grit-wc-button";
    public static final String BUTTON_FULL_WIDTH_TARGET_BLANK_CSS = "div:nth-of-type(3) > .fullwidth-button-margin > .hydrated";
    public static final String BUTTON_TARGET_BLANK_CSS = "div:nth-of-type(3) > div:nth-of-type(2) > .hydrated";
    public static final String BUTTON_TARGET_SELF_CSS = "div:nth-of-type(3) > div:nth-of-type(3) > .hydrated";
    public static final String BUTTON_TARGET_DEFAULT_CSS = "div:nth-of-type(3) > div:nth-of-type(4) > .hydrated";
    public static final String BUTTON_DOWNLOAD_CSS = "div:nth-of-type(3) > div:nth-of-type(5) > .hydrated";
    public static final String BUTTON_REL_PROP_CSS = "div:nth-of-type(3) > div:nth-of-type(6) > .hydrated";
    public static final String BUTTON_COLOR_PRIMARY_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(1) .hydrated";
    public static final String BUTTON_COLOR_SECONDARY_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(2) .hydrated";
    public static final String BUTTON_COLOR_SUBTLE_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(3) .hydrated";
    public static final String BUTTON_COLOR_SUCCESS_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(4) .hydrated";
    public static final String BUTTON_COLOR_DESTROY_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(5) .hydrated";
    public static final String BUTTON_COLOR_PRIMARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(6) .hydrated";
    public static final String BUTTON_COLOR_SECONDARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(7) .hydrated";
    public static final String BUTTON_COLOR_SUBTLE_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(8) .hydrated";
    public static final String BUTTON_COLOR_DESTROY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(9) .hydrated";
    public static final String BUTTON_COLOR_SUCCESS_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(4) .button-margin:nth-of-type(10) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_PRIMARY_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(1) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_SECONDARY_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(2) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_SUBTLE_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(3) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_SUCCESS_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(4) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_DESTROY_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(5) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_PRIMARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(6) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_SECONDARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(7) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_SUBTLE_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(8) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_DESTROY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(9) .hydrated";
    public static final String BUTTON_DISABLED_COLOR_SUCCESS_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(10) .hydrated";
    public static final String BUTTON_SMALL_PRIMARY_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(1) .hydrated";
    public static final String BUTTON_SMALL_SECONDARY_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(2) .hydrated";
    public static final String BUTTON_SMALL_SUBTLE_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(3) .hydrated";
    public static final String BUTTON_SMALL_SUCCESS_CSS= "buttons-demo .button-padding:nth-of-type(5) .button-margin:nth-of-type(4) .hydrated";
    public static final String BUTTON_SMALL_DESTROY_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(5) .hydrated";
    public static final String BUTTON_SMALL_PRIMARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(6) .hydrated";
    public static final String BUTTON_SMALL_SECONDARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(7) .hydrated";
    public static final String BUTTON_SMALL_SUBTLE_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(8) .hydrated";
    public static final String BUTTON_SMALL_DESTROY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(9) .hydrated";
    public static final String BUTTON_SMALL_SUCCESS_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(6) .button-margin:nth-of-type(10) .hydrated";
    public static final String BUTTON_MEDIUM_PRIMARY_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(1) .hydrated";
    public static final String BUTTON_MEDIUM_SECONDARY_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(2) .hydrated";
    public static final String BUTTON_MEDIUM_SUBTLE_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(3) .hydrated";
    public static final String BUTTON_MEDIUM_SUCCESS_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(4) .hydrated";
    public static final String BUTTON_MEDIUM_DESTROY_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(5) .hydrated";
    public static final String BUTTON_MEDIUM_PRIMARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(6) .hydrated";
    public static final String BUTTON_MEDIUM_SECONDARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(7) .hydrated";
    public static final String BUTTON_MEDIUM_SUBTLE_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(8) .hydrated";
    public static final String BUTTON_MEDIUM_DESTROY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(9) .hydrated";
    public static final String BUTTON_MEDIUM_SUCCESS_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(7) .button-margin:nth-of-type(10) .hydrated";
    public static final String BUTTON_LARGE_PRIMARY_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(1) .hydrated";
    public static final String BUTTON_LARGE_SECONDARY_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(2) .hydrated";
    public static final String BUTTON_LARGE_SUBTLE_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(3) .hydrated";
    public static final String BUTTON_LARGE_SUCCESS_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(4) .hydrated";
    public static final String BUTTON_LARGE_DESTROY_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(5) .hydrated";
    public static final String BUTTON_LARGE_PRIMARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(6) .hydrated";
    public static final String BUTTON_LARGE_SECONDARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(7) .hydrated";
    public static final String BUTTON_LARGE_SUBTLE_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(8) .hydrated";
    public static final String BUTTON_LARGE_DESTROY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(9) .hydrated";
    public static final String BUTTON_LARGE_SUCCESS_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(8) .button-margin:nth-of-type(10) .hydrated";
    public static final String BUTTON_LOADING_PRIMARY_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(1) .hydrated";
    public static final String BUTTON_LOADING_SECONDARY_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(2) .hydrated";
    public static final String BUTTON_LOADING_SUBTLE_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(3) .hydrated";
    public static final String BUTTON_LOADING_SUCCESS_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(4) .hydrated";
    public static final String BUTTON_LOADING_DESTROY_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(5) .hydrated";
    public static final String BUTTON_LOADING_PRIMARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(6) .hydrated";
    public static final String BUTTON_LOADING_SECONDARY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(7) .hydrated";
    public static final String BUTTON_LOADING_SUBTLE_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(8) .hydrated";
    public static final String BUTTON_LOADING_DESTROY_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(9) .hydrated";
    public static final String BUTTON_LOADING_SUCCESS_LIGHT_CSS= "buttons-demo .button-padding:nth-of-type(9) .button-margin:nth-of-type(10) .hydrated";
    public static final String INPUT_CSS= "input[name='inputTest']";
    public static final String BUTTON_SUBMIT_CSS= ".example-form .hydrated:nth-child(2)";
    public static final String BUTTON_RESET_CSS= ".example-form .hydrated:nth-child(3)";
    public static final String BUTTON_VANILLA_SUBMIT_CSS= "input[value='Input type submit']";
    public static final String DISPLAY_SUBMITTED_VALUE_CSS = "h5";



    public String getColorPrimary(){
        return getElementShadowRootByCSS(BUTTON_COLOR_PRIMARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorSecondary(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SECONDARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorSubtle(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SUBTLE_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorSuccess(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SUCCESS_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorDestroy(){
        return getElementShadowRootByCSS(BUTTON_COLOR_DESTROY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorPrimaryLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_PRIMARY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorSecondaryLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SECONDARY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorSubtleLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SUBTLE_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorDestroyLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_DESTROY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getColorSuccessLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SUCCESS_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("background-color");
    }

    public String getFontColorPrimary(){
        return getElementShadowRootByCSS(BUTTON_COLOR_DESTROY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("color");
    }

    public String getFont(){
        return getElementShadowRootByCSS(BUTTON_COLOR_DESTROY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("font-family");
    }

    public String getFontColorPrimaryLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_PRIMARY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("color");
    }

    public String getFontColorSecondaryLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SECONDARY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("color");
    }

    public String getFontColorSubtleLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SUBTLE_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("color");
    }

    public String getFontColorDestroyLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_DESTROY_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("color");
    }

    public String getFontColorSuccessLight(){
        return getElementShadowRootByCSS(BUTTON_COLOR_SUCCESS_LIGHT_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"))
                .getCssValue("color");
    }

    public String getLoaderColor(){
        return getElementShadowRootByCSS(BUTTON_LOADING_PRIMARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm")).getCssValue("color");
    }

    public String getHeightSmallButton(){
        return String.valueOf(getElementShadowRootByCSS(BUTTON_SMALL_PRIMARY_CSS).findElement(By.cssSelector(
                ".grit-button .grit-button-sm")).getSize().height);
    }

    public String getHeightMediumButton(){
        return String.valueOf(getElementShadowRootByCSS(BUTTON_MEDIUM_PRIMARY_CSS).findElement(By.cssSelector(
                ".grit-button .grit-button-md")).getSize().height);
    }

    public String getHeightLargeButton(){
        return String.valueOf(getElementShadowRootByCSS(BUTTON_LARGE_PRIMARY_CSS).findElement(By.cssSelector(
                ".grit-button .grit-button-lg")).getSize().height);
    }

    public WebElement getSmallPrimaryButton(){
        return getElementShadowRootByCSS(BUTTON_COLOR_PRIMARY_CSS).findElement(By.cssSelector(".grit-button .grit-button-sm"));
    }

}