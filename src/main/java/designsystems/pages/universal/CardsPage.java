package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CardsPage extends GritCoreUniversalPage {

    public CardsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_CALENDAR = ServicesURLEnum.GritCoreUniversal.getURL() + "/cards";

    public static final String CARD_EXPANDABLE_ID = "card-1";
    public static final String CARD_UPDATE_CARD_ID = "card-2";
    public static final String CARD_ELEVATION_0_CSS = "cards-demo div:nth-of-type(1) .hydrated:nth-child(5)";
    public static final String CARD_ELEVATION_1_CSS = "cards-demo div:nth-of-type(1) .hydrated:nth-child(6)";
    public static final String CARD_ELEVATION_2_CSS = "cards-demo div:nth-of-type(1) .hydrated:nth-child(7)";
    public static final String CARD_ELEVATION_3_CSS = "cards-demo div:nth-of-type(1) .hydrated:nth-child(8)";
    public static final String CARD_ELEVATION_4_CSS = "cards-demo div:nth-of-type(1) .hydrated:nth-child(9)";
    public static final String CARD_ELEVATION_5_CSS = "cards-demo div:nth-of-type(1) .hydrated:nth-child(10)";
    public static final String CARD_DIV_CSS = "div";

    public String getBoxShadow(String Identifier){
        return getElementShadowRootByCSS(Identifier).findElement(By.cssSelector(CARD_DIV_CSS)).getCssValue(
                "box-shadow");
    }

    public String getCardHeight(){
        return String.valueOf(getElementShadowRootByCSS(CARD_ELEVATION_5_CSS).findElement(By.cssSelector(CARD_DIV_CSS))
                .getSize().height);
    }
}