package designsystems.pages.universal;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TopNavBarPage extends GritCoreUniversalPage {

    public TopNavBarPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String TOP_BAR_DEMO_CSS = "#sample-content";
    public static final String TOP_BAR_LEFT_ICON_CSS = "#sample-left-icon-parent .material-icons";
    public static final String TOP_BAR_RIGHT_ICON_CSS = "#sample-right-icon-parent .material-icons";
    public static final String PARAM_FIX_TOP_BAR_CSS = "input#grit-checkbox-control__input-viaxk";
    public static final String PARAM_HIDE_TOP_BAR_LEFT_ICON_CSS = "#grit-checkbox-control__input-qknmj";
    public static final String PARAM_HIDE_TOP_BAR_RIGHT_ICON_CSS = "#grit-checkbox-control__input-utvjb";
    public static final String EVENTS_FROM_TOP_BAR = ".top-nav-bar-container div:nth-child(8)";

    public static final Integer TOP_BAR_HEIGHT = 64;

}
