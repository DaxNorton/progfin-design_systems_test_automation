package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GritCoreUniversalPage extends HostAppPage {

    public GritCoreUniversalPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static String urlUniversalHome = ServicesURLEnum.GritCoreUniversal.getURL();

    public WebElement getElementShadowRoot(WebElement element){ //add .children after the shadowRoot
        return (WebElement) ((JavascriptExecutor)driver).executeScript("return arguments[0].shadowRoot", element);
    }

    public WebElement getElementShadowRootById(String identifier){
        WebElement element = driver.findElement(By.id(identifier));
        return getElementShadowRoot(element);
    }

    public WebElement getElementShadowRootByCSS(String identifier){
        WebElement element = driver.findElement(By.cssSelector(identifier));
        return getElementShadowRoot(element);
    }

    public void clickAutoComplete(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(1)  .hydrated")).click();
    }

    public void clickBadges(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(2) .hydrated")).click();
    }

    public void clickButtons() {
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(3) .hydrated")).click();
    }

    public void clickCalendars(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(4) .hydrated")).click();
    }

    public void clickCards(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(5) .hydrated")).click();
    }

    public void clickCheckboxes() {
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(6) .hydrated")).click();
    }

    public void clickIcons(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(7)  .hydrated")).click();
    }

    public void clickInputs(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(8)  .hydrated")).click();
    }

    public void clickLinearLoaders(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(9)  .hydrated")).click();
    }

    public void clickListSelect(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(10)  .hydrated")).click();
    }

    public void clickNotifications(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(11)  .hydrated")).click();
    }

    public void clickOverlay(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(12)  .hydrated")).click();
    }

    public void clickRadios(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(13)  .hydrated")).click();
    }

    public void clickSliders(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(14)  .hydrated")).click();
    }

    public void clickSpinners(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(15)  .hydrated")).click();
    }

    public void clickSwitches(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(16)  .hydrated")).click();
    }

    public void clickSkeleton(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(17)  .hydrated")).click();
    }

    public void clickTopNavBar(){
        driver.findElement(By.cssSelector("stencil-route-link:nth-of-type(18) .hydrated")).click();
    }

}