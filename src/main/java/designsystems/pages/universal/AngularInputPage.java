package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AngularInputPage extends GritCoreUniversalPage {

    public AngularInputPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_ANGULAR_INPUTS = ServicesURLEnum.GritCoreUniversalAngular.getURL() + "/input";

    public static final String NG_INPUT_NAME_XPATH = "//*[label='First Name']";
    public static final String NG_INPUT_NAME_LABEL_XPATH = "//*[label='First Name']/label";
    public static final String NG_INPUT_PASSWORD_XPATH = "//*[label=\"Password\"]";
    public static final String NG_RESULTS_CSS = "app-input > div > p";
    public static final String REACTIVE_SELECT_ID = "cars";
    public static final String REACTIVE_SELECT_INPUT_ID = "cars-select-input-el";
    public static final String REACTIVE_INPUT_NAME_XPATH = "//*[label='Full name']";
    public static final String REACTIVE_INPUT_EMAIL_XPATH = "//*[label='Email address']";
    public static final String REACTIVE_CHECKBOX_ID = "terms";
    public static final String REACTIVE_SUBMIT_CSS = ".ng-dirty grit-wc-button";
    public static final String REACTIVE_RESULTS_CSS = ".ng-dirty.ng-invalid.ng-touched > div > p";

    public Dimension getInputFieldHeight(){
        return driver.findElement(By.xpath(NG_INPUT_NAME_XPATH)).getSize();
    }

    public String getInputLabelFont(){
        return driver.findElement(By.id(NG_INPUT_NAME_LABEL_XPATH)).getCssValue("font-family").toString();
    }

    public void ngInputFirstName(String text){
        driver.findElement(By.xpath(NG_INPUT_NAME_XPATH)).sendKeys(text);
    }

    public void ngInputPassword(String text){
        driver.findElement(By.xpath(NG_INPUT_PASSWORD_XPATH)).sendKeys(text);
    }

    public String ngGetFormText(){
        return driver.findElement(By.cssSelector(NG_RESULTS_CSS)).getText();
    }

    public void  reactiveSelectCar(Integer option){
        Select selectDropDown = new Select(driver.findElement(By.id(REACTIVE_SELECT_INPUT_ID)));
        selectDropDown.selectByIndex(option);
        driver.findElement(By.id(REACTIVE_SELECT_INPUT_ID)).click();
    }

    public void reactiveInputFullName(String text){
        driver.findElement(By.xpath(REACTIVE_INPUT_NAME_XPATH)).sendKeys(text);
    }

    public void reactiveInputEmail(String text){
        driver.findElement(By.xpath(REACTIVE_INPUT_EMAIL_XPATH)).sendKeys(text);
    }

    public void reactiveClickAgreeToTerms(){
        driver.findElement(By.id(REACTIVE_CHECKBOX_ID)).click();
    }
    public void reactiveClickSubmit(){
        driver.findElement(By.cssSelector(REACTIVE_SUBMIT_CSS)).click();
    }

    public String reactiveGetFormText(){
        return driver.findElement(By.cssSelector(REACTIVE_RESULTS_CSS)).getText();
    }
}