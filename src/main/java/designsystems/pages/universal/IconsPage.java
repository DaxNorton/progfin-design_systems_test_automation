package designsystems.pages.universal;

import helpers.ServicesURLEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IconsPage extends GritCoreUniversalPage {

    public IconsPage(WebDriver driverIn, WebDriverWait driverWaitIn) {
        super(driverIn, driverWaitIn);
    }

    public static final String URL_ICONS = ServicesURLEnum.GritCoreUniversal.getURL() + "/icons";

    public static final String ICON_SIZE_10_CSS = "icons-demo div .hydrated:nth-child(3)";
    public static final String ICON_SIZE_10_BORDER_CSS = "icons-demo div .hydrated:nth-child(4)";
    public static final String ICON_SIZE_20_CSS = "icons-demo div .hydrated:nth-child(6)";
    public static final String ICON_SIZE_20_BORDER_CSS = "icons-demo div .hydrated:nth-child(7)";
    public static final String ICON_SIZE_44_CSS = "icons-demo div .hydrated:nth-child(9)";
    public static final String ICON_SIZE_44_BORDER_CSS = "icons-demo div .hydrated:nth-child(10)";
    public static final String ICON_SIZE_80_CSS = "icons-demo div .hydrated:nth-child(12)";
    public static final String ICON_SIZE_80_BORDER_CSS = "icons-demo div .hydrated:nth-child(13)";
    public static final String ICON_ALARM_CSS = "icons-demo div .hydrated:nth-child(16)";
    public static final String ICON_CREDIT_CARD_CSS = "icons-demo div .hydrated:nth-child(17)";
    public static final String ICON_HEART_CSS = "icons-demo div .hydrated:nth-child(18)";
    public static final String ICON_STAR_CSS = "icons-demo div .hydrated:nth-child(19)";
    public static final String ICON_PAW_CSS = "icons-demo div .hydrated:nth-child(20)";
    public static final String ICON_EDIT_CSS = "icons-demo div .hydrated:nth-child(21)";
    public static final String ICON_ALARM_BLACK_CSS = "icons-demo div .hydrated:nth-child(23)";
    public static final String ICON_ALARM_PURPLE_CSS = "icons-demo div .hydrated:nth-child(24)";
    public static final String ICON_ALARM_BLUE_CSS = "icons-demo div .hydrated:nth-child(25)";
    public static final String ICON_ALARM_GREEN_CSS = "icons-demo div .hydrated:nth-child(26)";
    public static final String ICON_ALARM_YELLOW_CSS = "icons-demo div .hydrated:nth-child(27)";
    public static final String ICON_ALARM_ORANGE_CSS = "icons-demo div .hydrated:nth-child(28)";
    public static final String ICON_ALARM_RED_CSS = "icons-demo div .hydrated:nth-child(29)";
    public static final String ICON_CREDIT_CARD_BLACK_CSS = "icons-demo div .hydrated:nth-child(31)";
    public static final String ICON_CREDIT_CARD_PURPLE_CSS = "icons-demo div .hydrated:nth-child(32)";
    public static final String ICON_CREDIT_CARD_BLUE_CSS = "icons-demo div .hydrated:nth-child(33)";
    public static final String ICON_CREDIT_CARD_GREEN_CSS = "icons-demo div .hydrated:nth-child(34)";
    public static final String ICON_CREDIT_CARD_YELLOW_CSS = "icons-demo div .hydrated:nth-child(35)";
    public static final String ICON_CREDIT_CARD_ORANGE_CSS = "icons-demo div .hydrated:nth-child(36)";
    public static final String ICON_CREDIT_CARD_RED_CSS = "icons-demo div .hydrated:nth-child(37)";
    public static final String ICON_HEART_BLACK_CSS = "icons-demo div .hydrated:nth-child(39)";
    public static final String ICON_HEART_PURPLE_CSS = "icons-demo div .hydrated:nth-child(40)";
    public static final String ICON_HEART_BLUE_CSS = "icons-demo div .hydrated:nth-child(41)";
    public static final String ICON_HEART_GREEN_CSS = "icons-demo div .hydrated:nth-child(42)";
    public static final String ICON_HEART_YELLOW_CSS = "icons-demo div .hydrated:nth-child(43)";
    public static final String ICON_HEART_ORANGE_CSS = "icons-demo div .hydrated:nth-child(44)";
    public static final String ICON_HEART_RED_CSS = "icons-demo div .hydrated:nth-child(45)";
    public static final String ICON_STAR_BLACK_CSS = "icons-demo div .hydrated:nth-child(47)";
    public static final String ICON_STAR_PURPLE_CSS = "icons-demo div .hydrated:nth-child(48)";
    public static final String ICON_STAR_BLUE_CSS = "icons-demo div .hydrated:nth-child(49)";
    public static final String ICON_STAR_GREEN_CSS = "icons-demo div .hydrated:nth-child(50)";
    public static final String ICON_STAR_YELLOW_CSS = "icons-demo div .hydrated:nth-child(51)";
    public static final String ICON_STAR_ORANGE_CSS = "icons-demo div .hydrated:nth-child(52)";
    public static final String ICON_STAR_RED_CSS = "icons-demo div .hydrated:nth-child(53)";
    public static final String ICON_PAW_BLACK_CSS = "icons-demo div .hydrated:nth-child(55)";
    public static final String ICON_PAW_PURPLE_CSS = "icons-demo div .hydrated:nth-child(56)";
    public static final String ICON_PAW_BLUE_CSS = "icons-demo div .hydrated:nth-child(57)";
    public static final String ICON_PAW_GREEN_CSS = "icons-demo div .hydrated:nth-child(58)";
    public static final String ICON_PAW_YELLOW_CSS = "icons-demo div .hydrated:nth-child(59)";
    public static final String ICON_PAW_ORANGE_CSS = "icons-demo div .hydrated:nth-child(60)";
    public static final String ICON_PAW_RED_CSS = "icons-demo div .hydrated:nth-child(61)";
    public static final String ICON_EDIT_BLACK_CSS = "icons-demo div .hydrated:nth-child(63)";
    public static final String ICON_EDIT_PURPLE_CSS = "icons-demo div .hydrated:nth-child(64)";
    public static final String ICON_EDIT_BLUE_CSS = "icons-demo div .hydrated:nth-child(65)";
    public static final String ICON_EDIT_GREEN_CSS = "icons-demo div .hydrated:nth-child(66)";
    public static final String ICON_EDIT_YELLOW_CSS = "icons-demo div .hydrated:nth-child(67)";
    public static final String ICON_EDIT_ORANGE_CSS = "icons-demo div .hydrated:nth-child(68)";
    public static final String ICON_EDIT_RED_CSS = "icons-demo div .hydrated:nth-child(69)";
    public static final String ICON_ALARM_BLACK_BORDERED_CSS = "icons-demo div .hydrated:nth-child(72)";
    public static final String ICON_ALARM_PURPLE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(73)";
    public static final String ICON_ALARM_BLUE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(74)";
    public static final String ICON_ALARM_GREEN_BORDERED_CSS = "icons-demo div .hydrated:nth-child(75)";
    public static final String ICON_ALARM_YELLOW_BORDERED_CSS = "icons-demo div .hydrated:nth-child(76)";
    public static final String ICON_ALARM_ORANGE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(77)";
    public static final String ICON_ALARM_RED_BORDERED_CSS = "icons-demo div .hydrated:nth-child(78)";
    public static final String ICON_CREDIT_CARD_BLACK_BORDERED_CSS = "icons-demo div .hydrated:nth-child(80)";
    public static final String ICON_CREDIT_CARD_PURPLE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(81)";
    public static final String ICON_CREDIT_CARD_BLUE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(82)";
    public static final String ICON_CREDIT_CARD_GREEN_BORDERED_CSS = "icons-demo div .hydrated:nth-child(83)";
    public static final String ICON_CREDIT_CARD_YELLOW_BORDERED_CSS = "icons-demo div .hydrated:nth-child(84)";
    public static final String ICON_CREDIT_CARD_ORANGE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(85)";
    public static final String ICON_CREDIT_CARD_RED_BORDERED_CSS = "icons-demo div .hydrated:nth-child(86)";
    public static final String ICON_HEART_BLACK_BORDERED_CSS = "icons-demo div .hydrated:nth-child(88)";
    public static final String ICON_HEART_PURPLE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(89)";
    public static final String ICON_HEART_BLUE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(90)";
    public static final String ICON_HEART_GREEN_BORDERED_CSS = "icons-demo div .hydrated:nth-child(91)";
    public static final String ICON_HEART_YELLOW_BORDERED_CSS = "icons-demo div .hydrated:nth-child(92)";
    public static final String ICON_HEART_ORANGE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(93)";
    public static final String ICON_HEART_RED_BORDERED_CSS = "icons-demo div .hydrated:nth-child(94)";
    public static final String ICON_STAR_BLACK_BORDERED_CSS = "icons-demo div .hydrated:nth-child(96)";
    public static final String ICON_STAR_PURPLE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(97)";
    public static final String ICON_STAR_BLUE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(98)";
    public static final String ICON_STAR_GREEN_BORDERED_CSS = "icons-demo div .hydrated:nth-child(99)";
    public static final String ICON_STAR_YELLOW_BORDERED_CSS = "icons-demo div .hydrated:nth-child(100)";
    public static final String ICON_STAR_ORANGE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(101)";
    public static final String ICON_STAR_RED_BORDERED_CSS = "icons-demo div .hydrated:nth-child(102)";
    public static final String ICON_PAW_BLACK_BORDERED_CSS = "icons-demo div .hydrated:nth-child(104)";
    public static final String ICON_PAW_PURPLE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(105)";
    public static final String ICON_PAW_BLUE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(106)";
    public static final String ICON_PAW_GREEN_BORDERED_CSS = "icons-demo div .hydrated:nth-child(107)";
    public static final String ICON_PAW_YELLOW_BORDERED_CSS = "icons-demo div .hydrated:nth-child(108)";
    public static final String ICON_PAW_ORANGE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(109)";
    public static final String ICON_PAW_RED_BORDERED_CSS = "icons-demo div .hydrated:nth-child(110)";
    public static final String ICON_EDIT_BLACK_BORDERED_CSS = "icons-demo div .hydrated:nth-child(112)";
    public static final String ICON_EDIT_PURPLE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(113)";
    public static final String ICON_EDIT_BLUE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(114)";
    public static final String ICON_EDIT_GREEN_BORDERED_CSS = "icons-demo div .hydrated:nth-child(115)";
    public static final String ICON_EDIT_YELLOW_BORDERED_CSS = "icons-demo div .hydrated:nth-child(116)";
    public static final String ICON_EDIT_ORANGE_BORDERED_CSS = "icons-demo div .hydrated:nth-child(117)";
    public static final String ICON_EDIT_RED_BORDERED_CSS = "icons-demo div .hydrated:nth-child(118)";

    public String getIconColor(String identifier){
        return driver.findElement(By.cssSelector(identifier)).getCssValue("color");
    }

    public String getBorderColor(String identifier){
        return driver.findElement(By.cssSelector(identifier)).getCssValue("border");
    }

    public  String getBoxShadowColor(String identifier){
        return driver.findElement(By.cssSelector(identifier)).getCssValue("box-shadow");
    }
}