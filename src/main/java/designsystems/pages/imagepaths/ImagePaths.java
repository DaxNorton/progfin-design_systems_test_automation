package designsystems.pages.imagepaths;

import java.io.File;

public class ImagePaths {

    static File currentDirFile = new File(".");
    static String helper = currentDirFile.getAbsolutePath();
    public static String currentDir = helper.substring(0, helper.length()-1);
    static final String BASELINE_PATH = currentDir + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "screenshots" + File.separator + "baseline" + File.separator;
    static final String ACTUAL_PATH = currentDir + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "screenshots" + File.separator + "actual" + File.separator;
    static final String FEEDBACK_PATH = currentDir + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "screenshots" + File.separator + "feedback" + File.separator;

    //LocalChrome_Paths
    static final String LOCAL_CHROME_BASELINE = BASELINE_PATH + "chrome_local" + File.separator + "chrome_local";
    static final String LOCAL_CHROME_ACTUAL = ACTUAL_PATH + "chrome_local" + File.separator + "chrome_local";
    static final String LOCAL_CHROME_FEEDBACK = FEEDBACK_PATH + "chrome_local" + File.separator + "chrome_local";

    static final String LOCAL_FIREFOX_BASELINE = BASELINE_PATH + "firefox_local" + File.separator + "firefox_local";
    static final String LOCAL_FIREFOX_ACTUAL = ACTUAL_PATH + "firefox_local" + File.separator + "firefox_local";
    static final String LOCAL_FIREFOX_FEEDBACK = FEEDBACK_PATH + "firefox_local" + File.separator + "firefox_local";

    static final String SAUCE_MAC_SAFARI_BASELINE = BASELINE_PATH + "sauce_mac_safari" + File.separator + "sauce_mac_safari";
    static final String SAUCE_MAC_SAFARI_ACTUAL = ACTUAL_PATH + "sauce_mac_safari" + File.separator + "sauce_mac_safari";
    static final String SAUCE_MAC_SAFARI_FEEDBACK = FEEDBACK_PATH + "sauce_mac_safari" + File.separator + "sauce_mac_safari";

    static final String SAUCE_MAC_CHROME_BASELINE = BASELINE_PATH + "sauce_mac_chrome" + File.separator + "sauce_mac_chrome";
    static final String SAUCE_MAC_CHROME_ACTUAL = ACTUAL_PATH + "sauce_mac_chrome" + File.separator + "sauce_mac_chrome";
    static final String SAUCE_MAC_CHROME_FEEDBACK = FEEDBACK_PATH + "sauce_mac_chrome" + File.separator + "sauce_mac_chrome";

    static final String SAUCE_WINDOWS_CHROME_BASELINE = BASELINE_PATH + "windows_chrome_sauce" + File.separator + "windows_chrome_sauce";
    static final String SAUCE_WINDOWS_CHROME_ACTUAL = ACTUAL_PATH + "windows_chrome_sauce" + File.separator + "windows_chrome_sauce";
    static final String SAUCE_WINDOWS_CHROME_FEEDBACK = FEEDBACK_PATH + "windows_chrome_sauce" + File.separator + "windows_chrome_sauce";

    static final String SAUCE_WINDOWS_FIREFOX_BASELINE = BASELINE_PATH + "windows_firefox_sauce" + File.separator + "windows_firefox_sauce";
    static final String SAUCE_WINDOWS_FIREFOX_ACTUAL = ACTUAL_PATH + "windows_firefox_sauce" + File.separator + "windows_firefox_sauce";
    static final String SAUCE_WINDOWS_FIREFOX_FEEDBACK = FEEDBACK_PATH + "windows_firefox_sauce" + File.separator + "windows_firefox_sauce";

    static final String SAUCE_WINDOWS_EDGE_BASELINE = BASELINE_PATH + "windows_edge_sauce" + File.separator + "windows_edge_sauce";
    static final String SAUCE_WINDOWS_EDGE_ACTUAL = ACTUAL_PATH + "windows_edge_sauce" + File.separator + "windows_edge_sauce";
    static final String SAUCE_WINDOWS_EDGE_FEEDBACK = FEEDBACK_PATH + "windows_edge_sauce" + File.separator + "windows_edge_sauce";

    static final String SAUCE_IPHONEXR_BASELINE = BASELINE_PATH + "iphonexr_sauce" + File.separator + "iphonexr_sauce";
    static final String SAUCE_IPHONEXR_ACTUAL = ACTUAL_PATH + "iphonexr_sauce" + File.separator + "iphonexr_sauce";
    static final String SAUCE_IPHONEXR_FEEDBACK = FEEDBACK_PATH + "iphonexr_sauce" + File.separator + "iphonexr_sauce";




    static final String ACTUAL = "_actual_";
    static final String FEEDBACK = "_feedback_";
    static final String BASELINE = "_baseline_";

    static final String MOBILEVIEWPORT = "_mobileview_";
    static final String TABLETVIEWPORT = "_tabletview_";

    //*paths used for building new tests if needed*
    static final String ACTUAL_TESTSHOT = ACTUAL_PATH + "actual_testShot.png";
    static final String ACTUAL_TESTSHOT_1 = ACTUAL_PATH + "actual_testShot1.png";
    static final String ACTUAL_TESTSHOT_2 = ACTUAL_PATH + "actual_testShot2.png";
    static final String BASELINE_TESTSHOT = BASELINE_PATH +"baseline_testShot.png";
    static final String BASELINE_TESTSHOT_1 = BASELINE_PATH + "baseline_testShot1.png";
    static final String BASELINE_TESTSHOT_2 = BASELINE_PATH + "baseline_testShot2.png";
    static final String FEEDBACK_TESTSHOT = FEEDBACK_PATH + "feedback_testShot.png";
    static final String FEEDBACK_TESTSHOT_1 = FEEDBACK_PATH + "feedback_testShot1.png";
    static final String FEEDBACK_TESTSHOT_2 = FEEDBACK_PATH + "feedback_testShot2.png";

    // Grit - DevDocs page initial views .png helper
    static final String HOME_VIEW = "HomeView.png";
    static final String GETSTARTED_COLLAPSED = "GetStarted_Collapsed.png";
    static final String GETSTARTED = "GetStartedPage.png";
    static final String GETSTARTED_HERO_WRAPPER = "GetStarted_HeroWrapper.png";
    static final String GETSTARTED_HOME_CONTENT = "GetStarted_HomeContent.png";
    static final String GETSTARTED_DEVELOPER = "GetStarted_Developer.png";
    static final String GETSTARTED_DESIGNER = "GetStarted_Designer.png";
    static final String GETSTARTED_DEVELOPER_HERO = "GetStarted_Developer_Hero.png";
    static final String GETSTARTED_DEVELOPER_SECTION_1 = "GetStarted_Developer_Section1.png";
    static final String GETSTARTED_DEVELOPER_SECTION_2 = "GetStarted_Developer_Section2.png";
    static final String GETSTARTED_DEVELOPER_SECTION_3 = "GetStarted_Developer_Section3.png";
    static final String GETSTARTED_DEVELOPER_SECTION_4 = "GetStarted_Developer_Section4.png";
    static final String GETSTARTED_DEVELOPER_SECTION_5 = "GetStarted_Developer_Section5.png";
    static final String GETSTARTED_DEVELOPER_SECTION_6 = "GetStarted_Developer_Section6.png";
    static final String GETSTARTED_DESIGNER_HERO = "GetStarted_Designer_Hero.png";
    static final String GETSTARTED_DESIGNER_SECTION_1 = "GetStarted_Designer_Section1.png";
    static final String GETSTARTED_DESIGNER_SECTION_2 = "GetStarted_Designer_Section2.png";
    static final String GETSTARTED_DESIGNER_SECTION_3 = "GetStarted_Designer_Section3.png";
    static final String GETSTARTED_DESIGNER_SECTION_4 = "GetStarted_Designer_Section4.png";
    static final String GETSTARTED_DESIGNER_SECTION_5 = "GetStarted_Designer_Section5.png";
    static final String GETSTARTED_DESIGNER_SECTION_6 = "GetStarted_Designer_Section6.png";
    static final String GETSTARTED_DESIGNER_SECTION_7 = "GetStarted_Designer_Section7.png";
    static final String RELEASE_CURRENT = "Release_Current.png";
    static final String ROADMAP = "Roadmap.png";
    static final String FOUNDATIONS = "FoundationsPagePage.png";
    static final String FOUNDATIONS_COLOR = "Foundations_Color.png";
    static final String FOUNDATIONS_ELEVATION = "Foundations_Elevation.png";
    static final String FOUNDATIONS_SPACING = "Foundations_Spacing.png";
    static final String FOUNDATIONS_TYPOGRAPHY = "Foundations_Typography.png";
    static final String COMPONENTS = "ComponentsPage.png";
    static final String COMPONENTS_HERO_WRAPPER = "Components_Hero_Wrapper.png";
    static final String COMPONENTS_HOME_CONTENT = "Components_Home_Content.png";
    static final String COMPONENTS_BADGES_CODE = "Components_Badges_Code.png";
    static final String COMPONENTS_BADGES_DESIGN = "Components_Badges_Design.png";
    static final String COMPONENTS_BUTTONS = "Components_Buttons.png";
    static final String COMPONENTS_BUTTONS_CODE = "Components_Buttons_Code.png";
    static final String COMPONENTS_BUTTONS_DESIGN = "Components_Buttons_Design.png";
    static final String COMPONENTS_CHECKBOXES_CODE = "Components_Checkboxes_Code.png";
    static final String COMPONENTS_CHECKBOXES_DESIGN = "Components_Checkboxes_Design.png";
    static final String COMPONENTS_LOADERS_CODE = "Components_Loaders_Code.png";
    static final String COMPONENTS_LOADERS_DESIGN = "Components_Loaders_Design.png";
    static final String COMPONENTS_NOTIFICATIONS_CODE = "Components_Notifications_Code.png";
    static final String COMPONENTS_NOTIFICATIONS_DESIGN = "Components_Notifications_Design.png";
    static final String COMPONENTS_OVERLAYS_CODE = "Components_Overlays_Code.png";
    static final String COMPONENTS_OVERLAYS_DESIGN = "Components_Overlays_Design.png";
    static final String COMPONENTS_RADIOS_CODE = "Components_Radios_Code.png";
    static final String COMPONENTS_RADIOS_CODE_HERO = "Components_Radios_Code_Hero.png";
    static final String COMPONENTS_RADIOS_CODE_SECTION_1 = "Components_Radios_Code_Section1.png";
    static final String COMPONENTS_RADIOS_CODE_SECTION_2 = "Components_Radios_Code_Section2.png";
    static final String COMPONENTS_RADIOS_CODE_SECTION_3 = "Components_Radios_Code_Section3.png";
    static final String COMPONENTS_RADIOS_DESIGN = "Components_Radios_Design.png";
    static final String COMPONENTS_SWITCHES_CODE = "Components_Switches_Code.png";
    static final String COMPONENTS_SWITCHES_DESIGN = "Components_Switches_Design.png";
    static final String COMPONENTS_TEXT_INPUTS_CODE = "Components_TextInputs_Code.png";
    static final String COMPONENTS_TEXT_INPUTS_DESIGN = "Components_TextInputs_Design.png";
    static final String COMPONENT_HERO_BADGE_AVAILABLE = "Components_HeroBadge_Available.png";
    static final String COMPONENT_BADGE_DEFAULT_WITH_ICON = "Badge_DefaultWithIcon.png";

    //*GritCore - ComponentsPage.png helper
    static final String BADGES = "BadgesTestPage.png";
    static final String BUTTONS = "ButtonsTestPage.png";
    static final String CALENDARS = "Calendar.png";
    static final String CARDS = "Cards.png";
    static final String CHECKBOXES = "Checkbox.png";
    static final String DROPDOWNS = "Dropdown.png";
    static final String ICONS = "Icons.png";
    static final String INPUTS = "Inputs.png";
    static final String LINEARLOADERS = "LinearLoaders.png";
    static final String LISTSELECT = "ListSelect.png";
    static final String NOTIFICATIONS = "Notifications.png";
    static final String RADIOS = "Radio.png";
    static final String SLIDERS = "Slider.png";
    static final String SPINNERS = "Spinners.png";
    static final String SWITCHES = "Switches.png";
    static final String TOPBARS = "TopBars.png";
    static final String OVERLAYS = "Overlay.png";
    static final String MASTERVIEW = "Home.png";

    static final String BADGE_OVERVIEW = "BadgeOverview.png";
    static final String BADGE_DEFAULT_ICON = "BadgeDefaultIcon.png";
    static final String BADGE_INFO_ICON = "BadgeInfoIcon.png";
    static final String BADGE_PRIMARY_ICON = "BadgePrimaryIcon.png";
    static final String BADGE_ATTENTION_ICON = "BadgeAttentionIcon.png";
    static final String BADGE_ATTENTIONSTRONG_ICON = "BadgeAttentionStrongIcon.png";
    static final String BADGE_SUCCESS_ICON = "BadgeSuccessIcon.png";
    static final String BADGE_WARNING_ICON = "BadgeWarningIcon.png";

    static final String INPUT_NORMAL_FIELD = "Input_NormalField.png";
    static final String INPUT_OVERVIEW = "Input_Overview.png";
    static final String INPUT_OVERVIEW_MOBILE = "Input_Overview_Mobile.png";

    //GritUniversal
    static final String UNIVERSAL_AUTOCOMPLETE_DEFAULT = "Universal_AutocompleteDefault.png";
}
