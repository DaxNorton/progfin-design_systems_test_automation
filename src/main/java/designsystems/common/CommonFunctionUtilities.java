package designsystems.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Abstract out shared functions commonly called from field validation tests.
 * <p>
 * Version 1.1 2019-03-05 add vertical scroll, fix, and re-test
 * <p>
 * Version 2.0 2019-07-31:
 * <ul>
 * <li>Revert static. Static was causing downstream problems, and wasn't working in all browsers.</li>
 * <li>Add full-length vertical scrolls.</li>
 * </ul>
 * <p>
 * Version 2.1 2019-08:
 * <ul>
 * <li>No longer require the driver to be passed in to the constructor.</li>
 * <li>Do require the driver in specific methods that need it.</li>
 * <li>Add sleeper method that takes millis argument.</li>
 * </ul>
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @author <a href="mailto:sandeep.m@progleasing.com">Sandeep M</a>
 * @version 2.1
 */
public class CommonFunctionUtilities {

    public CommonFunctionUtilities() {
        // default no args
    }

    public boolean isElementPresent(WebElement ele) {
        boolean flag = false;
        try {
            if(ele.isDisplayed())
            {
                flag = true;
            }
        } catch (org.openqa.selenium.NoSuchElementException e) {
            flag = false;
        }
        return flag;
    }

    public boolean isElementNotPresent(WebDriver driver, By by) {
        boolean flag = false;
        try {
            int size = driver.findElements(by).size();
            if(size == 0)
            {
                flag = true;
            }
        } catch (org.openqa.selenium.NoSuchElementException e) {
            flag = false;
        }
        return flag;
    }

    public boolean isElementEnabled(WebElement ele) {
        boolean flag = false;
        try {
            if(ele.isEnabled())
            {
                flag = true;
            }
        } catch (org.openqa.selenium.InvalidElementStateException e) {
            flag = false;
        }
        return flag;
    }

    public boolean isChecked(WebElement ele, String eleName) {
        boolean flag = false;
        if(isElementPresent(ele))
        {
            if(ele.isSelected())
            {
                flag = true;
            }
        }
        else
        {
            System.out.println(eleName + " is not displayed");
        }
        return flag;
    }

    public void enterText(WebElement ele, String text, String eleName)
    {
        if(isElementPresent(ele))
        {
            if(isElementEnabled(ele))
            {
                ele.clear();
                ele.sendKeys(text);
                System.out.println("Entered text " + text + "in textbox " + eleName);
            }
            else
            {
                System.out.println("Textbox " + eleName + " is not enabled");
            }
        }
        else
        {
            System.out.println("Textbox " + eleName + " is not displayed");
        }
    }

    public void selectByVisibleText(WebElement ele, String val, String eleName)
    {
        if(isElementPresent(ele))
        {
            if(isElementEnabled(ele))
            {
                Select sel = new Select(ele);
                sel.selectByVisibleText(val);
                System.out.println("Selected value \"" + val + "\" from dropdown " + eleName);
            }
            else
            {
                System.out.println("Dropdown " + eleName + " is not enabled");
            }
        }
        else
        {
            System.out.println("Dropdown " + eleName + " is not displayed");
        }
    }

    public void selectByPartialVisibleText(WebElement ele, String val, String eleName)
    {
        if(isElementPresent(ele))	{
            if(isElementEnabled(ele))	{
                clickOn(ele, eleName);
                Select sel = new Select(ele);
                List<WebElement> options = sel.getOptions();

                for (WebElement option : options) {
                    if (option.getText().contains(val)) {
                        option.click();
                        break;
                    }
                }
            } else {
                System.out.println("Dropdown " + eleName + " is not enabled");
            }
        } else {
            System.out.println("Dropdown " + eleName + " is not displayed");
        }
    }

    public void selectByIndex(WebElement ele, int index, String eleName)
    {
        if(isElementPresent(ele))
        {
            if(isElementEnabled(ele))
            {
                Select sel = new Select(ele);
                sel.selectByIndex(index);
                String val = sel.getFirstSelectedOption().getText();
                System.out.println("Selected value \"" + val + "\" from dropdown " + eleName);
            }
            else
            {
                System.out.println("Dropdown " + eleName + " is not enabled");
            }
        }
        else
        {
            System.out.println("Dropdown " + eleName + " is not displayed");
        }
    }

    public void clickOn(WebElement ele, String eleName)
    {
        if(isElementPresent(ele))
        {
            if(isElementEnabled(ele))
            {
                ele.click();
                System.out.println("Clicked on " + eleName);
            }
            else
            {
                System.out.println(eleName + " is not enabled");
            }
        }
        else
        {
            System.out.println(eleName + " is not displayed");
        }
    }

    public String getTextboxValue(WebElement ele, String eleName)
    {
        String val = "";
        if(isElementPresent(ele))
        {
            val = ele.getAttribute("value");
        }
        else
        {
            System.out.println("Textbox " + eleName + " is not displayed");
        }
        return val;
    }

    public String getListSelectedText(WebElement ele, String eleName)
    {
        String val = "";
        if(isElementPresent(ele))
        {
            Select sel = new Select(ele);
            val = sel.getFirstSelectedOption().getText();
        }
        else
        {
            System.out.println("Dropdown " + eleName + " is not displayed");
        }
        return val;
    }

    public void waitForPageLoaded(WebDriver driver)
    {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(pageLoadCondition);
    }

    public void waitForElement(WebDriver driver, By by, int timeInSecs) throws InterruptedException
    {
        int count = 0;
        int waitTime = timeInSecs / 10;
        do
        {
            Thread.sleep(1000);
            int size = driver.findElements(by).size();
            if(size > 0)
            {
                break;
            }
            count = count + 1;
            if(count == waitTime)
            {
                break;
            }
            Thread.sleep(9000);
        }	while(true);
    }

    public void waitForElementNotPresent(WebDriver driver, By by, int timeInSecs) throws InterruptedException
    {
        int count = 0;
        int waitTime = timeInSecs / 10;
        do
        {
            Thread.sleep(1000);
            Dimension size = driver.findElement(by).getSize();
            if(size.height <= 0)
            {
                break;
            }
            count = count + 1;
            if(count == waitTime)
            {
                break;
            }
            Thread.sleep(9000);
        }	while(true);
    }

    public String getAttribute(WebElement ele, String attr) {
        String attrVal = "";
        if(isElementPresent(ele)) {
            switch(attr.toLowerCase()) {
                case "text":
                    attrVal = ele.getText();
                    break;
                default:
                    attrVal = ele.getAttribute(attr);
                    break;
            }
        }
        return attrVal;
    }

    public void waitForElementAttribute(WebElement ele, String attr, String attrVal) throws InterruptedException
    {
        int count = 0;
        boolean flag = false;
        do
        {
            Thread.sleep(1000);
            switch(attr.toLowerCase())
            {
                case "text":
                    if(ele.getText().contains(attrVal)) {
                        flag = true;
                        break;
                    }
                default:
                    if(ele.getAttribute(attr).contains(attrVal)) {
                        flag = true;
                        break;
                    }
            }
            if(flag) break;
            count = count + 1;
            if(count == 10) break;
            Thread.sleep(9000);
        }	while(true);
    }

    public String getFieldText(WebElement ele, String eleName)
    {
        String val = "";
        if(isElementPresent(ele))
        {
            val = ele.getText();
        }
        else
        {
            System.out.println("Field " + eleName + " is not displayed");
        }
        return val;
    }

    public void ScrollByPage(WebDriver driver) {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        //This will scroll the web page till end.
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public boolean isAlertPresent(WebDriver driver)
    {
        try
        {
            driver.switchTo().alert();
            return true;
        }   // try
        catch (NoAlertPresentException Ex)
        {
            return false;
        }
    }

    public void clickOnAlert(WebDriver driver, String YesOrNo)
    {
        if(isAlertPresent(driver))
        {
            switch(YesOrNo.toLowerCase()) {
                case "yes":
                    driver.switchTo().alert().accept();
                    System.out.println("Clicked on Yes/OK in alert");
                    break;
                case "no":
                    driver.switchTo().alert().dismiss();
                    System.out.println("Clicked on No/Cancel in alert");
                    break;
            }
        }
        else
        {
            System.out.println("Alert is not displayed");
        }
    }


}