/*
 * Copyright Progressive Leasing LLC.
 */
package designsystems.common;

/**
 * A designsystems.common class for constants, following Oracle's example.
 * <p>
 * For any constants specific to Sauce Labs, and public-facing already. 
 * <p>
 * Add anything you like here, so long as you make it a constant.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class SauceLabsConstants {

    /**
     * Do not instantiate this class.
     */
    private SauceLabsConstants() {
        // A single private no-args constructor.
    }

    /**
     * Tests running in Sauce Labs will need this URL in their setup methods,
     * before navigation to any URL under test.
     * <p>
     * For European Union data center, see URL_DEVICE_CLOUD_EU.
     * <p>
     */
    public static final String URL_DEVICE_CLOUD_USA = "https://ondemand.saucelabs.com/wd/hub";

    /**
     * This URL is used for connecting to SauceLabs API for management purposes, Eg.
     * Removing jobs by their id.
     */
    public static final String URL_ManagementAPI_V1 = "https://saucelabs.com/rest/v1/";

    /**
     * Temporary value for testing.
     * <p>
     * When we move forward with real devices, we need to rename
     * these URLs.
     * <p>
     * The real device URLs will
     */
    public static final String URL_DEVICE_CLOUD_USA_RD = "https://us1.appium.testobject.com/wd/hub";

    public static final String TESTOBJECT_APPIUM_API_ENDPOINT = "https://app.testobject.com/api/rest/v2/appium/";

    /**
     * Tests running in Sauce Labs will need this URL in their setup methods,
     * before navigation to any URL under test.
     * <p>
     * For United States data center, see URL_DEVICE_CLOUD_USA.
     * <p>
     */
    public static final String URL_DEVICE_CLOUD_EU = "https://ondemand.eu-central-1.saucelabs.com/wd/hub";

    /**
     * Sauce Labs public-facing landing page.
     */
    public static final String URL_SAUCE_PORTAL = "https://saucelabs.com/";


    /**
     * Sauce Labs portal login.
     */
    public static final String URL_SAUCE_PORTAL_LOGIN = "https://app.saucelabs.com/login";


    /**
     * Optional (and very useful!) platform configurator saves you some typing (and typos).
     */
    public static final String URL_PLATFORM_CONFIGURATOR = "https://wiki.saucelabs.com/display/DOCS/Platform+Configurator#/";

    /**
     * From the Sauce Labs wiki:
     * <p>
     * "As a safety measure to prevent tests from running indefinitely,
     * Sauce limits the duration of tests to 30 minutes by default. You
     * can adjust this limit on per-job basis and the maximum value is
     * 10800 seconds...A test should never last more than 30 minutes and
     * ideally should take less than five minutes...."
     */
    public static final int MAX_DURATION = 480; // max runtime in seconds

    /**
     * From the Sauce Labs wiki:
     * <p>
     * "As a safety measure to prevent tests from running too long after
     * something has gone wrong, Sauce limits how long a browser can wait
     * for a test to send a new command. This is set to 90 seconds by
     * default and limited to a maximum value of 1000 seconds. You can
     * adjust this limit on a per-job basis. The value of this setting
     * is given in seconds."
     * <p>
     * 2019-10-22 increase from 120 to 180 for mobile emulators which are slower
     */
    public static final int IDLE_TIMEOUT = 180; // max idle in seconds

    /**
     * From the Sauce Labs wiki:
     * <p>
     * "As a safety measure to prevent Selenium crashes from making your
     * tests run indefinitely, Sauce limits how long Selenium can take
     * to run a command in our browsers. This is set to 300 seconds by
     * default. The value of this setting is given in seconds. The maximum
     * command timeout value allowed is 600 seconds."
     */
    public static final int COMMAND_TIMEOUT = 300; // max command time in seconds

}