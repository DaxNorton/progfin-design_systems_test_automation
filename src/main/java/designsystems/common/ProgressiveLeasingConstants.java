/*
 * Copyright Progressive Leasing LLC.
 */
package designsystems.common;

/**
 * A designsystems.common class for constants, following Oracle's example.
 * <p>
 * For any constants specific to Progressive Leasing: URLs, titles, and so on.
 * <p>
 * Add anything you like here, so long as you make it a constant.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class ProgressiveLeasingConstants {

    /**
     * Do not instantiate this class.
     */
    private ProgressiveLeasingConstants() {
        // A single private no-args.
        // Do not instantiate.
    }

    private static final String GRITQA19 = "https://vdc-qaswebapp19.stormwind.local/grit/";

    private static final String GRITQA08 = "https://vdc-qaswebapp08.stormwind.local/grit/";

    private static final String GRITRC = "http://rc.gritui.io/grit/";

    private static final String GRITPROD = "https://slc-prdwebiws.stormwind.local/grit/";

    private static final String GRITCOREQA = "https://vdc-qaswebapp19.stormwind.local/gritcore/#/";

    private static final String GRITCOREQA08 = "https://vdc-qaswebapp08.stormwind.local/gritcore/#/";

    private static final String MICROHOSTQA = "https://vdc-qaswebapp19.stormwind.local/host-app-test-harness/";

    private static final String MICROHARNESSQA = "https://vdc-qaswebapp19.stormwind.local/micro-frontend-test-harness/";

    public static final String DOCENVUNDERTEST = GRITQA08;

    public static final String COMPONENTENVRIONMENTUNDERTEST = GRITCOREQA08;

    public static final String MICROHOSTUNDERTEST = MICROHOSTQA;

    public static final String MICROHARNESSUNDERTEST = MICROHARNESSQA;


    /**
     * Banner test page introduced 2019-12.
     * <p>
     * For testing purposes. All possible banners and buttons in one place.
     * <p>
     * This isn't going to appear in any customer-facing context.
     * <p>
     * As of this update, all of the clickthroughs are to the QA13 environment.
     */
    public static final String BANNER_TEST_PAGE =
            "https://shartkedev.myshopify.com/pages/banner-test-page";

    /**
     * Demo store often used in testing.
     */
    public static final String URL_STORE_DEMO = "http://storedemo.progressivelp.com/";

    /**
     * Our customer-facing landing page.
     */
    public static final String URL_LANDING_PRODUCTION = "https://progleasing.com/";

    /**
     * URL for NopCommerce Admin and Customer designsystems.pages
     */
    public static final String URL_NOPCOMM_ADMIN = "http://phx-slkappecw01.stormwind.local/nopCommerce/admin";
    public static final String URL_NOPCOMM_CUSTOMER = "http://phx-slkappecw01.stormwind.local/nopCommerce";
    public static final String URL_WOOCOMM_ADMIN = "http://phx-slkappect01.stormwind.local/wp-login.php";
    public static final String URL_WOOCOMM_CUSTOMER = "http://phx-slkappect01.stormwind.local/";
    public static final String URL_OPENCART_ADMIN = "http://phx-slkappect02.stormwind.local/admin";
    public static final String URL_OPENCART_CUSTOMER = "http://phx-slkappect02.stormwind.local/";


    /**
     * URL for direct navigation to welcome page, also called marketing page.
     * <p>
     * "No credit needed, pay over time!"
     * <p>
     * "Lease with Progressive and pay for your items...."
     * <p>
     * For field validation only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_WELCOME_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/welcome";

    public static final String URL_WELCOME_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/welcome";

    public static final String URL_WELCOME_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/welcome";

    public static final String URL_WELCOME_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/welcome";

    public static final String URL_WELCOME_RC =
            "https://slc-rcpwebpws.stormwind.local/eComUI/#/welcome";

    public static final String URL_WELCOME_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/welcome";



    public static final String URL_OTP_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/get-started";

    public static final String URL_OTP_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/get-started";

    public static final String URL_OTP_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/get-started";

    public static final String URL_OTP_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/get-started";

    public static final String URL_OTP_RC =
            "https://slc-rcpwebpws.stormwind.local/eComUI/#/apply/get-started";

    public static final String URL_OTP_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/get-started";

    public static final String URL_OTP_PROD =
            "https://www.progressivelp.com/eComUI/#/apply/get-started";




    public static final String URL_BASIC_INFO_PAGE_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/basic-info";

    public static final String URL_BASIC_INFO_PAGE_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info";

    public static final String URL_BASIC_INFO_PAGE_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/basic-info";

    public static final String URL_BASIC_INFO_PAGE_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/basic-info";

    public static final String URL_BASIC_INFO_PAGE_RC =
            "https://slc-rcpwebpws.stormwind.local/eComUI/#/apply/basic-info";

    public static final String URL_BASIC_INFO_PAGE_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/basic-info";

    public static final String URL_BASIC_INFO_PAGE_PROD =
            "https://www.progressivelp.com/eComUI/#/apply/basic-info";



    public static final String URL_API_12 =
            "http://vdc-qaswebapp12.stormwind.local/eComApi";

    public static final String URL_API_13 =
            "http://vdc-qaswebapp13.stormwind.local/eComApi";

    /**
     * URL for direct navigation to contact info page ("How can we contact you?")
     * <p>
     * For field validation only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_CONTACT_INFO_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/contact-info";

    public static final String URL_CONTACT_INFO_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/contact-info";

    public static final String URL_CONTACT_INFO_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/contact-info";

    public static final String URL_CONTACT_INFO_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/contact-info";

    public static final String URL_CONTACT_INFO_RC =
            "https://slc-rcpwebpws.stormwind.local/eComUI/#/apply/contact-info";

    public static final String URL_CONTACT_INFO_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/contact-info";


    /**
     * URL for direct navigation to bank info page ("Your bank account")
     * <p>
     * Three fields:
     * <ul>
     * <li>Bank routing number</li>
     * <li>Checking account number</li>
     * <li>Monthly Income</li>
     * </ul>
     * <p>
     * 'income-info' is the URL parameter and 'bank info' the spoken identifier
     * <p>
     * For field validation only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_BANK_INFO_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/income-info";

    public static final String URL_BANK_INFO_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/income-info";

    public static final String URL_BANK_INFO_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/income-info";

    public static final String URL_BANK_INFO_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/income-info";

    public static final String URL_BANK_INFO_RC =
            "https://slc-rcpwebpws.stormwind.local/eComUI/#/apply/income-info";

    public static final String URL_BANK_INFO_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/income-info";

    public static final String URL_BANK_INFO_PROD =
            "https://www.progressivelp.com/eComUI/#/apply/income-info";


    /**
     * URL for direct navigation to financial info page (short flow | slim app | B flow)
     * <p>
     * Two fields:
     * <ul>
     * <li>SSN or ITIN</li>
     * <li>Monthly Income</li>
     * </ul>
     * <p>
     * For field validation only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_FINANCIAL_INFO_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/financial-info";

    public static final String URL_FINANCIAL_INFO_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/financial-info";

    public static final String URL_FINANCIAL_INFO_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/financial-info";

    public static final String URL_FINANCIAL_INFO_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/financial-info";

    public static final String URL_FINANCIAL_INFO_RC =
            "https://slc-rcpwebpws.stormwind.local/eComUI/#/apply/financial-info";

    public static final String URL_FINANCIAL_INFO_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/financial-info";



    /**
     * URL for direct navigation to results page
     * <p>
     * 'results' is the URL parameter
     * <p>
     * For health check testing only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_RESULTS_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/results";

    public static final String URL_RESULTS_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/results";

    public static final String URL_RESULTS_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/results";

    public static final String URL_RESULTS_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/results";

    public static final String URL_RESULTS_RC =
            "https://vdc-rcpwebpws.stormwind.local/eComUI/#/apply/results";

    public static final String URL_RESULTS_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/results";



    /**
     * URL for direct navigation to cart review page
     * <p>
     * 'cart-review' is the URL parameter
     * <p>
     * For health check testing only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_CART_REVIEW_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/cart-review";

    public static final String URL_CART_REVIEW_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/cart-review";

    public static final String URL_CART_REVIEW_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/cart-review";

    public static final String URL_CART_REVIEW_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/cart-review";

    public static final String URL_CART_REVIEW_RC =
            "https://vdc-rcpwebpws.stormwind.local/eComUI/#/apply/cart-review";

    public static final String URL_CART_REVIEW_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/cart-review";



    /**
     * URL for direct navigation to pay-frequency page
     * <p>
     * One field/input:
     * <ul>
     * <li>Pay frequency (can select 'weekly', 'biweekly', 'semimonthly', or 'monthly')</li>
     * </ul>
     * <p>
     * 'pay-frequency' is the URL parameter
     * <p>
     * For field validation only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_PAY_FREQUENCY_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/pay-frequency";

    public static final String URL_PAY_FREQUENCY_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/pay-frequency";

    public static final String URL_PAY_FREQUENCY_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/pay-frequency";

    public static final String URL_PAY_FREQUENCY_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/pay-frequency";

    public static final String URL_PAY_FREQUENCY_RC =
            "https://vdc-rcpwebpws.stormwind.local/eComUI/#/apply/pay-frequency";

    public static final String URL_PAY_FREQUENCY_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/pay-frequency";



    /**
     * URL for direct navigation to last pay date page
     * <p>
     * One or two fields (when navigating directly to the page, only one is shown):
     * <ul>
     * <li>Last pay date (date picker)</li>
     * <li>Next pay date (date picker)</li>
     * </ul>
     * <p>
     * 'last-pay-date' is the URL parameter
     * <p>
     * For field validation only. For functional testing, you can't use
     * the direct nav URL.
     * <p>
     * @return a String representing a limited, direct nav URL.
     */
    public static final String URL_LAST_PAY_DATE_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/apply/last-pay-date";

    public static final String URL_LAST_PAY_DATE_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/last-pay-date";

    public static final String URL_LAST_PAY_DATE_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/last-pay-date";

    public static final String URL_LAST_PAY_DATE_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/apply/last-pay-date";

    public static final String URL_LAST_PAY_DATE_RC =
            "https://vdc-rcpwebpws.stormwind.local/eComUI/#/apply/last-pay-date";



    public static final String URL_SESSION_RESUME_08 =
            "https://vdc-qaswebapp08.stormwind.local/eComUI/#/app-loading";

    public static final String URL_SESSION_RESUME_12 =
            "https://vdc-qaswebapp12.stormwind.local/eComUI/#/app-loading";

    public static final String URL_SESSION_RESUME_13 =
            "https://vdc-qaswebapp13.stormwind.local/eComUI/#/app-loading";

    public static final String URL_SESSION_RESUME_29 =
            "https://vdc-qaswebapp29.stormwind.local/eComUI/#/app-loading";

    public static final String URL_SESSION_RESUME_RC =
            "https://vdc-rcpwebpws.stormwind.local/eComUI/#/app-loading";

    public static final String URL_SESSION_RESUME_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/app-loading";


    public static final String URL_LAST_PAY_DATE_DEMO =
            "https://demo2.progressivelp.com/eComUI/#/apply/last-pay-date";



    public static final String[] ADDRESS_DRAPER_HQ = {
            "256 W Data Dr",
            "Draper",
            "UT",
            "84020"
    };

    /**
     * Basic Info page requires name, birth date, SSN, DL/GID, state.
     */
    public static final String[] BASIC_INFO_511 = {
            "Tester", // nameFirst 0
            "Lester", // nameLast 1
            "09",     // month 2
            "10",     // day 3
            "1979",   // year 4
            "511",    // first three SSN 5
            "02",     // middle two SSN 6
            "2020",   // last four SSN 7
            "12345",  // driver license number 8
            "UT"      // two letter state 9
    };


    /**
     * Basic Info page requires name, birth date, SSN, DL/GID, state.
     */
    public static final String[] BASIC_INFO_511_b = {
            "Tester",	 // nameFirst 0
            "Lester",	 // nameLast 1
            "09101979",  // date 2
            "511022020", // SSN 3
            "12345",	 // driver license number 4
            "UT"		 // two letter state 5
    };


    /**
     * Basic Info page short flow: First, Last, DOB.
     */
    public static final String[] BASIC_INFO_511_C = {
            "Tester",	 // nameFirst 0
            "Lester",	 // nameLast 1
            "09101985",  // date 2
    };


    /**
     * Basic Info page requires name, birth date, SSN, DL/GID, state.
     */
    public static final String[] BASIC_INFO_611 = {
            "Tester", // nameFirst 0
            "Lester", // nameLast 1
            "09",     // month 2
            "10",     // day 3
            "1985",   // year 4
            "611",    // first three SSN 5
            "02",     // middle two SSN 6
            "0302",   // last four SSN 7
            "12345",  // driver license number 8
            "UT"      // two letter state 9
    };


    /**
     * Contact Info page requires an email address (not here) plus this.
     * <p>
     * In most of our tests, address line 2 is not used, hence null here.
     */
    public static final String[] CONTACT_INFO_84095 = {
            "8014464357",
            "1600 Towne Center Dr",
            null, // address line 2; override it if you need it
            "South Jordan",
            "UT",
            "84095"
    };


    /**
     * Required on the Bank Info page, this is the only known valid value for testing.
     */
    public static final String ROUTING_NUMBER_FOR_TESTING = "000000000";


    /**
     * Required on the Enter The Code page, this is the only known valid value for testing.
     */
    public static final String OTP_CODE_FOR_TESTING = "000000";


    /**
     * A few email addresses valid for use in the "mobile or email" field
     * during ECOM tests.
     * <p>
     * NOTE: 020 to 030 inclusive are short flow otherwise known as B flow
     * otherwise known as slim app.
     * <p>
     * NOTE: January 2020 and forward, slip app / B flow rules the day.
     * It's our primary deliverable, with A flow as edge case.
     */
    public static final String[] TEST_DOT_COM_ADDRESSES = {
            "000@test.com",
            "001@test.com",
            "002@test.com",
            "003@test.com",
            "004@test.com",
            "005@test.com",
            "006@test.com",
            "007@test.com",
            "008@test.com",
            "009@test.com",
            "010@test.com",
            "011@test.com",
            "012@test.com",
            "013@test.com",
            "014@test.com",
            "015@test.com",
            "016@test.com",
            "017@test.com",
            "018@test.com",
            "019@test.com",
            /* begin short flow or B flow or slim app */
            "020@test.com",
            "021@test.com",
            "022@test.com",
            "023@test.com",
            "024@test.com",
            "025@test.com",
            "026@test.com",
            "027@test.com",
            "028@test.com",
            "029@test.com",
            "030@test.com",
    };

    /**
     * For both prohibited ship states and prohibited lease states, this messaging
     * is identical.
     * <p>
     * This is the base message. In the customer-facing application, it is followed
     * by a full state name, followed by a period. For example:
     * <p>
     * "Unfortunately, we are unable to lease products to customers residing in Colorado."
     * <p>
     */
    public static final String MSG_BASE_PROHIB_SHIP_LEASE =
            "Unfortunately, we are unable to lease products to customers residing in ";


    /**
     * Text expecated (Compliance signed off) of the "accept spam" / marketing communications
     * acceptance on Contact Info page.
     */
    public static final String MSG_MARCOMM =
            "Yes! I would like to receive marketing communications from Progressive Leasing, its agents and/or retailers. These parties may use pre-recorded or artificial voice via automatic telephone dialing systems or text messages at the phone number(s) I provided in the application.";

}
