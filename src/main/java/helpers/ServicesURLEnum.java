package helpers;

import static java.lang.String.format;

public enum ServicesURLEnum implements ServicesURLBuilder {
    Application("Application", PWS),
    AppPrcoessOrch("AppPrcoessOrch", COR),
    ApprovalLimitService("ApprovalLimitService", COR),
    AppMeOrchestrator("AppMeOrchestrator", COR),
    ApproveMe("ApproveMe", COR),
    ApproveMeAPI() {
        @Override
        protected void setURL() {
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch (env) {
                case QA:
                    if (env.getQaBoxNum().equals("40")) {
                        url = format(APPROVE_ME_URL_API, "staging");
                        break;
                    } else if (env.getQaBoxNum().equals("08")) {
                        url = format(APPROVE_ME_URL_API, "10-qa");
                        break;
                    } else if (env.getQaBoxNum().equals("17")) {
                        url = format(APPROVE_ME_URL_API, "qa");
                        break;
                    } else if (env.getQaBoxNum().equals("18")) {
                        url = format(APPROVE_ME_URL_API, "1-qa");
                        break;
                    } else {
                        url = null;
                        break;
                    }
                case DEMO:
                    url = format(APPROVE_ME_URL_API, "demo2");
                    break;
                case RC:
                case RELEASECANDIDATE:
                    url = format(APPROVE_ME_URL_API, "rc");
                    break;
                default:
                    url = null;
                    break;
            }
        }
    },
    ApproveMeDDE("ApproveMeDDE", VDO),
    ApproveMeMobileAPI("ApproveMeMobileAPI", PWS),
    ApproveMeNewWorldArch("ApproveMeNewWorldArch", PWS),
    ApproveMeRetailAPI("ApproveMeRetailAPI", PWS),
    AutoFunding("AutoFunding", COR),
    BureauTransSvc("BureauTransSvc", COR),
    CardPaymentService("CardPaymentService", VDO),
    CardPaymentWorkerService("CardPayment.WorkerService", COR),
    Cart("Lease.Cart.Api", COR, CART_COP_URL),
    CartSettings("lease.settings.api", COR),
    CartUI("LeaseCartUi", IWS),
    CartUITest("leasecartuitest", IWS),
    CartOrchestrator("Lease.Cart.Orchestrator", COR, CART_ORC_COP_URL),
    CCMApiInternal("CCMApiInternal", COR),
    CCMBounceSvc("CCMBounceSvc", COR),
    CCMComHub("CCMComHub", COR),
    CCMOrchestrator("CCMOrchestrator", COR),
    CCMDalOrchestrator("CCMDalOrchestrator", COR),
    CCMDalSvc("CCMDalSvc", COR),
    CCMQueue("CCMqueue", COR),
    CCMExternal("CCMExternal", VDO),
    CCMAPIExternal("CCMApiExternal") {
        @Override
        protected void setURL() {
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch (env) {
                case RELEASECANDIDATE:
                case RC:
                    url = "https://vdi-rc.progressivelp.com";
                    break;
                case DEMO:
                case DEMO2:
                    url = "https://demovdi.progressivelp.com";
                    break;
                case PROD:
                case PROD_PHX:
                case RC_PHX:
                    url = null;
                    break;
                default:
                    url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL;
            }
            url = (url != null ? url + "/" + service : null);
        }
    },
    CCMJobs("CCMJobs", COR),
    CCMMailSvc("CCMMailSvc", COR),
    CCMSms("CCMSms", VDO),
    CCMSmtp("CCMSmtp", COR),
    CCMTemplateSvc("CCMTemplateSvc", COR),
    CCMURLShortener("CCMURLShortener", VDO),
    CCMWeb("CCMWeb", IWS),
    CenLogPortalAPI("CenLogPortalAPI", COR),
    CLAMAPI("CPAPI", COR) {
        @Override
        protected void setURL() {
            // The QA Envs are the only environments that use the Service Name "CLAMAPI".
            // All others use CPAPI.
            if (EnvManager.getEnvName().contains(QA_ENV_PREFIX))
                service = "CLAMAPI";
            super.setURL();
        }
    },
    CM("CM", IWS),
    COReServices("COReServices", VDO),
    CustomerComprehensionOrch("CustomerComprehensionOrch", COR),
    CreditCardService("CreditCardService", COR),
    CustomerPaymentOrch("CustomerPaymentOrch", COR),
    CPScheduleOrchestrator("CustomerPaymentScheduleOrchestrator", COR),
    CPSchedulesService("CustomerPaymentSchedulesService", COR),
    DataGateway("DataGateway", PWS),
    dde("DDE", VDO),
    dde_Dig("DdeDig", COR),
    DocBlend("DocBlend", COR),
    DocBlendApi("DocBlendApi", COR),
    DocGen("DocGen", COR),
    DoVAS("DoVAS", IWS),
    DoVasApi("DoVasApi", COR),
    eComAPI("eComAPI", PWS),
    eComDelivery("eComDelivery", COR),
    eComMerchant("eComMerchant", COR),
    EcomMicro("EcomMicro", COR),
    eCommOrder("eCommOrder", COR),
    eCommService("eCommService", VDO),
    eCommsession("eCommsession", COR),
    eComPMApi("eComPMApi", COR),
    eComUI("eComUI", PWS),
    EsignPM("EsignPM", COR),
    EsignUI("EsignUI", PWS){
        @Override
        protected void setURL(){
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch(env) {
                case RELEASECANDIDATE:
                case RC:
                    url = String.format(ESIGN_UI_URL, "esignrc");
                    break;
                case DEMO:
                case DEMO2:
                    url = String.format(ESIGN_UI_URL, "esign-demo");
                    break;
                case PROD:
                case PROD_PHX:
                case RC_PHX:
                    url = null;
                    break;
                default:
                    url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL  + "/EsignUI/#/";
            }
        }
    },
    EsignUIAPI("EsignUI", PWS){
        @Override
        protected void setURL(){
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch(env) {
                case RELEASECANDIDATE:
                case RC:
                    url = String.format(ESIGN_UI_API, "esignrc");
                    break;
                case DEMO:
                case DEMO2:
                    url = String.format(ESIGN_UI_API, "esign-demo");
                    break;
                case PROD:
                case PROD_PHX:
                case RC_PHX:
                    url = null;
                    break;
                default:
                    url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL  + "/EsignUI/API";
            }
        }
    },
    GritCoreUniversal("host-app-test-harness/grit-universal-components", IWS),
    GritCoreUniversalAngular("host-app-test-harness/grit-universal-angular", IWS),
    GritHangTag("host-app-test-harness/grit-hang-tag", IWS),
    GritMicroFrontLoader("host-app-test-harness/micro-frontend-loader", IWS),
    GritHostAppTestHarness("host-app-test-harness/home", IWS),
    InvcMgr("InvcMgr", COR),
    ItemizationAPI("ItemizationAPI", COR),
    IVR("IVR", COR),
    JerryCrmApiExternal("JerryCrmApiExternal", PWS),
    JerryCrmApiInternal("JerryCrmApiInternal", IWS),
    JiraIntergrationsService("JiraIntergrationsService", PWS),
    LeapDDE("LeapDDE", VDO),
    LeaseAcptPM("LeaseAcptPM", COR),
    LeaseAdminPM("LeaseAdminPM", COR),
    LeaseAdminSVC("LeaseAdminSVC", VDO),
    LeaseAPI("Lease", COR),
    LeaseMatrix("LeaseMatrix", COR),
    LeasePaymentCalculator("Lease.PaymentCalculator", PWS),
    LeasePaymentDeferralApi("Lease.PaymentDeferral.Api", COR) {
        @Override
        protected void setURL() {
            switch (EnvManager.getEnvNameAsEnum()) {
                case RELEASECANDIDATE:
                case RC:
                    url = String.format(RC_URL, serverSuffix);
                    break;
                case DEMO:
                case DEMO2:
                    url = String.format(DEMO_URL, serverSuffix);
                    break;
                case PROD:
                    url = String.format(PROD_URL, serverSuffix);
                    break;
                case PROD_PHX:
                    url = String.format(PROD_PHX, serverSuffix);
                    break;
                case RC_PHX:
                    url = String.format(RC_PHX, serverSuffix);
                    break;
                default:
                    // This Orchestrator needs to look exclusively at QA08.  Everything else matches what the other URLS would do.
                    url = QA_URL + "08" + STORMWIND_LOCAL;
                    break;
            }
            url+= "/" + service;
        }
    },
    LeasePaymentDeferralOrchestrator("Lease.PaymentDeferral.Orchestrator", COR) {
        @Override
        protected void setURL() {
            switch (EnvManager.getEnvNameAsEnum()) {
                case RELEASECANDIDATE:
                case RC:
                    url = String.format(RC_URL, serverSuffix);
                    break;
                case DEMO:
                case DEMO2:
                    url = String.format(DEMO_URL, serverSuffix);
                    break;
                case PROD:
                    url = String.format(PROD_URL, serverSuffix);
                    break;
                case PROD_PHX:
                    url = String.format(PROD_PHX, serverSuffix);
                    break;
                case RC_PHX:
                    url = String.format(RC_PHX, serverSuffix);
                    break;
                default:
                    // This Orchestrator needs to look exclusively at QA08.  Everything else matches what the other URLS would do.
                    url = QA_URL + "08" + STORMWIND_LOCAL;
                    break;
            }
            url+= "/" + service;
        }
    },
    LeaseTerms("LeaseTerms", COR),
    LISA("LISA", IWS),
    MarquetaVpayAuth(){
        @Override
        protected void setURL(){
            switch(EnvManager.getEnvNameAsEnum()){
                case PROD:
                case PROD_PHX:
                    url = null;
                case RELEASECANDIDATE:
                case RC:
                case DEMO:
                case DEMO2:
                case RC_PHX:
                default:
                    url = MARQETA_QA_RC;
            }
        }
    },
    MerchandiseReturnsPM("MerchandiseReturnsPM", COR),
    MerchandiseReturnsSvc("MerchandiseReturnsSvc", COR),
    Mercury("Mercury", COR),
    MercuryUserMgmt("MercuryUserMgmt", COR),
    MicroEsign("MicroEsign", COR),
    myAccount("myAccount", PWS),
    myAccountCms("myaccount/cms/api", PWS),
    PaymentMethodOrch("PaymentMethodOrchestrator", COR),
    PaymentEventProcessor("Payments.PaymentEventProcessor", VDO),
    retailDashboardOrchestratorMicroService() {
        @Override
        protected  void setURL() {
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch(env){
                case PROD:
                case PROD_PHX:
                case DEMO2:
                case RC_PHX:
                    url = null;
                    break;
                case RELEASECANDIDATE:
                case RC:
                case DEMO:
                    url = format(RC_URL, PWS) + "/MyStoreMiSvcOrchestrator";
                    break;
                default:	// QA and DEV envs.
                    url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL + "/MyStoreMiSvcOrchestrator";
            }
        }
    },
    retailDashboardDataMicroService() {
        @Override
        protected  void setURL() {
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch(env) {
                case PROD:
                case PROD_PHX:
                case DEMO2:
                case RC_PHX:
                    url = null;
                    break;
                case RELEASECANDIDATE:
                case RC:
                case DEMO:
                    url = format(RC_URL, COR) + "/MyStoreDataMicroService";
                    break;
                default:	// QA and DEV envs.
                    url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL + "/MyStoreDataMicroService";
            }
        }
    },
    NACHA("NACHA", COR),
    NitroDDE("NitroDDE", VDO),
    OktaAuthToken() {
        @Override
        protected void setURL(){
            switch(EnvManager.getEnvNameAsEnum()){
                case PROD:
                case PROD_PHX:
                case DEMO2:
                case RC_PHX:
                    url = null;
                    break;
                case RELEASECANDIDATE:
                case RC:
                case DEMO:
                    url = OKTA_AUTH_URL_RC_DEMO;
                    break;
                default:	// QA and DEV envs.
                    url = format(OKTA_AUTH_URL_QA_DEV, EnvName.DEV.name().toLowerCase());
            }
        }
    },
    OnlineApplication("OnlineApplication", PWS),
    PAC("PAC", VDO),
    PaymentToolUI("payment-tool.ui-harness/home", PWS),
    PayMethodResource("PayMethodResource", COR),
    PEXS("PEXS", IWS),
    PexsApi("PexsApi", COR),
    Pexspex("Pexspex", IWS),
    PhoneVerificationAPI("PhoneVerificationApi", VDO),
    ProgMobileApi("ProgMobileApi", PWS),
    ProgOneTimePsd("ProgOneTimePsd", COR),
    ProgSignClient("ProgSignClient", VDI),
    ProgSignMicro("ProgSignMicro", COR),
    ProgSignPm("ProgSignPm", COR),
    QAHelper("QAHelper", IWS),
    QAHelper_PipelineMgr("QAHelper-PipelineMgr", IWS),
    QAHelper_POS("QAHelper-POS", IWS),
    QAHelper_UI("QAHelper-UI", IWS),
    RAD("RAD", VDO),
    RADUI() {
        @Override
        protected void setURL() {
            switch (EnvManager.getEnvNameAsEnum()) {
                case RELEASECANDIDATE:
                case RC:
                    url = format(RAD_UI_URL, EnvName.RC.name().toLowerCase());
                    break;
                case DEMO:
                case DEMO2:
                    url = format(RAD_UI_URL, EnvName.DEMO.name().toLowerCase());
                    break;
                case PROD:
                case PROD_PHX:
                case RC_PHX:
                    url = null;
                    break;
                default:
                    url = format(RAD_UI_URL, "qa");
            }
        }
    },
    ReportAPIService("ReportAPIService", COR),
    ReportProcess("ReportProcess", COR),
    RetailDashboardUI() {
        @Override
        protected  void setURL() {
            EnvName env = EnvManager.getEnvNameAsEnum();
            switch(env){
                case PROD:
                case PROD_PHX:
                case DEMO2:
                case RC_PHX:
                    url = null;
                    break;
                case RELEASECANDIDATE:
                case RC:
                case DEMO:
                    url = format(RC_URL, PWS) + "/retaildashboardui/";
                    break;
                default:
                    url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL + "/retaildashboardui/";
            }
        }
    },
    BasicHttpBinding_ISalesTaxService("BasicHttpBinding_ISalesTaxService", VDO),
    spnew("spnew", PWS),
    SSO("SSO", PWS),
    SSOAuth("SSOAuth", COR),
    StoreLocatorService("StoreLocatorService", COR),
    StoresPaid("StoresPaid", COR),
    StoresPaidMiSvc("StoresPaidMiSvc", COR),
    StoresPaidReceipt("StoresPaidReceipt", COR),
    StoresPdPipelineMgr("StoresPdPipelineMgr", COR),
    StoresPaidPortal("storespaidportal", IWS),
    StorePortal("SPNEW", PWS),
    STORIS("STORIS", PWS),
    Storis("Storis", PWS),
    supportme("supportme", COR),
    SupportMeUI(){
        @Override
        protected void setURL(){
            switch(EnvManager.getEnvNameAsEnum()){
                case RELEASECANDIDATE:
                case RC:
                    url = format(SUPPORT_ME_UI_URL, EnvName.RC.name().toLowerCase());
                    break;
                case PROD:
                case PROD_PHX:
                case RC_PHX:
                    url = format(SUPPORT_ME_UI_URL, "");
                    break;
                default:
                    url = format(RAD_UI_URL, "qa");
            }
        }
    },
    TancUI("TancUI", IWS),
    TaxService("TaxService", VDO),
    VCardProviderService("VCardProviderService", VDI),
    VPayService("VPayService", VDO),
    VPayUI("VPayUI", IWS);

    protected String service;
    protected String serverSuffix;
    protected String urlTemplate;
    protected String url;

    ServicesURLEnum(String service) {
        this(service, null);
    }

    ServicesURLEnum() {
        this(null, null);
    }

    ServicesURLEnum(String service, String serverSuffix) {
        this.service = service;
        this.serverSuffix = serverSuffix;
        setURL();
    }

    ServicesURLEnum(String service, String serverSuffix, String urlTemplate) {
        this.service = service;
        this.serverSuffix = serverSuffix;
        this.urlTemplate = urlTemplate;

        if (EnvManager.shouldUseDockerizedServices() && EnvManager.getDockerizedServices().contains(service)) {
            setDockerizedURL();
        } else {
            setURL();
        }
    }

    public String getURL() {
        return url;
    }

    // This will be used for dockerized services.
    protected void setDockerizedURL() {
        EnvName env = EnvManager.getEnvNameAsEnum();
        switch (env) {
            case RELEASECANDIDATE:
            case RC:
                url = String.format(urlTemplate, "rcp", "");
                break;
            case DEMO:
            case DEMO2:
                url = String.format(urlTemplate, "dmo", "");
                break;
            default:
                if (env.getQaBoxNum().contains("08")) {
                    url = String.format(urlTemplate, "qas", "");
                } else { // DEV env like QAENV01, QAENV02, QAENV28, ... and it requires a branch name
                    String branchName = "-" + EnvManager.getBranchName().replaceAll("[._/\\\\]", "-");
                    url = String.format(urlTemplate, EnvName.DEV.name().toLowerCase(), branchName);
                }
        }
    }

    // Default setURL method.  If your Service needs a unique URL building process,
    // that is done by overridding the setURL method in the Enum member above.
    // SEE ApproveMeAPI AS AN EXAMPLE
    protected void setURL() {
        EnvName env = EnvManager.getEnvNameAsEnum();
        switch (env) {
            case RELEASECANDIDATE:
            case RC:
                url = format(RC_URL, serverSuffix);
                break;
            case DEMO:
            case DEMO2:
                url = format(DEMO_URL, serverSuffix);
                break;
            case PROD:
                url = format(PROD_URL, serverSuffix);
                break;
            case PROD_PHX:
                url = format(PROD_PHX, serverSuffix);
                break;
            case RC_PHX:
                url = format(RC_PHX, serverSuffix);
                break;
            default:
                url = QA_URL + env.getQaBoxNum() + STORMWIND_LOCAL;
                break;
        }
        url += "/" + service;
    }
}