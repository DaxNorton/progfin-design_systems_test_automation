package helpers;

public interface ServicesURLBuilder {
    String STORMWIND_LOCAL = ".stormwind.local";
    String QA_URL = "https://vdc-qaswebapp";
    String RC_URL = "https://slc-rcpweb%s" + STORMWIND_LOCAL;
    String DEMO_URL = "https://slc-dmoweb%s" + STORMWIND_LOCAL;
    String PROD_URL = "https://slc-prdweb%s" + STORMWIND_LOCAL;
    String PROD_PHX = "https://phx-prdweb%s" + STORMWIND_LOCAL;
    String RC_PHX = "https://phx-rcpweb%s" + STORMWIND_LOCAL;
    String DEMO2_PWS = "https://demo2.progressivelp.com";
    String DEMO2_VDI = "https://demovdi.progressivelp.com";
    String QA_ENV_PREFIX = "QAENV";
    String APPROVE_ME_URL_API = "https://%s.approve.me/api";
    String MARQETA_VCARD_AUTH = "https://progressive-leasing-dev.marqeta.com/v3/simulate/authorization";
    String MARQETA_QA_RC = "https://progressive-leasing-dev.marqeta.com/v3/simulate";
    String RAD_UI_URL = "https://%srad.progressivelp.com";;
    String ESIGN_UI_URL = "https://%s.progressivelp.com/EsignUI/#/";
    String ESIGN_UI_API = "https://%s.progressivelp.com/EsignUI/API";
    String SUPPORT_ME_UI_URL = "https://%ssupportme" + STORMWIND_LOCAL;
    String OKTA_AUTH_URL_RC_DEMO = "https://progressiveleasing.oktapreview.com";
    String OKTA_AUTH_URL_QA_DEV = "https://progleasing%s.okta.com";
    String CART_COP_URL = "https://api-%s-leasecart%s.k8qas.stormwind.local";
    String CART_ORC_COP_URL = "https://orch-%s-leasecart%s.k8qas.stormwind.local";
    String COR = "cor";
    String IWS = "iws";
    String PWS = "pws";
    String VDO = "vdo";
    String VDI = "vdi";

}