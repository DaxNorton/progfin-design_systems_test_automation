package helpers;

import io.github.cdimascio.dotenv.Dotenv;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public class LogManager {
    private static ConcurrentMap<String, String> m_mavenOptions = new ConcurrentHashMap<String, String>();
    private static ConcurrentMap<String, Logger> m_logs;

    private static final String m_generateMvnOptionsConfig = "GENERATE_MVN_OPTIONS";
    private static boolean m_generateMvnOptions = false;

    static {
        initialize();
    }

    public static synchronized Logger getLogger(Class c) {
        Logger retLog = m_logs.getOrDefault(c.getName(), new Logger(c));
        m_logs.put(c.getName(), retLog);
        return retLog;
    }

    public static boolean getGenerateMnvOptions() {
        return m_generateMvnOptions;
    }

    public static synchronized void log(Logger.LogLevel level, String message) {
        log(level, message, null);
    }

    public static synchronized void log(Logger.LogLevel level, String message, Throwable e) {
        // Find first calling class with logger.
        Logger logger = null;
        for (StackTraceElement elem : Thread.currentThread().getStackTrace()) {
            if (m_logs.containsKey(elem.getClassName())) {
                logger = m_logs.get(elem.getClassName());
                break;
            }
        }
        // Check that a logger was found, log to root logger
        if (logger == null)
            logger = m_logs.get("root");
        logger.log(level, message, e);
    }

    public static synchronized void logAllToConsole() {
        m_logs.entrySet().stream()
                .forEach(x -> x.getValue().toConsole());
    }

    public static synchronized void printMvnOptions() {
        System.out.println(m_mavenOptions.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .map(x -> "-D" + x.getKey() + "=" + x.getValue())
                .collect(Collectors.joining(" ", "\nMAVEN_OPTIONS: { ", " }")));
    }

    //This is not synchronized because all methods that call it are.  This allows us to modify the log map if needed.
    public static void initialize() {
        if (m_logs == null || m_logs.size() == 0) {
            m_logs = new ConcurrentHashMap();
            m_logs.putIfAbsent("root", new Logger("root"));
        }

        try {
            m_generateMvnOptions = Boolean.valueOf(Dotenv.load().get(m_generateMvnOptionsConfig));
        } catch (Throwable e) {
            // Do nothing if this throws. We already have a default value for the console log level
        }
    }

    public static void addMvnOption(String key, String value) {
        if (m_generateMvnOptions)
            m_mavenOptions.putIfAbsent(key, value);
    }

    public static void printLoggers() {
        initialize();
        System.out.println(m_logs.toString());
    }
}