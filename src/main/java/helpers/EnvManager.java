package helpers;

import helpers.EnvName;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class EnvManager {
    private static String ENV_NAME;
    private static EnvName ENV_NAME_AS_ENUM;
    private static String BRANCH_NAME;
    private static Boolean USE_DOCKERIZED_SERVICES;
    private static List<String> DOCKERIZED_SERVICES = new ArrayList<>();
    private static String WHERE_TO_RUN_TESTS;
    private static String BROWSER_TYPE;
    private static long DEFAULT_WAIT_TIME;
    private static String HARDWARE_TYPE;
    private static String BROWSER_VERSION;
    private static String CHROME_EMULATION_DEVICE;
    private static String SAUCE_OS;
    private static String SAUCE_USER_NAME;
    private static String SAUCE_AUTH_KEY;
    private static String SAUCE_TUNNEL_NAME;
    private static String SAUCE_TEST_OBJ_API_KEY;
    private static String MOBILE_SIM;
    private static String MOBILE_DEVICE;
    private static String PLATFORM_VERSION;
    private static String APPIUM_VERSION;
    private static String TEST_USER_ID;
    private static String TEST_USER_PASSWORD;
    private static String OKTA_USER_ID;
    private static String OKTA_USER_PASSWORD;
    private static String STORE_PORTAL_USERNAME;
    private static String STORE_PORTAL_PASSWORD;
    private static String SUPPORT_ME_USERNAME;
    private static String SUPPORT_ME_PASSWORD;
    private static String SPLUNK_USERNAME;
    private static String SPLUNK_PASSWORD;
    private static String RESULTS_DB_ID;
    private static String RESULTS_DB_PASSWORD;
    private static Random randCCInfoIndex = new Random();

    // Helix variables
    private static String HELIX_API_KEY;
    private static String HELIX_FILTER_ID;
    private static String HELIX_OUTPUT_FILE_PATH;

    // TestLink Variables
    private static boolean USE_TEST_LINK;

    // Jenkins Lease Gen
    private static int JENKINS_LEASE_GEN_NUMBER_OF_LEASES;
    private static int JENKINS_LEASE_GEN_STORE_ID;
    private static String JENKINS_LEASE_GEN_MERCHANT_CUSTOMER_ID;
    private static String JENKINS_LEASE_GEN_STATUS;
    private static String JENKINS_LEASE_GEN_STATE_CITY_ZIP_LEASE;
    private static Boolean JENKINS_LEASE_GEN_PRINT_CSV_FILE_ON_CONSOLE;
    private static String JENKINS_LEASE_GEN_LEASE_ID_TO_BE_UPDATED;
    private static Double JENKINS_LEASE_GEN_NEW_CREDIT_LIMIT;
    private static String JENKINS_LEASE_GEN_LEASE_ID_TO_BE_DELETED;
    private static String JENKINS_LEASE_GEN_NEW_STATUS;

    private EnvManager() {
    }

    public static String getEnvName() {
        if (StringUtils.isBlank(ENV_NAME)) {
            ENV_NAME = EnvUtils.getEnvString("ENV_NAME");
        }
        return ENV_NAME;
    }

    public static EnvName getEnvNameAsEnum() {
        if (ENV_NAME_AS_ENUM == null) {
            String envName = getEnvName().toUpperCase();
            if (envName.contains("QAENV")) {
                envName = "QA";
            }
            try {
                ENV_NAME_AS_ENUM = EnvName.valueOf(envName);
            } catch (IllegalArgumentException e) {
                throw new RuntimeException("An invalid Env was passed in, please try again and check to make sure your Env is listed in EnvName.");
            }
        }
        return ENV_NAME_AS_ENUM;
    }

    public static String getBranchName() {
        if (StringUtils.isBlank(BRANCH_NAME)) {
            BRANCH_NAME = EnvUtils.getEnvString("BRANCH_NAME");
        }
        return BRANCH_NAME;
    }

    public static boolean shouldUseDockerizedServices() {
        if (USE_DOCKERIZED_SERVICES == null) {
            USE_DOCKERIZED_SERVICES = EnvUtils.getEnvBoolean("USE_DOCKERIZED_SERVICES");
        }
        return USE_DOCKERIZED_SERVICES;
    }

    public static List<String> getDockerizedServices() {
        if (DOCKERIZED_SERVICES.isEmpty()) {
            DOCKERIZED_SERVICES = Stream.of(EnvUtils.getEnvString("DOCKERIZED_SERVICES").split(","))
                    .filter(str -> StringUtils.isNotBlank(str))
                    .map(String::trim)
                    .collect(Collectors.toList());
        }
        return DOCKERIZED_SERVICES;
    }

    public static String getWhereToRunTests() {
        if (StringUtils.isBlank(WHERE_TO_RUN_TESTS)) {
            WHERE_TO_RUN_TESTS = EnvUtils.getEnvString("WHERE_TO_RUN_TESTS");
        }
        return WHERE_TO_RUN_TESTS;
    }

    public static String getBrowserType() {
        if (StringUtils.isBlank(BROWSER_TYPE)) {
            BROWSER_TYPE = EnvUtils.getEnvString("BROWSER_TYPE");
        }
        return BROWSER_TYPE;
    }

    public static String getChromeEmulationDevice() {
        if (StringUtils.isBlank(CHROME_EMULATION_DEVICE)) {
            CHROME_EMULATION_DEVICE = EnvUtils.getEnvString("CHROME_EMULATION_DEVICE");
        }
        return CHROME_EMULATION_DEVICE;
    }

    public static long getDefaultWaitTime() {
        if (DEFAULT_WAIT_TIME < 1) {
            DEFAULT_WAIT_TIME = EnvUtils.getEnvInteger("DEFAULT_WAIT_TIME");
        }
        return DEFAULT_WAIT_TIME;
    }

    public static String getHardwareType() {
        if (StringUtils.isBlank(HARDWARE_TYPE)) {
            HARDWARE_TYPE = EnvUtils.getEnvString("HARDWARE_TYPE");
        }
        return HARDWARE_TYPE;
    }

    public static String getBrowserVersion() {
        if (StringUtils.isBlank(BROWSER_VERSION)) {
            BROWSER_VERSION = EnvUtils.getEnvString("BROWSER_VERSION");
        }
        return BROWSER_VERSION;
    }

    public static String getSauceOs() {
        if (StringUtils.isBlank(SAUCE_OS)) {
            SAUCE_OS = EnvUtils.getEnvString("SAUCE_OS");
        }
        return SAUCE_OS;
    }

    public static String getSauceUserName() {
        if (StringUtils.isBlank(SAUCE_USER_NAME)) {
            SAUCE_USER_NAME = EnvUtils.getEnvString("SAUCE_USER_NAME");
        }
        return SAUCE_USER_NAME;
    }

    public static String getSauceAuthKey() {
        if (StringUtils.isBlank(SAUCE_AUTH_KEY)) {
            SAUCE_AUTH_KEY = EnvUtils.getEnvString("SAUCE_AUTH_KEY");
        }
        return SAUCE_AUTH_KEY;
    }

    public static String getSauceTunnelName() {
        if (StringUtils.isBlank(SAUCE_TUNNEL_NAME)) {
            SAUCE_TUNNEL_NAME = EnvUtils.getEnvString("SAUCE_TUNNEL_NAME");
        }
        return SAUCE_TUNNEL_NAME;
    }

    public static String getSauceTestObjApiKey() {
        if (StringUtils.isBlank(SAUCE_TEST_OBJ_API_KEY)) {
            SAUCE_TEST_OBJ_API_KEY = EnvUtils.getEnvString("SAUCE_TEST_OBJ_API_KEY");
        }
        return SAUCE_TEST_OBJ_API_KEY;
    }

    public static String getMobileSim() {
        if (StringUtils.isBlank(MOBILE_SIM)) {
            MOBILE_SIM = EnvUtils.getEnvString("MOBILE_SIM");
        }
        return MOBILE_SIM;
    }

    public static String getMobileDevice() {
        if (StringUtils.isBlank(MOBILE_DEVICE)) {
            MOBILE_DEVICE = EnvUtils.getEnvString("MOBILE_DEVICE");
        }
        return MOBILE_DEVICE;
    }

    public static String getPlatformVersion() {
        if (StringUtils.isBlank(PLATFORM_VERSION)) {
            PLATFORM_VERSION = EnvUtils.getEnvString("PLATFORM_VERSION");
        }
        return PLATFORM_VERSION;
    }

    public static String getAppiumVersion() {
        if (StringUtils.isBlank(APPIUM_VERSION)) {
            APPIUM_VERSION = EnvUtils.getEnvString("APPIUM_VERSION");
        }
        return APPIUM_VERSION;
    }

    public static String getTestUserId() {
        if (StringUtils.isBlank(TEST_USER_ID)) {
            TEST_USER_ID = EnvUtils.getEnvString("TEST_USER_ID");
        }
        return TEST_USER_ID;
    }

    public static String getTestUserPassword() {
        if (StringUtils.isBlank(TEST_USER_PASSWORD)) {
            TEST_USER_PASSWORD = EnvUtils.getEnvString("TEST_USER_PASSWORD");
        }
        return TEST_USER_PASSWORD;
    }

    public static String getOktaUserId() {
        if (StringUtils.isBlank(OKTA_USER_ID)) {
            OKTA_USER_ID = EnvUtils.getEnvString("OKTA_USER_ID");
        }
        return OKTA_USER_ID;
    }

    public static String getOktaUserPassword() {
        if (StringUtils.isBlank(OKTA_USER_PASSWORD)) {
            OKTA_USER_PASSWORD = EnvUtils.getEnvString("OKTA_USER_PASSWORD");
        }

        // For some teams, passwords might be different for RC
        if (ENV_NAME.toLowerCase().equals("rc") || ENV_NAME.toLowerCase().equals("releasecandidate")) {
            String pass = EnvUtils.getEnvString("OKTA_USER_PASSWORD_RC");
            if (StringUtils.isNotBlank(pass)) {
                OKTA_USER_PASSWORD = pass;
            }
        }
        return OKTA_USER_PASSWORD;
    }

    public static String getStorePortalUsername() {
        if (StringUtils.isBlank(STORE_PORTAL_USERNAME)) {
            STORE_PORTAL_USERNAME = EnvUtils.getEnvString("STORE_PORTAL_USERNAME");
        }
        return STORE_PORTAL_USERNAME;
    }

    public static String getStorePortalPassword() {
        if (StringUtils.isBlank(STORE_PORTAL_PASSWORD)) {
            STORE_PORTAL_PASSWORD = EnvUtils.getEnvString("STORE_PORTAL_PASSWORD");
        }
        return STORE_PORTAL_PASSWORD;
    }

    public static String getSupportMeUsername() {
        if (StringUtils.isBlank(SUPPORT_ME_USERNAME)) {
            SUPPORT_ME_USERNAME = EnvUtils.getEnvString("SUPPORT_ME_USERNAME");
        }
        return SUPPORT_ME_USERNAME;
    }

    public static String getSupportMePassword() {
        if (StringUtils.isBlank(SUPPORT_ME_PASSWORD)) {
            SUPPORT_ME_PASSWORD = EnvUtils.getEnvString("SUPPORT_ME_PASSWORD");
        }
        return SUPPORT_ME_PASSWORD;
    }

    public static String getSplunkUsername() {
        if (StringUtils.isBlank(SPLUNK_USERNAME)) {
            SPLUNK_USERNAME = EnvUtils.getEnvString("SPLUNK_USERNAME");
        }
        return SPLUNK_USERNAME;
    }

    public static String getSplunkPassword() {
        if (StringUtils.isBlank(SPLUNK_PASSWORD)) {
            SPLUNK_PASSWORD = EnvUtils.getEnvString("SPLUNK_PASSWORD");
        }
        return SPLUNK_PASSWORD;
    }

    public static String getResultsDbId(){
        if(StringUtils.isBlank(RESULTS_DB_ID)){
            RESULTS_DB_ID = EnvUtils.getEnvString("RESULTS_DB_ID");
        }
        return RESULTS_DB_ID;
    }

    public static String getResultsDbPassword(){
        if(StringUtils.isBlank(RESULTS_DB_PASSWORD)){
            RESULTS_DB_PASSWORD = EnvUtils.getEnvString("RESULTS_DB_PASSWORD");
        }
        return RESULTS_DB_PASSWORD;
    }

    // Helix
    public static String getHelixApiKey() {
        if (StringUtils.isBlank(HELIX_API_KEY)) {
            HELIX_API_KEY = EnvUtils.getEnvString("HELIX_API_KEY");
        }
        return HELIX_API_KEY;
    }

    public static String getHelixFilterId() {
        if (StringUtils.isBlank(HELIX_FILTER_ID)) {
            HELIX_FILTER_ID = EnvUtils.getEnvString("HELIX_FILTER_ID");
        }
        return HELIX_FILTER_ID;
    }

    public static String getHelixOutputFilePath() {
        if (StringUtils.isBlank(HELIX_OUTPUT_FILE_PATH)) {
            HELIX_OUTPUT_FILE_PATH = EnvUtils.getEnvString("HELIX_OUTPUT_FILE_PATH");
        }
        return HELIX_OUTPUT_FILE_PATH;
    }

    // TestLink Variables
    public static boolean getUseTestLink() {
        USE_TEST_LINK = EnvUtils.getEnvBoolean("USE_TEST_LINK");
        return USE_TEST_LINK;
    }


    // Jenkins Lease Gen
    public static int getJenkinsLeaseGenNumberOfLeases() {
        if (JENKINS_LEASE_GEN_NUMBER_OF_LEASES < 1) {
            JENKINS_LEASE_GEN_NUMBER_OF_LEASES = EnvUtils.getEnvInteger("JENKINS_LEASE_GEN_NUMBER_OF_LEASES");
        }
        return JENKINS_LEASE_GEN_NUMBER_OF_LEASES;
    }

    public static int getJenkinsLeaseGenStoreId() {
        if (JENKINS_LEASE_GEN_STORE_ID < 1) {
            JENKINS_LEASE_GEN_STORE_ID = EnvUtils.getEnvInteger("JENKINS_LEASE_GEN_STORE_ID");
        }
        return JENKINS_LEASE_GEN_STORE_ID;
    }

    public static String getJenkinsLeaseGenMerchantCustomerId() {
        if (StringUtils.isBlank(JENKINS_LEASE_GEN_MERCHANT_CUSTOMER_ID)) {
            JENKINS_LEASE_GEN_MERCHANT_CUSTOMER_ID = EnvUtils.getEnvString("JENKINS_LEASE_GEN_MERCHANT_CUSTOMER_ID");
        }
        return JENKINS_LEASE_GEN_MERCHANT_CUSTOMER_ID;
    }

    public static String getJenkinsLeaseGenStatus() {
        if (StringUtils.isBlank(JENKINS_LEASE_GEN_STATUS)) {
            JENKINS_LEASE_GEN_STATUS = EnvUtils.getEnvString("JENKINS_LEASE_GEN_STATUS");
        }
        return JENKINS_LEASE_GEN_STATUS;
    }

    public static String getJenkinsLeaseGenStateCityZipLease() {
        if (StringUtils.isBlank(JENKINS_LEASE_GEN_STATE_CITY_ZIP_LEASE)) {
            JENKINS_LEASE_GEN_STATE_CITY_ZIP_LEASE = EnvUtils.getEnvString("JENKINS_LEASE_GEN_STATE_CITY_ZIP_LEASE");
        }
        return JENKINS_LEASE_GEN_STATE_CITY_ZIP_LEASE;
    }

    public static boolean shouldJenkinsLeaseGenPrintCsvFileOnConsole() {
        if (JENKINS_LEASE_GEN_PRINT_CSV_FILE_ON_CONSOLE == null) {
            JENKINS_LEASE_GEN_PRINT_CSV_FILE_ON_CONSOLE = EnvUtils.getEnvBoolean("JENKINS_LEASE_GEN_PRINT_CSV_FILE_ON_CONSOLE");
        }
        return JENKINS_LEASE_GEN_PRINT_CSV_FILE_ON_CONSOLE;
    }

    public static String getJenkinsLeaseGenLeaseIdToBeUpdated() {
        if (StringUtils.isBlank(JENKINS_LEASE_GEN_LEASE_ID_TO_BE_UPDATED)) {
            JENKINS_LEASE_GEN_LEASE_ID_TO_BE_UPDATED = EnvUtils.getEnvString("JENKINS_LEASE_GEN_LEASE_ID_TO_BE_UPDATED");
        }
        return JENKINS_LEASE_GEN_LEASE_ID_TO_BE_UPDATED;
    }

    public static Double getJenkinsLeaseGenNewCreditLimit() {
        if (JENKINS_LEASE_GEN_NEW_CREDIT_LIMIT < 0) {
            JENKINS_LEASE_GEN_NEW_CREDIT_LIMIT = EnvUtils.getEnvDouble("JENKINS_LEASE_GEN_NEW_CREDIT_LIMIT");
        }
        return JENKINS_LEASE_GEN_NEW_CREDIT_LIMIT;
    }

    public static String getJenkinsLeaseGenLeaseIdToBeDeleted() {
        if (StringUtils.isBlank(JENKINS_LEASE_GEN_LEASE_ID_TO_BE_DELETED)) {
            JENKINS_LEASE_GEN_LEASE_ID_TO_BE_DELETED = EnvUtils.getEnvString("JENKINS_LEASE_GEN_LEASE_ID_TO_BE_DELETED");
        }
        return JENKINS_LEASE_GEN_LEASE_ID_TO_BE_DELETED;
    }

    public static String getJenkinsLeaseGenNewStatus() {
        if (StringUtils.isBlank(JENKINS_LEASE_GEN_NEW_STATUS)) {
            JENKINS_LEASE_GEN_NEW_STATUS = EnvUtils.getEnvString("JENKINS_LEASE_GEN_NEW_STATUS");
        }
        return JENKINS_LEASE_GEN_NEW_STATUS;
    }
}

