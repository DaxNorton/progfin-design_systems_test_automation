package helpers;

import helpers.LogManager;
import helpers.Logger;
import io.github.cdimascio.dotenv.Dotenv;
import org.apache.commons.lang3.StringUtils;
import java.util.Arrays;
import java.util.List;
import static java.lang.String.format;

public final class EnvUtils {
    private static final String LOG_ENV_VALUE = "**** Getting value of %s. ***";
    private static final String LOG_ENV_NO_VALUE = "**** Value for %s is returning NULL/ZERO/FALSE. ***";
    private static List<String> ignoreList = Arrays.asList("password", "key");

    private EnvUtils() {}

    private static String getEnvValue(String envName) {
        try {
            LogManager.log(Logger.LogLevel.INFO, format(LOG_ENV_VALUE, envName));
            String envValue = System.getProperty(envName);
            envValue = StringUtils.isNotBlank(envValue) ? envValue : Dotenv.load().get(envName);
            addParamToMvnMap(envName, envValue);
            return envValue;
        } catch (Exception ex) {
            LogManager.log(Logger.LogLevel.WARN, format(LOG_ENV_NO_VALUE, envName), ex);
        }
        return null;
    }

    public static String getEnvString(String envName) {
        return getEnvValue(envName);
    }

    public static int getEnvInteger(String envName) {
        String value = getEnvValue(envName);
        return StringUtils.isBlank(value) ? 0 : Integer.parseInt(value);
    }

    public static double getEnvDouble(String envName) {
        String value = getEnvValue(envName);
        return StringUtils.isBlank(value) ? 0 : Double.parseDouble(value);
    }

    public static boolean getEnvBoolean(String envName) {
        String value = getEnvValue(envName);
        return StringUtils.isNotBlank(value) && value.equalsIgnoreCase("true");
    }

    private static void addParamToMvnMap(String key, String value) {
        if (ignoreList.stream().anyMatch(str -> StringUtils.containsIgnoreCase(key, str))) {
            value = "*****";
        }
        LogManager.addMvnOption(key, value);
    }
}
