package helpers;

import helpers.EnvManager;
import io.github.cdimascio.dotenv.Dotenv;
import org.apache.commons.lang3.StringUtils;

public enum EnvName {
    RELEASECANDIDATE,
    RC,
    DEMO,
    DEMO2,
    PROD,
    PROD_PHX,
    RC_PHX,
    QA,
    DEV;

    private final String QA_BOX_NUM;

    EnvName() {
        String env = EnvManager.getEnvName().toUpperCase();
        if (env.contains("QAENV")) {
            QA_BOX_NUM = env.substring(5,7);
        }
        else {
            QA_BOX_NUM = null;
        }
    }

    public String getQaBoxNum() {
        if (StringUtils.isBlank(QA_BOX_NUM)) {
            throw new IllegalStateException("We didn't provide a valid QA box number");
        }
        return QA_BOX_NUM;
    }

}