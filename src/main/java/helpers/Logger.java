package helpers;

import io.github.cdimascio.dotenv.Dotenv;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Logger {
    private Deque<Triple<LocalDateTime, LogLevel, String>> m_logs = new ConcurrentLinkedDeque<Triple<LocalDateTime, LogLevel, String>>();
    private LogLevel m_consoleLogLevel;
    private LogLevel m_consoleLiveLogLevel;
    private LogLevel m_fileLogLevel;
    private final String m_fileLogLevelConfig = "FILE_LOG_LEVEL";
    private final String m_consoleLogLevelConfig = "CONSOLE_LOG_LEVEL";
    private final String m_consoleLiveLogLevelConfig = "CONSOLE_LOG_LIVE_LEVEL";
    private final Class m_logClassName;
    private final String m_className;

    Logger(Class c) {
        m_logClassName = c;
        m_className = c.getSimpleName();
        initialize();
    }

    Logger(String p) {
        m_logClassName = null;
        m_className = p;
        initialize();
    }

    private void initialize() {
        m_consoleLogLevel = LogLevel.from("GetDefaultValue");
        m_consoleLiveLogLevel = LogLevel.NONE;
        try {
            m_consoleLogLevel = LogLevel.from(Dotenv.load().get(m_consoleLogLevelConfig));
        } catch (Throwable e) {
        } // Do nothing if this throws.  We already have a default value for the console log level
        try {
            m_consoleLiveLogLevel = LogLevel.fromOrDefault(Dotenv.load().get(m_consoleLiveLogLevelConfig), LogLevel.NONE);
        } catch (Throwable e) {
        } // Do nothing if this throws.  We already have a default value for the console live log level
    }

    public void trace(String message, Throwable e) {
        log(LogLevel.TRACE, message, e);
    }

    public void trace(String message) {
        trace(message, null);
    }

    public void debug(String message) {
        debug(message, null);
    }

    public void debug(String message, Throwable e) {
        log(LogLevel.DEBUG, message, e);
    }

    public void info(String message) {
        info(message, null);
    }

    public void info(String message, Throwable e) {
        log(LogLevel.INFO, message, e);
    }

    public void warn(String message) {
        warn(message, null);
    }

    public void warn(String message, Throwable e) {
        log(LogLevel.WARN, message, e);
    }

    public void error(String message) {
        error(message, null);
    }

    public void error(String message, Throwable e) {
        log(LogLevel.ERROR, message, e);
    }

    public void fatal(String message) {
        fatal(message, null);
    }

    public void fatal(String message, Throwable e) {
        log(LogLevel.FATAL, message, e);
    }

    synchronized void log(LogLevel m_l, String m_message, Throwable e) {
        Triple log = Triple.of(
                LocalDateTime.now()
                , m_l
                , m_message + (e == null ? "" : getErrorDetail(e))
        );
        m_logs.offer(log);
        if (m_consoleLiveLogLevel.includes(m_l))
            m_l.getOutPut().println(formatConsoleOutput(log));
    }

    private String getErrorDetail(Throwable e) {
        String tmp = Arrays.toString(e.getStackTrace());
        String stackTrace = Stream.of(tmp.split(", "))
                .filter(s -> s.contains(m_className.split("[.]")[0]))
                .collect(Collectors.joining("\n"));
        String fullStackTrace = tmp.replace(", ", "\n");
        return String.format("\n%s\n%s\n\n%s\n==========================\n", stackTrace, e.toString(), fullStackTrace);
    }

    synchronized void prependLog(LogLevel m_l, String m_message, Throwable e) {
        m_logs.offerFirst(Triple.of(
                LocalDateTime.now()
                , m_l
                , m_message + (e == null ? "" : getErrorDetail(e))
        ));
    }

    synchronized void toConsole() {
        if(m_logs.stream().anyMatch(x->m_consoleLogLevel.includes((LogLevel) x.getMiddle()))) {
            System.out.println("\n#### Printing logs for " + m_className + "####\n");
            for (Triple x : m_logs) {
                if (m_consoleLogLevel.includes((LogLevel) x.getMiddle())) {
                    ((LogLevel) x.getMiddle()).getOutPut().println(formatConsoleOutput(x));
                }
            }
        }
    }

    private String formatConsoleOutput(Triple log) {
        return log.toString("\033[1m[%1$-29s] %2$-6s: \033[0m" + m_className + ": %3$s");
    }

    // Used to override the log level defined in the .env file in the case that a test fails.
    public void setLogLevel(LogLevel l) {
        m_consoleLogLevel = l;
    }

    public enum LogLevel {
        ALL(1),
        TRACE(2),
        DEBUG(3),
        INFO(4),
        WARN(5),
        ERROR(6, System.err),
        FATAL(7, System.err),
        NONE(8);

        private int level;
        private PrintStream output;

        LogLevel(int level) {
            this(level, System.out);
        }

        LogLevel(int level, PrintStream output) {
            this.level = level;
            this.output = output;
        }

        public boolean includes(LogLevel l) {
            return this.level <= l.level;
        }

        public static LogLevel fromOrDefault(String level, LogLevel defaultLogLevel) {
            if (StringUtils.isBlank(level)) {
                return defaultLogLevel;
            }
            switch (level.toLowerCase()) {
                case "all":
                    return ALL;
                case "trace":
                    return TRACE;
                case "debug":
                    return DEBUG;
                case "info":
                    return INFO;
                case "warn":
                    return WARN;
                case "error":
                    return ERROR;
                case "fatal":
                    return FATAL;
                case "none":
                    return NONE;
                default:
                    return defaultLogLevel;
            }
        }

        public static LogLevel from(String level) {
            return fromOrDefault(level, LogLevel.ALL);
        }

        public PrintStream getOutPut() {
            return output;
        }
    }

}
